<?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    // START BACKEND AUTH
    Route::controller(AuthController::class)->group(function () {
        Route::post('/admin/login', 'login')->name('login');
        Route::post('/admin/signup', 'register');
        Route::post('/admin/logout', 'logout');
        Route::post('/admin/refresh','refresh');
        Route::post('/admin/me', 'me');
        // START FRONTEND AUTH
        Route::post('/login', 'frontLogin');
        Route::post('/signup', 'register');
        Route::post('/logout', 'logout');
        Route::post('/refresh','refresh');
        Route::post('/me', 'me');
        Route::post('/signup', 'register');
    });


});



Route::post('/status/{stockTransferId}/change/{status}', '\App\Http\Controllers\Backend\StockTransferChangeStatusController');

// MODAL
// Category
Route::apiResource('/category', '\App\Http\Controllers\Backend\CategoryController')->middleware('auth:api');
// Sub Category
Route::apiResource('/subcategory', '\App\Http\Controllers\Backend\SubCategoryController');
// Role
Route::apiResource('/role', '\App\Http\Controllers\Backend\RoleController');
// Note
Route::apiResource('/note', '\App\Http\Controllers\Backend\NoteController');
// Task
Route::apiResource('/task', '\App\Http\Controllers\Backend\TaskController');
// Shipping Company
//Route::apiResource('/shipping-company', '\App\Http\Controllers\Backend\ShippingCompanyController');
// Shipping Company
Route::apiResource('/shipping-company', '\App\Http\Controllers\Backend\ShippingCompanyController');
// Shipping Company Deci
Route::apiResource('/shipping-company-deci', '\App\Http\Controllers\Backend\ShippingCompanyDeciController');
// Shipping Company Distance
Route::apiResource('/shipping-company-distance', '\App\Http\Controllers\Backend\ShippingCompanyDistanceController');
// Shipping Company Service
Route::apiResource('/shipping-company-service', '\App\Http\Controllers\Backend\ShippingCompanyServiceController');
// Shipping Company Post Type
Route::apiResource('/shipping-company-post-type', '\App\Http\Controllers\Backend\ShippingCompanyPostTypeController');
// Size
Route::apiResource('/size', '\App\Http\Controllers\Backend\SizeController');
// Color
Route::apiResource('/color', '\App\Http\Controllers\Backend\ColorController');
// Unit
Route::apiResource('/unit', '\App\Http\Controllers\Backend\UnitController');
// Payment Method
Route::apiResource('/payment-method', '\App\Http\Controllers\Backend\PaymentMethodController');
// User Task
Route::post('/user-task/{id}', [\App\Http\Controllers\Backend\TaskController::class, 'userTask']);
// Get User Task
Route::get('/get-user-task/{id}', [\App\Http\Controllers\Backend\TaskController::class, 'showUserTask']);
// Completed User Task
Route::get('/completed-user-task/{id}', [\App\Http\Controllers\Backend\TaskController::class, 'completedUserTask']);
// Truck
Route::apiResource('/truck', '\App\Http\Controllers\Backend\TruckController');

//User Export
Route::get('/user-export', '\App\Http\Controllers\Backend\ExportController');



// NORMAL
// Auth User Notification
Route::get('/user/notification/{id}', [\App\Http\Controllers\Backend\AdminController::class, 'userNotification']);
// Admin
Route::apiResource('/admin', '\App\Http\Controllers\Backend\AdminController');
// Slider
Route::apiResource('/slider', '\App\Http\Controllers\Backend\SliderController');
// Collection
Route::apiResource('/collection', '\App\Http\Controllers\Backend\CollectionController');
// Customer
Route::apiResource('/customer', '\App\Http\Controllers\Backend\CustomerController');
// Blog
Route::apiResource('/blog', '\App\Http\Controllers\Backend\BlogController');
// Team
Route::apiResource('/team', '\App\Http\Controllers\Backend\TeamController');
// Faq
Route::apiResource('/faq', '\App\Http\Controllers\Backend\FaqController');
// Product
Route::apiResource('/product', '\App\Http\Controllers\Backend\ProductController');
// Main Warehouse By Product
Route::get('/main-warehouse/', [\App\Http\Controllers\Backend\ProductController::class, 'mainWarehouse']);
// Select Category By Product
Route::get('/select-to-category/{id}', [\App\Http\Controllers\Backend\ProductController::class, 'selectToCategory']);
// Select Country For City
Route::get('/select-to-country/{id}', [\App\Http\Controllers\Backend\CountryServiceController::class, 'index']);
// Variant
Route::apiResource('/variant', '\App\Http\Controllers\Backend\VariantController');
// Warehouse
Route::apiResource('/warehouse', '\App\Http\Controllers\Backend\WarehouseController');
// Service
Route::apiResource('/service', '\App\Http\Controllers\Backend\ServiceController');
// Referance
Route::apiResource('/referance', '\App\Http\Controllers\Backend\ReferanceController');
// Project
Route::apiResource('/project', '\App\Http\Controllers\Backend\ProjectController');
// Employee
Route::apiResource('/employee', '\App\Http\Controllers\Backend\EmployeeController');
// Supplier
Route::apiResource('/supplier', '\App\Http\Controllers\Backend\SupplierController');
// About Us
Route::apiResource('/about-us', '\App\Http\Controllers\Backend\AboutUsController');
// Campaign
Route::apiResource('/campaign', '\App\Http\Controllers\Backend\CampaignController');
// Coupon
Route::apiResource('/coupon', '\App\Http\Controllers\Backend\CouponController');
// Bank
Route::apiResource('/bank', '\App\Http\Controllers\Backend\BankController');
// Image Resize
Route::post('/image-resize/', [\App\Http\Controllers\Backend\ImageResizeController::class, 'update']);
// Get Image Resize
Route::get('/image-resize/{id}', [\App\Http\Controllers\Backend\ImageResizeController::class, 'show']);
// Panel Settings
Route::post('/panel-settings/', [\App\Http\Controllers\Backend\PanelSettingsController::class, 'update']);
// Get Panel Settings
Route::get('/panel-settings/{id}', [\App\Http\Controllers\Backend\PanelSettingsController::class, 'show']);
// PAnel Settings
Route::get('/panel-settings', [\App\Http\Controllers\Backend\PanelSettingsController::class, 'index']);
// Web Site Settings
Route::post('/website-settings/', [\App\Http\Controllers\Backend\WebSiteSettingsController::class, 'update']);
// Get Web Site Settings
Route::get('/website-settings/{id}', [\App\Http\Controllers\Backend\WebSiteSettingsController::class, 'show']);

// Product Priority
Route::post('/product-priority/{id}', [\App\Http\Controllers\Backend\ProductController::class, 'productPriority']);
// Get Product Priority
Route::get('/get-product-priority/{id}', [\App\Http\Controllers\Backend\ProductController::class, 'showProductPriority']);
// Product Variant
//Route::post('/product-variant/{id}', [\App\Http\Controllers\Backend\ProductController::class, 'productVariant']);
// Get Product Variant
Route::get('/get-product-variant/{id}', [\App\Http\Controllers\Backend\ProductController::class, 'showProductVariant']);
// Backup
Route::get('/backup',[\App\Http\Controllers\Backend\BackupController::class, 'backup']);
// Accounting
//Route::apiResource('/accounting', '\App\Http\Controllers\Backend\AccountingController');
// Expense
Route::apiResource('/expense', '\App\Http\Controllers\Backend\ExpenseController');
// Revenue
Route::apiResource('/revenue', '\App\Http\Controllers\Backend\RevenueController');
// Accounting Status
Route::apiResource('/accounting-status', '\App\Http\Controllers\Backend\AccountingStatusController');
// Revenue Detail
Route::get('/revenue/detail/{id}',[App\Http\Controllers\Backend\RevenueController::class,'showRevenueDetail']);
Route::post('/revenue-detail/{id}', [App\Http\Controllers\Backend\RevenueController::class, 'storeRevenueDetail']);
Route::post('/revenue-detail-update/{id}', [App\Http\Controllers\Backend\RevenueController::class, 'updateRevenueDetail']);
Route::delete('/revenue-detail-delete/{id}', [App\Http\Controllers\Backend\RevenueController::class, 'deleteRevenueDetail']);
// Expense Detail
Route::get('/expense/detail/{id}',[App\Http\Controllers\Backend\ExpenseController::class,'showExpenseDetail']);
Route::post('/expense-detail/{id}', [App\Http\Controllers\Backend\ExpenseController::class, 'storeExpenseDetail']);
Route::post('/expense-detail-update/{id}', [App\Http\Controllers\Backend\ExpenseController::class, 'updateExpenseDetail']);
Route::delete('/expense-detail-delete/{id}', [App\Http\Controllers\Backend\ExpenseController::class, 'deleteExpenseDetail']);
// Variant
Route::get('/show/variant/{id}',[App\Http\Controllers\Backend\VariantController::class,'show']);
Route::post('/variant-store/{id}', [App\Http\Controllers\Backend\VariantController::class, 'store']);
Route::post('/variant-update/{id}', [App\Http\Controllers\Backend\VariantController::class, 'update']);
Route::delete('/variant-delete/{id}', [App\Http\Controllers\Backend\VariantController::class, 'delete']);

// Product Language Store
Route::get('/show/product-language/{id}',[App\Http\Controllers\Backend\ProductLanguageController::class,'show']);
Route::post('/product-language-store/{id}', [App\Http\Controllers\Backend\ProductLanguageController::class, 'store']);
Route::delete('/product-language-delete/{id}/{code}', [App\Http\Controllers\Backend\ProductLanguageController::class, 'delete']);
Route::post('/product-language-update/{id}', [App\Http\Controllers\Backend\ProductLanguageController::class, 'update']);

// Slider Language Store
Route::get('/show/slider-language/{id}',[App\Http\Controllers\Backend\SliderLanguageController::class,'show']);
Route::post('/slider-language-store/{id}', [App\Http\Controllers\Backend\SliderLanguageController::class, 'store']);
Route::delete('/slider-language-delete/{id}/{code}', [App\Http\Controllers\Backend\SliderLanguageController::class, 'delete']);
Route::post('/slider-language-update/{id}', [App\Http\Controllers\Backend\SliderLanguageController::class, 'update']);

// Collection Language Store
Route::get('/show/collection-language/{id}',[App\Http\Controllers\Backend\CollectionLanguageController::class,'show']);
Route::post('/collection-language-store/{id}', [App\Http\Controllers\Backend\CollectionLanguageController::class, 'store']);
Route::delete('/collection-language-delete/{id}/{code}', [App\Http\Controllers\Backend\CollectionLanguageController::class, 'delete']);
Route::post('/collection-language-update/{id}', [App\Http\Controllers\Backend\CollectionLanguageController::class, 'update']);

// Campaign Language Store
Route::get('/show/campaign-language/{id}',[App\Http\Controllers\Backend\CampaignLanguageController::class,'show']);
Route::post('/campaign-language-store/{id}', [App\Http\Controllers\Backend\CampaignLanguageController::class, 'store']);
Route::delete('/campaign-language-delete/{id}/{code}', [App\Http\Controllers\Backend\CampaignLanguageController::class, 'delete']);
Route::post('/campaign-language-update/{id}', [App\Http\Controllers\Backend\CampaignLanguageController::class, 'update']);

// Project Language Store
Route::get('/show/project-language/{id}',[App\Http\Controllers\Backend\ProjectLanguageController::class,'show']);
Route::post('/project-language-store/{id}', [App\Http\Controllers\Backend\ProjectLanguageController::class, 'store']);
Route::delete('/project-language-delete/{id}/{code}', [App\Http\Controllers\Backend\ProjectLanguageController::class, 'delete']);
Route::post('/project-language-update/{id}', [App\Http\Controllers\Backend\ProjectLanguageController::class, 'update']);

// Faq Language Store
Route::get('/show/faq-language/{id}',[App\Http\Controllers\Backend\FaqLanguageController::class,'show']);
Route::post('/faq-language-store/{id}', [App\Http\Controllers\Backend\FaqLanguageController::class, 'store']);
Route::delete('/faq-language-delete/{id}/{code}', [App\Http\Controllers\Backend\FaqLanguageController::class, 'delete']);
Route::post('/faq-language-update/{id}', [App\Http\Controllers\Backend\FaqLanguageController::class, 'update']);

// About Us Language Store
Route::get('/show/about-language/{id}',[App\Http\Controllers\Backend\AboutUsLanguageController::class,'show']);
Route::post('/about-language-store/{id}', [App\Http\Controllers\Backend\AboutUsLanguageController::class, 'store']);
Route::delete('/about-language-delete/{id}/{code}', [App\Http\Controllers\Backend\AboutUsLanguageController::class, 'delete']);
Route::post('/about-language-update/{id}', [App\Http\Controllers\Backend\AboutUsLanguageController::class, 'update']);

// Service Language Store
Route::get('/show/service-language/{id}',[App\Http\Controllers\Backend\ServiceLanguageController::class,'show']);
Route::post('/service-language-store/{id}', [App\Http\Controllers\Backend\ServiceLanguageController::class, 'store']);
Route::delete('/service-language-delete/{id}/{code}', [App\Http\Controllers\Backend\ServiceLanguageController::class, 'delete']);
Route::post('/service-language-update/{id}', [App\Http\Controllers\Backend\ServiceLanguageController::class, 'update']);

// Referance Language Store
Route::get('/show/referance-language/{id}',[App\Http\Controllers\Backend\ReferanceLanguageController::class,'show']);
Route::post('/referance-language-store/{id}', [App\Http\Controllers\Backend\ReferanceLanguageController::class, 'store']);
Route::delete('/referance-language-delete/{id}/{code}', [App\Http\Controllers\Backend\ReferanceLanguageController::class, 'delete']);
Route::post('/referance-language-update/{id}', [App\Http\Controllers\Backend\ReferanceLanguageController::class, 'update']);

// Blog Language Store
Route::get('/show/blog-language/{id}',[App\Http\Controllers\Backend\BlogLanguageController::class,'show']);
Route::post('/blog-language-store/{id}', [App\Http\Controllers\Backend\BlogLanguageController::class, 'store']);
Route::delete('/blog-language-delete/{id}/{code}', [App\Http\Controllers\Backend\BlogLanguageController::class, 'delete']);
Route::post('/blog-language-update/{id}', [App\Http\Controllers\Backend\BlogLanguageController::class, 'update']);

// Category Language Store
Route::get('/show/category-language/{id}',[App\Http\Controllers\Backend\CategoryLanguageController::class,'show']);
Route::post('/category-language-store/{id}', [App\Http\Controllers\Backend\CategoryLanguageController::class, 'store']);
Route::delete('/category-language-delete/{id}/{code}', [App\Http\Controllers\Backend\CategoryLanguageController::class, 'delete']);
Route::post('/category-language-update/{id}', [App\Http\Controllers\Backend\CategoryLanguageController::class, 'update']);

// Sub Category Language Store
Route::get('/show/subcategory-language/{id}',[App\Http\Controllers\Backend\SubCategoryLanguageController::class,'show']);
Route::post('/subcategory-language-store/{id}', [App\Http\Controllers\Backend\SubCategoryLanguageController::class, 'store']);
Route::delete('/subcategory-language-delete/{id}/{code}', [App\Http\Controllers\Backend\SubCategoryLanguageController::class, 'delete']);
Route::post('/subcategory-language-update/{id}', [App\Http\Controllers\Backend\SubCategoryLanguageController::class, 'update']);

// Team Language Store
Route::get('/show/team-language/{id}',[App\Http\Controllers\Backend\TeamLanguageController::class,'show']);
Route::post('/team-language-store/{id}', [App\Http\Controllers\Backend\TeamLanguageController::class, 'store']);
Route::delete('/team-language-delete/{id}/{code}', [App\Http\Controllers\Backend\TeamLanguageController::class, 'delete']);
Route::post('/team-language-update/{id}', [App\Http\Controllers\Backend\TeamLanguageController::class, 'update']);

// Site Settings Language Store
Route::get('/show/website-settings-language/{id}',[App\Http\Controllers\Backend\SiteSettingLanguageController::class,'show']);
Route::post('/website-settings-language-store/{id}', [App\Http\Controllers\Backend\SiteSettingLanguageController::class, 'store']);
Route::delete('/website-settings-language-delete/{id}/{code}', [App\Http\Controllers\Backend\SiteSettingLanguageController::class, 'delete']);
Route::post('/website-settings-language-update/{id}', [App\Http\Controllers\Backend\SiteSettingLanguageController::class, 'update']);

// Exchange Rate
Route::get('/exchange-rate',[App\Http\Controllers\Backend\ExchangeRateController::class,'index']);
Route::post('/exchange-rate/converter',[App\Http\Controllers\Backend\ExchangeRateController::class,'converter']);


// Main Folder
Route::get('/main/folder',[App\Http\Controllers\Backend\FolderController::class,'index']);
Route::get('/main/folder/content/{folder}',[App\Http\Controllers\Backend\FolderController::class,'mainFolder']);
Route::get('/sub/folder/content/{folder}',[App\Http\Controllers\Backend\FolderController::class,'subFolder']);
Route::get('/download/file/{main}/{folder}',[App\Http\Controllers\Backend\FolderController::class,'getDownload']);
Route::get('/new/folder/{main}/{folder}',[App\Http\Controllers\Backend\FolderController::class,'newFolder']);
Route::get('/delete/folder/{main}/{folder}',[App\Http\Controllers\Backend\FolderController::class,'deleteFolder']);
Route::get('/zip/file/{main}/{folder}',[App\Http\Controllers\Backend\FolderController::class,'zipFile']);
Route::get('/open/file/{main}/{folder}',[App\Http\Controllers\Backend\FolderController::class,'openFileInEditor']);


// YAPILACAK

// Contact
//Route::apiResource('/contact', '\App\Http\Controllers\Backend\ContactController');
// Default
//Route::apiResource('/default', '\App\Http\Controllers\Backend\DefaultController');
// Report
//Route::apiResource('/report', '\App\Http\Controllers\Backend\ReportController');
// Pos
//Route::apiResource('/pos', '\App\Http\Controllers\Backend\PosController');
// Order
//Route::apiResource('/order', '\App\Http\Controllers\Backend\OrderController');
// Order Request
//Route::apiResource('/order-request', '\App\Http\Controllers\Backend\OrderRequestController');
// Order Tracking
//Route::apiResource('/order-tracking', '\App\Http\Controllers\Backend\OrderTrackingController');













// Product Request İptal Edildi
//// Post Request Product
//Route::post('/request-product', [\App\Http\Controllers\Backend\RequestProductController::class, 'requestProduct']);
//// Get Request Product
//Route::get('/get-request-product', [\App\Http\Controllers\Backend\RequestProductController::class, 'showRequestProduct']);
//// Delete Request Product
//Route::delete('/delete-request-product/{id}', [\App\Http\Controllers\Backend\RequestProductController::class, 'deleteRequestProduct']);
//// Cancel Request Product
//Route::get('/cancel-request-product/{id}', [\App\Http\Controllers\Backend\RequestProductController::class, 'cancelRequestProduct']);
//// Completed Request Product
//Route::get('/completed-request-product/{id}', [\App\Http\Controllers\Backend\RequestProductController::class, 'completedRequestProduct']);
//// Process Request Product
//Route::get('/process-request-product/{id}', [\App\Http\Controllers\Backend\RequestProductController::class, 'processRequestProduct']);
//// Accept Request Product
//Route::get('/accept-request-product/{id}', [\App\Http\Controllers\Backend\RequestProductController::class, 'acceptRequestProduct']);













// Getting Product
    Route::get('/getting/product/{id}', [\App\Http\Controllers\Backend\StockTransferController::class, 'gettingProduct']);


// START SORTABLE

// Blog
Route::post('/blog-sortable/', [\App\Http\Controllers\Backend\BlogController::class, 'blogSortable'])->name('blog.Sortable');
// Faq
Route::post('/faq-sortable/', [\App\Http\Controllers\Backend\FaqController::class, 'faqSortable'])->name('faq.Sortable');
// Service
Route::post('/service-sortable/', [\App\Http\Controllers\Backend\ServiceController::class, 'serviceSortable'])->name('service.Sortable');
// Team
Route::post('/team-sortable/', [\App\Http\Controllers\Backend\TeamController::class, 'teamSortable'])->name('team.Sortable');
// Project
Route::post('/project-sortable/', [\App\Http\Controllers\Backend\ProjectController::class, 'projectSortable'])->name('project.Sortable');
// Referance
Route::post('/referance-sortable/', [\App\Http\Controllers\Backend\ReferanceController::class, 'referanceSortable'])->name('referance.Sortable');
// Slider
Route::post('/slider-sortable/', [\App\Http\Controllers\Backend\SliderController::class, 'sliderSortable'])->name('slider.Sortable');
// Payment Method
Route::post('/payment-method-sortable/', [\App\Http\Controllers\Backend\PaymentMethodController::class, 'paymentMethodSortable'])->name('paymentMethod.Sortable');
// Collection
Route::post('/collection-sortable/', [\App\Http\Controllers\Backend\CollectionController::class, 'collectionSortable'])->name('collection.Sortable');
// Campaign
Route::post('/campaign-sortable/', [\App\Http\Controllers\Backend\CampaignController::class, 'campaignSortable'])->name('campaign.Sortable');
// END SORTABLE


// STOCK TRANSFER AND STOCK TRANSFER CART
// Add To Stock Transfer Cart
Route::get('/add/stocktransfer/cart/{variant_id}/{warehouse_id}', [\App\Http\Controllers\Backend\StockTransferCartController::class, 'addToStockTransferCart']);
// All Stock Transfer Cart
Route::get('/stocktransfer/cart/', [\App\Http\Controllers\Backend\StockTransferCartController::class, 'stockTransferCart']);
// Remove Stock Transfer Cart
Route::get('/remove/stocktransfer/cart/{id}', [\App\Http\Controllers\Backend\StockTransferCartController::class, 'removeStockTransferCart']);
// Increment Stock Transfer Cart
Route::get('/increment/stocktransfer/cart/{id}/{warehouse_id}', [\App\Http\Controllers\Backend\StockTransferCartController::class, 'incrementStockTransferCart']);
// Decrement Stock Transfer Cart
Route::get('/decrement/stocktransfer/cart/{id}', [\App\Http\Controllers\Backend\StockTransferCartController::class, 'decrementStockTransferCart']);
// Get Product Out Of Stock
//Route::get('/get-out-of-stock', [\App\Http\Controllers\Backend\ProductController::class, 'outOfStock']);
Route::get('/get-out-of-stock', [\App\Http\Controllers\Backend\StockTransferCartController::class, 'outOfStock']);

// Stock Transfer
Route::post('/store/stock-transfer/', [\App\Http\Controllers\Backend\StockTransferController::class, 'storeStockTransfer']);

// Select To Warehouse
Route::get('/select-from-warehouse/{id}', [\App\Http\Controllers\Backend\StockTransferCartController::class, 'selectFromWarehouse']);

// Select To Category
Route::get('/control-main-warehouse/', [\App\Http\Controllers\Backend\WarehouseController::class, 'controlMainWarehouse']);

// Select To Warehouse Item
Route::get('/select-from-warehouse-item/{id}', [\App\Http\Controllers\Backend\StockTransferCartController::class, 'selectFromWarehouseItem']);

// Select To Warehouse Item Variant
Route::get('/select-from-warehouse-item-variant/{id}', [\App\Http\Controllers\Backend\StockTransferCartController::class, 'selectFromWarehouseItemVariant']);

// All Stock Transfer
Route::get('/stockTransfer', [\App\Http\Controllers\Backend\StockTransferController::class, 'index']);
// Stock Transfer Show Detail
Route::get('/stockTransfer/show/{id}', [\App\Http\Controllers\Backend\StockTransferController::class, 'show']);
// Stock Transfer Show Product
Route::get('/stockTransfer/show/product/{id}', [\App\Http\Controllers\Backend\StockTransferController::class, 'showProduct']);
// Stock Transfer Show Product Variant
Route::get('/stockTransfer/show/product/variant/{id}', [\App\Http\Controllers\Backend\StockTransferController::class, 'showVariant']);
// Stock Transfer Prepared
Route::get('/prepared/stock-transfer/{id}', [\App\Http\Controllers\Backend\StockTransferController::class, 'prepared']);
// Stock Transfer Process
Route::get('/process/stock-transfer/{id}', [\App\Http\Controllers\Backend\StockTransferController::class, 'process']);
// Stock Transfer Road
Route::get('/road/stock-transfer/{id}', [\App\Http\Controllers\Backend\StockTransferController::class, 'road']);
// Stock Transfer Arrived
Route::get('/arrived/stock-transfer/{id}', [\App\Http\Controllers\Backend\StockTransferController::class, 'arrived']);
// Stock Transfer Delivered
Route::get('/delivered/stock-transfer/{id}', [\App\Http\Controllers\Backend\StockTransferController::class, 'delivered']);
// Stock Transfer Return Request
Route::get('/returned-request/stock-transfer/{id}', [\App\Http\Controllers\Backend\StockTransferController::class, 'returnedRequest']);
// Stock Transfer Return Prepared
Route::get('/returned-prepared/stock-transfer/{id}', [\App\Http\Controllers\Backend\StockTransferController::class, 'returnedPrepared']);
// Stock Transfer Return Process
Route::get('/returned-process/stock-transfer/{id}', [\App\Http\Controllers\Backend\StockTransferController::class, 'returnedProcess']);
// Stock Transfer Return Road
Route::get('/returned-road/stock-transfer/{id}', [\App\Http\Controllers\Backend\StockTransferController::class, 'returnedRoad']);
// Stock Transfer Return Arrived
Route::get('/returned-arrived/stock-transfer/{id}', [\App\Http\Controllers\Backend\StockTransferController::class, 'returnedArrived']);
// Stock Transfer Return Delivered
Route::get('/returned-delivered/stock-transfer/{id}', [\App\Http\Controllers\Backend\StockTransferController::class, 'returnedDelivered']);
// Stock Transfer Cancel
Route::get('/cancel/stock-transfer/{id}', [\App\Http\Controllers\Backend\StockTransferController::class, 'canceled']);


// Sub Warehouse
Route::get('/sub-warehouse/list', [\App\Http\Controllers\Backend\WarehouseItemController::class, 'subWarehouseList']);
// Select Warehouse For Admin
Route::get('/select-warehouse-item/{id}', [\App\Http\Controllers\Backend\WarehouseItemController::class, 'selectWarehouseItems']);
Route::get('/select-warehouse-item-variant/{id}', [\App\Http\Controllers\Backend\WarehouseItemController::class, 'selectWarehouseItemVariants']);
// Delete Warehouse Item & Variant
//Route::get('/warehouse-item/delete/{id}/{userId}', [\App\Http\Controllers\Backend\WarehouseItemController::class, 'deleteWarehouseItem']);
//Route::get('/warehouse-item-variant/delete/{id}', [\App\Http\Controllers\Backend\WarehouseItemController::class, 'deleteWarehouseItemVariant']);
// Get Warehouse Item
Route::get('/get-warehouse-item', [\App\Http\Controllers\Backend\WarehouseItemController::class, 'getWarehouseItems']);
Route::get('/get-warehouse-item-variant', [\App\Http\Controllers\Backend\WarehouseItemController::class, 'getWarehouseItemVariants']);
// Show Warehouse Item
Route::get('/show-warehouse-item/{id}', [\App\Http\Controllers\Backend\WarehouseItemController::class, 'showWarehouseItem']);
Route::get('/show-warehouse-item-variant/{id}', [\App\Http\Controllers\Backend\WarehouseItemController::class, 'showWarehouseItemVariant']);

// Show Product
Route::get('/show-item/{id}', [\App\Http\Controllers\Backend\ProductController::class, 'showItem']);
Route::get('/show-item-variant/{id}', [\App\Http\Controllers\Backend\VariantController::class, 'showItemVariant']);

Route::get('/from/warehouse/change/{id}',[\App\Http\Controllers\Backend\StockTransferCartController::class, 'fromWarehouseChange']);
Route::get('/to/warehouse/change/{id}',[\App\Http\Controllers\Backend\StockTransferCartController::class, 'toWarehouseChange']);
Route::get('/supplier/change/{id}',[\App\Http\Controllers\Backend\StockTransferCartController::class, 'getTruckBySupplierChange']);

// Month
Route::get('/month',[\App\Http\Controllers\Backend\MonthController::class, 'index']);
// Year
Route::get('/year','\App\Http\Controllers\Backend\YearController');
// Employee Month Salary Control
//Route::get('/salary/employee/month/control/{employee_id}/{month_id}',[\App\Http\Controllers\Backend\SalaryController::class, 'employeeMonthSalaryControl']);
Route::get('/salary/employee/month/control',[\App\Http\Controllers\Backend\SalaryController::class, 'employeeMonthSalaryControl']);

// Employee Month Salary Add
Route::post('/add/salary/', [\App\Http\Controllers\Backend\SalaryController::class, 'store']);
// Employee Month Salary Edit
Route::post('/edit/salary/{id}', [\App\Http\Controllers\Backend\SalaryController::class, 'update']);

Route::get('/log/record',[\App\Http\Controllers\Backend\LogController::class, 'logRecord']);

Route::get('/route/list',[\App\Http\Controllers\Backend\RouteController::class, 'routeList']);
Route::get('/route/group',[\App\Http\Controllers\Backend\RouteController::class, 'routeGroup']);
Route::post('/store/route/group',[\App\Http\Controllers\Backend\RouteController::class, 'storeRouteGroup']);
Route::get('/route/group/{id}',[\App\Http\Controllers\Backend\RouteController::class, 'showRouteGroupList']);
Route::post('/store/route/permission',[\App\Http\Controllers\Backend\RouteController::class, 'storeRoutePermission']);
Route::get('/route/permission/{id}',[\App\Http\Controllers\Backend\RouteController::class, 'showRoutePermission']);
Route::delete('/route/permission/{id}',[\App\Http\Controllers\Backend\RouteController::class, 'deleteRoutePermission']);
Route::delete('/route/group/{id}',[\App\Http\Controllers\Backend\RouteController::class, 'deleteRouteGroup']);
Route::get('/route/permission',[\App\Http\Controllers\Backend\RouteController::class, 'routePermission']);

Route::apiResource('/calendar', '\App\Http\Controllers\Backend\CalendarController');
Route::apiResource('/announcements', '\App\Http\Controllers\Backend\AnnouncementsController');
Route::get('/user/announcements',[\App\Http\Controllers\Backend\AnnouncementsController::class, 'getUserAnnouncements']);
Route::get('/read/announcements/{id}',[\App\Http\Controllers\Backend\AnnouncementsController::class, 'readUserAnnouncements']);


Route::get('/user/stock/transfer/delete',[\App\Http\Controllers\Backend\StockTransferCartController::class, 'deleteAllStockTransferCart']);
Route::get('/supplier/stock/transfer/transaction/{id}',[\App\Http\Controllers\Backend\SupplierController::class, 'supplierStockTransferTransaction']);
Route::get('/supplier/order/transaction/{id}',[\App\Http\Controllers\Backend\SupplierController::class, 'supplierOrderTransaction']);
Route::get('/truck/stock/transfer/transaction/{id}',[\App\Http\Controllers\Backend\TruckController::class, 'truckStockTransferTransaction']);
Route::get('/truck/order/transaction/{id}',[\App\Http\Controllers\Backend\TruckController::class, 'truckOrderTransaction']);

// Out Of Stock Control
Route::get('/sub-warehouse/stock/control/item/{id}',[\App\Http\Controllers\Backend\WarehouseItemController::class, 'stockControlItem']);
Route::get('/sub-warehouse/stock/control/variant/{id}',[\App\Http\Controllers\Backend\WarehouseItemController::class, 'stockControlVariant']);
Route::get('/get/sub-warehouse/stock/control/item',[\App\Http\Controllers\Backend\WarehouseItemController::class, 'getStockControlItem']);
Route::get('/get/sub-warehouse/stock/control/variant',[\App\Http\Controllers\Backend\WarehouseItemController::class, 'getStockControlVariant']);

Route::get('/get/main-warehouse/stock/control/item',[\App\Http\Controllers\Backend\ProductController::class, 'mainStockControlItem']);
Route::get('/get/main-warehouse/stock/control/variant',[\App\Http\Controllers\Backend\VariantController::class, 'mainStockControlVariant']);

// Tables, Export And Import Excel
Route::get('/get/tables','\App\Http\Controllers\Backend\ExcelController');
Route::post('/table/export','\App\Http\Controllers\Backend\ExcelExportController');
Route::post('/table/import','\App\Http\Controllers\Backend\ExcelImportController');
Route::get('/get/tables','\App\Http\Controllers\Backend\ExcelController');

// Report V2
Route::get('/get/report/graphics',[\App\Http\Controllers\Backend\ReportController::class, 'reportGraphics']);

// TEST EMAIL
Route::get('/test/email',[\App\Http\Controllers\MailController::class, 'index']);

// Chat
Route::get('/chat/rooms',[\App\Http\Controllers\ChatController::class, 'rooms']);
Route::get('/chat/room/{roomId}/message',[\App\Http\Controllers\ChatController::class, 'messages']);
Route::post('/chat/room/{roomId}/message',[\App\Http\Controllers\ChatController::class, 'newMessage']);
//Country
Route::get('/country-list', '\App\Http\Controllers\Backend\CountryIndexController');

// DENEME CHAT
Route::get('/chat/user',[\App\Http\Controllers\ChatController::class, 'chatUser']);
Route::post('/chat/crm-user/{roomId}',[\App\Http\Controllers\ChatController::class, 'chatCrmUserLogin']);
Route::post('/chat/user/{roomId}/{id}',[\App\Http\Controllers\ChatController::class, 'chatUserLogin']);
Route::post('/chat/user/control/{id}',[\App\Http\Controllers\ChatController::class, 'chatUserControl']);


Route::get('/contacts',[\App\Http\Controllers\ChatController::class, 'getContacts']);
// Contact
Route::post('/contact',[\App\Http\Controllers\Backend\ContactController::class, 'index']);

Route::get('/conversation/{id}',[\App\Http\Controllers\ChatController::class, 'getMessagesFor']);
Route::post('/conversation/send',[\App\Http\Controllers\ChatController::class, 'send']);
//Route::get('/authInformation',[\App\Http\Controllers\ChatController::class, 'getAuthInformation']);

// Country
Route::apiResource('/country', '\App\Http\Controllers\Backend\CountryController');
// City
Route::apiResource('/city', '\App\Http\Controllers\Backend\CityController');
// Palet
Route::apiResource('/pallet', '\App\Http\Controllers\Backend\PalletController');
// Language
Route::apiResource('/language', '\App\Http\Controllers\Backend\LanguageController');

Route::get('/stockTransfer/show/process/{id}',[\App\Http\Controllers\Backend\StockTransferController::class, 'getStockTransferProcess']);

Route::get('/user/list',[\App\Http\Controllers\Backend\UserController::class, 'index']);
Route::get('/user/show/{id}',[\App\Http\Controllers\Backend\UserController::class, 'show']);
Route::delete('/user/delete/{id}',[\App\Http\Controllers\Backend\UserController::class, 'destroy']);

// Report
Route::get('/report/stock-transfer',[\App\Http\Controllers\Backend\ReportController::class, 'reportStockTransfer']);
Route::get('/report/total/admin',[\App\Http\Controllers\Backend\ReportController::class, 'totalAdmin']);
Route::get('/report/total/user',[\App\Http\Controllers\Backend\ReportController::class, 'totalUser']);
Route::get('/report/total/customer',[\App\Http\Controllers\Backend\ReportController::class, 'totalCustomer']);
Route::get('/report/total/product',[\App\Http\Controllers\Backend\ReportController::class, 'totalProduct']);
Route::get('/report/monthly/stock-transfer',[\App\Http\Controllers\Backend\ReportController::class, 'reportMonthlyStockTransfer']);
Route::get('/report/monthly/order',[\App\Http\Controllers\Backend\ReportController::class, 'reportMonthlyOrder']);


Route::get('/admin/offline/{id}',[\App\Http\Controllers\Backend\AdminController::class, 'offline']);


// FRONTENT
// Newsletter
Route::apiResource('/newsletter', '\App\Http\Controllers\Backend\NewsletterController');

Route::get('/blog-details', '\App\Http\Controllers\Front\Blog\BlogIndexController');

Route::get('/blog-details/{id}', '\App\Http\Controllers\Front\Blog\SearchBlogDetailsController');
Route::get('/about-us-details', '\App\Http\Controllers\Front\AboutUs\AboutUsDetailsController');

Route::get('/logs-details', '\App\Http\Controllers\Backend\IndexLogsController');
//front product list
Route::get('/product-details', '\App\Http\Controllers\Front\Product\FrontProductIndexController')->middleware('auth:api');
//front product limit 4 category list
Route::get('/category-list-limit', '\App\Http\Controllers\Front\Product\FrontProductLimitCategorySearchController');
//front slider index
Route::get('/slider-list', '\App\Http\Controllers\Front\Slider\FrontSliderListIndexController');
//Front search product for category
Route::get('/product-list/{categoryId}', '\App\Http\Controllers\Front\Product\FrontCategorySearchForProductController');
Route::get('/product-priority', '\App\Http\Controllers\Front\Product\FrontProductPriorityController');

//smtp Settings
Route::post('/smtp-settings', '\App\Http\Controllers\Backend\SmtpSettingsController');
Route::get('/smtp-settings', '\App\Http\Controllers\Backend\SmtpSettingIndexController');

//Google Maps Api
Route::post('/google-maps-api', '\App\Http\Controllers\Backend\GoogleMapsApiController');
Route::get('/google-maps-api', '\App\Http\Controllers\Backend\GoogleMapsApiIndexController');

//Front category and subcategory
Route::get('/category-all','\App\Http\Controllers\Front\Category\FrontCategorySubCategoryIndexController');

//Front category and subcategory search
Route::get('/category-search','\App\Http\Controllers\Front\Category\CategorySubCategorySearchController');

Route::post('/search-stock-transfer/{id}/{processId}', '\App\Http\Controllers\Backend\SearchStockTransferProcessController');

//Order and Order Cart
//add to order cart
Route::get('/add/order/cart/{variant_id}/{warehouse_id}', [\App\Http\Controllers\Backend\OrderCartController::class, 'addOrderCart']);
// all order cart
Route::get('/order/cart/', [\App\Http\Controllers\Backend\OrderCartController::class, 'orderCart']);
//Remove Order Cart
Route::get('/remove/order/cart/{id}', [\App\Http\Controllers\Backend\OrderCartController::class, 'removeOrderCart']);
//Increment Order Cart
Route::get('/increment/order/cart/{id}/{warehouse_id}', [\App\Http\Controllers\Backend\OrderCartController::class, 'incrementOrderCart']);
//Decrement order cart
Route::get('/decrement/order/cart/{id}', [\App\Http\Controllers\Backend\OrderCartController::class, 'decrementOrderCart']);

Route::get('/user/order/delete',[\App\Http\Controllers\Backend\OrderCartController::class, 'deleteAllOrderCart']);



//order
Route::post('/store/order/', [\App\Http\Controllers\Backend\OrderController::class, 'storeOrder']);

//order list
Route::get('/order', [\App\Http\Controllers\Backend\OrderController::class, 'index']);

// Order Advance Payment
Route::get('/order/advance/payment', [\App\Http\Controllers\Backend\OrderController::class, 'advancePayment']);
Route::get('/order/payment', [\App\Http\Controllers\Backend\OrderController::class, 'payment']);

//order status change
Route::post('/order/status/{orderId}/change/{status}', '\App\Http\Controllers\Backend\OrderStatusChangeController');
//report order
Route::get('/report/order','\App\Http\Controllers\Backend\OrderReportController');
Route::get('/transfer-list','\App\Http\Controllers\Backend\OrderAndStockTransferIndexController');


Route::delete('/delete/{productId}/image/{imageId}','\App\Http\Controllers\Backend\DeleteProductImageController');

Route::get('/order/{id}', [\App\Http\Controllers\Backend\OrderController::class, 'show']);
Route::get('/order-process/{id}', [\App\Http\Controllers\Backend\OrderController::class, 'getOrderProcess']);


Route::post('/search-order-process/{id}/{processId}', '\App\Http\Controllers\Backend\SearchOrderProcessController');
Route::get('/product-search','\App\Http\Controllers\Front\Product\ProductSearchForFrontController');

Route::get('/product-variant-search','\App\Http\Controllers\Front\Product\SearchProductSizeAndColorController');
Route::get('/variant-size-search','\App\Http\Controllers\Front\Product\SearhSizeForColorController');
Route::get('/related-search','\App\Http\Controllers\Front\Product\RelatedProductSearchForCategoryController');

Route::post('/send/private/', [\App\Http\Controllers\Backend\EmailController::class, 'sendPrivate']);
Route::post('/send/bulk/', [\App\Http\Controllers\Backend\EmailController::class, 'sendBulk']);

Route::post('/reset-password', '\App\Http\Controllers\Front\ResetPasswordForFrontController');

Route::post('/reset-url', '\App\Http\Controllers\Front\ResetUserPasswordController');

Route::get('/service-list', '\App\Http\Controllers\Front\Service\IndexServiceListController');










