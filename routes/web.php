<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/admin', function () {
//    return view('welcome');
//});

Route::get('/', function () {
    return view('frontend');
});

//Route::get('/debug-sentry', function () {
//    throw new Exception('My first Sentry error!');
//});

Route::get('/{vue_capture?}',function (){
    return view('frontend');
})->where('vue_capture', '[\/\w\.-]*');

Route::get('/{vue_capture?}',function (){
    return view('welcome');
})->where('vue_capture', '[\/\w\.-]*');


//Auth::routes();
//
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
