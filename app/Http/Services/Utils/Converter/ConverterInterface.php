<?php

namespace App\Http\Services\Utils\Converter;

interface ConverterInterface
{
    public static function convertToFloat($value): float;

    public static function convertToView($value,$decimal): string;
}
