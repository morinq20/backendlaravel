<?php

namespace App\Http\Services\Utils\Converter;

class Converter implements ConverterInterface
{
    public static function convertToFloat($value): float
    {
        if (is_float($value))
            return $value;
        $value = str_replace(',','',$value);
        return floatval($value);
    }

    public static function convertToView($value, $decimal): string
    {
        return number_format($value, $decimal, '.',',');
    }
}
