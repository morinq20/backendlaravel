<?php

namespace App\Http\Services\CodeService;

class CodeService
{
    public static function createCode($codeType){
        return $codeType."-".uniqid();
    }
}
