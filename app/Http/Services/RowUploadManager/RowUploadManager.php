<?php

namespace App\Http\Services\RowUploadManager;

use Exception;
use Intervention\Image\Facades\Image;

class RowUploadManager
{

    private $path;
    private $image;
    private $width;
    private $height;

    public function __construct($path, $image, $width, $height)
    {
        $this->path = $path;
        $this->image = $image;
        $this->width = $width;
        $this->height = $height;
    }

    /**
     * @throws Exception
     */
    public function rowUpload(): void
    {
        $newImage = Image::make($this->getImage())->resize($this->getWidth(),$this->getHeight());
        try {
            $newImage->save($this->makeFileName());
        }catch (\Exception $exception){
            throw new Exception($exception->getMessage());
        }

    }

    public function makeFileName(): string
    {
        $position = strpos($this->getImage(), ';');
        $sub = substr($this->getImage(), 0, $position);
        $ext = explode('/', $sub)[1];
        $name = time().".".$ext;
        return $this->getPath().$name;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path): void
    {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width): void
    {
        $this->width = $width;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height): void
    {
        $this->height = $height;
    }




}
