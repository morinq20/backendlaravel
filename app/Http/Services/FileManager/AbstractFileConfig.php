<?php

namespace App\Http\Services\FileManager;

use Intervention\Image\Facades\Image;

abstract class AbstractFileConfig implements FileConfigInterface
{
    protected $photo;
    protected $path;
    protected $width = 130;
    protected $height = 130;

    /**
     * @param $photo
     * @param $path
     * @param int $width
     * @param int $height
     */
    public function __construct($photo, $path, int $width, int $height)
    {
        $this->photo = $photo;
        $this->path = $path;
        $this->width = $width;
        $this->height = $height;
    }


    public function send(): bool
    {
        $img = Image::make($this->getPhoto())->resize($this->getWidth(),$this->getHeight());
        try {
            $img->save($this->createFile());
            return true;
        }
        catch (\Exception $exception)
        {
            return false;
        }
    }

    public abstract static function createConfig($photo, $path, int $width, int $height);

    public function createFile(): string
    {
        // Content
//        $explode = explode(',', $this->getPhoto());
//        $content = $explode[1];
        // Type
        $position = strpos($this->getPhoto(), ';');
        $sub = substr($this->getPhoto(), 0, $position);
        $ext = explode('/', $sub)[1];
        $name = time().".".$ext;
        $filePath = $this->getPath().$name;
//        $filePath = env('BASE_URL').$this->getPath().$name;
//        file_put_contents($filePath, (string)base64_decode($content));
        return $filePath;
//        return $filePath;
    }

//    public function createFiles()
//    {
//        // Content
//        $explode = explode(',', $this->getPhoto());
//        $content = $explode[1];
//        // Type
//        $position = strpos($this->getPhoto(), ';');
//        $sub = substr($this->getPhoto(), 0, $position);
//        $ext = explode('/', $sub)[1];
//        $name = time().".".$ext;
//        $filePath = $this->getPath().$name;
//        file_put_contents($filePath, (string)base64_decode($content));
//        return $filePath;
////        return $filePath;
//    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     */
    public function setPhoto($photo): void
    {
        $this->photo = $photo;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path): void
    {
        $this->path = $path;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth(int $width): void
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight(int $height): void
    {
        $this->height = $height;
    }





}
