<?php

namespace App\Http\Services\FileManager;

class FileSaver
{
    private $fileConfig;
    private static $instance;

    private function __construct()
    {
    }

    public function send(AbstractFileConfig $fileConfig){
        $this->setConfig($fileConfig);
        return $this->fileConfig->send();
    }

    public static function getInstance():self{
        if (self::$instance == null)
            self::$instance = new self();
        return self::$instance;
    }

    public function setConfig(AbstractFileConfig $fileConfig){
        return $this->fileConfig = $fileConfig;
    }

}
