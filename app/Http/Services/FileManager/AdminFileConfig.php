<?php

namespace App\Http\Services\FileManager;

use App\Models\Faq;

class AdminFileConfig extends AbstractFileConfig
{
    protected $photo;
    protected $path;
    protected $width = 130;
    protected $height = 130;


    private function __construct($photo, $path, int $width, int $height)
    {
        parent::__construct($photo, $path, $width, $height);
    }

    public static function createConfig($photo, $path, int $width, int $height):self{
        return new self($photo, $path, $width, $height);
    }
}
