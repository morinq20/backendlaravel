<?php

namespace App\Http\Services\FileManager;

interface FileConfigInterface
{

    public function send(): bool;

    public function createFile(): string;

}
