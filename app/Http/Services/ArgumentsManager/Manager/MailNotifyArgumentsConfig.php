<?php

namespace App\Http\Services\ArgumentsManager\Manager;

use App\Http\Services\ArgumentsManager\Config\AbstractArgumentsConfig;

class MailNotifyArgumentsConfig extends AbstractArgumentsConfig
{
    protected $arg = [];

    private function __construct(array $arg)
    {
        parent::__construct($arg);
    }

    public static function createConfig(array $arg):self{
        return new self($arg);
    }

}
