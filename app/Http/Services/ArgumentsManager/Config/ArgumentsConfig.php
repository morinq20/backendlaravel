<?php

namespace App\Http\Services\ArgumentsManager\Config;

class ArgumentsConfig
{
    private $argumentsConfig;
    private static $instance;

    private function __construct()
    {
    }

    public function sendArguments(AbstractArgumentsConfig $argumentsConfig){
        $this->setConfig($argumentsConfig);
        return $this->argumentsConfig->createArguments();
    }

    public static function getInstance():self{
        if (self::$instance == null)
            self::$instance = new self();
        return self::$instance;
    }

    public function setConfig(AbstractArgumentsConfig $argumentsConfig){
        return $this->argumentsConfig = $argumentsConfig;
    }
}
