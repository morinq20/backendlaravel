<?php

namespace App\Http\Services\ArgumentsManager\Config;

abstract class AbstractArgumentsConfig implements ArgumentsConfigInterface
{
    protected $arg = [];


    /**
     * @param array $arg
     */
    public function __construct(array $arg)
    {
        $this->arg = $arg;
    }

    public abstract static function createConfig(array $arg);

    public function createArguments()
    {
        return $this->getArg();
    }

    /**
     * @return array
     */
    public function getArg(): array
    {
        return $this->arg;
    }

    /**
     * @param array $arg
     */
    public function setArg(array $arg): void
    {
        $this->arg = $arg;
    }
}
