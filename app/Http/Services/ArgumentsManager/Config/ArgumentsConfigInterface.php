<?php

namespace App\Http\Services\ArgumentsManager\Config;

interface ArgumentsConfigInterface
{
    public function createArguments();
}
