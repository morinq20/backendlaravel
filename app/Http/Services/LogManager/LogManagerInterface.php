<?php

namespace App\Http\Services\LogManager;

interface LogManagerInterface
{
    public static function logInsert($log);

    public static function logRead($logDate = null);
}
