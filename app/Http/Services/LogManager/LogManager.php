<?php

namespace App\Http\Services\LogManager;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class LogManager implements LogManagerInterface
{
    const LOG_DIRECTORY = "logs";

    public static function logInsert($log)
    {
        $logFile = date("Y-m-d").".log";
        if (!file_exists(self::LOG_DIRECTORY)) {
            mkdir(self::LOG_DIRECTORY);
        }
        if (!in_array(gettype($log),["boolean","integer","double","string","object"])) {
            self::logInsert("Log type not supported!");
            return "Log type not supported!";
        }
        try {
            $file = fopen(self::LOG_DIRECTORY."/".$logFile,'a');
            $write = fwrite($file,"[".date("Y-m-d H:i:s")."]: ".$log.PHP_EOL);
            fclose($file);
        } catch (\Exception $exception) {
            self::logInsert($exception->getMessage());
            return $exception->getMessage();
        }
    }

    public static function logRead($logDate = null)
    {
        if ($logDate == null) {
            $logDate = date("Y-m-d");
        }
        $file = fopen(self::LOG_DIRECTORY."/".$logDate.".log",'r');
        while ($read = fgets($file)) {
            echo $read."<br/>";
        }
        fclose($file);
    }


}
