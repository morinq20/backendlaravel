<?php

namespace App\Http\Services\TwoFactorManager;

use Illuminate\Notifications\Facades\Vonage;

class TwoFactor
{
    public static function twoFactorSendSMS($receiver, $sender, $text, $code){
        try {
            Vonage::message()->send([
                'to' => $receiver,
                'from' => $sender,
                'text' => $text." ".$code
            ]);
        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }

    }
}
