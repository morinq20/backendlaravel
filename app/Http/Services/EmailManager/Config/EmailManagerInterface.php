<?php

namespace App\Http\Services\EmailManager\Config;

interface EmailManagerInterface
{
    public function sendMail();
}
