<?php

namespace App\Http\Services\EmailManager\Manager\Register;

use App\Http\Services\EmailManager\Config\EmailManagerInterface;
use App\Mail\Register;
use Illuminate\Support\Facades\Mail;

class RegisterMailConfig implements EmailManagerInterface
{
    private $userEmail;
    private $userFullName;
    private $userGender;
    CONST SUBJECT = "Welcome";
    CONST CONTENT = "Thank you for being a member. Welcome to our family.";

    public function __construct()
    {

    }

    public function sendMail()
    {
        Mail::to($this->getUserEmail())->send(new Register(self::SUBJECT,self::CONTENT,date("Y-m-d H:i:s"),$this->getUserFullName(),$this->getUserGender()));
    }

    /**
     * @return mixed
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    /**
     * @param mixed $userEmail
     */
    public function setUserEmail($userEmail): void
    {
        $this->userEmail = $userEmail;
    }

    /**
     * @return mixed
     */
    public function getUserFullName()
    {
        return $this->userFullName;
    }

    /**
     * @param mixed $userFullName
     */
    public function setUserFullName($userFullName): void
    {
        $this->userFullName = $userFullName;
    }

    /**
     * @return mixed
     */
    public function getUserGender()
    {
        return $this->userGender;
    }

    /**
     * @param mixed $userGender
     */
    public function setUserGender($userGender): void
    {
        $this->userGender = $userGender;
    }







}
