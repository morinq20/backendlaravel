<?php

namespace App\Http\Services\EmailManager\Manager\Newsletter;

use App\Http\Services\EmailManager\Config\EmailManagerInterface;
use App\Mail\Newsletter;
use App\Models\WebSiteSettings;
use Illuminate\Support\Facades\Mail;

class NewsletterMailConfig implements EmailManagerInterface
{
    private $newsletterEmail;
    CONST SUBJECT = "Subscriber";
    CONST CONTENT = "Thank you for subscribing.";

    public function __construct()
    {

    }

    public function sendMail()
    {
        Mail::to($this->getNewsletterEmail())->send(new Newsletter(self::SUBJECT,self::CONTENT,date("Y-m-d H:i:s")));
    }

    /**
     * @return mixed
     */
    public function getNewsletterEmail(): mixed
    {
        return $this->newsletterEmail;
    }

    /**
     * @param mixed $newsletterEmail
     */
    public function setNewsletterEmail($newsletterEmail): void
    {
        $this->newsletterEmail = $newsletterEmail;
    }



}
