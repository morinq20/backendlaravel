<?php

namespace App\Http\Controllers;

use App\Enum\EmailTemplateEnum;
use App\Events\StockTransferEmail;
use App\Events\WelcomeEmail;
use App\Http\Services\ArgumentsManager\Config\ArgumentsConfig;
use App\Http\Services\ArgumentsManager\Manager\AdminArgumentsConfig;
use App\Http\Services\TwoFactorManager\TwoFactor;
use App\Models\Campaign;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('JWT', ['except' => ['login','signup','frontLogin','register']]);
    }

    public function register(Request $request)
    {
        $validateData = $request->validate([
            'email' => 'required|unique:users|max:255',
            'name' => 'required',
            'surname' => 'required',
            'password' => 'required|between:6,255|confirmed',
            'password_confirmation' => 'required',
        ]);
        $dt = new User();
        $dt->name = $request->name;
        $dt->surname = $request->surname;
        $dt->email = $request->email;
        $dt->password = Hash::make($request->password);
        $dt->phone = $request->phone == null ? "" : $request->phone;
        $dt->save();
        $this->sendEmail($dt);


    }
    private function sendEmail($user)
    {
        $eventOrder = User::find($user->id);
        event(new WelcomeEmail($eventOrder,EmailTemplateEnum::WELCOME_TEMPLATE->value));
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validateData = $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Email or Password Invalid!'], 401);
        }
        return $this->respondWithToken($token);

    }

    public function frontLogin(Request $request)
    {
        $validateData = $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Email or Password Invalid!'], 401);
        }

        $user = User::where('email',$request->email)->first();
        if($user && $user->role_id !== null){
            abort(403);
        }
        return $this->frontToken($token);

    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }
    protected function frontToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60 * 24,
            'name' => auth()->user()->name,
            'surname' => auth()->user()->surname,
            'user_id' => auth()->user()->id,
            'email' => auth()->user()->email,
            'phone' => auth()->user()->phone,
            'roles'=>array(User::find(auth()->user()->id)->role),

        ]);
    }


    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        $user = User::find(auth()->user()->id);
        $user->isOnline = 1;
        $user->save();
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60 * 24,
            'name' => auth()->user()->name,
            'surname' => auth()->user()->surname,
            'user_id' => auth()->user()->id,
            'email' => auth()->user()->email,
            'phone' => auth()->user()->phone,
            'roles'=>array(User::find(auth()->user()->id)->role),
            'isOnline' => Cache::has('user-is-online-' . auth()->user()->id),
        ]);
    }


}
