<?php

namespace App\Http\Controllers;

use App\Events\NewMessage;
use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Models\ChatUser;
use App\Models\Messages;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ChatController extends Controller
{
    public function rooms(Request $request)
    {
        return ChatRoom::all();
    }

    public function messages(Request $request, $roomId)
    {
        return ChatMessage::where('chat_room_id',$roomId)->with('user')->orderBy('created_at','DESC')->get();
    }

    public function newMessage(Request $request, $roomId)
    {
        $newMessage = new ChatMessage();
        $newMessage->user_id = auth()->user()->getAuthIdentifier();
        $newMessage->chat_room_id = $roomId;
        $newMessage->message = $request->message;
        $newMessage->save();
        return response()->json($newMessage);

    }


    public function chatUser(): \Illuminate\Http\JsonResponse
    {
        $chatUser = DB::table('chat_user')
            ->join('users','chat_user.crm_user_id','=','users.id')
            ->get();
        return response()->json($chatUser);
    }

    public function chatCrmUserLogin($chatUserId): \Illuminate\Http\JsonResponse
    {
       $find = ChatUser::where('crm_user_id',$chatUserId)->first();
       return response()->json($find);
    }

    public function chatUserLogin($roomId,$id)
    {
        $find = ChatUser::where('crm_user_id',$roomId)->first();
        $find->user_id = $id;
        $find->status = 1;
        $find->save();
    }

    public function chatUserControl($id): \Illuminate\Http\JsonResponse
    {
        $find = DB::table('chat_user')->where('user_id',$id)->where('status',1)->first();
        return response()->json($find);

    }



    // NEW CHAT
    public function getContacts(): \Illuminate\Http\JsonResponse
    {
        $contacts = User::where('role_id','!=',null)->where('id','!=',auth()->user()->getAuthIdentifier())->get();
        return response()->json($contacts);

    }

    public function getMessagesFor($id): \Illuminate\Http\JsonResponse
    {
        $messages = Messages::where('from',$id)->orWhere('to',$id)->get();
        return response()->json($messages);
    }

    public function send(Request $request){
        $message = new Messages();
        $message->from = auth()->user()->getAuthIdentifier();
        $message->to = $request->contact_id;
        $message->text = $request->text;
        $message->save();
        broadcast(new NewMessage($message));
        return response()->json($message);
    }

//    public function getAuthInformation(): \Illuminate\Http\JsonResponse
//    {
//        return response()->json();
//    }


}
