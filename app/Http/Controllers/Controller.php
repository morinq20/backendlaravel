<?php

namespace App\Http\Controllers;

use App\Component\CreateLogs\CreateLogsHandler;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function createLogs(): CreateLogsHandler
    {
        return new CreateLogsHandler();
    }

}
