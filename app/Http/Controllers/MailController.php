<?php

namespace App\Http\Controllers;

use App\Component\MailTemplatePayloadContent\ModelsCheck;
use App\Enum\EmailTemplateEnum;
use App\Events\OrderEmail;
use App\Events\StockTransferEmail;
use App\Http\Services\ArgumentsManager\Config\ArgumentsConfig;
use App\Http\Services\ArgumentsManager\Manager\AdminArgumentsConfig;
use App\Http\Services\ArgumentsManager\Manager\MailNotifyArgumentsConfig;
use App\Jobs\SendEmailByNewsletter;
use App\Jobs\SendEmailJob;
use App\Mail\MailNotify;
use App\Models\Campaign;
use App\Models\Customer;
use App\Models\Newsletter;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\PanelSettings;
use App\Models\StockTransfer;
use App\Models\User;
use App\Models\Warehouse;
use App\Models\WebSiteSettings;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{

    public function __construct(private ModelsCheck $modelsCheck)
    {
    }

    /**
     * @throws Exception
     *
     *
     */
    public function index()
    {
        $siteSettings = WebSiteSettings::all();
        $user = Campaign::class;
        $template = $this->modelsCheck->getModel(Campaign::class);


        try{
//            $data['email'] = "efe@balinsoft.com";
//            SendEmailJob::dispatch($data);

//            $settings = WebSiteSettings::find(0);
//            $newsletter = Newsletter::find(41);
//            SendEmailByNewsletter::dispatch($settings,$newsletter);

        }catch (\Exception $exception)
        {
            throw new Exception($exception->getMessage());
        }

    }
}
