<?php

namespace App\Http\Controllers\Front;

use App\Component\ResetPassword\SendResetPasswordHandler;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResetPasswordForFrontController extends Controller
{

    public function __construct(private SendResetPasswordHandler $resetPasswordHandler)
    {

    }
    //

    public function __invoke(Request $request){
        $this->resetPasswordHandler->handle($request);
    }
}
