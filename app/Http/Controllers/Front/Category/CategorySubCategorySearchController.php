<?php

namespace App\Http\Controllers\Front\Category;

use App\Component\Front\CategorySubCategory\CategorySubCategorySearchHandler;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategorySubCategorySearchController extends Controller
{

    public function __construct(private CategorySubCategorySearchHandler $categorySubCategorySearchHandler){

    }
    public function __invoke(Request $request){
        return $this->categorySubCategorySearchHandler->handle($request);
    }
    //
}
