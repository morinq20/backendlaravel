<?php

namespace App\Http\Controllers\Front\Category;

use App\Component\Front\CategorySubCategory\CategorySubCategoryIndexHandler;
use App\Http\Controllers\Controller;

class FrontCategorySubCategoryIndexController extends Controller
{
    //
    public function __construct(private CategorySubCategoryIndexHandler $categorySubCategoryIndexHandler)
    {
    }

    public function __invoke(){
    return $this->categorySubCategoryIndexHandler->handle();
    }
}
