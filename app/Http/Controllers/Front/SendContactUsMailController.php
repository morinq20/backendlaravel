<?php

namespace App\Http\Controllers\Front;

use App\Component\ContactUsMailSend\SendContactUsMailHandler;
use App\Enum\EmailTemplateEnum;
use App\Events\ContactUsEmail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SendContactUsMailController extends Controller
{
    //
    public function __construct(){

    }

    public function index(Request $request)
    {
        event(new ContactUsEmail($request->toArray(),EmailTemplateEnum::CONTACTUS_EMAIL->value));
    }
}
