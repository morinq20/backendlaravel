<?php

namespace App\Http\Controllers\Front\Slider;

use App\Component\Front\Slider\SliderIndexHandler;
use App\Http\Controllers\Controller;

class FrontSliderListIndexController extends Controller
{
    //
    public function __construct(private SliderIndexHandler $sliderIndexHandler)
    {
    }

    public function __invoke()
    {
        return $this->sliderIndexHandler->handle();
    }
}
