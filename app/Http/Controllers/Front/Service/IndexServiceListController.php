<?php

namespace App\Http\Controllers\Front\Service;

use App\Component\FrontService\IndexServiceHandler;
use App\Http\Controllers\Controller;

class IndexServiceListController extends Controller
{
    //

    public function __construct(private IndexServiceHandler $indexServiceHandler)
    {
    }

    public function __invoke()
    {
        return $this->indexServiceHandler->handle();
    }
}
