<?php

namespace App\Http\Controllers\Front\Blog;

use App\Component\BlogComponent\SearchBlogDetailsHandler;
use App\Http\Controllers\Controller;

class SearchBlogDetailsController extends Controller
{

    public function __construct(private SearchBlogDetailsHandler $blogDetailsHandler)
    {

    }

    public function __invoke($id)
    {
        return $this->blogDetailsHandler->handle($id);
        // TODO: Implement __invoke() method.
    }
    //
}
