<?php

namespace App\Http\Controllers\Front\Blog;

use App\Component\BlogComponent\BlogIndexHandler;
use App\Http\Controllers\Controller;

class BlogIndexController extends Controller
{
    public function __construct(private BlogIndexHandler  $blogIndexHandler)
    {

    }

    public function  __invoke()
    {
        return $this->blogIndexHandler->handle();
    }
    //
}
