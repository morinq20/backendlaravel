<?php

namespace App\Http\Controllers\Front\AboutUs;

use App\Component\AboutUsComponent\AboutUsDetailsHandler;
use App\Http\Controllers\Controller;

class AboutUsDetailsController extends Controller
{
    public function __construct(private AboutUsDetailsHandler $aboutUsDetailsHandler)
    {

    }

    public function __invoke()
    {
        return $this->aboutUsDetailsHandler->handle();

    }
    //
}
