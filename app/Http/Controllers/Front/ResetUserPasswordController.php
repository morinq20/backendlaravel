<?php

namespace App\Http\Controllers\Front;

use App\Component\ResetPassword\SetNewPasswordHandler;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResetUserPasswordController extends Controller
{
    //
    public function __construct(private SetNewPasswordHandler $setNewPasswordHandler){

    }

    public function __invoke(Request $request)
    {
      return  $this->setNewPasswordHandler->handle($request);

    }
}
