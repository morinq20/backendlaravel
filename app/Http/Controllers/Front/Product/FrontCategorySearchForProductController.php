<?php

namespace App\Http\Controllers\Front\Product;

use App\Component\Front\Product\CategorySearchForProductHandler;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FrontCategorySearchForProductController extends Controller
{
    //

    public function __construct(private CategorySearchForProductHandler $categorySearchForProductHandler)
    {

    }
    public function __invoke(Request $request)
    {

        return $this->categorySearchForProductHandler->handle($request);
    }
}
