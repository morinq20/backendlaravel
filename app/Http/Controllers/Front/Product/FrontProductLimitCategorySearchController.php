<?php

namespace App\Http\Controllers\Front\Product;

use App\Component\Front\Product\ProductCategoryLimitIndexHandler;
use App\Http\Controllers\Controller;

class FrontProductLimitCategorySearchController extends Controller
{

    public function __construct(private ProductCategoryLimitIndexHandler $categoryLimitIndexHandler)
    {

    }

    public function __invoke(){

        return $this->categoryLimitIndexHandler->handle();

    }
    //
}
