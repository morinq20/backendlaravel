<?php

namespace App\Http\Controllers\Front\Product;

use App\Component\Front\Product\SearchProductSizeAndColorHandler;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SearchProductSizeAndColorController extends Controller
{
    //
    public function __construct(private SearchProductSizeAndColorHandler $searchProductSizeAndColorHandler)
    {
    }

    public function __invoke(Request $request)
    {
        return $this->searchProductSizeAndColorHandler->handle($request);

    }
}
