<?php

namespace App\Http\Controllers\Front\Product;

use App\Component\Front\Product\SearchSizeForColorHandler;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SearhSizeForColorController extends Controller
{
    //
    public function __construct(private SearchSizeForColorHandler $searchSizeForColorHandler)
    {

    }

    public function __invoke(Request $request)
    {
        // TODO: Implement __invoke() method.
        return $this->searchSizeForColorHandler->handle($request);
    }
}
