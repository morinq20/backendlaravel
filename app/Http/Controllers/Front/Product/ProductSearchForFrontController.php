<?php

namespace App\Http\Controllers\Front\Product;

use App\Component\Front\Product\ProductSearchForFrontHandler;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductSearchForFrontController extends Controller
{

    public function __construct(private ProductSearchForFrontHandler $productSearchForFrontHandler){

    }

    public function __invoke(Request $request){
            return $this->productSearchForFrontHandler->handle($request);
    }
    //
}
