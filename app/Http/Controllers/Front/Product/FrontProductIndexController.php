<?php

namespace App\Http\Controllers\Front\Product;

use App\Component\Front\Product\ProductIndexHandler;
use App\Http\Controllers\Controller;
use AWS\CRT\HTTP\Request;

class FrontProductIndexController extends Controller
{
    public function __construct(private ProductIndexHandler $productIndexHandler)
    {
    }

    public function __invoke()
    {
        return $this->productIndexHandler->handle();

    }
    //
}
