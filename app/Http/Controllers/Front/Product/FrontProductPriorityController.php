<?php

namespace App\Http\Controllers\Front\Product;

use App\Component\Front\Product\ProductPriorityIndexController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FrontProductPriorityController extends Controller
{
    //
    public function __construct(private ProductPriorityIndexController $priorityIndexController)
    {
    }
    public function __invoke(Request $request)
    {
        return $this->priorityIndexController->handle($request);
    }
}
