<?php

namespace App\Http\Controllers\Front\Product;

use App\Component\Front\Product\RelatedProductIndexHandler;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RelatedProductSearchForCategoryController extends Controller
{
    //

    public function __construct(private RelatedProductIndexHandler $relatedProductIndexHandler)
    {
    }


    public function __invoke(Request $request)
    {
        return $this->relatedProductIndexHandler->handle($request);
    }
}
