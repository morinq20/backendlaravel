<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\FileManager\FileSaver;
use App\Http\Services\FileManager\ServiceFileConfig;
use App\Http\Traits\FolderControlTrait;
use App\Models\ImageResize;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServiceController extends Controller
{
    use FolderControlTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $services = Service::orderBy('service_seq','ASC')->get();
        return response()->json($services);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $service = new Service();
            $service->service_title = $request->service_title;
            $service->service_status = $request->service_status;
            $service->service_description = strip_tags($request->service_description);
            $service->service_seq = self::makeSeq();
            if (isset($request->service_image)) {
                $path = 'backend/uploads/service/';
                $this->folderIfNotExists($path);
                $file = FileSaver::getInstance();
                $config = ServiceFileConfig::createConfig($request->service_image,$path,$imageResize->service_image_width,$imageResize->service_image_height);
                $file_save = $file->send($config);
                if (isset($file_save))
                {
                    $service->service_image = $config->createFile();
                }
                $service->save();
            } else {
                $service->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $service = Service::find($id);
        return response()->json($service);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $newPhoto = $request->new_service_image;
            $oldPhoto = $request->service_image;
            $service = Service::find($id);
            $service->service_title = $request->service_title;
            $service->service_status = $request->service_status;
            $service->service_description = strip_tags($request->service_description);
//            $service->service_seq = $request->service_seq;
            if (isset($newPhoto)) {
                if ($oldPhoto != null) {
                    unlink($oldPhoto);
                    $path = 'backend/uploads/service/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = ServiceFileConfig::createConfig($newPhoto,$path,$imageResize->service_image_width,$imageResize->service_image_height);
                    $file_save = $file->send($config);
                    $service->service_image = $config->createFile();
                    $service->save();
                } else {
                    $path = 'backend/uploads/service/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = ServiceFileConfig::createConfig($newPhoto,$path,$imageResize->service_image_width,$imageResize->service_image_height);
                    $file_save = $file->send($config);
                    $service->service_image = $config->createFile();
                    $service->save();
                }
            } else {
                $service->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $service = Service::find($id);
            $photo = $service->service_image;
            if (isset($photo)) {
                unlink($photo);
                $service->delete();
            } else {
                $service->delete();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    // Service Sortable

    /**
     * @throws \Exception
     */
    public function serviceSortable(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->item as $key => $value) {
                $service = Service::find(intval($value));
                $service->service_seq = intval($key);
                $service->save();
            }
            echo true;
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    private static function makeSeq(): int|string
    {
        $control = Service::latest('id')->first();
        if (!$control){
            return "1";
        }else{
            return strval($control->team_seq + 1);
        }
    }
}
