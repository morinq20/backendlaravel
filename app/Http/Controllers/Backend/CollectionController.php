<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\FileManager\CollectionFileConfig;
use App\Http\Services\FileManager\FileSaver;
use App\Http\Traits\FolderControlTrait;
use App\Models\Collection;
use App\Models\ImageResize;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CollectionController extends Controller
{
    use FolderControlTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $collection = Collection::orderBy('collection_seq','ASC')->get();
        return response()->json($collection);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $collection = new Collection();
            $collection->collection_title = $request->collection_title;
            $collection->collection_code = $request->collection_code;
            $collection->collection_status = $request->collection_status;
            $collection->is_published = $request->is_published;
            $collection->collection_description = strip_tags($request->collection_description);
            $collection->collection_seq = self::makeSeq();
            if (isset($request->collection_image)) {
                $path = 'backend/uploads/collection/';
                $this->folderIfNotExists($path);
                $file = FileSaver::getInstance();
                $config = CollectionFileConfig::createConfig($request->collection_image,$path,$imageResize->collection_image_width,$imageResize->collection_image_height);
                $file_save = $file->send($config);
                if (isset($file_save))
                {
                    $collection->collection_image = $config->createFile();
                }
                $collection->save();
            } else {
                $collection->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $collection = Collection::find($id);
        return response()->json($collection);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $newPhoto = $request->new_collection_image;
            $oldPhoto = $request->collection_image;
            $collection = Collection::find($id);
            $collection->collection_title = $request->collection_title;
            $collection->collection_code = $request->collection_code;
            $collection->collection_status = $request->collection_status;
            $collection->is_published = $request->is_published;
            $collection->collection_description = strip_tags($request->collection_description);
//            $collection->collection_seq = $request->collection_seq;
            if (isset($newPhoto)) {
                if ($oldPhoto != null) {
                    unlink($oldPhoto);
                    $path = 'backend/uploads/collection/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = CollectionFileConfig::createConfig($newPhoto,$path,$imageResize->collection_image_width,$imageResize->collection_image_height);
                    $file_save = $file->send($config);
                    $collection->collection_image = $config->createFile();
                    $collection->save();
                } else {
                    $path = 'backend/uploads/collection/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = CollectionFileConfig::createConfig($newPhoto,$path,$imageResize->collection_image_width,$imageResize->collection_image_height);
                    $file_save = $file->send($config);
                    $collection->collection_image = $config->createFile();
                    $collection->save();
                }
            } else {
                $collection->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $collection = Collection::find($id);
            $photo = $collection->collection_image;
            if (isset($photo)) {
                unlink($photo);
                $collection->delete();
            } else {
                $collection->delete();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    // Blog Sortable

    /**
     * @throws Exception
     */
    public function collectionSortable(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->item as $key => $value) {
                $collection = Collection::find(intval($value));
                $collection->collection_seq = intval($key);
                $collection->save();
            }
            echo true;
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    private static function makeSeq(): int|string
    {
        $control = Collection::latest('id')->first();
        if (!$control){
            return "1";
        }else{
            return strval($control->team_seq + 1);
        }
    }
}
