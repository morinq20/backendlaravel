<?php

namespace App\Http\Controllers\Backend;

use App\Component\Excel\ExportHandler;
use App\Http\Controllers\Controller;
use App\Http\Requests\ExcelExportRequest;
use Illuminate\Http\Request;

class ExcelExportController extends Controller
{
    public function __construct(private ExportHandler $exportHandler)
    {
    }

    public function __invoke(ExcelExportRequest $request)
    {
        $this->exportHandler->handle($request);
    }
}
