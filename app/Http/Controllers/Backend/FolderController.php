<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Storage;
use ZipArchive;
use ZanySoft\Zip\Zip;
use ZanySoft\Zip\ZipManager;

class FolderController extends Controller
{
    // Çalışan fonksiyonlar
    public function index(): JsonResponse
    {
        $folders = Storage::disk('public-folder')->directories();
        $files = Storage::disk('public-folder')->files();
        $folder = [];
        if ($folders !== null){
            foreach ($folders as $idx => $value){
                $result = $value;
                $folder[][$idx] = ['name' => $result, 'type' => null];
            }
        }
        if ($files !== null){
            foreach ($files as $key => $item){
                $type = explode('.',$item);
                $folder[][$key] = ['name' => $item, 'type' => array_reverse($type)[0]];
            }
        }
        return response()->json($folder);
    }

    public function mainFolder($mainFolder): JsonResponse
    {
        $folders = $this->folderContent($mainFolder);
        return response()->json($folders);
    }

    private function folderContent($mainFolder): array
    {
        $folder = [];
//        $lcFirstFolder = lcfirst($mainFolder);
        $folders = Storage::disk('public-folder')->directories($mainFolder);
        $files = Storage::disk('public-folder')->files($mainFolder);
        if ($folders !== null) {
            foreach ($folders as $idx => $value){
                $explode = explode('/',$value);
                $result = $explode[1];
                $folder[][$idx] = ['name' => $result, 'type' => null];
            }
        }
        if ($files !== null){
            foreach ($files as $key => $item){
                $explode = explode('/',$item);
                $type = explode('.',$explode[1]);
                $folder[][$key] = ['name' => $explode[1], 'type' => array_reverse($type)[0]];
            }
        }
        return $folder;
    }

    public function subFolder($mainFolder): JsonResponse
    {
        $result = $this->subFolderContent($mainFolder);
        return response()->json($result);
    }

    private function subFolderContent($mainFolder): array
    {
        $replace = str_replace(">","/",$mainFolder);
        $folder = [];
        $folders = Storage::disk('public-folder')->directories($replace);
        $files = Storage::disk('public-folder')->files($replace);
        if ($folders !== null) {
            foreach ($folders as $idx => $value){
                $explode = explode('/',$value);
                $endExplode = array_reverse($explode)[0];
                $result = $endExplode;
                $folder[][$idx] = ['name' => $result, 'type' => null];
            }
        }
        if ($files !== null){
            foreach ($files as $key => $item){
                $explode = explode('/',$item);
                $endExplode = array_reverse($explode)[0];
                $type = explode('.',$endExplode);
                $folder[][$key] = ['name' => $endExplode, 'type' => array_reverse($type)[0]];
            }
        }
        return $folder;
    }

    public function getDownload($mainFolder,$folder)
    {
        $replace = str_replace(">","/",$mainFolder);
        $url = public_path().'/'.$replace.'/'.$folder;
        return response()->download($url);
    }

    public function newFolder($mainFolder,$folder)
    {
        if ($mainFolder !== 'null'){
            $replace = str_replace(">","/",$mainFolder);
            $path = public_path().'/'.$replace.'/'.$folder;
            if (!file_exists($path))
                mkdir($path);
            else
                return response()->json('error');
        }else{
            $path = public_path().'/'.$folder;
            if (!file_exists($path))
                mkdir($path);
            else
                return response()->json('error');
        }
    }


    // Tekrar bakılacak olan fonksiyonlar
    public function deleteFolder($mainFolder,$folder)
    {
        if ($mainFolder !== 'null'){
            $replace = str_replace(">","/",$mainFolder);
            $path = public_path().'/'.$replace.'/'.$folder;
            $folders = Storage::disk('public-folder')->allDirectories($replace.'/'.$folder);
            $files = Storage::disk('public-folder')->allFiles($replace.'/'.$folder);
            if ($path){
                if ($files){
                    foreach ($files as $key => $item){
                        $path = public_path().'/'.$item;
                        $deneme = explode('/',$item);
                        $deneme1 = array_reverse($deneme)[0];
                        if (str_contains($deneme1,'.')){
                            $deneme2 = str_contains($item,$deneme1);
                            if ($deneme2){
                                unlink($path);
                            }
                        }
                    }
                }
                if ($folders){
                    foreach ($folders as $key => $item){
                        $folders = Storage::disk('public-folder')->allDirectories($item);
//                        return response()->json($folders);
                        $path = public_path().'/'.$item;
                        $deneme = explode('/',$item);
                        $deneme1 = array_reverse($deneme)[0];
                        if (str_contains($path,$deneme1)){
                            $deneme2 = str_contains($item,$deneme1);
                            if ($deneme2){
                                rmdir($path);
                            }
                        }
                    }
                }
            }else{
                return response()->json('error');
            }
        }else{
            $path = public_path().'/'.$folder;
            if ($path){
                rmdir($path);
            }else{
                return response()->json('error');
            }

        }
    }

    public function zipFile($mainFolder,$folder)
    {
        if ($mainFolder !== 'null'){
            $replace = str_replace(">","/",$mainFolder);
            $path = $replace.'/'.$folder;
            $zip = new ZipArchive();
            $explode = explode('.',$folder);
            $fileName = "zip_".$explode[0].time();
            if(str_contains($folder, '.')){
                if ($zip->open($fileName,ZipArchive::CREATE | ZipArchive::OVERWRITE) === true){
                    $files = basename($path);
                    $zip->addFile($folder,$files);
                    $zip->close();
                }
//                return response()->download($path);
            }else{
                if ($zip->open($fileName,ZipArchive::CREATE | ZipArchive::OVERWRITE) === true){
                    $folders = Storage::disk('public-folder')->directories($replace);
                    foreach ($folders as $key => $value){
                        $relativeNameInZipFile = basename($value);
                        $zip->addFile($value,$relativeNameInZipFile);
                    }
                    $zip->close();
//                    return response()->download($path);
                }
            }
            return response()->json($explode);
        }else{
            $path = public_path().'/'.$folder;
            $zip = new ZipArchive();
            $explode = explode('.',$folder);
            $fileName = "zip_".$explode[0].time();
            if(str_contains($folder, '.')){
                if ($zip->open($fileName,ZipArchive::CREATE | ZipArchive::OVERWRITE) === true){
                    $files = basename($path);
                    $zip->addFile($folder,$files);
                    $zip->close();
                }
//                return response()->download($path);
            }else{
                $folders = Storage::disk('public-folder')->directories($folder);
                if ($zip->open($fileName,ZipArchive::CREATE | ZipArchive::OVERWRITE) === true){
                    $folders = Storage::disk('public-folder')->directories($folder);
                    foreach ($folders as $key => $value){
                        $relativeNameInZipFile = basename($value);
                        $zip->addFile($value,$relativeNameInZipFile);
                    }
                    $zip->close();
//                    return response()->download($path);
                }
            }
        }
        return response()->download($path);
    }


    // Test edilecek editor ile
    public function openFileInEditor($mainFolder,$file): JsonResponse
    {
        $replace = str_replace(">","/",$mainFolder);
//        $path = $replace.'/'.$file;
        $path = $replace;
//        $openFile = file_get_contents($path);
        $openFile = fopen($path,'r');
        $readFile = fread($openFile,filesize($path));
        return response()->json($readFile);
    }


}
