<?php

namespace App\Http\Controllers\Backend;

use App\Component\Order\OrderReportSearch\OrderReportSearchHandler;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OrderReportController extends Controller
{
    public function __construct(
        private OrderReportSearchHandler $orderReportSearchHandler
    ){

    }
    //
    public function __invoke(Request $request)
    {
        return $this->orderReportSearchHandler->handle($request);

    }
}
