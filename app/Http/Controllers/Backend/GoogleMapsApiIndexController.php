<?php

namespace App\Http\Controllers\Backend;

use App\Component\GoogleMapsApi\GoogleMapsApiIndex;
use App\Http\Controllers\Controller;

class GoogleMapsApiIndexController extends Controller
{
    //
    public function __construct(private GoogleMapsApiIndex $googleMapsApiIndex)
    {
    }

    public function __invoke(){
        return $this->googleMapsApiIndex->handle();
    }
}
