<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Cities;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\StockTransfer;
use App\Models\StockTransferStatus;
use App\Models\Supplier;
use App\Models\Truck;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SupplierController extends Controller
{
    public function __construct(private StockTransferStatus $stockTransferStatus,
                                private StockTransfer $stockTransfer,
                                private OrderStatus $orderStatus)
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $supplier = Supplier::orderBy('id','DESC')->get();
        return response()->json($supplier);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $country = Country::where('id',$request->country_id)->first();
            $city = Cities::where('id',$request->city_id)->first();
            $supplier = new Supplier();
            $supplier->supplier_name = $request->supplier_name;
            $supplier->supplier_surname = $request->supplier_surname;
            $supplier->supplier_email = $request->supplier_email;
            $supplier->supplier_phone = $request->supplier_phone;
            $supplier->supplier_country = $country->country;
            $supplier->supplier_city = $city->city;
            $supplier->country_id = $request->country_id;
            $supplier->city_id = $request->city_id;
            $supplier->supplier_phone_dial_code = $request->supplierPhoneDialCode;
            $supplier->supplier_zipcode = $request->supplier_zipcode;
            $supplier->supplier_address = strip_tags($request->supplier_address);
            $supplier->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $supplier = Supplier::find($id);
        return response()->json($supplier);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $country = Country::where('id',$request->country_id)->first();
            $city = Cities::where('id',$request->city_id)->first();
            $supplier = Supplier::find($id);
            $supplier->supplier_name = $request->supplier_name;
            $supplier->supplier_surname = $request->supplier_surname;
            $supplier->supplier_email = $request->supplier_email;
            $supplier->supplier_phone = $request->supplier_phone;
            $supplier->supplier_country = $country->country;
            $supplier->supplier_city = $city->city;
            $supplier->country_id = $request->country_id;
            $supplier->city_id = $request->city_id;
            $supplier->supplier_phone_dial_code = $request->supplierPhoneDialCode;
            $supplier->supplier_zipcode = $request->supplier_zipcode;
            $supplier->supplier_address = strip_tags($request->supplier_address);
            $supplier->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $supplier = Supplier::find($id);
            $supplier->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }


    public function supplierStockTransferTransaction($id): \Illuminate\Http\JsonResponse
    {
        $supplierId = '';
        $stockTransfer = [];
        $transactions = StockTransfer::where('isCanceled',0)->orderBy('id','DESC')->get();
        foreach ($transactions as $key => $item) {
            $fromWarehouse = Warehouse::select('warehouse_name','warehouse_type')->where('id',$item->from_warehouse_id)->first();
            $toWarehouse = Warehouse::select('warehouse_name','warehouse_type')->where('id',$item->to_warehouse_id)->first();
            $process = json_decode($item->stock_transfer_process);
            foreach ($process as $value){
                if ($value->supplier == $id){
                    $supplier = Supplier::with('truck')->where('id',$id)->first();
                    $supplierId = $supplier->id;
                    $stockTransfer[$key]['supplier_name'] = $supplier->supplier_name;
                    $stockTransfer[$key]['supplier_surname'] = $supplier->supplier_surname;
                    foreach ($supplier->truck as $index){
                        if ($value->truck == $index->id){
                            $stockTransfer[$key]['truck_plate_number'] = $index->truck_plate_number;
                        }
                    }
                }
            }
            $stockTransfer[$key]['code'] = $item->code;
            $stockTransfer[$key]['fromWarehouse_name'] = $fromWarehouse->warehouse_name;
            $stockTransfer[$key]['fromWarehouse_type'] = $fromWarehouse->warehouse_type;
            $stockTransfer[$key]['toWarehouse_name'] = $toWarehouse->warehouse_name;
            $stockTransfer[$key]['toWarehouse_type'] = $toWarehouse->warehouse_type;
            $stockTransfer[$key]['start_transfer_date'] = $item->start_transfer_date;
            $stockTransfer[$key]['finish_transfer_date'] = $item->finish_transfer_date;
            $stockTransfer[$key]['status'] = $item->status;
            $stockTransfer[$key]['id'] = $item->id;
            $stockTransfer[$key]['status_text'] = $this->stockTransferStatus->where('status_id',$item->status)->get();

        }
        if ($supplierId == $id){
            return response()->json($stockTransfer);
        }else{
            return response()->json([]);
        }
    }

    public function supplierOrderTransaction($id): \Illuminate\Http\JsonResponse
    {
        $supplierId = '';
        $order = [];
        $orders = Order::where('isCanceled',0)->orderBy('id','DESC')->get();
        foreach ($orders as $key => $item) {
            $fromWarehouse = Warehouse::select('warehouse_name','warehouse_type')->where('id',$item->from_warehouse_id)->first();
            $customer = Customer::select('customer_name','customer_surname','customer_email','customer_phone')->where('id',$item->customer_id)->first();
            $process = json_decode($item->order_transfer_process);
            foreach ($process as $value){
                if ($value->supplier == $id){
                    $supplier = Supplier::with('truck')->where('id',$id)->first();
                    $supplierId = $supplier->id;
                    $order[$key]['supplier_name'] = $supplier->supplier_name;
                    $order[$key]['supplier_surname'] = $supplier->supplier_surname;
                    foreach ($supplier->truck as $index){
                        if ($value->truck == $index->id){
                            $order[$key]['truck_plate_number'] = $index->truck_plate_number;
                        }
                    }
                }
            }
            $order[$key]['code'] = $item->code;
            $order[$key]['fromWarehouse_name'] = $fromWarehouse->warehouse_name;
            $order[$key]['fromWarehouse_type'] = $fromWarehouse->warehouse_type;
            $order[$key]['customer_name'] = $customer->customer_name;
            $order[$key]['customer_surname'] = $customer->customer_surname;
            $order[$key]['customer_email'] = $customer->customer_email;
            $order[$key]['customer_phone'] = $customer->customer_phone;
            $order[$key]['start_transfer_date'] = $item->start_transfer_date;
            $order[$key]['finish_transfer_date'] = $item->finish_transfer_date;
            $order[$key]['status'] = $item->status;
            $order[$key]['id'] = $item->id;
            $order[$key]['status_text'] = $this->orderStatus->where('status_id',$item->status)->get();
        }
        if ($supplierId == $id){
            return response()->json($order);
        }else{
            return response()->json([]);
        }

    }


}
