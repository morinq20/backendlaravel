<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Cities;
use App\Models\Country;
use App\Models\Product;
use App\Models\Warehouse;
use App\Models\WarehouseItem;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $warehouse = Warehouse::orderBy('id','DESC')->get();
        return response()->json($warehouse);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $country = Country::where('id',$request->country_id)->first();
            $city = Cities::where('id',$request->city_id)->first();
            $warehouse = new Warehouse();
            $warehouse->warehouse_name = $request->warehouse_name;
            $warehouse->warehouse_phone = $request->warehouse_phone;
            $warehouse->warehouse_email = $request->warehouse_email;
            $warehouse->warehouse_country = $country->country;
            $warehouse->warehouse_city = $city->city;
            $warehouse->warehouse_phone_dial_code = $request->warehousePhoneDialCode;
            $warehouse->country_id = $request->country_id;
            $warehouse->city_id = $request->city_id;
            $warehouse->warehouse_postcode = $request->warehouse_postcode;
            $warehouse->warehouse_type = $request->warehouse_type;
            $warehouse->warehouse_status = $request->warehouse_status;
            $warehouse->warehouse_address = strip_tags($request->warehouse_address);
            $warehouse->warehouse_latitude = $request->warehouse_latitude;
            $warehouse->warehouse_longitude = $request->warehouse_longitude;
            $warehouse->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $warehouse = Warehouse::find($id);
        return response()->json($warehouse);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $country = Country::where('id',$request->country_id)->first();
            $city = Cities::where('id',$request->city_id)->first();
            $warehouse = Warehouse::find($id);
            $warehouse->warehouse_name = $request->warehouse_name;
            $warehouse->warehouse_phone = $request->warehouse_phone;
            $warehouse->warehouse_email = $request->warehouse_email;
            $warehouse->warehouse_country = $country->country;
            $warehouse->warehouse_city = $city->city;
            $warehouse->warehouse_phone_dial_code = $request->warehousePhoneDialCode;
            $warehouse->country_id = $request->country_id;
            $warehouse->city_id = $request->city_id;
            $warehouse->warehouse_postcode = $request->warehouse_postcode;
            $warehouse->warehouse_type = $request->warehouse_type;
            $warehouse->warehouse_status = $request->warehouse_status;
            $warehouse->warehouse_address = strip_tags($request->warehouse_address);
            $warehouse->warehouse_latitude = $request->warehouse_latitude;
            $warehouse->warehouse_longitude = $request->warehouse_longitude;
            $warehouse->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $warehouse = Warehouse::find($id);
            $warehouse->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }


    public function controlMainWarehouse()
    {
        $controlMainWarehouse = Warehouse::where('warehouse_type',1)->first();
        return response()->json($controlMainWarehouse);
    }



}
