<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\FileManager\EmployeeFileConfig;
use App\Http\Services\FileManager\FileSaver;
use App\Http\Traits\FolderControlTrait;
use App\Models\Employee;
use App\Models\ImageResize;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    use FolderControlTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $employees = Employee::orderBy('id','DESC')->get();
        return response()->json($employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $employee = new Employee();
            $employee->employee_name = $request->employee_name;
            $employee->employee_surname = $request->employee_surname;
            $employee->employee_phone = $request->employee_phone;
            $employee->employee_email = $request->employee_email;
            $employee->employee_code = $request->employee_code;
            $employee->employee_description = strip_tags($request->employee_description);
            $employee->employee_location = strip_tags($request->employee_location);
            $employee->employee_position = $request->employee_position;
            $employee->employee_practice_area = $request->employee_practice_area;
            $employee->employee_experience = $request->employee_experience;
            $employee->employee_job = $request->employee_job;
            $employee->country_id = $request->country_id;
            $employee->city_id = $request->city_id;
            $employee->employee_phone_dial_code = $request->employeePhoneDialCode;
            if (isset($request->employee_image)) {
                $path = 'backend/uploads/employee/';
                $this->folderIfNotExists($path);
                $file = FileSaver::getInstance();
                $config = EmployeeFileConfig::createConfig($request->employee_image,$path,$imageResize->employee_image_width,$imageResize->employee_image_height);
                $file_save = $file->send($config);
                if (isset($file_save))
                {
                    $employee->employee_image = $config->createFile();
                }
                $employee->save();
            } else {
                $employee->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $employee = Employee::find($id);
        return response()->json($employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $newPhoto = $request->new_employee_image;
            $oldPhoto = $request->employee_image;
            $employee = Employee::find($id);
            $employee->employee_name = $request->employee_name;
            $employee->employee_surname = $request->employee_surname;
            $employee->employee_phone = $request->employee_phone;
            $employee->employee_email = $request->employee_email;
            $employee->employee_code = $request->employee_code;
            $employee->employee_description = strip_tags($request->employee_description);
            $employee->employee_location = strip_tags($request->employee_location);
            $employee->employee_position = $request->employee_position;
            $employee->employee_practice_area = $request->employee_practice_area;
            $employee->employee_experience = $request->employee_experience;
            $employee->employee_job = $request->employee_job;
            $employee->country_id = $request->country_id;
            $employee->city_id = $request->city_id;
            $employee->employee_phone_dial_code = $request->employeePhoneDialCode;
            if (isset($newPhoto)) {
                if ($oldPhoto != null) {
                    unlink($oldPhoto);
                    $path = 'backend/uploads/employee/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = EmployeeFileConfig::createConfig($newPhoto,$path,$imageResize->employee_image_width,$imageResize->employee_image_height);
                    $file_save = $file->send($config);
                    $employee->employee_image = $config->createFile();
                    $employee->save();
                } else {
                    $path = 'backend/uploads/employee/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = EmployeeFileConfig::createConfig($newPhoto,$path,$imageResize->employee_image_width,$imageResize->employee_image_height);
                    $file_save = $file->send($config);
                    $employee->employee_image = $config->createFile();
                    $employee->save();
                }
            } else {
                $employee->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $employee = Employee::find($id);
            $photo = $employee->employee_image;
            if (isset($photo)) {
                unlink($photo);
                $employee->delete();
            } else {
                $employee->delete();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

}
