<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\EmailManager\Manager\Newsletter\NewsletterMailConfig;
use App\Jobs\SendEmailByNewsletter;
use App\Models\Newsletter;
use App\Models\User;
use App\Models\WebSiteSettings;
use App\Notifications\SendNewsletter;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class NewsletterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {

        $newsletter_email = Newsletter::orderBy('id','DESC')->get();
        return response()->json($newsletter_email);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $control = Newsletter::where('newsletter_email',$request->newsletter_email)->first();
            if ($control) {
                return response()->json('false');
            } else {
//                $admin = User::all();
                $newsletter = array();
                $newsletter['newsletter_email'] = $request->newsletter_email;
                $newsletterId = DB::table('newsletter')->insertGetId($newsletter);
//                $this->sendMail($newsletterId);
//                Notification::send($admin, new SendNewsletter($request->newsletter_email));
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    private function sendMail($newsletterId){

        $newsletter = Newsletter::find($newsletterId);
        $settings = WebSiteSettings::find(0);
        SendEmailByNewsletter::dispatch($settings,$newsletter);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $newsletter = Newsletter::find($id);
        return response()->json($newsletter);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $newsletter = Newsletter::find($id);
            $newsletter->newsletter_email = $request->newsletter_email;
            $newsletter->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $newsletter = Newsletter::find($id);
            $newsletter->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
