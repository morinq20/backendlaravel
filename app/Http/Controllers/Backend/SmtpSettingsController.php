<?php

namespace App\Http\Controllers\Backend;

use App\Component\SmtpSettings\SmtpSettingsHandler;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SmtpSettingsController extends Controller
{
    public function __construct(private SmtpSettingsHandler $smtpSettingsHandler)
    {

    }

    public function __invoke(Request $request)
    {
        $this->smtpSettingsHandler->handle($request);
    }
}
