<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\FileService\FileSaver;
use App\Models\Expense;
use App\Models\ExpenseDetail;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $expense = Expense::with('accountingStatus')->orderBy('id','DESC')->get();
        return response()->json($expense);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'expense_title' => 'required',
                'accounting_status_id' => 'required',
            ]);
            $expense = new Expense();
            $expense->expense_title = $request->expense_title;
            $expense->expense_date = $request->expense_date;
            $expense->expense_tax = $request->expense_tax;
            $expense->expense_description = strip_tags($request->expense_description);
            $expense->accounting_status_id = $request->accounting_status_id;
            $expense->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $expense = Expense::find($id);
        return response()->json($expense);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'expense_title' => 'required',
                'accounting_status_id' => 'required',
            ]);
            $expense = Expense::find($id);
            $expense->expense_title = $request->expense_title;
            $expense->expense_date = $request->expense_date;
            $expense->expense_tax = $request->expense_tax;
            $expense->expense_description = strip_tags($request->expense_description);
            $expense->accounting_status_id = $request->accounting_status_id;
            $expense->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $expense = Expense::find($id);
            $expense->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }



    // Expense Detail Store

    /**
     * @throws Exception
     */
    public function storeExpenseDetail(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            foreach ($requestAll as $item){
                $expenseDetail = new ExpenseDetail();
                $expenseDetail->expense_id = $id;
                $expenseDetail->quantity = $item['quantity'];
                $expenseDetail->amount = $item['amount'];
                $expenseDetail->total_amount = $item['total_amount'];
                $expenseDetail->tax = $item['tax'];
                $expenseDetail->total_tax = $item['total_tax'];
                $expenseDetail->total = $item['total'];
                $expenseDetail->description = $item['description'];
                $expenseDetail->invoice_no = $item['invoice_no'];
                $expenseDetail->invoice_date = $item['invoice_date'];
                $expenseDetail->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    // Expense Detail Show
    public function showExpenseDetail(Request $request,$id): \Illuminate\Http\JsonResponse
    {
        $expenseDetails = ExpenseDetail::where('expense_id',$id)->get();
        return response()->json($expenseDetails);
    }

    // Expense Detail Update

    /**
     * @throws Exception
     */
    public function updateExpenseDetail(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $allRequest = $request->all();
            foreach ($allRequest as $item){
                $control = ExpenseDetail::find($item['id']);
                $control->expense_id = $id;
                $control->quantity = $item['quantity'];
                $control->amount = $item['amount'];
                $control->total_amount = $item['total_amount'];
                $control->tax = $item['tax'];
                $control->total_tax = $item['total_tax'];
                $control->total = $item['total'];
                $control->description = $item['description'];
                $control->invoice_no = $item['invoice_no'];
                $control->invoice_date = $item['invoice_date'];
                $control->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function deleteExpenseDetail($id)
    {
        DB::beginTransaction();
        try {
            $findExpenseDetail = ExpenseDetail::find($id);
            $findExpenseDetail->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

}
