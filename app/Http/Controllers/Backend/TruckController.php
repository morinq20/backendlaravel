<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\CodeService\CodeService;
use App\Http\Services\FileManager\FileSaver;
use App\Http\Services\FileManager\TruckFileConfig;
use App\Http\Traits\FolderControlTrait;
use App\Models\Customer;
use App\Models\ImageResize;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\StockTransfer;
use App\Models\StockTransferStatus;
use App\Models\Supplier;
use App\Models\Truck;
use App\Models\Warehouse;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TruckController extends Controller
{
    use FolderControlTrait;

    public function __construct(private StockTransferStatus $stockTransferStatus,
                                private StockTransfer $stockTransfer,
                                private OrderStatus $orderStatus )
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $truck = Truck::with('supplier')->orderBy('id','DESC')->get();
        return response()->json($truck);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $code = CodeService::createCode("TRUCK");
            $truck = new Truck();
            $truck->truck_brand = $request->truck_brand;
            $truck->truck_model = $request->truck_model;
            $truck->truck_code = $code;
            $truck->truck_plate_number = $request->truck_plate_number;
            $truck->truck_start_km = $request->truck_start_km;
            $truck->supplier_id = $request->supplier_id;
            if (isset($request->truck_image)) {
                $path = 'backend/uploads/truck/';
                $this->folderIfNotExists($path);
                $file = FileSaver::getInstance();
                $config = TruckFileConfig::createConfig($request->truck_image,$path,$imageResize->truck_image_width,$imageResize->truck_image_height);
                $file_save = $file->send($config);
                if (isset($file_save))
                {
                    $truck->truck_image = $config->createFile();
                }
                $truck->save();
            } else {
                $truck->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $truck = Truck::find($id);
        return response()->json($truck);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $newPhoto = $request->new_truck_image;
            $oldPhoto = $request->truck_image;
            $truck = Truck::find($id);
            $truck->truck_brand = $request->truck_brand;
            $truck->truck_model = $request->truck_model;
            $truck->truck_plate_number = $request->truck_plate_number;
            $truck->truck_start_km = $request->truck_start_km;
            $truck->truck_image = $request->truck_image;
            $truck->supplier_id = $request->supplier_id;
            if (isset($newPhoto)) {
                if ($oldPhoto != null) {
                    unlink($oldPhoto);
                    $path = 'backend/uploads/truck/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = TruckFileConfig::createConfig($newPhoto,$path,$imageResize->truck_image_width,$imageResize->truck_image_height);
                    $file_save = $file->send($config);
                    $truck->truck_image = $config->createFile();
                    $truck->save();
                } else {
                    $path = 'backend/uploads/truck/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = TruckFileConfig::createConfig($newPhoto,$path,$imageResize->truck_image_width,$imageResize->truck_image_height);
                    $file_save = $file->send($config);
                    $truck->truck_image = $config->createFile();
                    $truck->save();
                }
            } else {
                $truck->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $truck = Truck::find($id);
            $truck->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }


    public function truckStockTransferTransaction($id): \Illuminate\Http\JsonResponse
    {
        $truckId = '';
        $stockTransfer = [];
        $transactions = StockTransfer::where('isCanceled',0)->orderBy('id','DESC')->get();
        foreach ($transactions as $key => $item) {
            $fromWarehouse = Warehouse::select('warehouse_name','warehouse_type')->where('id',$item->from_warehouse_id)->first();
            $toWarehouse = Warehouse::select('warehouse_name','warehouse_type')->where('id',$item->to_warehouse_id)->first();
            $process = json_decode($item->stock_transfer_process);
            foreach ($process as $value){
                if ($value->truck == $id){
                    $truck = Truck::with('supplier')->where('id',$id)->first();
                    $truckId = $truck->id;
                    $stockTransfer[$key]['supplier_name'] = $truck->supplier->supplier_name;
                    $stockTransfer[$key]['supplier_surname'] = $truck->supplier->supplier_surname;
                }
            }
            $stockTransfer[$key]['code'] = $item->code;
            $stockTransfer[$key]['fromWarehouse_name'] = $fromWarehouse->warehouse_name;
            $stockTransfer[$key]['fromWarehouse_type'] = $fromWarehouse->warehouse_type;
            $stockTransfer[$key]['toWarehouse_name'] = $toWarehouse->warehouse_name;
            $stockTransfer[$key]['toWarehouse_type'] = $toWarehouse->warehouse_type;
            $stockTransfer[$key]['start_transfer_date'] = $item->start_transfer_date;
            $stockTransfer[$key]['finish_transfer_date'] = $item->finish_transfer_date;
            $stockTransfer[$key]['status'] = $item->status;
            $stockTransfer[$key]['id'] = $item->id;
            $stockTransfer[$key]['status_text'] = $this->stockTransferStatus->where('status_id',$item->status)->get();

        }
        if ($truckId == $id){
            return response()->json($stockTransfer);
        }else{
            return response()->json([]);
        }
    }

    public function truckOrderTransaction($id): \Illuminate\Http\JsonResponse
    {
        $truckId = '';
        $order = [];
        $orders = Order::where('isCanceled',0)->orderBy('id','DESC')->get();
        foreach ($orders as $key => $item) {
            $fromWarehouse = Warehouse::select('warehouse_name','warehouse_type')->where('id',$item->from_warehouse_id)->first();
            $customer = Customer::select('customer_name','customer_surname','customer_email','customer_phone')->where('id',$item->customer_id)->first();
            $process = json_decode($item->order_transfer_process);
            foreach ($process as $value){
                if ($value->truck == $id){
                    $truck = Truck::with('supplier')->where('id',$id)->first();
                    $truckId = $truck->id;
                    $order[$key]['supplier_name'] = $truck->supplier->supplier_name;
                    $order[$key]['supplier_surname'] = $truck->supplier->supplier_surname;
                }
            }
            $order[$key]['code'] = $item->code;
            $order[$key]['fromWarehouse_name'] = $fromWarehouse->warehouse_name;
            $order[$key]['fromWarehouse_type'] = $fromWarehouse->warehouse_type;
            $order[$key]['customer_name'] = $customer->customer_name;
            $order[$key]['customer_surname'] = $customer->customer_surname;
            $order[$key]['customer_email'] = $customer->customer_email;
            $order[$key]['customer_phone'] = $customer->customer_phone;
            $order[$key]['start_transfer_date'] = $item->start_transfer_date;
            $order[$key]['finish_transfer_date'] = $item->finish_transfer_date;
            $order[$key]['status'] = $item->status;
            $order[$key]['id'] = $item->id;
            $order[$key]['status_text'] = $this->orderStatus->where('status_id',$item->status)->get();

        }
        if ($truckId == $id){
            return response()->json($order);
        }else{
            return response()->json([]);
        }
    }

}
