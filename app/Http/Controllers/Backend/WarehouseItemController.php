<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Collection;
use App\Models\Product;
use App\Models\SubCategory;
use App\Models\Unit;
use App\Models\User;
use App\Models\Variant;
use App\Models\Warehouse;
use App\Models\WarehouseItem;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WarehouseItemController extends Controller
{
    public function subWarehouseList()
    {
        $subWarehouse = Warehouse::where('warehouse_type','!=',1)->get();
        return response()->json($subWarehouse);
    }

//    public function getWarehouseItems(): JsonResponse
//    {
//        $user = User::with('warehouse')->with('role')->find(auth()->user()->getAuthIdentifier());
//        $products = [];
//        if ($user->warehouse_id !== null && $user->role->role === 'ROLE_WAREHOUSE'){
//            $warehouseItems = WarehouseItem::where('warehouse_id', $user->warehouse_id)->get();
//            foreach ($warehouseItems as $item) {
//                if ($item->variant_id == null) {
//                    $product = DB::table('warehouse_item')
//                        ->select('warehouse_item.product_quantity','warehouse_item.status', 'warehouse_item.product_id', 'warehouse_item.warehouse_id',
//                            'warehouse_item.id', 'product.product_name', 'product.product_image', 'product.product_code','unit.unit_name',
//                            'unit.unit_symbol')
//                        ->join('product', 'warehouse_item.product_id', '=', 'product.id')
//                        ->join('unit', 'product.unit_id', '=', 'unit.id')
//                        ->where('warehouse_item.warehouse_id',$user->warehouse_id)
//                        ->where('product.id',$item->product_id)
//                        ->where('warehouse_item.variant_id','=',null)
//                        ->where('warehouse_item.status','=',1)
//                        ->first();
//                    $products[] = $product;
//                }
//            }
//        }
//        return response()->json($products);
//    }

//    public function showWarehouseItem($id): JsonResponse
//    {
//        $warehouseItem = WarehouseItem::find($id);
//        $product = Product::with('category')->with('subcategory')->with('collection')->find($warehouseItem->product_id);
//        return response()->json($product);
//    }

    public function getWarehouseItemVariants(): JsonResponse
    {
        $user = User::with('warehouse')->with('role')->find(auth()->user()->getAuthIdentifier());
        $variants = [];
        if ($user->warehouse_id !== null && $user->role->role === 'ROLE_WAREHOUSE'){
            $warehouseItems = WarehouseItem::where('warehouse_id', $user->warehouse_id)->get();
            foreach ($warehouseItems as $item) {
                if ($item->variant_id != null) {
                    $variant = DB::table('warehouse_item')
                        ->select('warehouse_item.product_quantity as variant_quantity','warehouse_item.status', 'warehouse_item.product_id', 'warehouse_item.warehouse_id',
                            'warehouse_item.variant_id as vid','warehouse_item.id', 'product.product_name', 'product.product_image', 'product.product_code',
                            'product.category_id', 'product.subcategory_id','product.warehouse_id','product.supplier_id','product.product_tax','product.product_status',
                            'variant.size_id','variant.color_id','variant.sku_no','variant.variant_image','variant.variant_weight','variant.variant_capacity',
                            'variant.variant_tax','variant.variant_selling_price','variant.variant_discount_price','unit.unit_name','unit.unit_symbol')
                        ->join('product', 'warehouse_item.product_id', '=', 'product.id')
                        ->join('categories', 'product.category_id', '=', 'categories.id')
                        ->join('subcategories', 'product.subcategory_id', '=', 'subcategories.id')
                        ->join('variant', 'warehouse_item.variant_id', '=', 'variant.id')
                        ->join('unit','variant.unit_id','=','unit.id')
                        ->where('warehouse_item.warehouse_id',$user->warehouse_id)
                        ->where('product.id', $item->product_id)
                        ->where('product.product_status','=',1)
                        ->where('warehouse_item.variant_id', $item->variant_id)
                        ->where('warehouse_item.status', '=',1)
                        ->first();
                    $variants[] = $variant;
                }
            }
        }

        return response()->json($variants);
    }

    public function showWarehouseItemVariant($id): JsonResponse
    {
        $warehouseItem = WarehouseItem::find($id);
        $variant = Variant::with('size')->with('color')->with('unit')->with('product')->find($warehouseItem->product_id);
        return response()->json($variant);
    }






//    public function selectWarehouseItems($id)
//    {
//        $warehouseItems = WarehouseItem::where('warehouse_id', $id)->get();
//        $products = [];
//        foreach ($warehouseItems as $item) {
//            if ($item->variant_id == null) {
//                $products[] = DB::table('warehouse_item')
//                    ->select('warehouse_item.product_quantity','warehouse_item.status', 'warehouse_item.product_id', 'warehouse_item.warehouse_id',
//                        'warehouse_item.id', 'product.product_name', 'product.product_image', 'product.product_code','unit.unit_name',
//                        'unit.unit_symbol')
//                    ->join('product', 'warehouse_item.product_id', '=', 'product.id')
//                    ->join('unit', 'product.unit_id', '=', 'unit.id')
//                    ->where('warehouse_item.warehouse_id',$id)
//                    ->where('product.id',$item->product_id)
//                    ->where('warehouse_item.variant_id','=',null)
//                    ->where('warehouse_item.status','=',1)
//                    ->first();
//            }
//        }
//        return response()->json($products);
//    }

    public function selectWarehouseItemVariants($id)
    {
        $warehouseItems = WarehouseItem::where('warehouse_id', $id)->get();
        $variant = [];
        foreach ($warehouseItems as $item) {
            if ($item->variant_id != null) {
                $variant[] = DB::table('warehouse_item')
                    ->select('warehouse_item.product_quantity as variant_quantity','warehouse_item.status', 'warehouse_item.product_id', 'warehouse_item.warehouse_id',
                        'warehouse_item.variant_id as vid','warehouse_item.id', 'product.product_name', 'product.product_image', 'product.product_code',
                        'variant.size_id','variant.color_id', 'variant.sku_no','variant.variant_image','unit.unit_name','unit.unit_symbol')
                    ->join('product', 'warehouse_item.product_id', '=', 'product.id')
                    ->join('variant', 'warehouse_item.variant_id', '=', 'variant.id')
                    ->join('unit','variant.unit_id','=','unit.id')
                    ->where('warehouse_item.warehouse_id',$id)
                    ->where('product.id', $item->product_id)
                    ->where('warehouse_item.variant_id', $item->variant_id)
                    ->where('warehouse_item.status', '=',1)
                    ->first();
            }
        }
        return response()->json($variant);
    }

//    public function stockControlItem($id): JsonResponse
//    {
//        $warehouseItems = WarehouseItem::where('warehouse_id', $id)->where('product_quantity',0)->get();
//        $products = [];
//        foreach ($warehouseItems as $item) {
//            if ($item->variant_id == null) {
//                $products[] = DB::table('warehouse_item')
//                    ->select('warehouse_item.product_quantity','warehouse_item.status', 'warehouse_item.product_id', 'warehouse_item.warehouse_id',
//                        'warehouse_item.id', 'product.product_name', 'product.product_image', 'product.product_code','unit.unit_name',
//                        'unit.unit_symbol')
//                    ->join('product', 'warehouse_item.product_id', '=', 'product.id')
//                    ->join('unit', 'product.unit_id', '=', 'unit.id')
//                    ->where('warehouse_item.warehouse_id',$id)
//                    ->where('product.id',$item->product_id)
//                    ->where('warehouse_item.variant_id','=',null)
//                    ->where('warehouse_item.status','=',1)
//                    ->first();
//            }
//        }
//        return response()->json($products);
//    }

    public function stockControlVariant($id): JsonResponse
    {
        $warehouseItems = WarehouseItem::where('warehouse_id', $id)->where('product_quantity',0)->get();
        $variant = [];
        foreach ($warehouseItems as $item) {
            if ($item->variant_id != null) {
                $variant[] = DB::table('warehouse_item')
                    ->select('warehouse_item.product_quantity as variant_quantity','warehouse_item.status', 'warehouse_item.product_id', 'warehouse_item.warehouse_id',
                        'warehouse_item.variant_id as vid','warehouse_item.id', 'product.product_name', 'product.product_image', 'product.product_code',
                        'variant.size_id','variant.color_id', 'variant.sku_no','variant.variant_image','unit.unit_name','unit.unit_symbol')
                    ->join('product', 'warehouse_item.product_id', '=', 'product.id')
                    ->join('variant', 'warehouse_item.variant_id', '=', 'variant.id')
                    ->join('unit','variant.unit_id','=','unit.id')
                    ->where('warehouse_item.warehouse_id',$id)
                    ->where('product.id', $item->product_id)
                    ->where('warehouse_item.variant_id', $item->variant_id)
                    ->where('warehouse_item.status', '=',1)
                    ->first();
            }
        }
        return response()->json($variant);
    }



//    public function getStockControlItem(): JsonResponse
//    {
//        $user = User::with('warehouse')->with('role')->find(auth()->user()->getAuthIdentifier());
//        $products = [];
//        if ($user->warehouse_id !== null && $user->role->role === 'ROLE_WAREHOUSE'){
//            $warehouseItems = WarehouseItem::where('warehouse_id', $user->warehouse_id)->where('product_quantity',0)->get();
//            foreach ($warehouseItems as $item) {
//                if ($item->variant_id == null) {
//                    $products[] = DB::table('warehouse_item')
//                        ->select('warehouse_item.product_quantity','warehouse_item.status', 'warehouse_item.product_id', 'warehouse_item.warehouse_id',
//                            'warehouse_item.id', 'product.product_name', 'product.product_image', 'product.product_code','unit.unit_name',
//                            'unit.unit_symbol')
//                        ->join('product', 'warehouse_item.product_id', '=', 'product.id')
//                        ->join('unit', 'product.unit_id', '=', 'unit.id')
//                        ->where('warehouse_item.warehouse_id',$user->warehouse_id)
//                        ->where('product.id',$item->product_id)
//                        ->where('warehouse_item.variant_id','=',null)
//                        ->where('warehouse_item.status','=',1)
//                        ->first();
//                }
//            }
//        }
//        return response()->json($products);
//    }

    public function getStockControlVariant(): JsonResponse
    {
        $user = User::with('warehouse')->with('role')->find(auth()->user()->getAuthIdentifier());
        $variant = [];
        if ($user->warehouse_id !== null && $user->role->role === 'ROLE_WAREHOUSE'){
            $warehouseItems = WarehouseItem::where('warehouse_id', $user->warehouse_id)->where('product_quantity',0)->get();
            foreach ($warehouseItems as $item) {
                if ($item->variant_id != null) {
                    $variant[] = DB::table('warehouse_item')
                        ->select('warehouse_item.product_quantity as variant_quantity','warehouse_item.status', 'warehouse_item.product_id', 'warehouse_item.warehouse_id',
                            'warehouse_item.variant_id as vid','warehouse_item.id', 'product.product_name', 'product.product_image', 'product.product_code',
                            'variant.size_id','variant.color_id', 'variant.sku_no','variant.variant_image','unit.unit_name','unit.unit_symbol')
                        ->join('product', 'warehouse_item.product_id', '=', 'product.id')
                        ->join('variant', 'warehouse_item.variant_id', '=', 'variant.id')
                        ->join('unit','variant.unit_id','=','unit.id')
                        ->where('warehouse_item.warehouse_id',$user->warehouse_id)
                        ->where('product.id', $item->product_id)
                        ->where('warehouse_item.variant_id', $item->variant_id)
                        ->where('warehouse_item.status', '=',1)
                        ->first();
                }
            }
        }

        return response()->json($variant);
    }



}
