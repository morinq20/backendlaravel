<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\Language;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FaqLanguageController extends Controller
{
    public function show(Request $request,$id): \Illuminate\Http\JsonResponse
    {
        $faq = Faq::where('id',$id)->first();
        return response()->json(json_decode($faq->lang));
    }

    /**
     * @throws Exception
     */
    public function store(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            $faq = Faq::where('id',$id)->first();
            $arr = [];
            foreach ($requestAll as $key => $item){
                $lang = Language::find($item['language_id']);
                $arr[$lang->code]['language_id'] = $item['language_id'];
                $arr[$lang->code]['language_code'] = $lang->code;
                $arr[$lang->code]['faq_title'] = $item['faq_title'];
                $arr[$lang->code]['faq_description'] = strip_tags($item['faq_description']);
            }
            if ($faq->lang !== null){
                $lang = json_decode($faq->lang);
                foreach ($lang as $key => $item){
                    $arr[$key] = $item;
                }
            }
            $faq->lang = json_encode($arr);
            $faq->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }

    }

    /**
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            $faq = Faq::where('id',$id)->first();
            $arr = [];
            foreach ($requestAll as $key => $item){
                $lang = Language::find($item['language_id']);
                $arr[$lang->code]['language_id'] = $item['language_id'];
                $arr[$lang->code]['language_code'] = $lang->code;
                $arr[$lang->code]['faq_title'] = $item['faq_title'];
                $arr[$lang->code]['faq_description'] = strip_tags($item['faq_description']);
            }
            $faq->lang = json_encode($arr);
            $faq->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function delete($id,$code)
    {
        DB::beginTransaction();
        try {
            $faq = Faq::where('id',$id)->first();
            $lang = json_decode($faq->lang);
            $arr = [];
            foreach ($lang as $key => $item){
                if ($key !== $code){
                    $arr[$key] = $item;
                }
            }
            $faq->lang = json_encode($arr);
            $faq->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
