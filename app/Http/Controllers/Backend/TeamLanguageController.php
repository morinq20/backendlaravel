<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Team;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeamLanguageController extends Controller
{
    public function show(Request $request,$id): \Illuminate\Http\JsonResponse
    {
        $team = Team::where('id',$id)->first();
        return response()->json(json_decode($team->lang));
    }

    /**
     * @throws Exception
     */
    public function store(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            $team = Team::where('id',$id)->first();
            $arr = [];
            foreach ($requestAll as $key => $item){
                $lang = Language::find($item['language_id']);
                $arr[$lang->code]['language_id'] = $item['language_id'];
                $arr[$lang->code]['language_code'] = $lang->code;
                $arr[$lang->code]['team_position'] = $item['team_position'];
                $arr[$lang->code]['team_practice_area'] = $item['team_practice_area'];
                $arr[$lang->code]['team_experience'] = $item['team_experience'];
                $arr[$lang->code]['team_job'] = $item['team_job'];
                $arr[$lang->code]['team_description'] = strip_tags($item['team_description']);
            }
            if ($team->lang !== null){
                $lang = json_decode($team->lang);
                foreach ($lang as $key => $item){
                    $arr[$key] = $item;
                }
            }
            $team->lang = json_encode($arr);
            $team->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }

    }

    /**
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            $team = Team::where('id',$id)->first();
            $arr = [];
            foreach ($requestAll as $key => $item){
                $lang = Language::find($item['language_id']);
                $arr[$lang->code]['language_id'] = $item['language_id'];
                $arr[$lang->code]['language_code'] = $lang->code;
                $arr[$lang->code]['team_position'] = $item['team_position'];
                $arr[$lang->code]['team_practice_area'] = $item['team_practice_area'];
                $arr[$lang->code]['team_experience'] = $item['team_experience'];
                $arr[$lang->code]['team_job'] = $item['team_job'];
                $arr[$lang->code]['team_description'] = strip_tags($item['team_description']);
            }
            $team->lang = json_encode($arr);
            $team->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function delete($id,$code)
    {
        DB::beginTransaction();
        try {
            $team = Team::where('id',$id)->first();
            $lang = json_decode($team->lang);
            $arr = [];
            foreach ($lang as $key => $item){
                if ($key !== $code){
                    $arr[$key] = $item;
                }
            }
            $team->lang = json_encode($arr);
            $team->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
