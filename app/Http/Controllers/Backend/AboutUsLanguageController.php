<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\AboutUs;
use App\Models\Language;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AboutUsLanguageController extends Controller
{
    public function show(Request $request,$id): \Illuminate\Http\JsonResponse
    {
        $aboutUs = AboutUs::where('id',$id)->first();
        return response()->json(json_decode($aboutUs->lang));
    }

    /**
     * @throws Exception
     */
    public function store(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            $aboutUs = AboutUs::where('id',$id)->first();
            $arr = [];
            foreach ($requestAll as $key => $item){
                $lang = Language::find($item['language_id']);
                $arr[$lang->code]['language_id'] = $item['language_id'];
                $arr[$lang->code]['language_code'] = $lang->code;
                $arr[$lang->code]['about_us_title'] = $item['about_us_title'];
                $arr[$lang->code]['about_us_description'] = strip_tags($item['about_us_description']);
            }
            if ($aboutUs->lang !== null){
                $lang = json_decode($aboutUs->lang);
                foreach ($lang as $key => $item){
                    $arr[$key] = $item;
                }
            }
            $aboutUs->lang = json_encode($arr);
            $aboutUs->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }

    }

    /**
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            $aboutUs = AboutUs::where('id',$id)->first();
            $arr = [];
            foreach ($requestAll as $key => $item){
                $lang = Language::find($item['language_id']);
                $arr[$lang->code]['language_id'] = $item['language_id'];
                $arr[$lang->code]['language_code'] = $lang->code;
                $arr[$lang->code]['about_us_title'] = $item['about_us_title'];
                $arr[$lang->code]['about_us_description'] = strip_tags($item['about_us_description']);
            }
            $aboutUs->lang = json_encode($arr);
            $aboutUs->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function delete($id,$code)
    {
        DB::beginTransaction();
        try {
            $aboutUs = AboutUs::where('id',$id)->first();
            $lang = json_decode($aboutUs->lang);
            $arr = [];
            foreach ($lang as $key => $item){
                if ($key !== $code){
                    $arr[$key] = $item;
                }
            }
            $aboutUs->lang = json_encode($arr);
            $aboutUs->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
