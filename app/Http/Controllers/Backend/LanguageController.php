<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\FileManager\FileSaver;
use App\Http\Services\FileManager\LanguageFileConfig;
use App\Http\Traits\FolderControlTrait;
use App\Models\Language;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class LanguageController extends Controller
{
    use FolderControlTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $lang = Language::orderBy('id','DESC')->get();
        return response()->json($lang);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $lang = new Language();
            $lang->name = $request->name;
            $lang->code = $request->code;
            if (isset($request->flag)) {
                $path = 'backend/uploads/language/';
                $this->folderIfNotExists($path);
                $file = FileSaver::getInstance();
                $config = LanguageFileConfig::createConfig($request->flag,$path,20,20);
                $file_save = $file->send($config);
                if (isset($file_save))
                {
                    $lang->flag = $config->createFile();
                }
                $lang->save();
            } else {
                $lang->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $lang = Language::find($id);
        return response()->json($lang);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $newPhoto = $request->new_flag;
            $oldPhoto = $request->flag;
            $lang = Language::find($id);
            $lang->name = $request->name;
            $lang->code = $request->code;
            if (isset($newPhoto)) {
                if ($oldPhoto != null) {
                    unlink($oldPhoto);
                    $path = 'backend/uploads/language/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = LanguageFileConfig::createConfig($newPhoto,$path,20,20);
                    $file_save = $file->send($config);
                    $lang->flag = $config->createFile();
                    $lang->save();
                } else {
                    $path = 'backend/uploads/language/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = LanguageFileConfig::createConfig($newPhoto,$path,20,20);
                    $file_save = $file->send($config);
                    $lang->flag = $config->createFile();
                    $lang->save();
                }
            } else {
                $lang->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $lang = Language::find($id);
            $lang->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
