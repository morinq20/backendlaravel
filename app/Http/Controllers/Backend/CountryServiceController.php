<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Cities;
use Illuminate\Http\Request;

class CountryServiceController extends Controller
{
    public function index(Request $request, $id)
    {
        $cities = Cities::where('country_id',$id)->get();
        return response()->json($cities);
    }
}
