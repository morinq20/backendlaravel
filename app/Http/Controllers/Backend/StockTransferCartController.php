<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Color;
use App\Models\Product;
use App\Models\Size;
use App\Models\StockTransferCart;
use App\Models\Truck;
use App\Models\Unit;
use App\Models\Variant;
use App\Models\Warehouse;
use App\Models\WarehouseItem;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;


class StockTransferCartController extends Controller
{
    public function addToStockTransferCart($variant_id,$warehouse_id,Request $request)
    {
        $findWarehouse = Warehouse::where('id',$warehouse_id)->first();
        if ($findWarehouse->warehouse_type == 1) {
            $variant = Variant::find($variant_id);
            $color = Color::find($variant->color_id);
            $size = Size::find($variant->size_id);
            $product = Product::find($variant->product_id);
            $unit = Unit::find($variant->unit_id);
            $control = StockTransferCart::where('user_id',auth()->user()->getAuthIdentifier())->where('product_id', $variant->product_id)->where('variant_id',$variant_id)->first();
            if($variant->variant_quantity < $request->query('product_quantity')){
                return response()->json('no found quantity');
            }
            if ($control) {

                if ($control->product_quantity >= $variant->variant_quantity) {
                    return response('unsuccessful');
                } else {
                    StockTransferCart::where('user_id',auth()->user()->getAuthIdentifier())->where('product_id', $variant->product_id)->where('variant_id',$variant_id)->update(['product_quantity'=>$request->query('product_quantity')]);
                    return response('successful');
                }
            } else {
                $stockTransferCart = new StockTransferCart();
                $stockTransferCart->user_id = auth()->user()->getAuthIdentifier();
                $stockTransferCart->product_id = $variant->product_id;
                $stockTransferCart->variant_id = $variant_id;
                $stockTransferCart->variant_sku_no = $variant->sku_no;
                $stockTransferCart->color_name = $color->color_name;
                $stockTransferCart->color_code = $color->color_code;
                $stockTransferCart->size_name = $size->size_name;
                $stockTransferCart->product_name = $product->product_name;
                $stockTransferCart->product_quantity = $request->query('product_quantity');
                $stockTransferCart->unit_id = $variant->unit_id;
                $stockTransferCart->unit_name = $unit->unit_name;
                $stockTransferCart->save();
            }
        } else {
            $variant = Variant::find($variant_id);
            $color = Color::find($variant->color_id);
            $size = Size::find($variant->size_id);
            $product = Product::find($variant->product_id);
            $unit = Unit::find($variant->unit_id);
            if($variant->variant_quantity < $request->query('product_quantity')){
                return response()->json('no found quantity');
            }
            $findWarehouseItemVariant = WarehouseItem::where('warehouse_id',$warehouse_id)->where('product_id',$variant->product_id)->where('variant_id',$variant_id)->first();
            $control = StockTransferCart::where('user_id',auth()->user()->getAuthIdentifier())->where('product_id', $variant->product_id)->where('variant_id',$variant_id)->first();
            if ($control) {

                if ($control->product_quantity >= $findWarehouseItemVariant->product_quantity) {
                    return response('unsuccessful');
                } else {
                    StockTransferCart::where('user_id',auth()->user()->getAuthIdentifier())->where('product_id', $variant->product_id)->where('variant_id',$variant_id)->update(['product_quantity'=>$request->query('product_quantity')]);
                    return response('successful');
                }
            } else {
                $stockTransferCart = new StockTransferCart();
                $stockTransferCart->user_id = auth()->user()->getAuthIdentifier();
                $stockTransferCart->product_id = $variant->product_id;
                $stockTransferCart->variant_id = $variant_id;
                $stockTransferCart->variant_sku_no = $variant->sku_no;
                $stockTransferCart->color_name = $color->color_name;
                $stockTransferCart->color_code = $color->color_code;
                $stockTransferCart->size_name = $size->size_name;
                $stockTransferCart->product_name = $product->product_name;
                $stockTransferCart->product_quantity = $request->query('product_quantity');
                $stockTransferCart->unit_id = $variant->unit_id;
                $stockTransferCart->unit_name = $unit->unit_name;
                $stockTransferCart->save();
            }
        }


    }

    public function stockTransferCart()
    {
        $stockTransferCart = DB::table('stock_transfer_cart')
            ->select('product.id as pid','product.*','stock_transfer_cart.id as cartId','stock_transfer_cart.*','product.product_quantity as qty','variant.id as vid', 'variant.*','unit.unit_name')
            ->where('user_id',auth()->user()->getAuthIdentifier())
            ->join('product','stock_transfer_cart.product_id','=','product.id')
            ->join('variant','stock_transfer_cart.variant_id','=','variant.id')
            ->join('unit','stock_transfer_cart.unit_id','=','unit.id')
            ->orderBy('stock_transfer_cart.id', 'DESC')->get();
        return response()->json($stockTransferCart);
    }

    public function removeStockTransferCart($id)
    {
        $stockTransferCart = StockTransferCart::where('user_id',auth()->user()->getAuthIdentifier())->where('id',$id)->first();
        $stockTransferCart->delete();
        return response('done');
    }

    /**
     * @throws \Exception
     */
    public function incrementStockTransferCart(Request $request, $id, $warehouse_id)
    {
        $findWarehouse = Warehouse::where('id',$warehouse_id)->first();
        $find = StockTransferCart::where(['user_id' => auth()->user()->getAuthIdentifier(),'id' => $id])->first();
        $findProductVariant = Variant::where('id',$find->variant_id)->first();
        $findWarehouseItemVariant = WarehouseItem::where('warehouse_id',$warehouse_id)->where('product_id',$find->product_id)->where('variant_id',$find->variant_id)->first();
        if ($findWarehouse->warehouse_type == 1) {
            if ($request->query('variant_quantity') > $findProductVariant->variant_quantity) {
                return response('unsuccessful');
            } else {
                StockTransferCart::where('user_id',auth()->user()->getAuthIdentifier())->where('id', $id)->update(['product_quantity'=>$request->query('variant_quantity')]);
                return response('successful');
            }
        } else {
            if ($request->query('variant_quantity') > $findWarehouseItemVariant->product_quantity) {
                return response('unsuccessful');
            } else {
                StockTransferCart::where('user_id',auth()->user()->getAuthIdentifier())->where('id', $id)->update(['product_quantity'=>$request->query('variant_quantity')]);
                return response('successful');
            }
        }

    }

    public function decrementStockTransferCart($id)
    {
        StockTransferCart::where('user_id',auth()->user()->getAuthIdentifier())->where('id', $id)->decrement('product_quantity');
        return response('done');
    }

    public function outOfStock()
    {
        $outOfStock = Product::with('category')->with('subcategory')->where('product_quantity', '=', 0)->orderBy('id', 'DESC')->get();
        return response()->json($outOfStock);
    }

    public function selectFromWarehouse($id)
    {
        $selectFromWarehouse = Warehouse::find($id);
        $removeArrayInSelectedFromWarehouse = Warehouse::where('id', '!=', $id)->get();
        return response()->json($removeArrayInSelectedFromWarehouse);
    }

    public function fromWarehouseChange($id)
    {

//        if ($id != null || $id == '') {
//
//        } else {
//            $this->deleteAllStockTransferCart();
//        }
        $findFromWarehouse = Warehouse::select('warehouse_latitude','warehouse_longitude')->where('id',$id)->get();
        return response()->json($findFromWarehouse);
    }

    public function deleteAllStockTransferCart(): \Illuminate\Http\JsonResponse
    {
        $stockTransferCart = StockTransferCart::where('user_id',auth()->user()->getAuthIdentifier())->get();
        foreach ($stockTransferCart as $cart) {
            $cart->delete();
        }
        return response()->json('success');
    }

    public function toWarehouseChange($id)
    {
        $findToWarehouse = Warehouse::select('warehouse_latitude','warehouse_longitude')->where('id',$id)->get();
        return response()->json($findToWarehouse);
    }

//    public function selectFromWarehouseItem($id)
//    {
//        $warehouse = Warehouse::find($id);
//        if ($warehouse->warehouse_type == 1) {
//            $warehouseItems = DB::table('product')->select('product.id', 'product.product_name', 'product.product_weight', 'product.product_capacity',
//                'product.product_width', 'product.product_length', 'product.product_height', 'product.product_image', 'product.product_code','unit.unit_name',
//                'unit.unit_symbol','product.product_quantity')
//                ->join('unit', 'product.unit_id', '=', 'unit.id')
//                ->where('product.warehouse_id', $id)->get();
//            return response()->json($warehouseItems);
//        } else {
//            $warehouseItems = WarehouseItem::where('warehouse_id', $id)->get();
//            $products = [];
//            foreach ($warehouseItems as $item) {
//                if ($item->variant_id == null) {
//                    $products[] = DB::table('warehouse_item')
//                        ->select('warehouse_item.product_quantity','warehouse_item.status', 'warehouse_item.product_id', 'warehouse_item.warehouse_id',
//                            'product.id', 'product.product_name', 'product.product_image', 'product.product_code','unit.unit_name',
//                        'unit.unit_symbol')
//                        ->join('product', 'warehouse_item.product_id', '=', 'product.id')
//                        ->join('unit', 'product.unit_id', '=', 'unit.id')
//                        ->where('warehouse_item.warehouse_id',$warehouse->id)
//                        ->where('product.id',$item->product_id)
//                        ->where('warehouse_item.variant_id','=',null)
//                        ->where('warehouse_item.status','=',1)
//                        ->first();
//                }
//            }
//            return response()->json($products);
//        }
//    }

    // Select Warehouse Item Product Variant
    public function selectFromWarehouseItemVariant($id)
    {
        $warehouse = Warehouse::find($id);
        if ($warehouse->warehouse_type == 1) {
            $variant = DB::table('variant')->select('variant.id as vid','variant.product_id','product.product_name','product.product_code',
                'product.product_quantity','product.warehouse_id','product.product_image','variant.size_id','variant.color_id', 'variant.variant_weight',
                'variant.variant_capacity', 'variant.variant_tax','variant.variant_selling_price','variant.variant_discount_price','product.product_tax',
                'variant.sku_no','variant.variant_image','variant.variant_quantity','size.size_name','color.color_name','color.color_code',
                'unit.unit_name','unit.unit_symbol','product.product_status')
                ->join('color','variant.color_id','=','color.id')
                ->join('size','variant.size_id','=','size.id')
                ->join('product','variant.product_id','=','product.id')
                ->join('unit','variant.unit_id','=','unit.id')
                ->where('product.product_status', '=',1)
                ->get();
            return response()->json($variant);
        } else {
            $warehouseItems = WarehouseItem::where('warehouse_id', $id)->get();
            $variant = [];
            foreach ($warehouseItems as $item) {
                if ($item->variant_id != null) {
                    $variant[] = DB::table('warehouse_item')
                        ->select('warehouse_item.product_quantity as variant_quantity','warehouse_item.status', 'warehouse_item.product_id', 'warehouse_item.warehouse_id',
                            'warehouse_item.variant_id as vid','warehouse_item.id', 'product.product_name', 'product.product_image', 'product.product_code',
                            'variant.size_id','variant.color_id', 'variant.sku_no','variant.variant_image','unit.unit_name','unit.unit_symbol', 'variant.variant_weight',
                            'variant.variant_capacity', 'variant.variant_tax','variant.variant_selling_price','variant.variant_discount_price','product.product_tax','product.product_status')
                        ->join('product', 'warehouse_item.product_id', '=', 'product.id')
                        ->join('variant', 'warehouse_item.variant_id', '=', 'variant.id')
                        ->join('unit','variant.unit_id','=','unit.id')
                        ->join('color','variant.color_id','=','color.id')
                        ->join('size','variant.size_id','=','size.id')
                        ->where('warehouse_item.warehouse_id',$warehouse->id)
                        ->where('product.id', $item->product_id)
                        ->where('warehouse_item.variant_id', $item->variant_id)
                        ->where('warehouse_item.status', '=',1)
                        ->first();
                }
            }
            return response()->json($variant);
        }
    }

    public function getTruckBySupplierChange($id){
        $supplierTruck = Truck::where('supplier_id',$id)->get();
        return response()->json($supplierTruck);
    }


}
