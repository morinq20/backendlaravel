<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\SliderRequest;
use App\Http\Services\FileManager\FileSaver;
use App\Http\Services\FileManager\SliderFileConfig;
use App\Http\Traits\FolderControlTrait;
use App\Models\ImageResize;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SliderController extends Controller
{
    use FolderControlTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
//        $sliders = Slider::all()->sortBy('slider_seq');
        $sliders = Slider::orderBy('id','DESC')->get();
        return response()->json($sliders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $slider = new Slider();
            $slider->slider_title = $request->slider_title;
            $slider->slider_status = $request->slider_status;
            $slider->slider_description = strip_tags($request->slider_description);
            $slider->slider_seq = self::makeSeq();
            if (isset($request->slider_image)) {
                $path = 'backend/uploads/slider/';
                $this->folderIfNotExists($path);
                $file = FileSaver::getInstance();
                $config = SliderFileConfig::createConfig($request->slider_image,$path,$imageResize->slider_image_width,$imageResize->slider_image_height);
                $file_save = $file->send($config);
                if (isset($file_save))
                {
                    $slider->slider_image = $config->createFile();
                }
                $slider->save();
            } else {
                $slider->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $slider = Slider::find($id);
        return response()->json($slider);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $newPhoto = $request->new_slider_image;
            $oldPhoto = $request->slider_image;
            $slider = Slider::find($id);
            $slider->slider_title = $request->slider_title;
            $slider->slider_status = $request->slider_status;
            $slider->slider_description = strip_tags($request->slider_description);
//            $slider->slider_seq = $request->slider_seq;
            if (isset($newPhoto)) {
                if ($oldPhoto != null) {
                    unlink($oldPhoto);
                    $path = 'backend/uploads/slider/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = SliderFileConfig::createConfig($newPhoto,$path,$imageResize->slider_image_width,$imageResize->slider_image_height);
                    $file_save = $file->send($config);
                    $slider->slider_image = $config->createFile();
                    $slider->save();
                } else {
                    $path = 'backend/uploads/slider/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = SliderFileConfig::createConfig($newPhoto,$path,$imageResize->slider_image_width,$imageResize->slider_image_height);
                    $file_save = $file->send($config);
                    $slider->slider_image = $config->createFile();
                    $slider->save();
                }
            } else {
                $slider->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $slider = Slider::find($id);
            $photo = $slider->slider_image;
            if (isset($photo)) {
                unlink($photo);
                $slider->delete();
            } else {
                $slider->delete();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    // Slider Sortable

    /**
     * @throws \Exception
     */
    public function sliderSortable(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->item as $key => $value) {
                $slider = Slider::find(intval($value));
                $slider->slider_seq = intval($key);
                $slider->save();
            }
            echo true;
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    private static function makeSeq(): int|string
    {
        $control = Slider::latest('id')->first();
        if (!$control){
            return "1";
        }else{
            return strval($control->team_seq + 1);
        }
    }
}

