<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ShippingCompanyDistance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShippingCompanyDistanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
//        $shippingCompanyDeci = ShippingCompanyDeci::with('shippingCompany')->orderBy('id','DESC')->get();
        $shippingCompanyDistance = DB::table('shipping_company_distance')
            ->select('shipping_company_distance.*','shipping_company.shipping_company_name','shipping_company.shipping_company_image')
            ->join('shipping_company','shipping_company_distance.shipping_company_id','=','shipping_company.id')
            ->orderBy('shipping_company_distance.id','DESC')->get();
        return response()->json($shippingCompanyDistance);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $shippingCompanyDistance = new ShippingCompanyDistance();
            $shippingCompanyDistance->shipping_company_id = $request->shipping_company_id;
            $shippingCompanyDistance->distance = $request->distance;
            $shippingCompanyDistance->distance_price = $request->distance_price;
            $shippingCompanyDistance->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $shippingCompanyDistance = ShippingCompanyDistance::find($id);
        return response()->json($shippingCompanyDistance);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $shippingCompanyDistance = ShippingCompanyDistance::find($id);
            $shippingCompanyDistance->shipping_company_id = $request->shipping_company_id;
            $shippingCompanyDistance->distance = $request->distance;
            $shippingCompanyDistance->distance_price = $request->distance_price;
            $shippingCompanyDistance->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $shippingCompanyDistance = ShippingCompanyDistance::find($id);
            $shippingCompanyDistance->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }
}
