<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Color;
use App\Models\OrderCart;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\Size;
use App\Models\StockTransferCart;
use App\Models\Truck;
use App\Models\Unit;
use App\Models\Variant;
use App\Models\Warehouse;
use App\Models\WarehouseItem;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;


class OrderCartController extends Controller
{
    /**
     * @throws \Exception
     */
    public function addOrderCart($variant_id, $warehouse_id, Request $request)
    {
        DB::beginTransaction();
        try {
            $findWarehouse = Warehouse::where('id',$warehouse_id)->first();
            if ($findWarehouse->warehouse_type == 1) {
                $variant = Variant::find($variant_id);
                $color = Color::find($variant->color_id);
                $size = Size::find($variant->size_id);
                $unit = Unit::find($variant->unit_id);
                $product = Product::find($variant->product_id);
                $control = OrderCart::where('user_id',auth()->user()->getAuthIdentifier())->where('product_id', $variant->product_id)->where('variant_id',$variant_id)->first();
                if($variant->variant_quantity < $request->query('product_quantity')){
                    return response()->json('no found quantity');
                }
                if ($control) {

                    if ($control->product_quantity >= $variant->variant_quantity) {
                        return response('unsuccessful');
                    } else {
                        OrderCart::where('user_id',auth()->user()->getAuthIdentifier())->where('product_id', $variant->product_id)->where('variant_id',$variant_id)->update(['product_quantity'=>$request->query('product_quantity')]);
                        return response('successful');
                    }
                } else {
                    $orderCart = new OrderCart();
                    $orderCart->user_id = auth()->user()->getAuthIdentifier();
                    $orderCart->product_id = $variant->product_id;
                    $orderCart->variant_id = $variant_id;
                    $orderCart->variant_sku = $variant->sku_no;
                    $orderCart->color_name = $color->color_name;
                    $orderCart->color_code = $color->color_code;
                    $orderCart->size_name = $size->size_name;
                    $orderCart->product_name = $product->product_name;
                    $orderCart->product_quantity = $request->query('product_quantity');
                    $orderCart->unit_id = $variant->unit_id;
                    $orderCart->unit_name = $unit->unit_name;
                    $orderCart->save();
                }
            } else {
                $variant = Variant::find($variant_id);
                $color = Color::find($variant->color_id);
                $size = Size::find($variant->size_id);
                $unit = Unit::find($variant->unit_id);
                $product = Product::find($variant->product_id);
                if($variant->variant_quantity < $request->query('product_quantity')){
                    return response()->json('no found quantity');
                }
                $findWarehouseItemVariant = OrderItem::where('warehouse_id',$warehouse_id)->where('product_id',$variant->product_id)->where('variant_id',$variant_id)->first();
                $control = OrderCart::where('user_id',auth()->user()->getAuthIdentifier())->where('product_id', $variant->product_id)->where('variant_id',$variant_id)->first();
                if ($control) {

                    if ($control->product_quantity >= $findWarehouseItemVariant->product_quantity) {
                        return response('unsuccessful');
                    } else {
                        OrderCart::where('user_id',auth()->user()->getAuthIdentifier())->where('product_id', $variant->product_id)->where('variant_id',$variant_id)->update(['product_quantity'=>$request->query('product_quantity')]);
                        return response('successful');
                    }
                } else {
                    $orderCart = new OrderCart();
                    $orderCart->user_id = auth()->user()->getAuthIdentifier();
                    $orderCart->product_id = $variant->product_id;
                    $orderCart->variant_id = $variant_id;
                    $orderCart->variant_sku = $variant->sku_no;
                    $orderCart->color_name = $color->color_name;
                    $orderCart->color_code = $color->color_code;
                    $orderCart->size_name = $size->size_name;
                    $orderCart->product_name = $product->product_name;
                    $orderCart->product_quantity = $request->query('product_quantity');
                    $orderCart->unit_id = $variant->unit_id;
                    $orderCart->unit_name = $unit->unit_name;
                    $orderCart->save();
                }
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    public function orderCart()
    {
        $ordersCart = DB::table('orders_cart')
            ->select('product.id as pid','product.*','orders_cart.id as cartId','orders_cart.*','product.product_quantity as qty','variant.id as vid', 'variant.*','unit.unit_name')->where('user_id',auth()->user()->getAuthIdentifier())
            ->join('product','orders_cart.product_id','=','product.id')
            ->join('variant','orders_cart.variant_id','=','variant.id')
            ->join('unit','orders_cart.unit_id','=','unit.id')
            ->orderBy('orders_cart.id', 'DESC')->get();
        return response()->json($ordersCart);
    }

    /**
     * @throws \Exception
     */
    public function removeOrderCart($id)
    {
        $orderCart = OrderCart::where('user_id',auth()->user()->getAuthIdentifier())->where('id',$id)->first();
        $orderCart->delete();
        return response('done');
    }

    /**
     * @throws \Exception
     */
    public function incrementOrderCart(Request $request, $id, $warehouse_id)
    {
        $findWarehouse = Warehouse::where('id',$warehouse_id)->first();
        $find = OrderCart::where(['user_id' => auth()->user()->getAuthIdentifier(),'id' => $id])->first();
        $findProductVariant = Variant::where('id',$find->variant_id)->first();
        $findWarehouseItemVariant = WarehouseItem::where('warehouse_id',$warehouse_id)->where('product_id',$find->product_id)->where('variant_id',$find->variant_id)->first();
        if ($findWarehouse->warehouse_type == 1) {
            if ($request->query('variant_quantity') > $findProductVariant->variant_quantity) {
                return response('unsuccessful');
            } else {
                OrderCart::where('user_id',auth()->user()->getAuthIdentifier())->where('id', $id)->update(['product_quantity'=>$request->query('variant_quantity')]);
                return response('successful');
            }
        } else {
            if ($request->query('variant_quantity') > $findWarehouseItemVariant->product_quantity) {
                return response('unsuccessful');
            } else {
                OrderCart::where('user_id',auth()->user()->getAuthIdentifier())->where('id', $id)->update(['product_quantity'=>$request->query('variant_quantity')]);
                return response('successful');
            }
        }
    }

    /**
     * @throws \Exception
     */
    public function decrementOrderCart($id)
    {
        DB::beginTransaction();
        try {
            OrderCart::where('user_id',auth()->user()->getAuthIdentifier())->where('id', $id)->decrement('product_quantity');
            DB::commit();
            return response('done');
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }

    }

    public function outOfStock()
    {
        $outOfStock = Product::with('category')->with('subcategory')->where('product_quantity', '=', 0)->orderBy('id', 'DESC')->get();
        return response()->json($outOfStock);
    }

    public function selectFromWarehouse($id)
    {
        $selectFromWarehouse = Warehouse::find($id);
        $removeArrayInSelectedFromWarehouse = Warehouse::where('id', '!=', $id)->get();
        return response()->json($removeArrayInSelectedFromWarehouse);
    }

    public function fromWarehouseChange($id)
    {

//        if ($id != null || $id == '') {
//
//        } else {
//            $this->deleteAllStockTransferCart();
//        }
        $findFromWarehouse = Warehouse::select('warehouse_latitude','warehouse_longitude')->where('id',$id)->get();
        return response()->json($findFromWarehouse);
    }

    /**
     * @throws \Exception
     */
    public function deleteAllOrderCart(): \Illuminate\Http\JsonResponse
    {
        DB::beginTransaction();
        try {
            $orderCart = OrderCart::where('user_id',auth()->user()->getAuthIdentifier())->get();
            foreach ($orderCart as $cart) {
                $cart->delete();
            }
            DB::commit();
            return response()->json('success');
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    public function toWarehouseChange($id)
    {
        $findToWarehouse = Warehouse::select('warehouse_latitude','warehouse_longitude')->where('id',$id)->get();
        return response()->json($findToWarehouse);
    }

//    public function selectFromWarehouseItem($id)
//    {
//        $warehouse = Warehouse::find($id);
//        if ($warehouse->warehouse_type == 1) {
//            $warehouseItems = DB::table('product')->select('product.id', 'product.product_name', 'product.product_weight', 'product.product_capacity',
//                'product.product_width', 'product.product_length', 'product.product_height', 'product.product_image', 'product.product_code','unit.unit_name',
//                'unit.unit_symbol','product.product_quantity')
//                ->join('unit', 'product.unit_id', '=', 'unit.id')
//                ->where('product.warehouse_id', $id)->get();
//            return response()->json($warehouseItems);
//        } else {
//            $warehouseItems = WarehouseItem::where('warehouse_id', $id)->get();
//            $product = [];
//            foreach ($warehouseItems as $item) {
//                if ($item->variant_id == null) {
//                    $product[] = DB::table('warehouse_item')
//                        ->select('warehouse_item.product_quantity','warehouse_item.status', 'warehouse_item.product_id', 'warehouse_item.warehouse_id',
//                            'product.id', 'product.product_name', 'product.product_image', 'product.product_code','unit.unit_name',
//                            'unit.unit_symbol')
//                        ->join('product', 'warehouse_item.product_id', '=', 'product.id')
//                        ->join('unit', 'product.unit_id', '=', 'unit.id')
//                        ->where('warehouse_item.warehouse_id',$warehouse->id)
//                        ->where('product.id',$item->product_id)
//                        ->where('warehouse_item.variant_id','=',null)
//                        ->where('warehouse_item.status','=',1)
//                        ->first();
//                }
//            }
//            return response()->json($product);
//        }
//    }

    // Select Warehouse Item Product Variant
    public function selectFromWarehouseItemVariant($id)
    {
        $warehouse = Warehouse::find($id);
        if ($warehouse->warehouse_type == 1) {
            $variant = DB::table('variant')->select('variant.id as vid','variant.product_id','product.product_name','product.product_code',
                'product.product_quantity','product.warehouse_id','product.product_image','variant.size_id','variant.color_id', 'variant.variant_weight',
                'variant.variant_capacity', 'variant.variant_tax','variant.variant_selling_price','variant.variant_discount_price','product.product_tax',
                'variant.sku_no','variant.variant_image','variant.variant_quantity','size.size_name','color.color_name','color.color_code',
                'unit.unit_name','unit.unit_symbol')
                ->join('color','variant.color_id','=','color.id')
                ->join('size','variant.size_id','=','size.id')
                ->join('product','variant.product_id','=','product.id')
                ->join('unit','variant.unit_id','=','unit.id')
                ->where('product.product_status', '=',1)
                ->get();
            return response()->json($variant);
        } else {
            $warehouseItems = WarehouseItem::where('warehouse_id', $id)->get();
            $variant = [];
            foreach ($warehouseItems as $item) {
                if ($item->variant_id != null) {
                    $variant[] = DB::table('warehouse_item')
                        ->select('warehouse_item.product_quantity as variant_quantity','warehouse_item.status', 'warehouse_item.product_id', 'warehouse_item.warehouse_id',
                            'warehouse_item.variant_id as vid','warehouse_item.id', 'product.product_name', 'product.product_image', 'product.product_code',
                            'variant.size_id','variant.color_id', 'variant.sku_no','variant.variant_image','unit.unit_name','unit.unit_symbol', 'variant.variant_weight',
                            'variant.variant_capacity', 'variant.variant_tax','variant.variant_selling_price','variant.variant_discount_price','product.product_tax')
                        ->join('product', 'warehouse_item.product_id', '=', 'product.id')
                        ->join('variant', 'warehouse_item.variant_id', '=', 'variant.id')
                        ->join('unit','variant.unit_id','=','unit.id')
                        ->join('color','variant.color_id','=','color.id')
                        ->join('size','variant.size_id','=','size.id')
                        ->where('warehouse_item.warehouse_id',$warehouse->id)
                        ->where('product.id', $item->product_id)
                        ->where('warehouse_item.variant_id', $item->variant_id)
                        ->where('warehouse_item.status', '=',1)
                        ->first();
                }
            }
            return response()->json($variant);
        }
    }

    public function getTruckBySupplierChange($id){
        $supplierTruck = Truck::where('supplier_id',$id)->get();
        return response()->json($supplierTruck);
    }


}
