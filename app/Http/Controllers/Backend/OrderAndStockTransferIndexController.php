<?php

namespace App\Http\Controllers\Backend;

use App\Component\OrderAndStockTransferIndex\OrderAndStockTransferHandler;
use App\Http\Controllers\Controller;

class OrderAndStockTransferIndexController extends Controller
{
    //
        public function __construct(private OrderAndStockTransferHandler $orderAndStockTransferHandler)
        {
        }

        public function __invoke()
        {
            return $this->orderAndStockTransferHandler->handle();
        }

}
