<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\RequestProduct;
use Illuminate\Http\Request;

class RequestProductController extends Controller
{
    public function showRequestProduct()
    {
        $requestProduct = RequestProduct::with('product')->with('supplier')->orderBy('id','DESC')->get();
        return response()->json($requestProduct);
    }

    public function deleteRequestProduct($id)
    {
        $requestProduct = RequestProduct::find($id);
        $requestProduct->delete();
    }

    public function cancelRequestProduct($id)
    {
        $requestProduct = RequestProduct::find($id);
        $requestProduct->status = 4;
        $requestProduct->save();
    }

    public function completedRequestProduct($id)
    {
        $requestProduct = RequestProduct::find($id);
        $requestProduct->status = 3;
        $requestProduct->save();
    }

    public function processRequestProduct($id)
    {
        $requestProduct = RequestProduct::find($id);
        $requestProduct->status = 2;
        $requestProduct->save();
    }

    public function acceptRequestProduct($id)
    {
        $requestProduct = RequestProduct::find($id);
        $requestProduct->status = 1;
        $requestProduct->save();
    }

    public function requestProduct(Request $request)
    {
        if ($request->supplier_id == null) {
            $findSupplier = Product::find($request->product_id);
            $requestProduct = new RequestProduct();
            $requestProduct->quantity = $request->quantity;
            $requestProduct->notes = $request->notes;
            $requestProduct->product_id = $request->product_id;
            $requestProduct->supplier_id = $findSupplier->supplier_id;
            $requestProduct->status = 0;
            $requestProduct->save();
        } else {
            $requestProduct = new RequestProduct();
            $requestProduct->quantity = $request->quantity;
            $requestProduct->notes = $request->notes;
            $requestProduct->product_id = $request->product_id;
            $requestProduct->supplier_id = $request->supplier_id;
            $requestProduct->status = 0;
            $requestProduct->save();
        }
    }
}
