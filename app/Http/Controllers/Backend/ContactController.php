<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmailByContact;
use App\Models\WebSiteSettings;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index(Request $request)
    {
        $settings = WebSiteSettings::find(0);
        $contact = [];
        $contact['email'] = $request->email;
        $contact['phone'] = $request->phone;
        $contact['name'] = $request->name;
        $contact['subject'] = $request->subject;
        $contact['message'] = $request->message;
        SendEmailByContact::dispatch($settings,$contact);
    }
}
