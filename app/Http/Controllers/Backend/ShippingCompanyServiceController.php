<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ShippingCompanyService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShippingCompanyServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
//        $shippingCompanyDeci = ShippingCompanyDeci::with('shippingCompany')->orderBy('id','DESC')->get();
        $shippingCompanyService = DB::table('shipping_company_service')
            ->select('shipping_company_service.*','shipping_company.shipping_company_name','shipping_company.shipping_company_image')
            ->join('shipping_company','shipping_company_service.shipping_company_id','=','shipping_company.id')
            ->orderBy('shipping_company_service.id','DESC')->get();
        return response()->json($shippingCompanyService);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $shippingCompanyService = new ShippingCompanyService();
            $shippingCompanyService->shipping_company_id = $request->shipping_company_id;
            $shippingCompanyService->service_title = $request->service_title;
            $shippingCompanyService->service_type = $request->service_type;
            $shippingCompanyService->service_price = $request->service_price;
            $shippingCompanyService->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $shippingCompanyService = ShippingCompanyService::find($id);
        return response()->json($shippingCompanyService);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $shippingCompanyService = ShippingCompanyService::find($id);
            $shippingCompanyService->shipping_company_id = $request->shipping_company_id;
            $shippingCompanyService->service_title = $request->service_title;
            $shippingCompanyService->service_type = $request->service_type;
            $shippingCompanyService->service_price = $request->service_price;
            $shippingCompanyService->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $shippingCompanyService = ShippingCompanyService::find($id);
            $shippingCompanyService->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }
}
