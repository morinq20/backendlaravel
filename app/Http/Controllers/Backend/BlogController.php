<?php

namespace App\Http\Controllers\Backend;

use App\Enum\EmailTemplateEnum;
use App\Events\BlogAdded;
use App\Events\NewsletterEmail;
use App\Http\Controllers\Controller;
use App\Http\Services\FileManager\BlogFileConfig;
use App\Http\Services\FileManager\FileSaver;
use App\Http\Traits\FolderControlTrait;
use App\Jobs\SendEmailJob;
use App\Models\Blog;
use App\Models\Cities;
use App\Models\Country;
use App\Models\Customer;
use App\Models\ImageResize;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\PanelSettings;
use App\Models\StockTransfer;
use App\Models\Warehouse;
use Exception;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class BlogController extends Controller
{
    use FolderControlTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $blog = Blog::orderBy('blog_seq','ASC')->get();
        return response()->json($blog);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
//            $path = 'backend/uploads/blog/';
//            // Content
//            $explode = explode(',', $request->blog_image);
//            $content = $explode[1];
//            // Type
//            $position = strpos($request->blog_image, ';');
//            $sub = substr($request->blog_image, 0, $position);
//            $ext = explode('/', $sub)[1];
//            $name = time().".".$ext;
//            $filePath = $path.$name;
//            file_put_contents($filePath, (string)base64_decode($content));
//            return response()->json($filePath);
            $imageResize = ImageResize::first();
            $blog = new Blog();
            $blog->blog_title = $request->blog_title;
            $blog->blog_status = $request->blog_status;
            $blog->blog_description = strip_tags($request->blog_description);
            $blog->blog_seq = self::makeSeq();
            if (isset($request->blog_image)) {
                $path = 'backend/uploads/blog/';
                $this->folderIfNotExists($path);
                $file = FileSaver::getInstance();
                $config = BlogFileConfig::createConfig($request->blog_image,$path,$imageResize->blog_image_width,$imageResize->blog_image_height);
                $file_save = $file->send($config);

                if (isset($file_save))
                {
                    $blog->blog_image = $config->createFile();
                }
                $blog->save();
            } else {
                $blog->save();
            }
//            event(new NewsletterEmail($blog,EmailTemplateEnum::WELCOME_TEMPLATE->value));
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id): \Illuminate\Http\JsonResponse
    {
        $blog = Blog::find($id);
        return response()->json($blog);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $newPhoto = $request->new_blog_image;
            $oldPhoto = $request->blog_image;
            $blog = Blog::find($id);
            $blog->blog_title = $request->blog_title;
            $blog->blog_status = $request->blog_status;
            $blog->blog_description = strip_tags($request->blog_description);
//            $blog->blog_seq = $request->blog_seq;
            if (isset($newPhoto)) {
                if ($oldPhoto != null) {
                    unlink($oldPhoto);
                    $path = 'backend/uploads/blog/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = BlogFileConfig::createConfig($newPhoto,$path,$imageResize->blog_image_width,$imageResize->blog_image_height);
                    $file_save = $file->send($config);
                    $blog->blog_image = $config->createFile();
                    $blog->save();
                } else {
                    $path = 'backend/uploads/blog/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = BlogFileConfig::createConfig($newPhoto,$path,$imageResize->blog_image_width,$imageResize->blog_image_height);
                    $file_save = $file->send($config);
                    $blog->blog_image = $config->createFile();
                    $blog->save();
                }
            } else {
                $blog->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $blog = Blog::find($id);
            $photo = $blog->blog_image;
            if (isset($photo)) {
                unlink($photo);
                $blog->delete();
            } else {
                $blog->delete();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    // Blog Sortable

    /**
     * @throws Exception
     */
    public function blogSortable(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->item as $key => $value) {
                $blog = Blog::find(intval($value));
                $blog->blog_seq = intval($key);
                $blog->save();
            }
            echo true;
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    private static function makeSeq(): int|string
    {
        $control = Blog::latest('id')->first();
        if (!$control){
            return "1";
        }else{
            return strval($control->blog_seq + 1);
        }
    }

}
