<?php

namespace App\Http\Controllers\Backend;

use App\Component\SalaryMonthEmployee\MonthlyAmountControl\MonthlyAmountControlHandler;
use App\Http\Controllers\Controller;
use App\Models\Month;
use App\Models\Salary;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SalaryController extends Controller
{
//    public function employeeMonthSalaryControl($employee_id,$month_id): JsonResponse
//    {
//        $employeeMonthSalaryControl = Salary::where('month_id',$month_id)
//            ->where('employee_id',$employee_id)->first();
//        return response()->json($employeeMonthSalaryControl);
//    }
public function __construct(private MonthlyAmountControlHandler $amountControlHandler)
{
}


    public function employeeMonthSalaryControl(Request $request)
    {

        $salaryEntity = $this->amountControlHandler->handle($request);
        return response()->json($salaryEntity);


        }


    /**
     * @throws Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'month_id' => 'required',
                'employee_id' => 'required',
                'amount' => 'required',
            ]);
            $controlSalary = $this->control($request->month_id,$request->employee_id);
            if (!$controlSalary) {
                $salary = new Salary();
                $salary->month_id = $request->month_id;
                $salary->employee_id = $request->employee_id;
                $salary->amount = $request->amount;
                $salary->year = date('Y');
                $salary->status = 1;
                $salary->save();
            }
            DB::commit();
            return response()->json('ok');
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }


    }

    private function control($monthId,$employeeID): bool
    {
        $controlSalary = Salary::where('month_id',$monthId)->where('employee_id',$employeeID)->first();
        if ($controlSalary) {
            return true;
        }else{
            return false;
        }
    }

    /**
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'amount' => 'required',
            ]);
            if ($id) {
                $salary = Salary::find($id);
                $salary->amount = $request->amount;
                $salary->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

}
