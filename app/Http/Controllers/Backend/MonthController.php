<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Month;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MonthController extends Controller
{
    public function index(): JsonResponse
    {
        $month = Month::all();
        return response()->json($month);
    }
}
