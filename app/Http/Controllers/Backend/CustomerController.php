<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Cities;
use App\Models\Country;
use App\Models\Customer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = Customer::orderBy('id','DESC')->get();
        return response()->json($customer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $country = Country::where('id',$request->country_id)->first();
            $city = Cities::where('id',$request->city_id)->first();
            $customer = new Customer();
            $customer->customer_name = $request->customer_name;
            $customer->customer_surname = $request->customer_surname;
            $customer->customer_email = $request->customer_email;
            $customer->customer_phone = $request->customer_phone;
            $customer->customer_phone_dial_code = $request->customerPhoneDialCode;
            $customer->customer_country = $country->country;
            $customer->customer_city = $city->city;
            $customer->customer_zipcode = $request->customer_zipcode;
            $customer->country_id = $request->country_id;
            $customer->city_id = $request->city_id;
            $customer->customer_address = strip_tags($request->customer_address);
            $customer->customer_shipping_address = strip_tags($request->customer_shipping_address);
            $customer->customer_billing_address = strip_tags($request->customer_billing_address);
            $customer->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $customer = Customer::find($id);
        return response()->json($customer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $country = Country::where('id',$request->country_id)->first();
            $city = Cities::where('id',$request->city_id)->first();
            $customer = Customer::find($id);
            $customer->customer_name = $request->customer_name;
            $customer->customer_surname = $request->customer_surname;
            $customer->customer_email = $request->customer_email;
            $customer->customer_phone = $request->customer_phone;
            $customer->customer_phone_dial_code = $request->customerPhoneDialCode;
            $customer->customer_country = $country->country;
            $customer->customer_city = $city->city;
            $customer->customer_zipcode = $request->customer_zipcode;
            $customer->country_id = $request->country_id;
            $customer->city_id = $request->city_id;
            $customer->customer_address = strip_tags($request->customer_address);
            $customer->customer_shipping_address = strip_tags($request->customer_shipping_address);
            $customer->customer_billing_address = strip_tags($request->customer_billing_address);
            $customer->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $customer = Customer::find($id);
            $customer->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
