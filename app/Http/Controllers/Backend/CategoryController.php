<?php

namespace App\Http\Controllers\Backend;

use App\Component\UserRoleCheck\Role;
use App\Http\Controllers\Controller;
use App\Http\Services\LogManager\LogManager;
use App\Models\Category;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use App\Component\CreateLogs\CreateLogsHandler;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class CategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function index()
    {


        $categories = Category::orderBy('id','DESC')->get();
        $this->createLogs()->handle('Index','Category');

        return response()->json($categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $validateData = $request->validate([
                'category_name' => 'required|unique:categories|max:255',
            ]);
            $category = new Category();
            $category->category_name = $request->category_name;
            $category->save();
            $this->createLogs()->handle('Create','Category',$request);
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $category = Category::find($id);
        $this->createLogs()->handle('Show','Category');
        return response()->json($category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $category = Category::find($id);
            $category->category_name = $request->category_name;
            $category->save();
            $this->createLogs()->handle('Update','Category',$request);
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $category = Category::find($id);
            $category->delete();
            $this->createLogs()->handle('Delete','Category');
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
