<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmailByAnnouncement;
use App\Models\Announcements;
use App\Models\PanelSettings;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AnnouncementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): \Illuminate\Http\JsonResponse
    {
        $announcements = Announcements::with('role')->with('user')->get();
        return response()->json($announcements);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $admins = User::where('role_id','!=',null)->get();
            foreach ($admins as $admin){
                $announcements = array();
                $announcements['role_id'] = $admin->role_id;
                $announcements['user_id'] = $admin->id;
                $announcements['title'] = $request->title;
                $announcements['description'] = strip_tags($request->description);
                $announcements['status'] = $request->status;
                $announcementId = DB::table('announcements')->insertGetId($announcements);
                if ($request->status == 1){
                    $this->sendMail($announcementId);
                }
            }
//        return response()->json($announcements);
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    private function sendMail($announcementId){

        $announcement = Announcements::find($announcementId);
        $settings = PanelSettings::find(0);
        SendEmailByAnnouncement::dispatch($announcement,$settings);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        $collection = Collection::find($id);
//        return response()->json($collection);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $announcements = Announcements::find($id);
            $announcements->title = $request->title;
            $announcements->description = strip_tags($request->description);
            $announcements->status = $request->status;
            $announcements->save();
            if ($request->status == 1){
                $this->sendMail($id);
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $announcements = Announcements::find($id);
            $announcements->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    public function getUserAnnouncements(): JsonResponse
    {
        $user = User::find(auth()->user()->getAuthIdentifier());
        $announcements = Announcements::where('user_id',$user->id)->where('role_id',$user->role_id)->where('isRead',null)->where('status',1)->get();
        return response()->json($announcements);
    }

    /**
     * @throws Exception
     */
    public function readUserAnnouncements($id)
    {
        DB::beginTransaction();
        try {
            $user = User::find(auth()->user()->getAuthIdentifier());
            $announcements = Announcements::where('id',$id)->where('user_id',$user->id)->where('role_id',$user->role_id)->where('isRead',null)->where('status',1)->first();
            $announcements->isRead = 1;
            $announcements->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

}
