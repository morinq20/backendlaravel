<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\FileManager\FileSaver;
use App\Http\Services\FileManager\VariantFileConfig;
use App\Http\Services\Utils\Converter\Converter;
use App\Http\Traits\FolderControlTrait;
use App\Models\ImageResize;
use App\Models\Product;
use App\Models\Size;
use App\Models\Variant;
use Exception;
use http\Env\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class VariantController extends Controller
{
    use FolderControlTrait;

    /**
     * @throws Exception
     */
    public function store(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            $imageResize = ImageResize::first();
            $product = Product::select('product_code')->where('id',$id)->first();
            foreach ($requestAll as $key => $item){
                $size = Size::where('id',$item['size_id'])->first();
                $sizeExplode = explode(' X ',$size->size_name);
                $capacity = Converter::convertToFloat($sizeExplode[0]) * Converter::convertToFloat($sizeExplode[1]) * Converter::convertToFloat($sizeExplode[2]);
                $image = $item['variant_image'];
                $variant = new Variant();
                $variant->product_id = $id;
                $variant->size_id = $item['size_id'];
                $variant->color_id = $item['color_id'];
                $variant->unit_id = $item['unit_id'];
                $variant->variant_weight = $item['variant_weight'];
//                $variant->variant_capacity = $item['variant_capacity'];
                $variant->variant_capacity = Converter::convertToView($capacity,3);
                $variant->variant_tax = $item['variant_tax'];
                $variant->variant_cost_price = $item['variant_cost_price'];
                $variant->variant_selling_price = $item['variant_selling_price'];
                $variant->variant_discount_price = $item['variant_discount_price'];
                $variant->sku_no = $product->product_code.'-'.$item['sku_no'];
                $variant->variant_quantity = $item['variant_quantity'];
                if (isset($image)) {
                    $path = 'backend/uploads/variant/';
                    $this->folderIfNotExists($path);
                    $img = Image::make($image)->resize($imageResize->variant_image_width,$imageResize->variant_image_height);
                    $position = strpos($image, ';');
                    $sub = substr($image, 0, $position);
                    $ext = explode('/', $sub)[1];
                    $name = time().$key.".".$ext;
                    $filePath = $path.$name;
                    $img->save($filePath);
//                $file = FileSaver::getInstance();
//                $config = VariantFileConfig::createConfig($image,$path,$imageResize->variant_image_width,$imageResize->variant_image_height);
//                $file_save = $file->send($config);

                    if (isset($filePath))
                    {
                        $variant->variant_image = $filePath;
//                    return response()->json($config->createFile());
                        $variant->save();
                    }
                } else {
                    $variant->save();
                }
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }

    }


//    public function store(Request $request,$id)
//    {
//        $requestAll = $request->toArray();
//        $imageResize = ImageResize::first();
//        $path = 'backend/uploads/variant/';
//        $fileTypes['data:image/png;base64'] = '.png';
//        $fileTypes['data:image/jpg;base64'] = '.jpg';
//        $fileTypes['data:image/jpeg;base64'] = '.jpeg';
//        $fileTypes['data:image/gif;base64'] = '.gif';
//        foreach ($requestAll as $key => $item){
//            $variant = new Variant();
//            $variant->product_id = $id;
//            $variant->size_id = $item['size_id'];
//            $variant->color_id = $item['color_id'];
//            $variant->sku_no = $item['sku_no'];
//            $variant->variant_quantity = $item['variant_quantity'];
//            // Explode
//            $explode = explode(',', $item['variant_image']);
//            // Content
//            $content = $explode[1];
//            // Uzantı
//            $ext = $fileTypes[$explode[0]] ?? '.png';
//            // Yeni isim ve uzanti
//            $fileName = time() . '_' . mt_rand(0, 100) . $ext;
//            // File path
//            $filePath = (string)$path . '/' . $fileName;
////            file_put_contents($filePath, (string)base64_decode($content));
//            return response()->json(base64_decode($content));
//            $variant->variant_image = $fileName;
//            $variant->save();
//        }
//    }

    // Variant Show
    public function show(Request $request,$id): \Illuminate\Http\JsonResponse
    {
        $variant = Variant::where('product_id',$id)->get();
        return response()->json($variant);
    }

    // Variant Update

    /**
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $allRequest = $request->all();
            $imageResize = ImageResize::first();
            $product = Product::select('product_code')->where('id',$id)->first();
            foreach ($allRequest as $key => $item){
                $size = Size::where('id',$item['size_id'])->first();
                $sizeExplode = explode(' X ',$size->size_name);
                $capacity = Converter::convertToFloat($sizeExplode[0]) * Converter::convertToFloat($sizeExplode[1]) * Converter::convertToFloat($sizeExplode[2]);
                if(in_array("new_variant_image",array_keys((array)$item))){
                    $newPhoto = $item['new_variant_image'];
                }
                $oldPhoto = $item['variant_image'];
                $variant = Variant::find($item['id']);
                $variant->product_id = $id;
                $variant->size_id = $item['size_id'];
                $variant->color_id = $item['color_id'];
                $variant->unit_id = $item['unit_id'];
                $variant->variant_weight = $item['variant_weight'];
//                $variant->variant_capacity = $item['variant_capacity'];
                $variant->variant_capacity = Converter::convertToView($capacity,3);
                $variant->variant_tax = $item['variant_tax'];
                $variant->variant_cost_price = $item['variant_cost_price'];
                $variant->variant_selling_price = $item['variant_selling_price'];
                $variant->variant_discount_price = $item['variant_discount_price'];
//            $variant->sku_no = $product->product_code.'-'.$item['sku_no'];
                $variant->variant_quantity = $item['variant_quantity'];

                if (isset($newPhoto)) {
                    if ($oldPhoto != null) {
                        unlink($oldPhoto);
                        $path = 'backend/uploads/variant/';
                        $this->folderIfNotExists($path);
                        $img = Image::make($newPhoto)->resize($imageResize->variant_image_width,$imageResize->variant_image_height);
                        $position = strpos($newPhoto, ';');
                        $sub = substr($newPhoto, 0, $position);
                        $ext = explode('/', $sub)[1];
                        $name = time().$key.".".$ext;
                        $filePath = $path.$name;
                        $img->save($filePath);
                        $variant->variant_image = $filePath;
                        $variant->save();
//                    $path = 'backend/uploads/variant/';
//                    $file = FileSaver::getInstance();
//                    $config = VariantFileConfig::createConfig($newPhoto,$path,$imageResize->variant_image_width,$imageResize->variant_image_height);
//                    $file_save = $file->send($config);
//                    $variant->variant_image = $config->createFile();
//                    $variant->save();
                    } else {
//                    $path = 'backend/uploads/variant/';
//                    $file = FileSaver::getInstance();
//                    $config = VariantFileConfig::createConfig($newPhoto,$path,$imageResize->variant_image_width,$imageResize->variant_image_height);
//                    $file_save = $file->send($config);
//                    $variant->variant_image = $config->createFile();
//                    $variant->save();
                        $path = 'backend/uploads/variant/';
                        $this->folderIfNotExists($path);
                        $img = Image::make($newPhoto)->resize($imageResize->variant_image_width,$imageResize->variant_image_height);
                        $position = strpos($newPhoto, ';');
                        $sub = substr($newPhoto, 0, $position);
                        $ext = explode('/', $sub)[1];
                        $name = time().$key.".".$ext;
                        $filePath = $path.$name;
                        $img->save($filePath);
                        $variant->variant_image = $filePath;
                        $variant->save();
                    }
                } else {
                    $variant->save();
                }
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    // Variant Delete

    /**
     * @throws Exception
     */
    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $findVariant = Variant::find($id);
            $photo = $findVariant->variant_image;
            if (isset($photo)) {
                unlink($photo);
                $findVariant->delete();
            } else {
                $findVariant->delete();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }


    public function mainStockControlVariant(): JsonResponse
    {
        $variants = DB::table('variant')->select('variant.id as id','variant.product_id','product.product_name','product.product_code',
            'product.product_quantity','product.warehouse_id','product.product_image','variant.size_id','variant.color_id','variant.unit_id',
            'variant.sku_no','variant.variant_image','variant.variant_quantity','size.size_name','color.color_name','color.color_code',
            'unit.unit_name','unit.unit_symbol')
            ->join('color','variant.color_id','=','color.id')
            ->join('size','variant.size_id','=','size.id')
            ->join('product','variant.product_id','=','product.id')
            ->join('unit','variant.unit_id','=','unit.id')
            ->where('variant_quantity',0)
            ->get();
        return response()->json($variants);
    }


    public function showItemVariant($id): JsonResponse
    {
        $variant = Variant::with('size')->with('color')->with('product')->find($id);
        return response()->json($variant);
    }


}
