<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductLanguageController extends Controller
{

    public function show(Request $request,$id): \Illuminate\Http\JsonResponse
    {
        $product = Product::where('id',$id)->first();
        return response()->json(json_decode($product->lang));
    }

    /**
     * @throws Exception
     */
    public function store(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            $product = Product::where('id',$id)->first();
            $arr = [];
            foreach ($requestAll as $key => $item){
                $lang = Language::find($item['language_id']);
                $arr[$lang->code]['language_id'] = $item['language_id'];
                $arr[$lang->code]['language_code'] = $lang->code;
                $arr[$lang->code]['product_name'] = $item['product_name'];
                $arr[$lang->code]['product_description'] = strip_tags($item['product_description']);
                $arr[$lang->code]['product_meta_description'] = strip_tags($item['product_meta_description']);
            }
            if ($product->lang !== null){
                $lang = json_decode($product->lang);
                foreach ($lang as $key => $item){
                    $arr[$key] = $item;
                }
            }
            $product->lang = json_encode($arr);
            $product->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }

    }

    /**
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            $product = Product::where('id',$id)->first();
            $arr = [];
            foreach ($requestAll as $key => $item){
                $lang = Language::find($item['language_id']);
                $arr[$lang->code]['language_id'] = $item['language_id'];
                $arr[$lang->code]['language_code'] = $lang->code;
                $arr[$lang->code]['product_name'] = $item['product_name'];
                $arr[$lang->code]['product_description'] = strip_tags($item['product_description']);
                $arr[$lang->code]['product_meta_description'] = strip_tags($item['product_meta_description']);
            }
            $product->lang = json_encode($arr);
            $product->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function delete($id,$code)
    {
        DB::beginTransaction();
        try {
            $product = Product::where('id',$id)->first();
            $lang = json_decode($product->lang);
            $arr = [];
            foreach ($lang as $key => $item){
                if ($key !== $code){
                    $arr[$key] = $item;
                }
            }
            $product->lang = json_encode($arr);
            $product->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
