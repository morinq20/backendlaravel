<?php

namespace App\Http\Controllers\Backend;

use App\Component\Country\CountryIndexHandler;
use App\Http\Controllers\Controller;

class CountryIndexController extends Controller
{
    public function __construct(private CountryIndexHandler $countryIndexHandler)
    {
    }

    public function __invoke()
    {
        return $this->countryIndexHandler->handle();
        // TODO: Implement __invoke() method.
    }

    //
}
