<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ImageResize;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Nette\Utils\Image;

class ImageResizeController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $imageResize = ImageResize::find($id);
        return response()->json($imageResize);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $control = ImageResize::first();
        if ($control == null) {
            $this->newImageResize($request);
        } else {
            $this->updateImageResize($request);
        }
    }

    /**
     * @throws Exception
     */
    private function newImageResize($request) {
        DB::beginTransaction();
        try {
            $imageResize = new ImageResize();
            $imageResize->id = 0;
            $imageResize->slider_image_width = $request->slider_image_width;
            $imageResize->slider_image_height = $request->slider_image_height;
            $imageResize->blog_image_width = $request->blog_image_width;
            $imageResize->blog_image_height = $request->blog_image_height;
            $imageResize->product_image_width = $request->product_image_width;
            $imageResize->product_image_height = $request->product_image_height;
            $imageResize->about_us_image_width = $request->about_us_image_width;
            $imageResize->about_us_image_height = $request->about_us_image_height;
            $imageResize->service_image_width = $request->service_image_width;
            $imageResize->service_image_height = $request->service_image_height;
            $imageResize->team_image_width = $request->team_image_width;
            $imageResize->team_image_height = $request->team_image_height;
            $imageResize->project_image_width = $request->project_image_width;
            $imageResize->project_image_height = $request->project_image_height;
            $imageResize->referance_image_width = $request->referance_image_width;
            $imageResize->referance_image_height = $request->referance_image_height;
            $imageResize->admin_image_width = $request->admin_image_width;
            $imageResize->admin_image_height = $request->admin_image_height;
            $imageResize->note_image_width = $request->note_image_width;
            $imageResize->note_image_height = $request->note_image_height;
            $imageResize->website_settings_image_width = $request->website_settings_image_width;
            $imageResize->website_settings_image_height = $request->website_settings_image_height;
            $imageResize->panel_settings_image_width = $request->panel_settings_image_width;
            $imageResize->panel_settings_image_height = $request->panel_settings_image_height;
            $imageResize->order_tracking_status_image_width = $request->order_tracking_status_image_width;
            $imageResize->order_tracking_status_image_height = $request->order_tracking_status_image_height;
            $imageResize->employee_image_width = $request->employee_image_width;
            $imageResize->employee_image_height = $request->employee_image_height;
            $imageResize->campaign_image_width = $request->campaign_image_width;
            $imageResize->campaign_image_height = $request->campaign_image_height;
            $imageResize->shipping_company_image_width = $request->shipping_company_image_width;
            $imageResize->shipping_company_image_height = $request->shipping_company_image_height;
            $imageResize->variant_image_width = $request->variant_image_width;
            $imageResize->variant_image_height = $request->variant_image_height;
            $imageResize->comment_image_width = $request->comment_image_width;
            $imageResize->comment_image_height = $request->comment_image_height;
            $imageResize->collection_image_width = $request->collection_image_width;
            $imageResize->collection_image_height = $request->collection_image_height;
            $imageResize->truck_image_width = $request->truck_image_width;
            $imageResize->truck_image_height = $request->truck_image_height;
            $imageResize->save();
            DB::commit();
            return $imageResize;
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    private function updateImageResize($request) {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $imageResize->slider_image_width = $request->slider_image_width;
            $imageResize->slider_image_height = $request->slider_image_height;
            $imageResize->blog_image_width = $request->blog_image_width;
            $imageResize->blog_image_height = $request->blog_image_height;
            $imageResize->product_image_width = $request->product_image_width;
            $imageResize->product_image_height = $request->product_image_height;
            $imageResize->about_us_image_width = $request->about_us_image_width;
            $imageResize->about_us_image_height = $request->about_us_image_height;
            $imageResize->service_image_width = $request->service_image_width;
            $imageResize->service_image_height = $request->service_image_height;
            $imageResize->team_image_width = $request->team_image_width;
            $imageResize->team_image_height = $request->team_image_height;
            $imageResize->project_image_width = $request->project_image_width;
            $imageResize->project_image_height = $request->project_image_height;
            $imageResize->referance_image_width = $request->referance_image_width;
            $imageResize->referance_image_height = $request->referance_image_height;
            $imageResize->admin_image_width = $request->admin_image_width;
            $imageResize->admin_image_height = $request->admin_image_height;
            $imageResize->note_image_width = $request->note_image_width;
            $imageResize->note_image_height = $request->note_image_height;
            $imageResize->website_settings_image_width = $request->website_settings_image_width;
            $imageResize->website_settings_image_height = $request->website_settings_image_height;
            $imageResize->panel_settings_image_width = $request->panel_settings_image_width;
            $imageResize->panel_settings_image_height = $request->panel_settings_image_height;
            $imageResize->order_tracking_status_image_width = $request->order_tracking_status_image_width;
            $imageResize->order_tracking_status_image_height = $request->order_tracking_status_image_height;
            $imageResize->employee_image_width = $request->employee_image_width;
            $imageResize->employee_image_height = $request->employee_image_height;
            $imageResize->campaign_image_width = $request->campaign_image_width;
            $imageResize->campaign_image_height = $request->campaign_image_height;
            $imageResize->shipping_company_image_width = $request->shipping_company_image_width;
            $imageResize->shipping_company_image_height = $request->shipping_company_image_height;
            $imageResize->variant_image_width = $request->variant_image_width;
            $imageResize->variant_image_height = $request->variant_image_height;
            $imageResize->comment_image_width = $request->comment_image_width;
            $imageResize->comment_image_height = $request->comment_image_height;
            $imageResize->collection_image_width = $request->collection_image_width;
            $imageResize->collection_image_height = $request->collection_image_height;
            $imageResize->truck_image_width = $request->truck_image_width;
            $imageResize->truck_image_height = $request->truck_image_height;
            $imageResize->save();
            DB::commit();
            return $imageResize;
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

}
