<?php

namespace App\Http\Controllers\Backend;

use App\Component\CreateExcelExport\CreateExcelExportHandler;
use App\Exports\UserExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{

    public function __construct(private CreateExcelExportHandler $createExcelExportHandler)
    {
    }

   public function __invoke(Request $request)
   {
       return $this->createExcelExportHandler->handle($request);

    }
}
