<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Service;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServiceLanguageController extends Controller
{
    public function show(Request $request,$id): \Illuminate\Http\JsonResponse
    {
        $service = Service::where('id',$id)->first();
        return response()->json(json_decode($service->lang));
    }

    /**
     * @throws Exception
     */
    public function store(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            $service = Service::where('id',$id)->first();
            $arr = [];
            foreach ($requestAll as $key => $item){
                $lang = Language::find($item['language_id']);
                $arr[$lang->code]['language_id'] = $item['language_id'];
                $arr[$lang->code]['language_code'] = $lang->code;
                $arr[$lang->code]['service_title'] = $item['service_title'];
                $arr[$lang->code]['service_description'] = strip_tags($item['service_description']);
            }
            if ($service->lang !== null){
                $lang = json_decode($service->lang);
                foreach ($lang as $key => $item){
                    $arr[$key] = $item;
                }
            }
            $service->lang = json_encode($arr);
            $service->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }

    }

    /**
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            $service = Service::where('id',$id)->first();
            $arr = [];
            foreach ($requestAll as $key => $item){
                $lang = Language::find($item['language_id']);
                $arr[$lang->code]['language_id'] = $item['language_id'];
                $arr[$lang->code]['language_code'] = $lang->code;
                $arr[$lang->code]['service_title'] = $item['service_title'];
                $arr[$lang->code]['service_description'] = strip_tags($item['service_description']);
            }
            $service->lang = json_encode($arr);
            $service->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function delete($id,$code)
    {
        DB::beginTransaction();
        try {
            $service = Service::where('id',$id)->first();
            $lang = json_decode($service->lang);
            $arr = [];
            foreach ($lang as $key => $item){
                if ($key !== $code){
                    $arr[$key] = $item;
                }
            }
            $service->lang = json_encode($arr);
            $service->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
