<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\Utils\Converter\Converter;
use App\Http\Services\Utils\UnitMeasure\Measurement;
use App\Models\Pallet;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $pallet = Pallet::orderBy('id','DESC')->get();
        return response()->json($pallet);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $capacity = Converter::convertToFloat($request->pallet_width) * Converter::convertToFloat($request->pallet_height) * Converter::convertToFloat($request->pallet_length);
            $pallet = new Pallet();
            $pallet->pallet_name = $request->pallet_name;
            $pallet->pallet_default_weight =$request->pallet_default_weight;
            $pallet->pallet_width = $request->pallet_width;
            $pallet->pallet_height = $request->pallet_height;
            $pallet->pallet_length = $request->pallet_length;
            $pallet->pallet_capacity = Converter::convertToView($capacity,3);
            $pallet->pallet_max_weight = $request->pallet_max_weight;
            $pallet->pallet_price = $request->pallet_price;
            $pallet->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $pallet = Pallet::find($id);
        return response()->json($pallet);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $capacity = Converter::convertToFloat($request->pallet_width) * Converter::convertToFloat($request->pallet_height) * Converter::convertToFloat($request->pallet_length);
            $pallet = Pallet::find($id);
            $pallet->pallet_name = $request->pallet_name;
            $pallet->pallet_default_weight =$request->pallet_default_weight;
            $pallet->pallet_width = $request->pallet_width;
            $pallet->pallet_height = $request->pallet_height;
            $pallet->pallet_length = $request->pallet_length;
            $pallet->pallet_capacity = Converter::convertToView($capacity,3);
            $pallet->pallet_max_weight = $request->pallet_max_weight;
            $pallet->pallet_price = $request->pallet_price;

            $pallet->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $pallet = Pallet::find($id);
            $pallet->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

}
