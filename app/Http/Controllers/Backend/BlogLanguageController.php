<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Language;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogLanguageController extends Controller
{
    public function show(Request $request,$id): \Illuminate\Http\JsonResponse
    {
        $blog = Blog::where('id',$id)->first();
        return response()->json(json_decode($blog->lang));
    }

    /**
     * @throws Exception
     */
    public function store(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            $blog = Blog::where('id',$id)->first();
            $arr = [];
            foreach ($requestAll as $key => $item){
                $lang = Language::find($item['language_id']);
                $arr[$lang->code]['language_id'] = $item['language_id'];
                $arr[$lang->code]['language_code'] = $lang->code;
                $arr[$lang->code]['blog_title'] = $item['blog_title'];
                $arr[$lang->code]['blog_description'] = strip_tags($item['blog_description']);
            }
            if ($blog->lang !== null){
                $lang = json_decode($blog->lang);
                foreach ($lang as $key => $item){
                    $arr[$key] = $item;
                }
            }
            $blog->lang = json_encode($arr);
            $blog->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }

    }

    /**
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            $blog = Blog::where('id',$id)->first();
            $arr = [];
            foreach ($requestAll as $key => $item){
                $lang = Language::find($item['language_id']);
                $arr[$lang->code]['language_id'] = $item['language_id'];
                $arr[$lang->code]['language_code'] = $lang->code;
                $arr[$lang->code]['blog_title'] = $item['blog_title'];
                $arr[$lang->code]['blog_description'] = strip_tags($item['blog_description']);
            }
            $blog->lang = json_encode($arr);
            $blog->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function delete($id,$code)
    {
        DB::beginTransaction();
        try {
            $blog = Blog::where('id',$id)->first();
            $lang = json_decode($blog->lang);
            $arr = [];
            foreach ($lang as $key => $item){
                if ($key !== $code){
                    $arr[$key] = $item;
                }
            }
            $blog->lang = json_encode($arr);
            $blog->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
