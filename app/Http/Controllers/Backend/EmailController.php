<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmailByBulk;
use App\Jobs\SendEmailByPrivate;
use App\Models\WebSiteSettings;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    public function sendPrivate(Request $request)
    {
        $settings = WebSiteSettings::find(0);
        $data = [];
        $data['to'] = $request->to;
        $data['customer_id'] = $request->customer_id;
        $data['subject'] = $request->subject;
        $data['content'] = $request->message;
        SendEmailByPrivate::dispatch($settings,$data);
    }

    public function sendBulk(Request $request)
    {
        $settings = WebSiteSettings::find(0);
        foreach ($request->customerList as $item){
            $data = [];
            $data['to'] = $item['email'];
            $data['customer_id'] = $item['id'];
            $data['subject'] = $request->subject;
            $data['content'] = $request->message;
            SendEmailByBulk::dispatch($settings,$data);
        }
    }
}
