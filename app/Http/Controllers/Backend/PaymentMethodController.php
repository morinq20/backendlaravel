<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\PaymentMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $paymentMethod = PaymentMethod::orderBy('payment_seq','ASC')->get();
        return response()->json($paymentMethod);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $paymentMethod = new PaymentMethod();
            $paymentMethod->payment_method = $request->payment_method;
            $paymentMethod->payment_tax = $request->payment_tax;
            $paymentMethod->payment_status = $request->payment_status;
            $paymentMethod->payment_seq = self::makeSeq();
            $paymentMethod->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $paymentMethod = PaymentMethod::find($id);
        return response()->json($paymentMethod);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $paymentMethod = PaymentMethod::find($id);
            $paymentMethod->payment_method = $request->payment_method;
            $paymentMethod->payment_tax = $request->payment_tax;
            $paymentMethod->payment_status = $request->payment_status;
//            $paymentMethod->payment_seq = $request->payment_seq;
            $paymentMethod->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $paymentMethod = PaymentMethod::find($id);
            $paymentMethod->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    // Faq Sortable

    /**
     * @throws \Exception
     */
    public function paymentMethodSortable(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->item as $key => $value) {
                $paymentMethod = PaymentMethod::find($value);
                $paymentMethod->payment_seq = $key;
                $paymentMethod->save();
            }
            echo true;
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    private static function makeSeq(): int|string
    {
        $control = PaymentMethod::latest('id')->first();
        if (!$control){
            return "1";
        }else{
            return strval($control->team_seq + 1);
        }
    }
}
