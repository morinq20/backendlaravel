<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ShippingCompanyDeci;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShippingCompanyDeciController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
//        $shippingCompanyDeci = ShippingCompanyDeci::with('shippingCompany')->orderBy('id','DESC')->get();
        $shippingCompanyDeci = DB::table('shipping_company_deci')
            ->select('shipping_company_deci.*','shipping_company.shipping_company_name','shipping_company.shipping_company_image')
            ->join('shipping_company','shipping_company_deci.shipping_company_id','=','shipping_company.id')
            ->orderBy('shipping_company_deci.id','DESC')->get();
        return response()->json($shippingCompanyDeci);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $shippingCompanyDeci = new ShippingCompanyDeci();
            $shippingCompanyDeci->shipping_company_id = $request->shipping_company_id;
            $shippingCompanyDeci->start_deci = $request->start_deci;
            $shippingCompanyDeci->finish_deci = $request->finish_deci;
            $shippingCompanyDeci->deci_price = $request->deci_price;
            $shippingCompanyDeci->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $shippingCompanyDeci = ShippingCompanyDeci::find($id);
        return response()->json($shippingCompanyDeci);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $shippingCompanyDeci = ShippingCompanyDeci::find($id);
            $shippingCompanyDeci->shipping_company_id = $request->shipping_company_id;
            $shippingCompanyDeci->start_deci = $request->start_deci;
            $shippingCompanyDeci->finish_deci = $request->finish_deci;
            $shippingCompanyDeci->deci_price = $request->deci_price;
            $shippingCompanyDeci->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $shippingCompanyDeci = ShippingCompanyDeci::find($id);
            $shippingCompanyDeci->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

}
