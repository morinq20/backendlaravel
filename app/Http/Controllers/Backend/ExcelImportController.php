<?php

namespace App\Http\Controllers\Backend;

use App\Component\Excel\ImportHandler;
use App\Http\Controllers\Controller;
use App\Http\Requests\ExcelImportRequest;
use Illuminate\Http\Request;

class ExcelImportController extends Controller
{
    public function __construct(private ImportHandler $importHandler)
    {
    }

    /**
     * @throws \Exception
     */
    public function __invoke(Request $request)
    {
//        return response()->json($this->importHandler->handle($request));
        return response()->json($request);
    }
}
