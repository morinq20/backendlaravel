<?php

namespace App\Http\Controllers\Backend;

use App\Component\Order\CreateOrder\OrderStatusChangeHandler;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\StockTransferItem;
use App\Models\Variant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderStatusChangeController extends Controller
{
    //

    public function __construct(private Order $order,
    private OrderStatusChangeHandler $orderStatusChangeHandler)
    {

    }

    /**
     * @throws \Exception
     */
    public function __invoke(Request $request)
    {
        DB::beginTransaction();
        try {
            if($request->item === 'cancel'){
                $order = $this->order->where('id',$request->orderId)->first();
                $orderItem = OrderItem::where('orders_id',$request->orderId)->get();
                if (isset($orderItem)){
                    foreach ($orderItem as $item){
                        $variant = Variant::find($item->variant_id);
                        $variant->variant_quantity += $item->product_quantity;
                        $variant->save();
                        $product = Product::find($variant->product_id);
                        $product->product_quantity += $item->product_quantity;
                        $product->save();
                    }
                }
                $order->isCanceled = '1';
                $order->status = "12";
                $order->save();
                return response()->json('Success');
            }
            $this->orderStatusChangeHandler->handle($request);
            DB::commit();
            return response()->json('Success');
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }

    }
}
