<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\RouteGroup;
use App\Models\RoutePermission;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class RouteController extends Controller
{
    public function routeList(): JsonResponse
    {
//        $routes = collect(Route::getRoutes())->map(function ($route) { return $route->uri(); });
        $routes = collect(Route::getRoutes());
        $routeList = [];
        foreach ($routes as $route){
            if (str_starts_with($route->uri, 'api')){
                $routeList[] = $route->uri. " - " . $route->methods[0];

            }
        }

        return response()->json($routeList);
    }

    public function routeGroup(): JsonResponse
    {
        $routeGroup = RouteGroup::all();
        $result = [];
        foreach ($routeGroup as $key => $group){
            $result[$key]['id'] = $group->id;
            $result[$key]['route_group_name'] = $group->route_group_name;
            $jsonDecode = json_decode($group->route_list);
            $result[$key]['route_list'] = $jsonDecode;

        }
        return response()->json($result);
    }

    public function routePermission(): JsonResponse
    {
        $routePermission = RoutePermission::with('role')->get();
        $result = [];
        foreach ($routePermission as $key => $permission){
            $result[$key]['id'] = $permission->id;
            $result[$key]['role'] = $permission->role->role;
            $jsonDecode = json_decode($permission->permission);
            $result[$key]['permission'] = $jsonDecode;

        }
        return response()->json($result);
    }


    public function showRouteGroupList($id): JsonResponse
    {
        $find = RouteGroup::find($id);
        $jsonDecode = json_decode($find->route_list);
        return response()->json($jsonDecode);
    }


    /**
     * @throws \Exception
     */
    public function storeRouteGroup(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'route_group_name' => 'required',
                'route_name' => 'required',
            ]);
            $routeGroup = new RouteGroup();
            $routeGroup->route_group_name = $request->route_group_name;
            $routeGroup->route_list = json_encode($request->route_name);
            $routeGroup->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }


    /**
     * @throws \Exception
     */
    public function storeRoutePermission(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'role_id' => 'required',
                'permission' => 'required',
            ]);
            $control = RoutePermission::where('role_id',$request->role_id)->first();
            if ($control){
                return response()->json('This role has permission');
            }else{
                $routePermission = new RoutePermission();
                $routePermission->role_id = $request->role_id;
                $routePermission->permission = json_encode($request->permission);
                $routePermission->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    public function showRoutePermission($id): JsonResponse
    {
        $find = RoutePermission::where('role_id',$id)->first();
        $jsonDecode = json_decode($find->permission);
        $result = [];
        $result['id'] = $find->id;
        $result['permission'] = $jsonDecode;
        return response()->json($result);
    }

    /**
     * @throws \Exception
     */
    public function deleteRoutePermission($id)
    {
        DB::beginTransaction();
        try {
            $routePermission = RoutePermission::find($id);
            $routePermission->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * @throws \Exception
     */
    public function deleteRouteGroup($id)
    {
        DB::beginTransaction();
        try {
            $routeGroup = RouteGroup::find($id);
            $routeGroup->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

}
