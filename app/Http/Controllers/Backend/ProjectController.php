<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\FileManager\FileSaver;
use App\Http\Services\FileManager\ProjectFileConfig;
use App\Http\Traits\FolderControlTrait;
use App\Models\ImageResize;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProjectController extends Controller
{
    use FolderControlTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $projects = Project::orderBy('project_seq','ASC')->get();
        return response()->json($projects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $project = new Project();
            $project->project_title = $request->project_title;
            $project->project_status = $request->project_status;
            $project->project_description = strip_tags($request->project_description);
            $project->project_seq = self::makeSeq();
            if (isset($request->project_image)) {
                $path = 'backend/uploads/project/';
                $this->folderIfNotExists($path);
                $file = FileSaver::getInstance();
                $config = ProjectFileConfig::createConfig($request->project_image,$path,$imageResize->project_image_width,$imageResize->project_image_height);
                $file_save = $file->send($config);
                if (isset($file_save))
                {
                    $project->project_image = $config->createFile();
                }
                $project->save();
            } else {
                $project->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $project = Project::find($id);
        return response()->json($project);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $newPhoto = $request->new_project_image;
            $oldPhoto = $request->project_image;
            $project = Project::find($id);
            $project->project_title = $request->project_title;
            $project->project_status = $request->project_status;
            $project->project_description = strip_tags($request->project_description);
//            $project->project_seq = $request->project_seq;
            if (isset($newPhoto)) {
                if ($oldPhoto != null) {
                    unlink($oldPhoto);
                    $path = 'backend/uploads/project/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = ProjectFileConfig::createConfig($newPhoto,$path,$imageResize->project_image_width,$imageResize->project_image_height);
                    $file_save = $file->send($config);
                    $project->project_image = $config->createFile();
                    $project->save();
                } else {
                    $path = 'backend/uploads/project/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = ProjectFileConfig::createConfig($newPhoto,$path,$imageResize->project_image_width,$imageResize->project_image_height);
                    $file_save = $file->send($config);
                    $project->project_image = $config->createFile();
                    $project->save();
                }
            } else {
                $project->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $project = Project::find($id);
            $photo = $project->project_image;
            if (isset($photo)) {
                unlink($photo);
                $project->delete();
            } else {
                $project->delete();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    // Project Sortable

    /**
     * @throws \Exception
     */
    public function projectSortable(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->item as $key => $value) {
                $project = Project::find(intval($value));
                $project->project_seq = intval($key);
                $project->save();
            }
            echo true;
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    private static function makeSeq(): int|string
    {
        $control = Project::latest('id')->first();
        if (!$control){
            return "1";
        }else{
            return strval($control->team_seq + 1);
        }
    }
}
