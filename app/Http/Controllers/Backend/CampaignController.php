<?php

namespace App\Http\Controllers\Backend;

use App\Enum\EmailTemplateEnum;
use App\Events\StockTransferEmail;
use App\Http\Controllers\Controller;
use App\Http\Services\FileManager\CampaignFileConfig;
use App\Http\Services\FileManager\FileSaver;
use App\Http\Traits\FolderControlTrait;
use App\Models\Campaign;
use App\Models\ImageResize;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CampaignController extends Controller
{
    use FolderControlTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $campaign = Campaign::orderBy('campaign_seq','ASC')->get();
        return response()->json($campaign);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    private function sendEmail($campaignId)
    {
        $eventOrder = Campaign::find($campaignId);
        event(new StockTransferEmail($eventOrder,EmailTemplateEnum::CAMPAIGN_TEMPLATE->value));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $campaign = new Campaign();
            $campaign->campaign_title = $request->campaign_title;
            $campaign->campaign_url = $request->campaign_url;
            $campaign->campaign_code = $request->campaign_code;
            $campaign->campaign_status = $request->campaign_status;
            $campaign->is_published = $request->is_published;
            $campaign->campaign_description = strip_tags($request->campaign_description);
            $campaign->campaign_seq = self::makeSeq();
            if (isset($request->campaign_image)) {
                $path = 'backend/uploads/campaign/';
                $this->folderIfNotExists($path);
                $file = FileSaver::getInstance();
                $config = CampaignFileConfig::createConfig($request->campaign_image,$path,$imageResize->campaign_image_width,$imageResize->campaign_image_height);
                $file_save = $file->send($config);
                if (isset($file_save))
                {
                    $campaign->campaign_image = $config->createFile();
                }
                $campaign->save();
            } else {
                $campaign->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $campaign = Campaign::find($id);
        return response()->json($campaign);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $newPhoto = $request->new_campaign_image;
            $oldPhoto = $request->campaign_image;
            $campaign = Campaign::find($id);
            $campaign->campaign_title = $request->campaign_title;
            $campaign->campaign_url = $request->campaign_url;
            $campaign->campaign_code = $request->campaign_code;
            $campaign->campaign_status = $request->campaign_status;
            $campaign->is_published = $request->is_published;
            $campaign->campaign_description = strip_tags($request->campaign_description);
//            $campaign->campaign_seq = $request->campaign_seq;
            if (isset($newPhoto)) {
                if ($oldPhoto != null) {
                    unlink($oldPhoto);
                    $path = 'backend/uploads/campaign/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = CampaignFileConfig::createConfig($newPhoto,$path,$imageResize->campaign_image_width,$imageResize->campaign_image_height);
                    $file_save = $file->send($config);
                    $campaign->campaign_image = $config->createFile();
                    $campaign->save();
                } else {
                    $path = 'backend/uploads/campaign/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = CampaignFileConfig::createConfig($newPhoto,$path,$imageResize->campaign_image_width,$imageResize->campaign_image_height);
                    $file_save = $file->send($config);
                    $campaign->campaign_image = $config->createFile();
                    $campaign->save();
                }
            } else {
                $campaign->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $campaign = Campaign::find($id);
            $photo = $campaign->campaign_image;
            if (isset($photo)) {
                unlink($photo);
                $campaign->delete();
            } else {
                $campaign->delete();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    // Campaign Sortable

    /**
     * @throws Exception
     */
    public function campaignSortable(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->item as $key => $value) {
                $campaign = Campaign::find(intval($value));
                $campaign->campaign_seq = intval($key);
                $campaign->save();
            }
            echo true;
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
    private static function makeSeq(): int|string
    {
        $control = Campaign::latest('id')->first();
        if (!$control){
            return "1";
        }else{
            return strval($control->team_seq + 1);
        }
    }
}
