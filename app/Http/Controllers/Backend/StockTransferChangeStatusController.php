<?php

namespace App\Http\Controllers\Backend;

use App\Component\changeStockTransferStatus\CreateChangeStatus;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\StockTransfer;
use App\Models\StockTransferItem;
use App\Models\Variant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Payload;



class StockTransferChangeStatusController extends Controller
{


    public function __construct(private CreateChangeStatus $createChangeStatus,
    private StockTransfer $stockTransfer)
    {
    }

    /**
     * @throws \Exception
     */
    public function __invoke(Request $request)
    {
        DB::beginTransaction();
        try {
            if($request->item === 'cancel'){
                $stockTransfer = $this->stockTransfer->where('id',$request->stockTransferId)->first();
                $stockTransferItem = StockTransferItem::where('stock_transfer_id',$request->stockTransferId)->get();
                if (isset($stockTransferItem)){
                    foreach ($stockTransferItem as $item){
                        $variant = Variant::find($item->variant_id);
                        $variant->variant_quantity += $item->product_quantity;
                        $variant->save();
                        $product = Product::find($variant->product_id);
                        $product->product_quantity += $item->product_quantity;
                        $product->save();
                    }
                }
                $stockTransfer->isCanceled = '1';
                $stockTransfer->status = "12";
                $stockTransfer->save();
                return response()->json('Success');
            }
            $this->createChangeStatus->handle($request);
            DB::commit();
            return response()->json('Success');
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }


}
