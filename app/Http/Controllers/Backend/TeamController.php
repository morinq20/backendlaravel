<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\FileManager\FileSaver;
use App\Http\Services\FileManager\TeamFileConfig;
use App\Http\Traits\FolderControlTrait;
use App\Models\ImageResize;
use App\Models\Team;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeamController extends Controller
{
    use FolderControlTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $teams = Team::orderBy('team_seq','ASC')->get();
        return response()->json($teams);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $team = new Team();
            $team->team_name = $request->team_name;
            $team->team_surname = $request->team_surname;
            $team->team_phone = $request->team_phone;
            $team->team_phone_dial_code = $request->teamPhoneDialCode;
            $team->team_email = $request->team_email;
//            $team->team_fax = $request->team_fax;
            $team->team_facebook = $request->team_facebook;
            $team->team_twitter = $request->team_twitter;
            $team->team_linkedin = $request->team_linkedin;
            $team->team_instagram = $request->team_instagram;
            $team->team_description = strip_tags($request->team_description);
            $team->team_location = strip_tags($request->team_location);
            $team->team_position = $request->team_position;
            $team->team_practice_area = $request->team_practice_area;
            $team->team_experience = $request->team_experience;
            $team->team_job = $request->team_job;
//            $team->team_seq = $request->team_seq;
            $team->team_seq = self::makeSeq();
            $team->country_id = $request->country_id;
            $team->city_id = $request->city_id;
            if (isset($request->team_image)) {
                $path = 'backend/uploads/team/';
                $this->folderIfNotExists($path);
                $file = FileSaver::getInstance();
                $config = TeamFileConfig::createConfig($request->team_image,$path,$imageResize->team_image_width,$imageResize->team_image_height);
                $file_save = $file->send($config);
                if (isset($file_save))
                {
                    $team->team_image = $config->createFile();
                }
                $team->save();
            } else {
                $team->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $team = Team::find($id);
        return response()->json($team);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $newPhoto = $request->new_team_image;
            $oldPhoto = $request->team_image;
            $team = Team::find($id);
            $team->team_name = $request->team_name;
            $team->team_surname = $request->team_surname;
            $team->team_phone = $request->team_phone;
            $team->team_phone_dial_code = $request->teamPhoneDialCode;
            $team->team_email = $request->team_email;
//            $team->team_fax = $request->team_fax;
            $team->team_facebook = $request->team_facebook;
            $team->team_twitter = $request->team_twitter;
            $team->team_linkedin = $request->team_linkedin;
            $team->team_instagram = $request->team_instagram;
            $team->team_description = strip_tags($request->team_description);
            $team->team_location = strip_tags($request->team_location);
            $team->team_position = $request->team_position;
            $team->team_practice_area = $request->team_practice_area;
            $team->team_experience = $request->team_experience;
            $team->team_job = $request->team_job;
//            $team->team_seq = $request->team_seq;
            $team->country_id = $request->country_id;
            $team->city_id = $request->city_id;
            if (isset($newPhoto)) {
                if ($oldPhoto != null) {
                    unlink($oldPhoto);
                    $path = 'backend/uploads/team/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = TeamFileConfig::createConfig($newPhoto,$path,$imageResize->team_image_width,$imageResize->team_image_height);
                    $file_save = $file->send($config);
                    $team->team_image = $config->createFile();
                    $team->save();
                } else {
                    $path = 'backend/uploads/team/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = TeamFileConfig::createConfig($newPhoto,$path,$imageResize->team_image_width,$imageResize->team_image_height);
                    $file_save = $file->send($config);
                    $team->team_image = $config->createFile();
                    $team->save();
                }
            } else {
                $team->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $team = Team::find($id);
            $photo = $team->team_image;
            if (isset($photo)) {
                unlink($photo);
                $team->delete();
            } else {
                $team->delete();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    // Team Sortable

    /**
     * @throws Exception
     */
    public function teamSortable(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->item as $key => $value) {
                $team = Team::find(intval($value));
                $team->team_seq = intval($key);
                $team->save();
            }
            echo true;
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }


    private static function makeSeq(): int|string
    {
        $control = Team::latest('id')->first();
        if (!$control){
            return "1";
        }else{
            return strval($control->team_seq + 1);
        }
    }
}
