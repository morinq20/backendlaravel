<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\FileManager\AboutUsFileConfig;
use App\Http\Services\FileManager\FileSaver;
use App\Http\Traits\FolderControlTrait;
use App\Models\AboutUs;
use App\Models\ImageResize;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AboutUsController extends Controller
{
    use FolderControlTrait;

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $aboutUs = AboutUs::orderBy('id','DESC')->get();
        return response()->json($aboutUs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $aboutUs = new AboutUs();
            $aboutUs->about_us_title = $request->about_us_title;
            $aboutUs->about_us_status = $request->about_us_status;
            $aboutUs->about_us_description = strip_tags($request->about_us_description);
            if (isset($request->about_us_image)) {
                $path = 'backend/uploads/about_us/';
                $this->folderIfNotExists($path);
                $file = FileSaver::getInstance();
                $config = AboutUsFileConfig::createConfig($request->about_us_image,$path,$imageResize->about_us_image_width,$imageResize->about_us_image_height);
                $file_save = $file->send($config);
                if (isset($file_save))
                {
                    $aboutUs->about_us_image = $config->createFile();
                }
                $aboutUs->save();
            } else {
                $aboutUs->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $aboutUs = AboutUs::find($id);
        return response()->json($aboutUs);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $newPhoto = $request->new_about_us_image;
            $oldPhoto = $request->about_us_image;
            $aboutUs = AboutUs::find($id);
            $aboutUs->about_us_title = $request->about_us_title;
            $aboutUs->about_us_status = $request->about_us_status;
            $aboutUs->about_us_description = strip_tags($request->about_us_description);
            if (isset($newPhoto)) {
                if ($oldPhoto != null) {
                    unlink($oldPhoto);
                    $path = 'backend/uploads/about_us/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = AboutUsFileConfig::createConfig($newPhoto,$path,$imageResize->about_us_image_width,$imageResize->about_us_image_height);
                    $file_save = $file->send($config);
                    $aboutUs->about_us_image = $config->createFile();
                    $aboutUs->save();
                } else {
                    $path = 'backend/uploads/about_us/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = AboutUsFileConfig::createConfig($newPhoto,$path,$imageResize->about_us_image_width,$imageResize->about_us_image_height);
                    $file_save = $file->send($config);
                    $aboutUs->about_us_image = $config->createFile();
                    $aboutUs->save();
                }
            } else {
                $aboutUs->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $aboutUs = AboutUs::find($id);
            $photo = $aboutUs->about_us_image;
            if (isset($photo)) {
                unlink($photo);
                $aboutUs->delete();
            } else {
                $aboutUs->delete();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
