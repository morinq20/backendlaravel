<?php

namespace App\Http\Controllers\Backend;

use App\Component\Excel\TableHandler;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExcelController extends Controller
{
    public function __construct(private TableHandler $tableHandler)
    {
    }

    public function __invoke()
    {
        return $this->tableHandler->handle();
    }
}
