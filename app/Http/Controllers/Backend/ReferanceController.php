<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\FileManager\FileSaver;
use App\Http\Services\FileManager\ReferanceFileConfig;
use App\Http\Traits\FolderControlTrait;
use App\Models\ImageResize;
use App\Models\Referance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReferanceController extends Controller
{
    use FolderControlTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $referances = Referance::orderBy('referance_seq','ASC')->get();
        return response()->json($referances);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $referance = new Referance();
            $referance->referance_title = $request->referance_title;
            $referance->referance_status = $request->referance_status;
            $referance->referance_description = strip_tags($request->referance_description);
            $referance->referance_seq = self::makeSeq();
            if (isset($request->referance_image)) {
                $path = 'backend/uploads/referance/';
                $this->folderIfNotExists($path);
                $file = FileSaver::getInstance();
                $config = ReferanceFileConfig::createConfig($request->referance_image,$path,$imageResize->referance_image_width,$imageResize->referance_image_height);
                $file_save = $file->send($config);
                if (isset($file_save))
                {
                    $referance->referance_image = $config->createFile();
                }
                $referance->save();
            } else {
                $referance->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $referance = Referance::find($id);
        return response()->json($referance);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $newPhoto = $request->new_referance_image;
            $oldPhoto = $request->referance_image;
            $referance = Referance::find($id);
            $referance->referance_title = $request->referance_title;
            $referance->referance_status = $request->referance_status;
            $referance->referance_description = strip_tags($request->referance_description);
//            $referance->referance_seq = $request->referance_seq;
            if (isset($newPhoto)) {
                if ($oldPhoto != null) {
                    unlink($oldPhoto);
                    $path = 'backend/uploads/referance/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = ReferanceFileConfig::createConfig($newPhoto,$path,$imageResize->referance_image_width,$imageResize->referance_image_height);
                    $file_save = $file->send($config);
                    $referance->referance_image = $config->createFile();
                    $referance->save();
                } else {
                    $path = 'backend/uploads/referance/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = ReferanceFileConfig::createConfig($newPhoto,$path,$imageResize->referance_image_width,$imageResize->referance_image_height);
                    $file_save = $file->send($config);
                    $referance->referance_image = $config->createFile();
                    $referance->save();
                }
            } else {
                $referance->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $referance = Referance::find($id);
            $photo = $referance->referance_image;
            if (isset($photo)) {
                unlink($photo);
                $referance->delete();
            } else {
                $referance->delete();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    // Referance Sortable

    /**
     * @throws \Exception
     */
    public function referanceSortable(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->item as $key => $value) {
                $referance = Referance::find(intval($value));
                $referance->referance_seq = intval($key);
                $referance->save();
            }
            echo true;
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    private static function makeSeq(): int|string
    {
        $control = Referance::latest('id')->first();
        if (!$control){
            return "1";
        }else{
            return strval($control->team_seq + 1);
        }
    }
}
