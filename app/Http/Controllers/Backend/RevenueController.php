<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\FileService\FileSaver;
use App\Models\Revenue;
use App\Models\RevenueDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RevenueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $revenue = Revenue::with('accountingStatus')->orderBy('id','DESC')->get();
        return response()->json($revenue);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'revenue_title' => 'required',
                'accounting_status_id' => 'required',
            ]);
            $revenue = new Revenue();
            $revenue->revenue_title = $request->revenue_title;
            $revenue->revenue_date = $request->revenue_date;
            $revenue->revenue_tax = $request->revenue_tax;
            $revenue->revenue_description = strip_tags($request->revenue_description);
            $revenue->accounting_status_id = $request->accounting_status_id;
            $revenue->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $revenue = Revenue::find($id);
        return response()->json($revenue);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'revenue_title' => 'required',
                'accounting_status_id' => 'required',
            ]);
            $revenue = Revenue::find($id);
            $revenue->revenue_title = $request->revenue_title;
            $revenue->revenue_date = $request->revenue_date;
            $revenue->revenue_tax = $request->revenue_tax;
            $revenue->revenue_description = strip_tags($request->revenue_description);
            $revenue->accounting_status_id = $request->accounting_status_id;
            $revenue->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $revenue = Revenue::find($id);
            $revenue->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    // Revenue Detail Store

    /**
     * @throws \Exception
     */
    public function storeRevenueDetail(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            foreach ($requestAll as $item){
                $revenueDetail = new RevenueDetail();
                $revenueDetail->revenue_id = $id;
                $revenueDetail->quantity = $item['quantity'];
                $revenueDetail->amount = $item['amount'];
                $revenueDetail->total_amount = $item['total_amount'];
                $revenueDetail->tax = $item['tax'];
                $revenueDetail->total_tax = $item['total_tax'];
                $revenueDetail->total = $item['total'];
                $revenueDetail->description = $item['description'];
                $revenueDetail->invoice_no = $item['invoice_no'];
                $revenueDetail->invoice_date = $item['invoice_date'];
                $revenueDetail->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    // Revenue Detail Show
    public function showRevenueDetail(Request $request,$id): \Illuminate\Http\JsonResponse
    {
        $revenueDetails = RevenueDetail::where('revenue_id',$id)->get();
        return response()->json($revenueDetails);
    }

    // Revenue Detail Update

    /**
     * @throws \Exception
     */
    public function updateRevenueDetail(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $allRequest = $request->all();
            foreach ($allRequest as $item){
                $control = RevenueDetail::find($item['id']);
                $control->revenue_id = $id;
                $control->quantity = $item['quantity'];
                $control->amount = $item['amount'];
                $control->total_amount = $item['total_amount'];
                $control->tax = $item['tax'];
                $control->total_tax = $item['total_tax'];
                $control->total = $item['total'];
                $control->description = $item['description'];
                $control->invoice_no = $item['invoice_no'];
                $control->invoice_date = $item['invoice_date'];
                $control->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * @throws \Exception
     */
    public function deleteRevenueDetail($id)
    {
        DB::beginTransaction();
        try {
            $findRevenueDetail = RevenueDetail::find($id);
            $findRevenueDetail->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }


}
