<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ShippingCompanyPostType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShippingCompanyPostTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
//        $shippingCompanyDeci = ShippingCompanyDeci::with('shippingCompany')->orderBy('id','DESC')->get();
        $shippingCompanyPostType = DB::table('shipping_company_post_type')
            ->select('shipping_company_post_type.*','shipping_company.shipping_company_name','shipping_company.shipping_company_image')
            ->join('shipping_company','shipping_company_post_type.shipping_company_id','=','shipping_company.id')
            ->orderBy('shipping_company_post_type.id','DESC')->get();
        return response()->json($shippingCompanyPostType);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $shippingCompanyPostType = new ShippingCompanyPostType();
            $shippingCompanyPostType->shipping_company_id = $request->shipping_company_id;
            $shippingCompanyPostType->post_type = $request->post_type;
            $shippingCompanyPostType->post_type_price = $request->post_type_price;
            $shippingCompanyPostType->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $shippingCompanyPostType = ShippingCompanyPostType::find($id);
        return response()->json($shippingCompanyPostType);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $shippingCompanyPostType = ShippingCompanyPostType::find($id);
            $shippingCompanyPostType->shipping_company_id = $request->shipping_company_id;
            $shippingCompanyPostType->post_type = $request->post_type;
            $shippingCompanyPostType->post_type_price = $request->post_type_price;
            $shippingCompanyPostType->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $shippingCompanyPostType = ShippingCompanyPostType::find($id);
            $shippingCompanyPostType->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }
}
