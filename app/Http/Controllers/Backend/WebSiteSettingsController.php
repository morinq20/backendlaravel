<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\FileManager\FileSaver;
use App\Http\Services\FileManager\IconSettingsFileConfig;
use App\Http\Services\FileManager\WebSiteSettingsFileConfig;
use App\Http\Traits\FolderControlTrait;
use App\Models\ImageResize;
use App\Models\WebSiteSettings;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WebSiteSettingsController extends Controller
{
    use FolderControlTrait;

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $websiteSettings = WebSiteSettings::find($id);
        return response()->json($websiteSettings);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $control = WebSiteSettings::first();
            if ($control == null) {
                $websiteSettings = new WebSiteSettings();
                $imageResize = ImageResize::first();
                $newPhoto = $request->new_website_image;
                $oldPhoto = $request->website_image;
                $newIcon = $request->new_website_icon_image;
                $oldIcon = $request->website_icon_image;
                $websiteSettings->id = 0;
                $websiteSettings->title = $request->title;
                $websiteSettings->description = strip_tags($request->description);
                $websiteSettings->phone = $request->phone;
                $websiteSettings->fax = $request->fax;
                $websiteSettings->email = $request->email;
                $websiteSettings->address = strip_tags($request->address);
                $websiteSettings->country = $request->country;
                $websiteSettings->city = $request->city;
                $websiteSettings->zipcode = $request->zipcode;
                $websiteSettings->facebook = $request->facebook;
                $websiteSettings->twitter = $request->twitter;
                $websiteSettings->youtube = $request->youtube;
                $websiteSettings->instagram = $request->instagram;
                $websiteSettings->linkedin = $request->linkedin;
                $websiteSettings->copyright = $request->copyright;
                $websiteSettings->tax = $request->tax;
                $websiteSettings->privacy_policy = strip_tags($request->privacy_policy);
                $websiteSettings->terms_conditions = strip_tags($request->terms_conditions);
                if (isset($newPhoto)) {
                    if ($oldPhoto != null) {
                        unlink($oldPhoto);
                        $path = 'backend/uploads/website_settings/logo/';
                        $this->folderIfNotExists($path);
                        $file = FileSaver::getInstance();
                        $config = WebSiteSettingsFileConfig::createConfig($newPhoto,$path,$imageResize->website_settings_image_width,$imageResize->website_settings_image_height);
                        $file_save = $file->send($config);
                        $websiteSettings->website_image = $config->createFile();
                    } else {
                        $path = 'backend/uploads/website_settings/logo/';
                        $this->folderIfNotExists($path);
                        $file = FileSaver::getInstance();
                        $config = WebSiteSettingsFileConfig::createConfig($newPhoto,$path,$imageResize->website_settings_image_width,$imageResize->website_settings_image_height);
                        $file_save = $file->send($config);
                        $websiteSettings->website_image = $config->createFile();
                    }
                }
                if (isset($newIcon)) {
                    if ($oldIcon != null) {
                        unlink($oldIcon);
                        $path = 'backend/uploads/website_settings/icon/';
                        $this->folderIfNotExists($path);
                        $file = FileSaver::getInstance();
                        $config = IconSettingsFileConfig::createConfig($newIcon,$path,20,20);
                        $file_save = $file->send($config);
                        $websiteSettings->website_icon_image = $config->createFile();
                    } else {
                        $path = 'backend/uploads/website_settings/icon/';
                        $this->folderIfNotExists($path);
                        $file = FileSaver::getInstance();
                        $config = IconSettingsFileConfig::createConfig($newIcon,$path,20,20);
                        $file_save = $file->send($config);
                        $websiteSettings->website_icon_image = $config->createFile();
                    }
                }
                $websiteSettings->save();
            } else {
                $imageResize = ImageResize::first();
                $newPhoto = $request->new_website_image;
                $oldPhoto = $request->website_image;
                $newIcon = $request->new_website_icon_image;
                $oldIcon = $request->website_icon_image;
                $websiteSettings = WebSiteSettings::first();
                $websiteSettings->title = $request->title;
                $websiteSettings->description = strip_tags($request->description);
                $websiteSettings->phone = $request->phone;
                $websiteSettings->fax = $request->fax;
                $websiteSettings->email = $request->email;
                $websiteSettings->address = strip_tags($request->address);
                $websiteSettings->country = $request->country;
                $websiteSettings->city = $request->city;
                $websiteSettings->zipcode = $request->zipcode;
                $websiteSettings->facebook = $request->facebook;
                $websiteSettings->twitter = $request->twitter;
                $websiteSettings->youtube = $request->youtube;
                $websiteSettings->instagram = $request->instagram;
                $websiteSettings->linkedin = $request->linkedin;
                $websiteSettings->copyright = $request->copyright;
                $websiteSettings->tax = $request->tax;
                $websiteSettings->privacy_policy = strip_tags($request->privacy_policy);
                $websiteSettings->terms_conditions = strip_tags($request->terms_conditions);
                if (isset($newPhoto)) {
                    if ($oldPhoto != null) {
                        unlink($oldPhoto);
                        $path = 'backend/uploads/website_settings/logo/';
                        $this->folderIfNotExists($path);
                        $file = FileSaver::getInstance();
                        $config = WebSiteSettingsFileConfig::createConfig($newPhoto,$path,$imageResize->website_settings_image_width,$imageResize->website_settings_image_height);
                        $file_save = $file->send($config);
                        $websiteSettings->website_image = $config->createFile();
                    } else {
                        $path = 'backend/uploads/website_settings/logo/';
                        $this->folderIfNotExists($path);
                        $file = FileSaver::getInstance();
                        $config = WebSiteSettingsFileConfig::createConfig($newPhoto,$path,$imageResize->website_settings_image_width,$imageResize->website_settings_image_height);
                        $file_save = $file->send($config);
                        $websiteSettings->website_image = $config->createFile();
                    }
                }
                if (isset($newIcon)) {
                    if ($oldIcon != null) {
                        unlink($oldIcon);
                        $path = 'backend/uploads/website_settings/icon/';
                        $this->folderIfNotExists($path);
                        $file = FileSaver::getInstance();
                        $config = IconSettingsFileConfig::createConfig($newIcon,$path,20,20);
                        $file_save = $file->send($config);
                        $websiteSettings->website_icon_image = $config->createFile();
                    } else {
                        $path = 'backend/uploads/website_settings/icon/';
                        $this->folderIfNotExists($path);
                        $file = FileSaver::getInstance();
                        $config = IconSettingsFileConfig::createConfig($newIcon,$path,20,20);
                        $file_save = $file->send($config);
                        $websiteSettings->website_icon_image = $config->createFile();
                    }
                }
                $websiteSettings->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
