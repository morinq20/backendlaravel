<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\FileManager\FileSaver;
use App\Http\Services\FileManager\IconSettingsFileConfig;
use App\Http\Services\FileManager\PanelSettingsFileConfig;
use App\Http\Traits\FolderControlTrait;
use App\Models\Customer;
use App\Models\ImageResize;
use App\Models\PanelSettings;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PanelSettingsController extends Controller
{
    use FolderControlTrait;

    public function index()
    {
        $array = [];
        $panelSettings = PanelSettings::first();
        $panelSettings['user'] = auth()->user();
        $admin = User::where('role_id','!=',null)->count();
        $user = User::where('role_id','=',null)->count();
        $customer = Customer::all()->count();
        $product = Product::all()->count();
        $array['Admin'] = $admin;
        $array['User'] = $user;
        $array['Customer'] = $customer;
        $array['Product'] = $product;
        $panelSettings['dashboard'] = $array;
        return response()->json($panelSettings);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $panelSettings = PanelSettings::find($id);
        return response()->json($panelSettings);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $control = PanelSettings::first();
            if ($control == null) {
                $panelSettings = new PanelSettings();
                $imageResize = ImageResize::first();
                $newPhoto = $request->new_panel_image;
                $oldPhoto = $request->panel_image;
                $newIcon = $request->new_panel_icon_image;
                $oldIcon = $request->panel_icon_image;
                $panelSettings->id = 0;
                $panelSettings->title = $request->title;
                $panelSettings->description = strip_tags($request->description);
                $panelSettings->phone = $request->phone;
                $panelSettings->fax = $request->fax;
                $panelSettings->email = $request->email;
                $panelSettings->address = strip_tags($request->address);
                $panelSettings->country = $request->country;
                $panelSettings->city = $request->city;
                $panelSettings->zipcode = $request->zipcode;
                $panelSettings->facebook = $request->facebook;
                $panelSettings->twitter = $request->twitter;
                $panelSettings->youtube = $request->youtube;
                $panelSettings->instagram = $request->instagram;
                $panelSettings->linkedin = $request->linkedin;
                $panelSettings->copyright = $request->copyright;
                $panelSettings->tax = $request->tax;
                if (isset($newPhoto)) {
                    if ($oldPhoto != null) {
                        unlink($oldPhoto);
                        $path = 'backend/uploads/panel_settings/logo/';
                        $this->folderIfNotExists($path);
                        $file = FileSaver::getInstance();
                        $config = PanelSettingsFileConfig::createConfig($newPhoto,$path,$imageResize->panel_settings_image_width,$imageResize->panel_settings_image_height);
                        $file_save = $file->send($config);
                        if (isset($file_save))
                        {
                            $panelSettings->panel_image = $config->createFile();
                        }
                    } else {
                        $path = 'backend/uploads/panel_settings/logo/';
                        $this->folderIfNotExists($path);
                        $file = FileSaver::getInstance();
                        $config = PanelSettingsFileConfig::createConfig($newPhoto,$path,$imageResize->panel_settings_image_width,$imageResize->panel_settings_image_height);
                        $file_save = $file->send($config);
                        if (isset($file_save))
                        {
                            $panelSettings->panel_image = $config->createFile();
                        }
                    }
                }
                if (isset($newIcon)) {
                    if ($oldIcon != null) {
                        unlink($oldIcon);
                        $path = 'backend/uploads/panel_settings/icon/';
                        $this->folderIfNotExists($path);
                        $file = FileSaver::getInstance();
                        $config = IconSettingsFileConfig::createConfig($newIcon,$path,20,20);
                        $file_save = $file->send($config);
                        if (isset($file_save))
                        {
                            $panelSettings->panel_icon_image = $config->createFile();
                        }
                    } else {
                        $path = 'backend/uploads/panel_settings/icon/';
                        $this->folderIfNotExists($path);
                        $file = FileSaver::getInstance();
                        $config = IconSettingsFileConfig::createConfig($newIcon,$path,20,20);
                        $file_save = $file->send($config);
                        if (isset($file_save))
                        {
                            $panelSettings->panel_icon_image = $config->createFile();
                        }
                    }
                }
                $panelSettings->save();
            } else {
                $imageResize = ImageResize::first();
                $newPhoto = $request->new_panel_image;
                $oldPhoto = $request->panel_image;
                $newIcon = $request->new_panel_icon_image;
                $oldIcon = $request->panel_icon_image;
                $panelSettings = PanelSettings::first();
                $panelSettings->title = $request->title;
                $panelSettings->description = strip_tags($request->description);
                $panelSettings->phone = $request->phone;
                $panelSettings->fax = $request->fax;
                $panelSettings->email = $request->email;
                $panelSettings->address = strip_tags($request->address);
                $panelSettings->country = $request->country;
                $panelSettings->city = $request->city;
                $panelSettings->zipcode = $request->zipcode;
                $panelSettings->facebook = $request->facebook;
                $panelSettings->twitter = $request->twitter;
                $panelSettings->youtube = $request->youtube;
                $panelSettings->instagram = $request->instagram;
                $panelSettings->linkedin = $request->linkedin;
                $panelSettings->copyright = $request->copyright;
                $panelSettings->tax = $request->tax;
                if (isset($newPhoto)) {
                    if ($oldPhoto != null) {
                        unlink($oldPhoto);
                        $path = 'backend/uploads/panel_settings/logo/';
                        $this->folderIfNotExists($path);
                        $file = FileSaver::getInstance();
                        $config = PanelSettingsFileConfig::createConfig($newPhoto,$path,$imageResize->panel_settings_image_width,$imageResize->panel_settings_image_height);
                        $file_save = $file->send($config);
                        if (isset($file_save))
                        {
                            $panelSettings->panel_image = $config->createFile();
                        }

                    } else {
                        $path = 'backend/uploads/panel_settings/logo/';
                        $this->folderIfNotExists($path);
                        $file = FileSaver::getInstance();
                        $config = PanelSettingsFileConfig::createConfig($newPhoto,$path,$imageResize->panel_settings_image_width,$imageResize->panel_settings_image_height);
                        $file_save = $file->send($config);
                        if (isset($file_save))
                        {
                            $panelSettings->panel_image = $config->createFile();
                        }

                    }
                }
                if (isset($newIcon)) {
                    if ($oldIcon != null) {
                        unlink($oldIcon);
                        $path = 'backend/uploads/panel_settings/icon/';
                        $this->folderIfNotExists($path);
                        $file = FileSaver::getInstance();
                        $config = IconSettingsFileConfig::createConfig($newIcon,$path,20,20);
                        $file_save = $file->send($config);
                        if (isset($file_save))
                        {
                            $panelSettings->panel_icon_image = $config->createFile();
                        }

                    } else {
                        $path = 'backend/uploads/panel_settings/icon/';
                        $this->folderIfNotExists($path);
                        $file = FileSaver::getInstance();
                        $config = IconSettingsFileConfig::createConfig($newIcon,$path,20,20);
                        $file_save = $file->send($config);
                        if (isset($file_save))
                        {
                            $panelSettings->panel_icon_image = $config->createFile();
                        }

                    }
                }
                $panelSettings->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

}
