<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\EmailManager\Manager\Register\RegisterMailConfig;
use App\Http\Services\FileManager\AdminFileConfig;
use App\Http\Services\FileManager\FileSaver;
use App\Http\Traits\FolderControlTrait;
use App\Models\Cities;
use App\Models\Country;
use App\Models\ImageResize;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    use FolderControlTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $admins = User::where('role_id','!=',null)->with('role')->orderBy('last_seen','DESC')->get();
        return response()->json($admins);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $country = Country::where('id',$request->country_id)->first();
            $city = Cities::where('id',$request->city_id)->first();
            $imageResize = ImageResize::first();
            $admin = new User();
            $admin->name = $request->name;
            $admin->surname = $request->surname;
            $admin->email = $request->email;
            $admin->phone = $request->phone;
            $admin->password = Hash::make($request->password);
            $admin->country = $country->country;
            $admin->city = $city->city;
            $admin->country_id = $request->country_id;
            $admin->city_id = $request->city_id;
            $admin->phone_dial_code = $request->phoneDialCode;
            $admin->district = $request->district;
            $admin->address = strip_tags($request->address);
            $admin->gender = $request->gender;
            $admin->role_id = $request->role;
            $admin->warehouse_id = $request->warehouse;
            if (isset($request->photo)) {
                $path = 'backend/uploads/admin/';
                $this->folderIfNotExists($path);
                $file = FileSaver::getInstance();
                $config = AdminFileConfig::createConfig($request->photo,$path,$imageResize->admin_image_width,$imageResize->admin_image_height);
                $file_save = $file->send($config);
                if (isset($file_save))
                {
                    $admin->photo = $config->createFile();
                }
                $admin->save();
            } else {
                $admin->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $admin = DB::table('users')->where('id',$id)->first();
        return response()->json($admin);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $country = Country::where('id',$request->country_id)->first();
            $city = Cities::where('id',$request->city_id)->first();
            $imageResize = ImageResize::first();
            $newPhoto = $request->newphoto;
            $oldPhoto = $request->photo;
            $admin = User::find($id);
            $admin->name = $request->name;
            $admin->surname = $request->surname;
            $admin->email = $request->email;
            $admin->phone = $request->phone;
            $admin->password = Hash::make($request->password);
            $admin->country = $country->country;
            $admin->city = $city->city;
            $admin->country_id = $request->country_id;
            $admin->city_id = $request->city_id;
            $admin->phone_dial_code = $request->phoneDialCode;
            $admin->district = $request->district;
            $admin->address = strip_tags($request->address);
            $admin->gender = $request->gender;
            $admin->role_id = $request->role;
            $admin->warehouse_id = $request->warehouse;
            if (isset($newPhoto)) {
                if ($oldPhoto != null) {
                    unlink($oldPhoto);
                    $path = 'backend/uploads/admin/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = AdminFileConfig::createConfig($newPhoto,$path,$imageResize->admin_image_width,$imageResize->admin_image_height);
                    $file_save = $file->send($config);
                    $admin->photo = $config->createFile();
                    $admin->save();
                } else {
                    $path = 'backend/uploads/admin/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = AdminFileConfig::createConfig($newPhoto,$path,$imageResize->admin_image_width,$imageResize->admin_image_height);
                    $file_save = $file->send($config);
                    $admin->photo = $config->createFile();
                    $admin->save();
                }
            } else {
                $admin->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $admin = User::find($id);
            $photo = $admin->photo;
            if (isset($photo)) {
                unlink($photo);
                $admin->delete();
            } else {
                $admin->delete();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }


    public function userNotification($id)
    {
        $userNotification = DB::table('notifications')->where('notifiable_id',$id)->orderBy('created_at','desc')->get();
        foreach ($userNotification as $u){
            $a = explode(":",$u->data);
            $b = explode("}",$a[1]);
            $c = explode('"',$b[0]);
            $u->data = $c[1];
        }
        return response()->json($userNotification);
    }


    public function offline($id)
    {
        $user = User::find($id);
        $user->isOnline = 0;
        $user->save();
    }

}
