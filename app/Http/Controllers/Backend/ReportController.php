<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\Utils\Converter\Converter;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderStatus;
use App\Models\Pallet;
use App\Models\Product;
use App\Models\StockTransfer;
use App\Models\StockTransferItem;
use App\Models\StockTransferStatus;
use App\Models\User;
use App\Models\Variant;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function __construct(private StockTransferStatus $stockTransferStatus,
                                private StockTransfer $stockTransfer, private OrderStatus $orderStatus)
    {
    }

    public function reportStockTransfer(Request $request): \Illuminate\Http\JsonResponse
    {
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $stockTransfer = [];
        $allStockTransfer = StockTransfer::whereBetween('start_transfer_date',[$startDate,$endDate])->orderBy('id','DESC')->get();
        foreach ($allStockTransfer as $key => $item) {
            $fromWarehouse = Warehouse::select('warehouse_name','warehouse_type')->where('id',$item->from_warehouse_id)->first();
            $toWarehouse = Warehouse::select('warehouse_name','warehouse_type')->where('id',$item->to_warehouse_id)->first();
            $pallet = Pallet::select('pallet_name')->where('id',$item->pallet_id)->first();
            $stockTransfer[$key]['code'] = $item->code;
            $stockTransfer[$key]['fromWarehouse_name'] = $fromWarehouse->warehouse_name;
            $stockTransfer[$key]['fromWarehouse_type'] = $fromWarehouse->warehouse_type;
            $stockTransfer[$key]['toWarehouse_name'] = $toWarehouse->warehouse_name;
            $stockTransfer[$key]['toWarehouse_type'] = $toWarehouse->warehouse_type;
            $stockTransfer[$key]['total'] = $item->total;
            $stockTransfer[$key]['total_product'] = $item->total_product;
            $stockTransfer[$key]['total_quantity'] = $item->total_quantity;
            $stockTransfer[$key]['total_process'] = $item->total_process;
            $stockTransfer[$key]['total_weight'] = $item->total_weight;
            $stockTransfer[$key]['pallet_count'] = $item->pallet_count;
            $stockTransfer[$key]['pallet_name'] = $pallet->pallet_name;
            $stockTransfer[$key]['start_transfer_date'] = $item->start_transfer_date;
            $stockTransfer[$key]['finish_transfer_date'] = $item->finish_transfer_date;
            $stockTransfer[$key]['status'] = $item->status;
            $stockTransfer[$key]['id'] = $item->id;
            $stockTransfer[$key]['status_text'] = $this->stockTransferStatus->where('status_id',$item->status)->get();
        }
        return response()->json($stockTransfer);
    }

    public function totalAdmin(): \Illuminate\Http\JsonResponse
    {
        $admin = User::where('role_id','!=',null)->count();
        return response()->json($admin);
    }

    public function totalUser(): \Illuminate\Http\JsonResponse
    {
        $user = User::where('role_id','=',null)->count();
        return response()->json($user);
    }

    public function totalCustomer(): \Illuminate\Http\JsonResponse
    {
        $customer = Customer::all()->count();
        return response()->json($customer);
    }

    public function totalProduct(): \Illuminate\Http\JsonResponse
    {
        $product = Product::all()->count();
        return response()->json($product);
    }

    public function reportMonthlyStockTransfer(Request $request)
    {
        $month = $request->month;
        $year = $request->year;
        $stockTransfer = [];
        $allStockTransfer = StockTransfer::where('month',$month)->where('year',$year)->orderBy('id','DESC')->get();
        foreach ($allStockTransfer as $key => $item) {
            $fromWarehouse = Warehouse::select('warehouse_name','warehouse_type')->where('id',$item->from_warehouse_id)->first();
            $toWarehouse = Warehouse::select('warehouse_name','warehouse_type')->where('id',$item->to_warehouse_id)->first();
            $pallet = Pallet::select('pallet_name')->where('id',$item->pallet_id)->first();
            $stockTransfer[$key]['code'] = $item->code;
            $stockTransfer[$key]['fromWarehouse_name'] = $fromWarehouse->warehouse_name;
            $stockTransfer[$key]['fromWarehouse_type'] = $fromWarehouse->warehouse_type;
            $stockTransfer[$key]['toWarehouse_name'] = $toWarehouse->warehouse_name;
            $stockTransfer[$key]['toWarehouse_type'] = $toWarehouse->warehouse_type;
            $stockTransfer[$key]['total'] = $item->total;
            $stockTransfer[$key]['total_product'] = $item->total_product;
            $stockTransfer[$key]['total_quantity'] = $item->total_quantity;
            $stockTransfer[$key]['total_process'] = $item->total_process;
            $stockTransfer[$key]['total_weight'] = $item->total_weight;
            $stockTransfer[$key]['pallet_count'] = $item->pallet_count;
            $stockTransfer[$key]['pallet_name'] = $pallet->pallet_name;
            $stockTransfer[$key]['start_transfer_date'] = $item->start_transfer_date;
            $stockTransfer[$key]['finish_transfer_date'] = $item->finish_transfer_date;
            $stockTransfer[$key]['status'] = $item->status;
            $stockTransfer[$key]['id'] = $item->id;
            $stockTransfer[$key]['status_text'] = $this->stockTransferStatus->where('status_id',$item->status)->get();
        }
        return response()->json($stockTransfer);
    }

    public function reportMonthlyOrder(Request $request): \Illuminate\Http\JsonResponse
    {
        $month = $request->month;
        $year = $request->year;
        $order = [];
        $allOrder = Order::where('month',$month)->where('year',$year)->orderBy('id','DESC')->get();
        foreach ($allOrder as $key => $item) {
            $fromWarehouse = Warehouse::select('warehouse_name','warehouse_type')->where('id',$item->from_warehouse_id)->first();
            $pallet = Pallet::select('pallet_name')->where('id',$item->pallet_id)->first();
            $customer = Customer::select('customer_name','customer_surname','customer_phone')->where('id',$item->customer_id)->first();
            $order[$key]['code'] = $item->code;
            $order[$key]['fromWarehouse_name'] = $fromWarehouse->warehouse_name;
            $order[$key]['fromWarehouse_type'] = $fromWarehouse->warehouse_type;
            $order[$key]['total'] = $item->total;
            $order[$key]['total_product'] = $item->total_product;
            $order[$key]['total_quantity'] = $item->total_quantity;
            $order[$key]['total_process'] = $item->total_process;
            $order[$key]['total_weight'] = $item->total_weight;
            $order[$key]['pallet_count'] = $item->pallet_count;
            $order[$key]['pallet_name'] = $pallet->pallet_name;
            $order[$key]['start_transfer_date'] = $item->start_transfer_date;
            $order[$key]['finish_transfer_date'] = $item->finish_transfer_date;
            $order[$key]['status'] = $item->status;
            $order[$key]['id'] = $item->id;
            $order[$key]['order_status'] = $this->orderStatus->where('status_id', $item->status)->get();
            $order[$key]['customer'] = $customer;
        }
        return response()->json($order);
    }

    public function reportGraphics(): \Illuminate\Http\JsonResponse
    {
        date_default_timezone_set('Europe/Istanbul');
        $datev2 = date('d-m-y');
        $date = date('d');
        $month = date('F');
        $year = date('Y');
        // Date Orders
        $createdDate = DB::table('orders')->where('date',$date)->where('month',$month)->where('year',$year)->where('status','=',0)->count('id');
        $preparedDate = DB::table('orders')->where('date',$date)->where('month',$month)->where('year',$year)->where('status','=',1)->count('id');
        $loadingDate = DB::table('orders')->where('date',$date)->where('month',$month)->where('year',$year)->where('status','=',2)->count('id');
        $roadDate = DB::table('orders')->where('date',$date)->where('month',$month)->where('year',$year)->where('status','=',3)->count('id');
        $arrivedDate = DB::table('orders')->where('date',$date)->where('month',$month)->where('year',$year)->where('status','=',4)->count('id');
        $deliveredDate = DB::table('orders')->where('date',$date)->where('month',$month)->where('year',$year)->where('status','=',5)->count('id');
        // Month Orders
        $createdMonth = DB::table('orders')->where('month',$month)->where('year',$year)->where('status','=',0)->count('id');
        $preparedMonth = DB::table('orders')->where('month',$month)->where('year',$year)->where('status','=',1)->count('id');
        $loadingMonth = DB::table('orders')->where('month',$month)->where('year',$year)->where('status','=',2)->count('id');
        $roadMonth = DB::table('orders')->where('month',$month)->where('year',$year)->where('status','=',3)->count('id');
        $arrivedMonth = DB::table('orders')->where('month',$month)->where('year',$year)->where('status','=',4)->count('id');
        $deliveredMonth = DB::table('orders')->where('month',$month)->where('year',$year)->where('status','=',5)->count('id');
        // Year Orders
        $createdYear = DB::table('orders')->where('year',$year)->where('status','=',0)->count('id');
        $preparedYear = DB::table('orders')->where('year',$year)->where('status','=',1)->count('id');
        $loadingYear = DB::table('orders')->where('year',$year)->where('status','=',2)->count('id');
        $roadYear = DB::table('orders')->where('year',$year)->where('status','=',3)->count('id');
        $arrivedYear = DB::table('orders')->where('year',$year)->where('status','=',4)->count('id');
        $deliveredYear = DB::table('orders')->where('year',$year)->where('status','=',5)->count('id');

        // Date Stock Transfers
        $createdDateTransfer = DB::table('stock_transfer')->where('date',$date)->where('month',$month)->where('year',$year)->where('status','=',0)->count('id');
        $preparedDateTransfer = DB::table('stock_transfer')->where('date',$date)->where('month',$month)->where('year',$year)->where('status','=',1)->count('id');
        $loadingDateTransfer = DB::table('stock_transfer')->where('date',$date)->where('month',$month)->where('year',$year)->where('status','=',2)->count('id');
        $roadDateTransfer = DB::table('stock_transfer')->where('date',$date)->where('month',$month)->where('year',$year)->where('status','=',3)->count('id');
        $arrivedDateTransfer = DB::table('stock_transfer')->where('date',$date)->where('month',$month)->where('year',$year)->where('status','=',4)->count('id');
        $deliveredDateTransfer = DB::table('stock_transfer')->where('date',$date)->where('month',$month)->where('year',$year)->where('status','=',5)->count('id');
        // Month Stock Transfers
        $createdMonthTransfer = DB::table('stock_transfer')->where('month',$month)->where('year',$year)->where('status','=',0)->count('id');
        $preparedMonthTransfer = DB::table('stock_transfer')->where('month',$month)->where('year',$year)->where('status','=',1)->count('id');
        $loadingMonthTransfer = DB::table('stock_transfer')->where('month',$month)->where('year',$year)->where('status','=',2)->count('id');
        $roadMonthTransfer = DB::table('stock_transfer')->where('month',$month)->where('year',$year)->where('status','=',3)->count('id');
        $arrivedMonthTransfer = DB::table('stock_transfer')->where('month',$month)->where('year',$year)->where('status','=',4)->count('id');
        $deliveredMonthTransfer = DB::table('stock_transfer')->where('month',$month)->where('year',$year)->where('status','=',5)->count('id');
        // Year Stock Transfers
        $createdYearTransfer = DB::table('stock_transfer')->where('year',$year)->where('status','=',0)->count('id');
        $preparedYearTransfer = DB::table('stock_transfer')->where('year',$year)->where('status','=',1)->count('id');
        $loadingYearTransfer = DB::table('stock_transfer')->where('year',$year)->where('status','=',2)->count('id');
        $roadYearTransfer = DB::table('stock_transfer')->where('year',$year)->where('status','=',3)->count('id');
        $arrivedYearTransfer = DB::table('stock_transfer')->where('year',$year)->where('status','=',4)->count('id');
        $deliveredYearTransfer = DB::table('stock_transfer')->where('year',$year)->where('status','=',5)->count('id');

        // Order Calculate Total
        // Date process total
        $dateTotal = DB::table('orders')->where('date',$date)->where('month',$month)->where('year',$year)->where('status','<',6)->sum('total');
        // Month process total
        $monthTotal = DB::table('orders')->where('month',$month)->where('year',$year)->where('status','<',6)->sum('total');
        // Year process total
        $yearTotal = DB::table('orders')->where('year',$year)->where('status','<',6)->sum('total');

        // Stock Transfer Calculate Total
        // Date process total
        $dateTotalTransfer = DB::table('stock_transfer')->where('date',$date)->where('month',$month)->where('year',$year)->where('status','<',6)->sum('total');
        // Month process total
        $monthTotalTransfer = DB::table('stock_transfer')->where('month',$month)->where('year',$year)->where('status','<',6)->sum('total');
        // Year process total
        $yearTotalTransfer = DB::table('stock_transfer')->where('year',$year)->where('status','<',6)->sum('total');

        // Order Calculate Products Total
        // Date Order
        $dateProductTotal = 0;
        $dateOrders = DB::table('orders')->where('date',$date)->where('month',$month)->where('year',$year)->where('status','<',6)->get();
        foreach ($dateOrders as $key => $order){
            $orderItems = OrderItem::where('orders_id',$order->id)->get();
            foreach ($orderItems as $orderItem) {
                $variant = Variant::find($orderItem->variant_id);
                if ($variant->variant_tax != null){
                    if ($variant->variant_discount_price != null){
                        $dateProductTotal += ((($variant->variant_discount_price * $variant->variant_tax) / 100) * $orderItem->product_quantity) + $order->total;
                    }else{
                        $dateProductTotal += ((($variant->variant_selling_price * $variant->variant_tax) / 100) * $orderItem->product_quantity) + $order->total;
                    }
                }else{
                    if ($variant->variant_discount_price != null){
                        $dateProductTotal += ($variant->variant_discount_price * $orderItem->product_quantity) + $order->total;
                    }else{
                        $dateProductTotal += ($variant->variant_selling_price * $orderItem->product_quantity) + $order->total;
                    }
                }
            }

        }
        // Month Order
        $monthProductTotal = 0;
        $monthOrders = DB::table('orders')->where('month',$month)->where('year',$year)->where('status','<',6)->get();
        foreach ($monthOrders as $key => $order){
            $orderItems = OrderItem::where('orders_id',$order->id)->get();
            foreach ($orderItems as $orderItem) {
                $variant = Variant::find($orderItem->variant_id);
                if ($variant->variant_tax != null){
                    if ($variant->variant_discount_price != null){
                        $monthProductTotal += ((($variant->variant_discount_price * $variant->variant_tax) / 100) * $orderItem->product_quantity) + $order->total;
                    }else{
                        $monthProductTotal += ((($variant->variant_selling_price * $variant->variant_tax) / 100) * $orderItem->product_quantity) + $order->total;
                    }
                }else{
                    if ($variant->variant_discount_price != null){
                        $monthProductTotal += ($variant->variant_discount_price * $orderItem->product_quantity) + $order->total;
                    }else{
                        $monthProductTotal += ($variant->variant_selling_price * $orderItem->product_quantity) + $order->total;
                    }
                }
            }

        }
        // Year Order
        $yearProductTotal = 0;
        $yearOrders = DB::table('orders')->where('year',$year)->where('status','<',6)->get();
        foreach ($yearOrders as $key => $order){
            $orderItems = OrderItem::where('orders_id',$order->id)->get();
            foreach ($orderItems as $orderItem) {
                $variant = Variant::find($orderItem->variant_id);
                if ($variant->variant_tax != null){
                    if ($variant->variant_discount_price != null){
                        $yearProductTotal += ((($variant->variant_discount_price * $variant->variant_tax) / 100) * $orderItem->product_quantity) + $order->total;
                    }else{
                        $yearProductTotal += ((($variant->variant_selling_price * $variant->variant_tax) / 100) * $orderItem->product_quantity) + $order->total;
                    }
                }else{
                    if ($variant->variant_discount_price != null){
                        $yearProductTotal += ($variant->variant_discount_price * $orderItem->product_quantity) + $order->total;
                    }else{
                        $yearProductTotal += ($variant->variant_selling_price * $orderItem->product_quantity) + $order->total;
                    }
                }
            }

        }
        // Stock Transfer Calculate Products Total
        // Date Stock Transfer
        $dateProductTotalTransfer = 0;
        $dateTransfer = DB::table('stock_transfer')->where('date',$date)->where('month',$month)->where('year',$year)->where('status','<',6)->get();
        foreach ($dateTransfer as $key => $transfer){
            $transferItems = StockTransferItem::where('stock_transfer_id',$transfer->id)->get();
            foreach ($transferItems as $transferItem) {
                $variant = Variant::find($transferItem->variant_id);
                if ($variant->variant_tax != null){
                    if ($variant->variant_discount_price != null){
                        $dateProductTotalTransfer += ((($variant->variant_discount_price * $variant->variant_tax) / 100) * $transferItem->product_quantity) + $transfer->total;
                    }else{
                        $dateProductTotalTransfer += ((($variant->variant_selling_price * $variant->variant_tax) / 100) * $transferItem->product_quantity) + $transfer->total;
                    }
                }else{
                    if ($variant->variant_discount_price != null){
                        $dateProductTotalTransfer += ($variant->variant_discount_price * $transferItem->product_quantity) + $transfer->total;
                    }else{
                        $dateProductTotalTransfer += ($variant->variant_selling_price * $transferItem->product_quantity) + $transfer->total;
                    }
                }
            }

        }
        // Month Stock Transfer
        $monthProductTotalTransfer = 0;
        $monthTransfer = DB::table('stock_transfer')->where('month',$month)->where('year',$year)->where('status','<',6)->get();
        foreach ($monthTransfer as $key => $transfer){
            $transferItems = StockTransferItem::where('stock_transfer_id',$transfer->id)->get();
            foreach ($transferItems as $transferItem) {
                $variant = Variant::find($transferItem->variant_id);
                if ($variant->variant_tax != null){
                    if ($variant->variant_discount_price != null){
                        $monthProductTotalTransfer += ((($variant->variant_discount_price * $variant->variant_tax) / 100) * $transferItem->product_quantity) + $transfer->total;
                    }else{
                        $monthProductTotalTransfer += ((($variant->variant_selling_price * $variant->variant_tax) / 100) * $transferItem->product_quantity) + $transfer->total;
                    }
                }else{
                    if ($variant->variant_discount_price != null){
                        $monthProductTotalTransfer += ($variant->variant_discount_price * $transferItem->product_quantity) + $transfer->total;
                    }else{
                        $monthProductTotalTransfer += ($variant->variant_selling_price * $transferItem->product_quantity) + $transfer->total;
                    }
                }
            }

        }
        // Year Stock Transfer
        $yearProductTotalTransfer = 0;
        $yearTransfer = DB::table('stock_transfer')->where('year',$year)->where('status','<',6)->get();
        foreach ($yearTransfer as $key => $transfer){
            $transferItems = StockTransferItem::where('stock_transfer_id',$transfer->id)->get();
            foreach ($transferItems as $transferItem) {
                $variant = Variant::find($transferItem->variant_id);
                if ($variant->variant_tax != null){
                    if ($variant->variant_discount_price != null){
                        $yearProductTotalTransfer += ((($variant->variant_discount_price * $variant->variant_tax) / 100) * $transferItem->product_quantity) + $transfer->total;
                    }else{
                        $yearProductTotalTransfer += ((($variant->variant_selling_price * $variant->variant_tax) / 100) * $transferItem->product_quantity) + $transfer->total;
                    }
                }else{
                    if ($variant->variant_discount_price != null){
                        $yearProductTotalTransfer += ($variant->variant_discount_price * $transferItem->product_quantity) + $transfer->total;
                    }else{
                        $yearProductTotalTransfer += ($variant->variant_selling_price * $transferItem->product_quantity) + $transfer->total;
                    }
                }
            }

        }
//        $denemeTotal = 0;
//        $deneme = DB::table('stock_transfer')->select('month','year','id','total')->where('year',$year)->where('status','<',6)
//            ->orderBy('id','ASC')->get();
//        foreach ($deneme as $key => $item){
//            $transferItems = StockTransferItem::where('stock_transfer_id',$item->id)->get();
//            foreach ($transferItems as $transferItem) {
//                $variant = Variant::find($transferItem->variant_id);
//                if ($variant->variant_tax != null){
//                    if ($variant->variant_discount_price != null){
//                        $denemeTotal += ((($variant->variant_discount_price * $variant->variant_tax) / 100) * $transferItem->product_quantity) + $item->total;
//                    }else{
//                        $denemeTotal += ((($variant->variant_selling_price * $variant->variant_tax) / 100) * $transferItem->product_quantity) + $item->total;
//                    }
//                }else{
//                    if ($variant->variant_discount_price != null){
//                        $denemeTotal += ($variant->variant_discount_price * $transferItem->product_quantity) + $item->total;
//                    }else{
//                        $denemeTotal += ($variant->variant_selling_price * $transferItem->product_quantity) + $item->total;
//                    }
//                }
//            }
//        }

        // YAPILDI
        $orderYearlyRevenueTotal = 0;
        $orderYearlyRevenue = DB::table('orders')->select('month','year','id','total')->where('year',$year)->where('status','<',6)
            ->orderBy('id','ASC')->get();
        foreach ($orderYearlyRevenue as $key => $item){
            $orderItems = OrderItem::where('orders_id',$item->id)->get();
            foreach ($orderItems as $orderItem) {
                $variant = Variant::find($orderItem->variant_id);
                if ($variant->variant_tax != null){
                    if ($variant->variant_discount_price != null){
                        $orderYearlyRevenueTotal += ((($variant->variant_discount_price * $variant->variant_tax) / 100) * $orderItem->product_quantity) + $item->total;
                    }else{
                        $orderYearlyRevenueTotal += ((($variant->variant_selling_price * $variant->variant_tax) / 100) * $orderItem->product_quantity) + $item->total;
                    }
                }else{
                    if ($variant->variant_discount_price != null){
                        $orderYearlyRevenueTotal += ($variant->variant_discount_price * $orderItem->product_quantity) + $item->total;
                    }else{
                        $orderYearlyRevenueTotal += ($variant->variant_selling_price * $orderItem->product_quantity) + $item->total;
                    }
                }
            }
        }
        $stockTransferYearlyExpenseTotal = DB::table('stock_transfer')->where('year',$year)
            ->where('status','<',6)->sum('total');
        // AY YIL TOTAL
        $stockTransferMontlyAndYearlyReport = DB::table('stock_transfer')->select(DB::raw("SUM(total) as total"),'month')->where('year',$year)->where('status','<',6)
            ->groupBy('month')->orderBy('month','DESC')->get();
        $orderMonthlyAndYearlyReport = DB::table('orders')->select(DB::raw("SUM(total) as total"),'month')->where('year',$year)->where('status','<',6)
            ->groupBy('month')->orderBy('month','DESC')->get();
//        return response()->json(Converter::convertToView($yearProductTotalTransfer,2));
        $reports = [];
        $reports['stockTransfer'] = $stockTransferMontlyAndYearlyReport;
        $reports['order'] = $orderMonthlyAndYearlyReport;
        $reports['orderYearlyRevenueTotal'] = Converter::convertToView($orderYearlyRevenueTotal,2);
        $reports['stockTransferYearlyExpenseTotal'] = Converter::convertToView($stockTransferYearlyExpenseTotal,2);
        return response()->json($reports);
    }


}
