<?php

namespace App\Http\Controllers\Backend;

use App\Component\Year\IndexYearHandler;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class YearController extends Controller
{
    public function __construct(private IndexYearHandler $indexYearHandler)
    {

    }

    public function __invoke()
    {
        return $this->indexYearHandler->handle();
    }
}
