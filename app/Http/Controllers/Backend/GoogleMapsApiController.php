<?php

namespace App\Http\Controllers\Backend;

use App\Component\GoogleMapsApi\GoogleMapsApiHandler;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GoogleMapsApiController extends Controller
{

    public function __construct(private GoogleMapsApiHandler $googleMapsApiHandler){

    }

    public function __invoke(Request $request){

        $this->googleMapsApiHandler->handle($request);
    }
    //
}
