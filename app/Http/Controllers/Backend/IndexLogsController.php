<?php

namespace App\Http\Controllers\Backend;

use App\Component\CreateLogs\IndexLogsHandler;
use App\Http\Controllers\Controller;

class IndexLogsController extends Controller
{
    //
    public function __construct(private IndexLogsHandler $indexLogsHandler)
    {
    }

    public function __invoke()
    {
       $data = $this->indexLogsHandler->handle();
       return response()->json($data);
    }
}
