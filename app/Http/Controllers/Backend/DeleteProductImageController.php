<?php

namespace App\Http\Controllers\Backend;

use App\Component\DeleteProductImage\DeleteProductImageHandler;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DeleteProductImageController extends Controller
{
    //

    public function __construct(private DeleteProductImageHandler $deleteProductImageHandler)
    {
    }

    public function __invoke(Request $request)
    {
        // TODO: Implement __invoke() method.
        return $this->deleteProductImageHandler->handle($request);
    }
}
