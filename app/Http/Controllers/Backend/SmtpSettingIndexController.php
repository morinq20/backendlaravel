<?php

namespace App\Http\Controllers\Backend;

use App\Component\SmtpSettings\SmtpSettingsIndexHandler;
use App\Http\Controllers\Controller;

class SmtpSettingIndexController extends Controller
{
    public function __construct(private SmtpSettingsIndexHandler $smtpSettingsIndexHandler){

    }

    public function __invoke(){
        return $this->smtpSettingsIndexHandler->handle();
    }
}
