<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\FileManager\FileSaver;
use App\Http\Services\FileManager\ProductFileConfig;
use App\Http\Traits\FolderControlTrait;
use App\Models\ImageResize;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductPriority;
use App\Models\SubCategory;
use App\Models\Variant;
use App\Models\Warehouse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Picqer\Barcode\BarcodeGeneratorHTML;

class ProductController extends Controller
{
    use FolderControlTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $product = Product::with('category')->with('subcategory')->with('supplier')->with('warehouse')->orderBy('id','DESC')->get();
        return response()->json($product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $code = rand(106890122,100000000);
            $imageResize = ImageResize::first();
            $product = new Product();
            $product->barcode = $this->barcodeGenerator($code);
            $product->category_id = $request->category_id;
            $product->subcategory_id = $request->subcategory_id;
            $product->collection_id = $request->collection_id ?? 1;
            $product->supplier_id = $request->supplier_id ?? 1;
            $product->warehouse_id = $request->warehouse_id;
            $product->product_name = $request->product_name;
            $product->product_code = $code;
            $product->product_description = strip_tags($request->product_description);
            $product->product_meta_description = strip_tags($request->product_meta_description);
            $product->product_status = $request->product_status;
            $product->product_quantity = $request->product_quantity;
            $product->product_tax = $request->product_tax;
            if (isset($request->product_image)) {
                $path = 'backend/uploads/product/';
                $this->folderIfNotExists($path);
                $file = FileSaver::getInstance();
                $config = ProductFileConfig::createConfig($request->product_image,$path,$imageResize->product_image_width,$imageResize->product_image_height);
                $file_save = $file->send($config);
                if (isset($file_save))
                {
                    $product->product_image = $config->createFile();
                }
                $product->save();
            } else {
                $product->save();
            }
            foreach ($request->multipleImage as $key => $item){
                if (isset($item)) {
                    $path = 'backend/uploads/product_image/';
                    $this->folderIfNotExists($path);
                    $img = Image::make($item)->resize($imageResize->product_image_width,$imageResize->product_image_height);
                    $position = strpos($item, ';');
                    $sub = substr($item, 0, $position);
                    $ext = explode('/', $sub)[1];
                    $name = time().$key.".".$ext;
                    $filePath = $path.$name;
                    $img->save($filePath);

                    if (isset($filePath))
                    {
                        $productImage = new ProductImage();
                        $productImage->product_id = $product->id;
                        $productImage->product_image = $filePath;
//                    return response()->json($config->createFile());
                        $productImage->save();
                    }
                }
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * @throws \Exception
     */
    private function barcodeGenerator($code){
        DB::beginTransaction();
        try {
            $generator = new BarcodeGeneratorHTML();
            DB::commit();
            return $generator->getBarcode($code,$generator::TYPE_STANDARD_2_5, 2, 60);
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $product = Product::find($id);
        $producttImage = ProductImage::where('product_id',$product->id)->get();
        $product['product_multiple_image'] = $producttImage;

        return response()->json($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $newPhoto = $request->new_product_image;
            $oldPhoto = $request->product_image;
            $product = Product::find($id);
            $product->category_id = $request->category_id;
            $product->subcategory_id = $request->subcategory_id;
            $product->collection_id = $request->collection_id;
            $product->supplier_id = $request->supplier_id;
            $product->warehouse_id = $request->warehouse_id;
            $product->product_name = $request->product_name;
            $product->product_description = strip_tags($request->product_description);
            $product->product_meta_description = strip_tags($request->product_meta_description);
            $product->product_status = $request->product_status;
            $product->product_quantity = $request->product_quantity;
            $product->product_tax = $request->product_tax;
            if (isset($newPhoto)) {
                if ($oldPhoto != null) {
                    unlink($oldPhoto);
                    $path = 'backend/uploads/product/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = ProductFileConfig::createConfig($newPhoto,$path,$imageResize->product_image_width,$imageResize->product_image_height);
                    $file_save = $file->send($config);
                    $product->product_image = $config->createFile();
                    $product->save();
                } else {
                    $path = 'backend/uploads/product/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = ProductFileConfig::createConfig($newPhoto,$path,$imageResize->product_image_width,$imageResize->product_image_height);
                    $file_save = $file->send($config);
                    $product->product_image = $config->createFile();
                    $product->save();
                }
            } else {
                $product->save();
            }
            foreach ($request->multipleImage as $key => $item){
                if (isset($item)) {
                    $path = 'backend/uploads/product_image/';
                    $this->folderIfNotExists($path);
                    $img = Image::make($item)->resize($imageResize->product_image_width,$imageResize->product_image_height);
                    $position = strpos($item, ';');
                    $sub = substr($item, 0, $position);
                    $ext = explode('/', $sub)[1];
                    $name = time().$key.".".$ext;
                    $filePath = $path.$name;
                    $img->save($filePath);

                    if (isset($filePath))
                    {
                        $productImage = new ProductImage();
                        $productImage->product_id = $product->id;
                        $productImage->product_image = $filePath;
//                    return response()->json($config->createFile());
                        $productImage->save();
                    }
                }
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $product = Product::find($id);
            $photo = $product->product_image;
            if (isset($photo)) {
                unlink($photo);
                $product->delete();
            } else {
                $product->delete();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }



    // Select To Category
    public function selectToCategory($id)
    {
        $findSubCategory = SubCategory::where('category_id',$id)->get();
        return response()->json($findSubCategory);
    }

    // Product Priority

    /**
     * @throws \Exception
     */
    public function productPriority(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $control = ProductPriority::where('product_id',$id)->first();
            if ($control) {
                $control->trend = $request->trend;
                $control->hot_new = $request->hot_new;
                $control->best_sellers = $request->best_sellers;
                $control->featured = $request->featured;
                $control->status = $request->status;
                $control->product_id = $id;
                $control->save();
            } else {
                $productPriority = new ProductPriority();
                $productPriority->trend = $request->trend;
                $productPriority->hot_new = $request->hot_new;
                $productPriority->best_sellers = $request->best_sellers;
                $productPriority->featured = $request->featured;
                $productPriority->status = $request->status;
                $productPriority->product_id = $id;
                $productPriority->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    public function showProductPriority($id): JsonResponse
    {
        $productPriority = ProductPriority::where('product_id',$id)->first();
//        dd($productPriority);
        return response()->json($productPriority);
    }

    // Product Priority
//    public function productVariant(Request $request,$id)
//    {
//        dd($request->files);
//        $control = ProductPriority::where('product_id',$id)->first();
//        if ($control) {
//            $control->trend = $request->trend;
//            $control->hot_new = $request->hot_new;
//            $control->best_sellers = $request->best_sellers;
//            $control->featured = $request->featured;
//            $control->status = $request->status;
//            $control->product_id = $id;
//            $control->save();
//        } else {
//            $productPriority = new ProductPriority();
//            $productPriority->trend = $request->trend;
//            $productPriority->hot_new = $request->hot_new;
//            $productPriority->best_sellers = $request->best_sellers;
//            $productPriority->featured = $request->featured;
//            $productPriority->status = $request->status;
//            $productPriority->product_id = $id;
//            $productPriority->save();
//        }
//    }

    public function showProductVariant($id)
    {
        $productVariant = Variant::with('product')->with('size')->with('color')->where('product_id',$id)->orderBy('id','DESC')->get();
        return response()->json($productVariant);
    }

//    public function outOfStock()
//    {
//        $outOfStock = Product::with('category')->with('subcategory')->where('product_quantity', '=',0)->orderBy('id','DESC')->get();
//        return response()->json($outOfStock);
//    }

    public function mainWarehouse()
    {
        $mainWarehouse = Warehouse::where('warehouse_type',1)->first();
        return response()->json($mainWarehouse);
    }

    public function mainStockControlItem(): JsonResponse
    {
        $items = Product::where('product_quantity',0)->get();
        return response()->json($items);
    }


    public function showItem($id): JsonResponse
    {
        $product = Product::with('category')->with('subcategory')->with('collection')->with('unit')->find($id);
        return response()->json($product);
    }




}
