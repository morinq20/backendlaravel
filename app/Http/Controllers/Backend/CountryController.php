<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Cities;
use App\Models\Country;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $country = Country::all();
        return response()->json($country);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $country = new Country();
            $country->country = $request->country;
            $country->country_dial_Code = $request->country_dial_code;
            $country->country_code = $request->country_code;
            $country->country_format = $request->country_format;
            $country->country_status = $request->country_status;
            $country->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $country = Country::find($id);
        return response()->json($country);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $country = Country::find($id);
            $country->country = $request->country;
            $country->country_dial_Code = $request->country_dial_code;
            $country->country_code = $request->country_code;
            $country->country_format = $request->country_format;
            $country->country_status = $request->country_status;
            $country->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $country = Country::find($id);
            $city = Cities::where('country_id',$id)->get();
            $country->delete();
            foreach ($city as $item){
                $item->delete();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }


}
