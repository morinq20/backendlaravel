<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\Utils\Converter\Converter;
use Illuminate\Http\Request;

class ExchangeRateController extends Controller
{
    public function index()
    {
        $exchangeRate = "https://v6.exchangerate-api.com/v6/ff0efbe844a1d92e6c7d22c3/latest/TRY";
        $response = file_get_contents($exchangeRate);
        $result = json_decode($response);
        if ($result->result == "success"){
            $currency = [];
            foreach ($result->conversion_rates as $key => $conversion_rate){
                if ($key == "USD"){
                    $currency['value'][] = Converter::convertToView($conversion_rate,3);
                    $currency['labels'][] = $key;
                } else if ($key == "EUR"){
                    $currency['value'][] = Converter::convertToView($conversion_rate,3);
                    $currency['labels'][] = $key;
                } else if ($key == "TRY"){
                    $currency['value'][] = Converter::convertToView($conversion_rate,3);
                    $currency['labels'][] = $key;
                } else if ($key == "RUB"){
                    $currency['value'][] = Converter::convertToView($conversion_rate,3);
                    $currency['labels'][] = $key;
                } else if ($key == "QAR"){
                    $currency['value'][] = Converter::convertToView($conversion_rate,3);
                    $currency['labels'][] = $key;
                } else if ($key == "KWD"){
                    $currency['value'][] = Converter::convertToView($conversion_rate,3);
                    $currency['labels'][] = $key;
                }
            }
            return response()->json($currency);
        }else{
            $currency = [];
            return response()->json($currency);
        }

    }

    public function converter(Request $request)
    {
        $currency = $request->currency;
        $firstCurrencySelect = $request->firstCurrencySelect;
        $secondCurrencySelect = $request->secondCurrencySelect;
        $firstResult = null;
        $secondResult = null;
        $exchangeRate = "https://v6.exchangerate-api.com/v6/ff0efbe844a1d92e6c7d22c3/latest/".$firstCurrencySelect;
        $response = file_get_contents($exchangeRate);
        $result = json_decode($response);
        if ($result->result == "success"){
            $data = [];
            foreach ($result->conversion_rates as $key => $conversion_rate){
                if ($key == $firstCurrencySelect){
                    $firstResult = $conversion_rate;
                } else if ($key == $secondCurrencySelect){
                    $secondResult = $conversion_rate;
                }
            }
            $data['currency'] = $currency;
            $data['firstCurrencySelect'] = $firstCurrencySelect;
            $data['secondCurrencySelect'] = $secondCurrencySelect;
            $data['result'] = Converter::convertToView($firstResult * $secondResult,3);
//            $data['result'] = Converter::convertToView(($firstResult * $secondResult),2);
            return response()->json($data);
        }else{
            $currency = [];
            return response()->json($currency);
        }
    }
}
