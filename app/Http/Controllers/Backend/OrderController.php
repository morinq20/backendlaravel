<?php

namespace App\Http\Controllers\Backend;

use App\Enum\EmailTemplateEnum;
use App\Events\OrderEmail;
use App\Http\Controllers\Controller;
use App\Http\Services\CodeService\CodeService;
use App\Http\Services\Utils\Converter\Converter;
use App\Jobs\SendEmailByOrder;
use App\Jobs\SendEmailJob;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderCart;
use App\Models\OrderItem;
use App\Models\OrderStatus;
use App\Models\Pallet;
use App\Models\PanelSettings;
use App\Models\PaymentMethod;
use App\Models\Product;
use App\Models\StockTransfer;
use App\Models\StockTransferCart;
use App\Models\StockTransferItem;
use App\Models\StockTransferStatus;
use App\Models\Supplier;
use App\Models\Truck;
use App\Models\Variant;
use App\Models\Warehouse;
use App\Models\WarehouseItem;
use App\Models\Years;
use Dompdf\Dompdf;
use http\Params;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Array_;

class OrderController extends Controller
{
    private int $total = 0;
    public function __construct(private Order $order,
    private OrderStatus $orderStatus)

    {
    }

    /**
     * @throws \Exception
     */
    public function index(Request $request)
    {
        DB::beginTransaction();
        try {
            $order = [];
            $allOrder = $this->order->where('status',$request->status)->orderBy('id','DESC')->get();
            foreach ($allOrder as $key => $item) {
                $fromWarehouse = Warehouse::select('warehouse_name','warehouse_type')->where('id',$item->from_warehouse_id)->first();
                $pallet = Pallet::select('pallet_name')->where('id',$item->pallet_id)->first();
                $paymentMethod = PaymentMethod::find($item->payment_method_id);
                $customer = Customer::select('customer_name','customer_surname','customer_phone','customer_phone_dial_code')->where('id',$item->customer_id)->first();
                $order[$key]['code'] = $item->code;
                $order[$key]['fromWarehouse_name'] = $fromWarehouse->warehouse_name;
                $order[$key]['fromWarehouse_type'] = $fromWarehouse->warehouse_type;
                $order[$key]['total'] = $item->total;
                $order[$key]['total_product'] = $item->total_product;
                $order[$key]['total_quantity'] = $item->total_quantity;
                $order[$key]['total_process'] = $item->total_process;
                $order[$key]['total_weight'] = $item->total_weight;
                $order[$key]['pallet_count'] = $item->pallet_count;
                $order[$key]['pallet_name'] = $pallet->pallet_name;
                $order[$key]['payment_status'] = $item->payment_status;
                $order[$key]['payment_method'] = $paymentMethod->payment_method;
                $order[$key]['advance_payment_status'] = $item->advance_payment_status;
                $order[$key]['advance_payment'] = $item->advance_payment;
                $order[$key]['order_status'] = $this->orderStatus->where('status_id', $item->status)->get();
                $order[$key]['start_transfer_date'] = $item->start_transfer_date;
                $order[$key]['finish_transfer_date'] = $item->finish_transfer_date;
                $order[$key]['status'] = $item->status;
                $order[$key]['id'] = $item->id;
                $order[$key]['customer'] = $customer;
//            $stockTransfer[$key]['status_text'] = $this->stockTransferStatus->where('status_id',$item->status)->get();
            }
            DB::commit();
            return response()->json($order);
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * @throws \Exception
     */
    public function advancePayment(Request $request): \Illuminate\Http\JsonResponse
    {
        DB::beginTransaction();
        try {
//            return response()->json(Converter::convertToFloat($request->advance_payment));
            $order = Order::find($request->id);
            if ($order->payment_status == 0 && $order->advance_payment_status ==0){
                $order->advance_payment = Converter::convertToFloat($request->advance_payment);
                $order->advance_payment_status = 1;
                $order->save();
            }
            DB::commit();
            return response()->json($order);
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * @throws \Exception
     */
    public function payment(Request $request): \Illuminate\Http\JsonResponse
    {
        DB::beginTransaction();
        try {
//            return response()->json(Converter::convertToFloat($request->advance_payment));
            $order = Order::find($request->id);
            $order->payment_status = 1;
            $order->save();
            DB::commit();
            return response()->json($order);
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    public function show($id)
    {
        $order = Order::find($id);
        $findOrderItem = OrderItem::where('orders_id',$id)->get();
        $fromWarehouse = Warehouse::where('id',$order->from_warehouse_id)->first();
        $pallet = Pallet::find($order->pallet_id);
        $paymentMethod = PaymentMethod::find($order->payment_method_id);
        $customer = Customer::find($order->customer_id)->first();
        $settings = PanelSettings::find(0);
        $orders = [];
        $orders['order']['fromWarehouse_name'] = $fromWarehouse->warehouse_name;
        $orders['order']['fromWarehouse_address'] = $fromWarehouse->warehouse_address;
        $orders['order']['fromWarehouse_country'] = $fromWarehouse->warehouse_country;
        $orders['order']['fromWarehouse_city'] = $fromWarehouse->warehouse_city;
        $orders['order']['fromWarehouse_phone'] = $fromWarehouse->warehouse_phone;
        $orders['order']['fromWarehouse_phone_dial_code'] = $fromWarehouse->warehouse_phone_dial_code;
        $orders['order']['fromWarehouse_email'] = $fromWarehouse->warehouse_email;
        $orders['order']['fromWarehouse_postcode'] = $fromWarehouse->warehouse_postcode;
        $orders['order']['fromWarehouse_status'] = $fromWarehouse->warehouse_status;
        $orders['order']['fromWarehouse_type'] = $fromWarehouse->warehouse_type;
        $orders['order']['start_transfer_date'] = $order->start_transfer_date;
        $orders['order']['finish_transfer_date'] = $order->finish_transfer_date;
        $orders['order']['status'] = $order->status;
//        $orders['order']['payment_status'] = $order->payment_status;
        if ($order->advance_payment_status === 1)
            $orders['order']['advance_payment_status'] = "Paid";
        else
            $orders['order']['advance_payment_status'] = "Not Paid";
        if ($order->payment_status === 1)
            $orders['order']['payment_status'] = "Paid";
        else
            $orders['order']['payment_status'] = "Not Paid";
//        $orders['order']['advance_payment_status'] = $order->advance_payment_status;
        $orders['order']['advance_payment'] = $order->advance_payment;
        $orders['order']['payment_method'] = $paymentMethod->payment_method;
        $orders['order']['id'] = $order->id;
        $orders['order']['total_product'] = $order->total_product;
        $orders['order']['total_quantity'] = $order->total_quantity;
        $orders['order']['total_process'] = $order->total_process;
        $orders['order']['total_weight'] = $order->total_weight;
        $orders['order']['pallet_name'] = $pallet->pallet_name;
        $orders['order']['pallet_count'] = $order->pallet_count;
        $orders['order']['total'] = $order->total;
        $orders['order']['subtotal_process'] = $order->subtotal_process;
        $orders['order']['subtotal_products'] = $order->subtotal_products;
        $orders['order']['total_pallet'] = $order->total_pallet;
        $orders['order']['created_at'] = $order->created_at->format('d-m-Y H:i:s');
        $orders['order']['code'] = $order->code;
        $orders['order']['logo'] = $settings->panel_image;
        $orders['order']['customer'] = $customer;

        foreach ($findOrderItem as $key => $item) {
            $variant = Variant::where('id',$item->variant_id)->first();
            if($variant->variant_discount_price !== null){
                 $this->total += (int)$item->product_quantity * (int)$variant->variant_discount_price;
            }
            $this->total += (int)$item->product_quantity * (int)$variant->variant_selling_price;


            $findVariant = DB::table('orders_item')
                ->select('variant.*','color.color_name','color.color_code','size.size_name',
                    'product.product_name','product.product_code','product.product_image','orders_item.product_quantity')
                ->join('variant','orders_item.variant_id','=','variant.id')
                ->join('color','variant.color_id','=','color.id')
                ->join('size','variant.size_id','=','size.id')
                ->join('product','orders_item.product_id','=','product.id')
                ->where('variant.id',$item->variant_id)
                ->where('product.id',$item->product_id)
                ->first();
            $orders['variant'][$key] = $findVariant;
        }
        $orders['order']['total_paye'] = $this->total;



        return response()->json($orders);
    }


    /**
     * @throws \Exception
     */
    public function storeOrder(Request $request)
    {
        DB::beginTransaction();
        try {
            $fromWarehouse = Warehouse::find($request->form['from_warehouse_id']);
            if($request->form['from_warehouse_id']){
                $type = "WTC";
            }
            $code = CodeService::createCode($type);
            $order = array();
            $order['code'] = $code;
            $order['from_warehouse_id'] = $request->form['from_warehouse_id'];
            $order['total_process'] = $request->form['processCount'];
            $order['customer_id'] = $request->form['customer_id'];
            $order['payment_method_id'] = $request->form['payment_method_id'];
            $order['payment_status'] = $request->form['payment_status'];
            $order['start_transfer_date'] = $request->form['start_transfer_date'];
            $order['finish_transfer_date'] = $request->form['finish_transfer_date'];
            $order['pallet_id'] = $request->form['pallet_id'];
            $order['pallet_count'] = $request->form['pallet_count'];
            $order['total_weight'] = $request->form['total_weight'];
            $order['total_product'] = $this->totalProduct();
            $order['total_quantity'] = $this->totalQuantity();
            $order['total'] = $request->form['total'];
            $order['subtotal_process'] = $request->form['subtotalProcess'];
            $order['subtotal_products'] = $request->form['subtotalProducts'];
            $order['total_pallet'] = $request->form['total_pallet'];
            $order['status'] = 0;
            $order['advance_payment_status'] = $request->form['advance_payment_status'];
            $order['advance_payment'] = $request->form['advance_payment'];
            $order['isCanceled'] = 0;
            $order['date'] = date('d', strtotime($request->form['start_transfer_date']));
            $order['month'] = date('F',strtotime($request->form['start_transfer_date']));
            $order['year'] = date('Y',strtotime($request->form['start_transfer_date']));
            $order['created_at'] = new \DateTime();
            $order['updated_at'] = new \DateTime();
            $orderId = DB::table('orders')->insertGetId($order);
            $process = [];
            $array = Order::where('id',$orderId)->first();
            foreach ($request->process as $item => $key){
                $process[] = $request->process[$item];
                if ($key['supplier'] != null){
                    $supplier = Supplier::select('supplier_name','supplier_surname','supplier_phone','supplier_email','supplier_phone_dial_code')
                        ->where('id','=',$key['supplier'])->first();
                    $process[$item]['supplierInformation'] = $supplier;
                }
                if ($key['truck'] != null){
                    $truck = Truck::select('truck_brand','truck_model','truck_plate_number','truck_code','truck_start_km','truck_end_km','truck_image')
                        ->where('id','=',$key['truck'])->first();
                    $process[$item]['truckInformation'] = $truck;
                }
            }
            $array->order_transfer_process = $process;
            $array->save();
            $this->orderItem($orderId);
            $this->orderCartDelete();
            $this->mainWarehouseItemDecrement($orderId);
            $archiveControl = Years::where('year',date('Y',strtotime($request->form['start_transfer_date'])))->first();
            if (!$archiveControl){
                $archive = new Years();
                $archive->year = date('Y',strtotime($request->form['start_transfer_date']));
                $archive->save();
            }
            $this->sendMail($orderId);
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    private function sendMail($orderId){
        $order = Order::find($orderId);
        $customer = Customer::find($order->customer_id);
        $fromWarehouse = Warehouse::find($order->from_warehouse_id);
        $orderStatus = OrderStatus::where('status_id',$order->status)->first();
        $orderItems = DB::table('orders_item')
            ->select('orders_item.*','product.product_name','product.product_code','product.product_image',
                'variant.sku_no','variant.variant_image','variant.variant_tax','variant.variant_selling_price','variant.variant_discount_price')
            ->join('product','orders_item.product_id','=','product.id')
            ->join('variant','orders_item.variant_id','=','variant.id')
            ->where('orders_item.orders_id',$order->id)
            ->get();
        $total = 0;
        $subtotal = 0;
        foreach ($orderItems as $item){
            if ($item->variant_tax == null){
                if ($item->variant_discount_price == null) {
                    $total += $item->product_quantity * $item->variant_selling_price;
                    $subtotal += $item->product_quantity * $item->variant_selling_price;
                }else{
                    $total += $item->product_quantity * $item->variant_discount_price;
                    $subtotal += $item->product_quantity * $item->variant_discount_price;
                }
            }else{
                if ($item->variant_discount_price == null) {
                    $total += ((($item->product_quantity * $item->variant_selling_price) * $item->variant_tax) / 100) + $item->product_quantity * $item->variant_selling_price;
                    $subtotal += $item->product_quantity * $item->variant_selling_price;
                }else{
                    $total += ((($item->product_quantity * $item->variant_discount_price) * $item->variant_tax) / 100) + $item->product_quantity * $item->variant_discount_price;
                    $subtotal += $item->product_quantity * $item->variant_discount_price;
                }
            }
        }
        $settings = PanelSettings::find(0);
        SendEmailByOrder::dispatch($customer,$order,$settings,$orderItems,$fromWarehouse,$orderStatus,Converter::convertToView($total,2),Converter::convertToView($subtotal,2));
//        $array = [
//            'order' => [
//                'code' => $order->code,
//                'start_transfer_date' => $order->start_transfer_date,
//                'finish_transfer_date' => $order->finish_transfer_date,
//                'total_product' => $order->total_product,
//                'total_quantity' => $order->total_quantity,
//                'total_process' => $order->total_process,
//                'total_weight' => $order->total_weight,
//                'pallet_count' => $order->pallet_count,
//                'status' => $orderStatus->status_text,
//                'process_total' => $order->total,
//                'total' => $total,
//                'subtotal' => $subtotal,
//            ],
//            'from' => [
//                'warehouse_name' => $fromWarehouse->warehouse_name,
//                'warehouse_address' => $fromWarehouse->warehouse_address,
//                'warehouse_country' => $fromWarehouse->warehouse_country,
//                'warehouse_city' => $fromWarehouse->warehouse_city,
//                'warehouse_phone' => $fromWarehouse->warehouse_phone,
//                'warehouse_email' => $fromWarehouse->warehouse_email,
//                'warehouse_type' => $fromWarehouse->warehouse_type == 0 ? 'Sub Warehouse' : 'Main Warehouse'
//            ],
//            'to' => [
//                'customer_name' => $customer->customer_name,
//                'customer_surname' => $customer->customer_surname,
//                'customer_shipping_address' => $customer->customer_shipping_address,
//                'customer_billing_address' => $customer->customer_billing_address,
//                'customer_phone' => $customer->customer_phone,
//                'customer_email' => $customer->customer_email,
//            ],
//            'items' => $orderItems,
//            'settings' => $settings
//        ];
//        $email = $customer->customer_email;


//        $eventOrder = Order::find($orderId);
//        event(new OrderEmail($eventOrder,EmailTemplateEnum::ORDER_TEMPLATE->value));
    }

    // Main Warehouse Item Product Quantity Decrement

    /**
     * @throws \Exception
     */
    private function mainWarehouseItemDecrement($id) {
        DB::beginTransaction();
        try {
            $orderItems = OrderItem::where('orders_id',$id)->get();
            foreach ($orderItems as $cart) {
                DB::table('product')->where('id',$cart->product_id)
                    ->update(['product_quantity' => DB::raw('product_quantity -' .$cart->product_quantity)]);
                DB::table('variant')->where('id',$cart->variant_id)
                    ->where('product_id',$cart->product_id)
                    ->update(['variant_quantity' => DB::raw('variant_quantity -' .$cart->product_quantity)]);
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    // All Stock Transfer Cart
    private function orderCart() {
        $orderCart = OrderCart::all();
        return $orderCart;
    }

    // Total Product
    private function totalProduct() {
        $total_product = count($this->orderCart());
        return $total_product;
    }

    // Total Quantity

    /**
     * @throws \Exception
     */
    private function totalQuantity() {
        DB::beginTransaction();
        try {
            $orderCart = $this->orderCart();
            $total_quantity = 0;
            foreach ($orderCart as $cart) {
                $total_quantity += $cart->product_quantity;
            }
            DB::commit();
            return $total_quantity;
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    // Stock Transfer Item

    /**
     * @throws \Exception
     */
    private function orderItem($orderId) {
        DB::beginTransaction();
        try {
            $orderCart = $this->orderCart();
            foreach ($orderCart as $cart) {
                $orderItem = new OrderItem();
                $orderItem->orders_id = $orderId;
                $orderItem->product_id = $cart->product_id;
                $orderItem->variant_id = $cart->variant_id;
                $orderItem->unit_id = $cart->unit_id;
                $orderItem->product_quantity = $cart->product_quantity;
                $orderItem->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }

    }



    // Stock Transfer Cart Delete

    /**
     * @throws \Exception
     */
    private function orderCartDelete() {
        DB::beginTransaction();
        try {
            foreach ($this->orderCart() as $cart) {
                OrderCart::destroy($cart->id);
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }


    public function getOrderProcess($id)
    {
        $order = Order::find($id);
        $process = json_decode($order->order_transfer_process);
        return response()->json($process);
    }



}
