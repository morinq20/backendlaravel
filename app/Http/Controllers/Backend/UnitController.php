<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Unit;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $unit = Unit::orderBy('id','DESC')->get();
        return response()->json($unit);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $unit = new Unit();
            $unit->unit_name = $request->unit_name;
            $unit->unit_symbol = $request->unit_symbol;
            $unit->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $unit = Unit::find($id);
        return response()->json($unit);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $unit = Unit::find($id);
            $unit->unit_name = $request->unit_name;
            $unit->unit_symbol = $request->unit_symbol;
            $unit->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $unit = Unit::find($id);
            $unit->delete();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
