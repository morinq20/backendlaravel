<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Services\FileManager\FileSaver;
use App\Http\Services\FileManager\ShippingCompanyFileConfig;
use App\Http\Traits\FolderControlTrait;
use App\Models\ImageResize;
use App\Models\ShippingCompany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShippingCompanyController extends Controller
{
    use FolderControlTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $shippingCompany = ShippingCompany::orderBy('id','DESC')->get();
        return response()->json($shippingCompany);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $shippingCompany = new ShippingCompany();
            $shippingCompany->shipping_company_name = $request->shipping_company_name;
            $shippingCompany->shipping_company_status = $request->shipping_company_status;
            $shippingCompany->country_id = $request->country_id ?? 1;
            $shippingCompany->city_id = $request->city_id ?? 1;
            if (isset($request->shipping_company_image)) {
                $path = 'backend/uploads/shipping_company/';
                $this->folderIfNotExists($path);
                $file = FileSaver::getInstance();
                $config = ShippingCompanyFileConfig::createConfig($request->shipping_company_image,$path,$imageResize->shipping_company_image_width,$imageResize->shipping_company_image_height);
                $file_save = $file->send($config);
                if (isset($file_save))
                {
                    $shippingCompany->shipping_company_image = $config->createFile();
                }
                $shippingCompany->save();
            } else {
                $shippingCompany->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $shippingCompany = ShippingCompany::find($id);
        return response()->json($shippingCompany);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $imageResize = ImageResize::first();
            $newPhoto = $request->new_shipping_company_image;
            $oldPhoto = $request->shipping_company_image;
            $shippingCompany = ShippingCompany::find($id);
            $shippingCompany->shipping_company_name = $request->shipping_company_name;
            $shippingCompany->shipping_company_status = $request->shipping_company_status;
            if (isset($newPhoto)) {
                if ($oldPhoto != null) {
                    unlink($oldPhoto);
                    $path = 'backend/uploads/shipping_company/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = ShippingCompanyFileConfig::createConfig($newPhoto,$path,$imageResize->shipping_company_image_width,$imageResize->shipping_company_image_height);
                    $file_save = $file->send($config);
                    $shippingCompany->shipping_company_image = $config->createFile();
                    $shippingCompany->save();
                } else {
                    $path = 'backend/uploads/shipping_company/';
                    $this->folderIfNotExists($path);
                    $file = FileSaver::getInstance();
                    $config = ShippingCompanyFileConfig::createConfig($newPhoto,$path,$imageResize->shipping_company_image_width,$imageResize->shipping_company_image_height);
                    $file_save = $file->send($config);
                    $shippingCompany->shipping_company_image = $config->createFile();
                    $shippingCompany->save();
                }
            } else {
                $shippingCompany->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $shippingCompany = ShippingCompany::find($id);
            $photo = $shippingCompany->shipping_company_image;
            if (isset($photo)) {
                unlink($photo);
                $shippingCompany->delete();
            } else {
                $shippingCompany->delete();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

}
