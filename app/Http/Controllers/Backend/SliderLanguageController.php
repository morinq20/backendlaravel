<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Slider;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SliderLanguageController extends Controller
{
    public function show(Request $request,$id): \Illuminate\Http\JsonResponse
    {
        $slider = Slider::where('id',$id)->first();
        return response()->json(json_decode($slider->lang));
    }

    /**
     * @throws Exception
     */
    public function store(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            $slider = Slider::where('id',$id)->first();
            $arr = [];
            foreach ($requestAll as $key => $item){
                $lang = Language::find($item['language_id']);
                $arr[$lang->code]['language_id'] = $item['language_id'];
                $arr[$lang->code]['language_code'] = $lang->code;
                $arr[$lang->code]['slider_title'] = $item['slider_title'];
                $arr[$lang->code]['slider_description'] = strip_tags($item['slider_description']);
            }
            if ($slider->lang !== null){
                $lang = json_decode($slider->lang);
                foreach ($lang as $key => $item){
                    $arr[$key] = $item;
                }
            }
            $slider->lang = json_encode($arr);
            $slider->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }

    }

    /**
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            $slider = Slider::where('id',$id)->first();
            $arr = [];
            foreach ($requestAll as $key => $item){
                $lang = Language::find($item['language_id']);
                $arr[$lang->code]['language_id'] = $item['language_id'];
                $arr[$lang->code]['language_code'] = $lang->code;
                $arr[$lang->code]['slider_title'] = $item['slider_title'];
                $arr[$lang->code]['slider_description'] = strip_tags($item['slider_description']);
            }
            $slider->lang = json_encode($arr);
            $slider->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function delete($id,$code)
    {
        DB::beginTransaction();
        try {
            $slider = Slider::where('id',$id)->first();
            $lang = json_decode($slider->lang);
            $arr = [];
            foreach ($lang as $key => $item){
                if ($key !== $code){
                    $arr[$key] = $item;
                }
            }
            $slider->lang = json_encode($arr);
            $slider->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
