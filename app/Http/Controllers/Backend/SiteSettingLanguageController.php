<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\WebSiteSettings;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SiteSettingLanguageController extends Controller
{
    public function show(Request $request,$id): \Illuminate\Http\JsonResponse
    {
        $setting = WebSiteSettings::where('id',$id)->first();
        return response()->json(json_decode($setting->lang));
    }

    /**
     * @throws Exception
     */
    public function store(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            $setting = WebSiteSettings::where('id',$id)->first();
            $arr = [];
            foreach ($requestAll as $key => $item){
                $lang = Language::find($item['language_id']);
                $arr[$lang->code]['language_id'] = $item['language_id'];
                $arr[$lang->code]['language_code'] = $lang->code;
                $arr[$lang->code]['copyright'] = $item['copyright'];
                $arr[$lang->code]['country'] = $item['country'];
                $arr[$lang->code]['city'] = $item['city'];
                $arr[$lang->code]['address'] = strip_tags($item['address']);
                $arr[$lang->code]['description'] = strip_tags($item['description']);
                $arr[$lang->code]['privacy_policy'] = strip_tags($item['privacy_policy']);
                $arr[$lang->code]['terms_conditions'] = strip_tags($item['terms_conditions']);
            }
            if ($setting->lang !== null){
                $lang = json_decode($setting->lang);
                foreach ($lang as $key => $item){
                    $arr[$key] = $item;
                }
            }
//            return response()->json($arr);
            $setting->lang = json_encode($arr);
            $setting->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }

    }

    /**
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            $setting = WebSiteSettings::where('id',$id)->first();
            $arr = [];
            foreach ($requestAll as $key => $item){
                $lang = Language::find($item['language_id']);
                $arr[$lang->code]['language_id'] = $item['language_id'];
                $arr[$lang->code]['language_code'] = $lang->code;
                $arr[$lang->code]['copyright'] = $item['copyright'];
                $arr[$lang->code]['country'] = $item['country'];
                $arr[$lang->code]['city'] = $item['city'];
                $arr[$lang->code]['address'] = strip_tags($item['address']);
                $arr[$lang->code]['description'] = strip_tags($item['description']);
                $arr[$lang->code]['privacy_policy'] = strip_tags($item['privacy_policy']);
                $arr[$lang->code]['terms_conditions'] = strip_tags($item['terms_conditions']);
            }
            $setting->lang = json_encode($arr);
            $setting->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function delete($id,$code)
    {
        DB::beginTransaction();
        try {
            $setting = WebSiteSettings::where('id',$id)->first();
            $lang = json_decode($setting->lang);
            $arr = [];
            foreach ($lang as $key => $item){
                if ($key !== $code){
                    $arr[$key] = $item;
                }
            }
            $setting->lang = json_encode($arr);
            $setting->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
