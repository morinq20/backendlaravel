<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Notifications\Facades\Vonage;
use Illuminate\Support\Facades\DB;
use Twilio\Rest\Client;

class DefaultController extends Controller
{
//    public function adminNotification(Request $request)
//    {
//        $adminNotification = DB::table('notifications')->where('notifiable_id',$request->user_id)->orderBy('created_at','DESC')->get();
//        return response()->json($adminNotification);
//    }
//
//    public function index($id = null)
//    {
//        $adminNotification = DB::table('notifications')->where('notifiable_id',$id)->orderBy('created_at','DESC')->get();
//        return view('welcome',compact('adminNotification'));
//    }

    public function deneme()
    {
        try {
            $a = env('TWILIO_SID');
            $b = env('TWILIO_TOKEN');
            $c = env('TWILIO_FROM');
            $client = new Client($a,$b);
            $client->messages->create('+905070628344',[
                'from' => $c,
                'body' => 'deneme sms'
            ]);
            return "Message sent....";
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }

}
}
