<?php

namespace App\Http\Controllers\Backend;

use App\Component\SearchStockTransferProcess\SearchStockTransferProcessHandler;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SearchStockTransferProcessController extends Controller
{
    //
    public function __construct(private  SearchStockTransferProcessHandler $searchStockTransferProcessHandler)
    {
    }


    public function __invoke(Request $request)
    {
         $this->searchStockTransferProcessHandler->handle($request);

    }
}
