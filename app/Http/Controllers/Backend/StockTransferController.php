<?php

namespace App\Http\Controllers\Backend;

use App\Enum\EmailTemplateEnum;
use App\Events\OrderEmail;
use App\Events\StockTransferEmail;
use App\Http\Controllers\Controller;
use App\Http\Services\CodeService\CodeService;
use App\Http\Services\Utils\Converter\Converter;
use App\Jobs\SendEmailByStockTransfer;
use App\Models\Pallet;
use App\Models\PanelSettings;
use App\Models\Product;
use App\Models\StockTransfer;
use App\Models\StockTransferCart;
use App\Models\StockTransferItem;
use App\Models\StockTransferStatus;
use App\Models\Supplier;
use App\Models\Truck;
use App\Models\Warehouse;
use App\Models\WarehouseItem;
use App\Models\Years;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StockTransferController extends Controller
{
    public function __construct(private StockTransferStatus $stockTransferStatus,
    private StockTransfer $stockTransfer)
    {
    }

    public function index(Request $request)
    {
        $stockTransfer = [];
        $allStockTransfer = $this->stockTransfer->where('status',$request->status)->orderBy('id','DESC')->get();
        foreach ($allStockTransfer as $key => $item) {
            $fromWarehouse = Warehouse::select('warehouse_name','warehouse_type')->where('id',$item->from_warehouse_id)->first();
            $toWarehouse = Warehouse::select('warehouse_name','warehouse_type')->where('id',$item->to_warehouse_id)->first();
            $pallet = Pallet::select('pallet_name')->where('id',$item->pallet_id)->first();
//            $supplier = Supplier::select('supplier_name','supplier_surname')->where('id',$item->supplier_id)->first();
//            $truck = Truck::select('truck_plate_number')->where('id',$item->truck_id)->first();
            $stockTransfer[$key]['code'] = $item->code;
            $stockTransfer[$key]['fromWarehouse_name'] = $fromWarehouse->warehouse_name;
            $stockTransfer[$key]['fromWarehouse_type'] = $fromWarehouse->warehouse_type;
            $stockTransfer[$key]['toWarehouse_name'] = $toWarehouse->warehouse_name;
            $stockTransfer[$key]['toWarehouse_type'] = $toWarehouse->warehouse_type;
            $stockTransfer[$key]['total'] = $item->total;
            $stockTransfer[$key]['total_product'] = $item->total_product;
            $stockTransfer[$key]['total_quantity'] = $item->total_quantity;
            $stockTransfer[$key]['total_process'] = $item->total_process;
            $stockTransfer[$key]['total_weight'] = $item->total_weight;
            $stockTransfer[$key]['pallet_count'] = $item->pallet_count;
            $stockTransfer[$key]['pallet_name'] = $pallet->pallet_name;
//            $stockTransfer[$key]['supplier_name'] = $supplier->supplier_name;
//            $stockTransfer[$key]['supplier_surname'] = $supplier->supplier_surname;
//            $stockTransfer[$key]['truck_plate_number'] = $truck->truck_plate_number;
            $stockTransfer[$key]['start_transfer_date'] = $item->start_transfer_date;
            $stockTransfer[$key]['finish_transfer_date'] = $item->finish_transfer_date;
            $stockTransfer[$key]['status'] = $item->status;
            $stockTransfer[$key]['id'] = $item->id;
            $stockTransfer[$key]['status_text'] = $this->stockTransferStatus->where('status_id',$item->status)->get();

        }
        return response()->json($stockTransfer);
    }

    public function show($id)
    {
        $findStockTransfer = StockTransfer::find($id);
        $findStockTransferItem = StockTransferItem::where('stock_transfer_id',$id)->get();
        $fromWarehouse = Warehouse::where('id',$findStockTransfer->from_warehouse_id)->first();
        $toWarehouse = Warehouse::where('id',$findStockTransfer->to_warehouse_id)->first();
        $pallet = Pallet::find($findStockTransfer->pallet_id);
        $settings = PanelSettings::find(0);
        $stockTransfer = [];
        $stockTransfer['stockTransfer']['fromWarehouse_name'] = $fromWarehouse->warehouse_name;
        $stockTransfer['stockTransfer']['fromWarehouse_address'] = $fromWarehouse->warehouse_address;
        $stockTransfer['stockTransfer']['fromWarehouse_country'] = $fromWarehouse->warehouse_country;
        $stockTransfer['stockTransfer']['fromWarehouse_city'] = $fromWarehouse->warehouse_city;
        $stockTransfer['stockTransfer']['fromWarehouse_phone'] = $fromWarehouse->warehouse_phone;
        $stockTransfer['stockTransfer']['fromWarehouse_phone_dial_code'] = $fromWarehouse->warehouse_phone_dial_code;
        $stockTransfer['stockTransfer']['fromWarehouse_email'] = $fromWarehouse->warehouse_email;
        $stockTransfer['stockTransfer']['fromWarehouse_postcode'] = $fromWarehouse->warehouse_postcode;
        $stockTransfer['stockTransfer']['fromWarehouse_status'] = $fromWarehouse->warehouse_status;
        $stockTransfer['stockTransfer']['fromWarehouse_type'] = $fromWarehouse->warehouse_type;
        $stockTransfer['stockTransfer']['toWarehouse_name'] = $toWarehouse->warehouse_name;
        $stockTransfer['stockTransfer']['toWarehouse_address'] = $toWarehouse->warehouse_address;
        $stockTransfer['stockTransfer']['toWarehouse_country'] = $toWarehouse->warehouse_country;
        $stockTransfer['stockTransfer']['toWarehouse_city'] = $toWarehouse->warehouse_city;
        $stockTransfer['stockTransfer']['toWarehouse_phone'] = $toWarehouse->warehouse_phone;
        $stockTransfer['stockTransfer']['toWarehouse_phone_dial_code'] = $toWarehouse->warehouse_phone_dial_code;
        $stockTransfer['stockTransfer']['toWarehouse_email'] = $toWarehouse->warehouse_email;
        $stockTransfer['stockTransfer']['toWarehouse_postcode'] = $toWarehouse->warehouse_postcode;
        $stockTransfer['stockTransfer']['toWarehouse_status'] = $toWarehouse->warehouse_status;
        $stockTransfer['stockTransfer']['toWarehouse_type'] = $toWarehouse->warehouse_type;
        $stockTransfer['stockTransfer']['start_transfer_date'] = $findStockTransfer->start_transfer_date;
        $stockTransfer['stockTransfer']['finish_transfer_date'] = $findStockTransfer->finish_transfer_date;
        $stockTransfer['stockTransfer']['status'] = $findStockTransfer->status;
        $stockTransfer['stockTransfer']['id'] = $findStockTransfer->id;
        $stockTransfer['stockTransfer']['total_product'] = $findStockTransfer->total_product;
        $stockTransfer['stockTransfer']['total_quantity'] = $findStockTransfer->total_quantity;
        $stockTransfer['stockTransfer']['total_process'] = $findStockTransfer->total_process;
        $stockTransfer['stockTransfer']['total_weight'] = $findStockTransfer->total_weight;
        $stockTransfer['stockTransfer']['pallet_name'] = $pallet->pallet_name;
        $stockTransfer['stockTransfer']['pallet_count'] = $findStockTransfer->pallet_count;
        $stockTransfer['stockTransfer']['total'] = $findStockTransfer->total;
        $stockTransfer['stockTransfer']['subtotal'] = $findStockTransfer->subtotal;
        $stockTransfer['stockTransfer']['total_pallet'] = $findStockTransfer->total_pallet;
        $stockTransfer['stockTransfer']['created_at'] = $findStockTransfer->created_at->format('d-m-Y H:i:s');
        $stockTransfer['stockTransfer']['code'] = $findStockTransfer->code;
        $stockTransfer['stockTransfer']['logo'] = $settings->panel_image;
        foreach ($findStockTransferItem as $key => $item) {
            $findVariant = DB::table('stock_transfer_item')
                ->select('variant.*','color.color_name','color.color_code','size.size_name',
                    'product.product_name','product.product_code','product.product_image')
                ->join('variant','stock_transfer_item.variant_id','=','variant.id')
                ->join('color','variant.color_id','=','color.id')
                ->join('size','variant.size_id','=','size.id')
                ->join('product','stock_transfer_item.product_id','=','product.id')
                ->where('variant.id',$item->variant_id)
                ->where('product.id',$item->product_id)
                ->first();
            $stockTransfer['variant'][$key] = $findVariant;
        }
        return response()->json($stockTransfer);
    }

//    public function showProduct($id)
//    {
//        $findStockTransfer = StockTransfer::find($id);
//        $findStockTransferItem = StockTransferItem::where('stock_transfer_id',$id)->get();
//        $stockTransferProduct = [];
//        foreach ($findStockTransferItem as $key => $item) {
//            $findProduct = DB::table('stock_transfer_item')
//                ->select('stock_transfer_item.*','product.product_code','product.product_name',
//                    'product.product_image','unit.unit_name')
//                ->join('unit','stock_transfer_item.unit_id','=','unit.id')
//                ->join('product','stock_transfer_item.product_id','=','product.id')
//                ->where('stock_transfer_id',$id)
//                ->where('product.id',$item->product_id)
//                ->where('variant_id','=',null)
//                ->first();
//            if (isset($findProduct)) {
//                $stockTransferProduct[] = $findProduct;
//            }
//        }
//        return response()->json($stockTransferProduct);
//    }

    public function showVariant($id)
    {
        $findStockTransfer = StockTransfer::find($id);
        $findStockTransferItem = StockTransferItem::where('stock_transfer_id',$id)->get();
        $stockTransferVariant = [];
        foreach ($findStockTransferItem as $key => $item) {
            $findVariant = DB::table('stock_transfer_item')
                ->select('stock_transfer_item.id','stock_transfer_item.product_quantity','variant.variant_image','variant.sku_no','color.color_name',
                    'color.color_code','size.size_name', 'product.product_name','product.product_code','unit.unit_name')
                ->join('variant','stock_transfer_item.variant_id','=','variant.id')
                ->join('color','variant.color_id','=','color.id')
                ->join('size','variant.size_id','=','size.id')
                ->join('unit','variant.unit_id','=','unit.id')
                ->join('product','stock_transfer_item.product_id','=','product.id')
                ->where('variant.id',$item->variant_id)
                ->where('product.id',$item->product_id)
                ->where('stock_transfer_id',$id)
                ->first();
            if (isset($findVariant)) {
                $stockTransferVariant[] = $findVariant;
            }

        }
        return response()->json($stockTransferVariant);
    }

    public function gettingProduct($id)
    {
        $product = Product::where('category_id',$id)->orderBy('id','DESC')->get();
        return response()->json($product);
    }
    private function sendMail($stockTransferID){

        $stockTransfer = StockTransfer::find($stockTransferID);
        $fromWarehouse = Warehouse::find($stockTransfer->from_warehouse_id);
        $toWarehouse = Warehouse::find($stockTransfer->to_warehouse_id);
        $stockTransferStatus = StockTransferStatus::where('status_id',$stockTransfer->status)->first();
        $stockTransferItems = DB::table('stock_transfer_item')
            ->select('stock_transfer_item.*','product.product_name','product.product_code','product.product_image',
                'variant.sku_no','variant.variant_image','variant.variant_tax','variant.variant_selling_price','variant.variant_discount_price')
            ->join('product','stock_transfer_item.product_id','=','product.id')
            ->join('variant','stock_transfer_item.variant_id','=','variant.id')
            ->where('stock_transfer_item.stock_transfer_id',$stockTransfer->id)
            ->get();
        $settings = PanelSettings::find(0);
        SendEmailByStockTransfer::dispatch($toWarehouse,$stockTransfer,$settings,$stockTransferItems,$fromWarehouse,$stockTransferStatus);
//        $array = [
//            'stockTransfer' => [
//                'code' => $stockTransfer->code,
//                'start_transfer_date' => $stockTransfer->start_transfer_date,
//                'finish_transfer_date' => $stockTransfer->finish_transfer_date,
//                'total_product' => $stockTransfer->total_product,
//                'total_quantity' => $stockTransfer->total_quantity,
//                'total_process' => $stockTransfer->total_process,
//                'total_weight' => $stockTransfer->total_weight,
//                'pallet_count' => $stockTransfer->pallet_count,
//                'status' => $stockTransferStatus->status_text,
//                'process_total' => $stockTransfer->total,
//            ],
//            'from' => [
//                'warehouse_name' => $fromWarehouse->warehouse_name,
//                'warehouse_address' => $fromWarehouse->warehouse_address,
//                'warehouse_country' => $fromWarehouse->warehouse_country,
//                'warehouse_city' => $fromWarehouse->warehouse_city,
//                'warehouse_phone' => $fromWarehouse->warehouse_phone,
//                'warehouse_email' => $fromWarehouse->warehouse_email,
//                'warehouse_type' => $fromWarehouse->warehouse_type == 0 ? 'Sub Warehouse' : 'Main Warehouse'
//            ],
//            'to' => [
//                'warehouse_name' => $toWarehouse->warehouse_name,
//                'warehouse_address' => $toWarehouse->warehouse_address,
//                'warehouse_country' => $toWarehouse->warehouse_country,
//                'warehouse_city' => $toWarehouse->warehouse_city,
//                'warehouse_phone' => $toWarehouse->warehouse_phone,
//                'warehouse_email' => $toWarehouse->warehouse_email,
//                'warehouse_type' => $toWarehouse->warehouse_type == 0 ? 'Sub Warehouse' : 'Main Warehouse'
//            ],
//            'items' => $stockTransferItems,
//            'settings' => $settings
//        ];
//        $email = $toWarehouse->warehouse_email;


//        $stockTransfer = StockTransfer::find($stockTransferID);
//        event(new StockTransferEmail($stockTransfer,EmailTemplateEnum::STOCK_TRANSFER_TEMPLATE->value));
    }
    /**
     * @throws \Exception
     */
    public function storeStockTransfer(Request $request)
    {
        DB::beginTransaction();
        try {
            $fromWarehouse = Warehouse::find($request->form['from_warehouse_id']);
            $toWarehouse = Warehouse::find($request->form['to_warehouse_id']);
            if ($fromWarehouse->warehouse_type == 1 &&$toWarehouse->warehouse_type == 0) {
                $type = "MTS";
            }
            elseif($fromWarehouse->warehouse_type == 0 &&$toWarehouse->warehouse_type == 1) {
                $type = "STM";
            } elseif($fromWarehouse->warehouse_type == 0 &&$toWarehouse->warehouse_type == 0) {
                $type = "STS";
            }
            $code = CodeService::createCode($type);
            if ($fromWarehouse->warehouse_type == 1) {
                $stockTransfer = array();
                $stockTransfer['code'] = $code;
                $stockTransfer['from_warehouse_id'] = $request->form['from_warehouse_id'];
                $stockTransfer['to_warehouse_id'] = $request->form['to_warehouse_id'];
                $stockTransfer['total_process'] = $request->form['processCount'];
                $stockTransfer['start_transfer_date'] = $request->form['start_transfer_date'];
                $stockTransfer['finish_transfer_date'] = $request->form['finish_transfer_date'];
                $stockTransfer['pallet_id'] = $request->form['pallet_id'];
                $stockTransfer['pallet_count'] = $request->form['pallet_count'];
                $stockTransfer['total_pallet'] = $request->form['total_pallet'];
                $stockTransfer['total_weight'] = $request->form['total_weight'];
                $stockTransfer['total_product'] = $this->totalProduct();
                $stockTransfer['total_quantity'] = $this->totalQuantity();
                $stockTransfer['total'] = $request->form['total'];
                $stockTransfer['subtotal'] = $request->form['subtotal'];
                $stockTransfer['status'] = 0;
                $stockTransfer['isCanceled'] = 0;
                $stockTransfer['date'] = date('d', strtotime($request->form['start_transfer_date']));
                $stockTransfer['month'] = date('F',strtotime($request->form['start_transfer_date']));
                $stockTransfer['year'] = date('Y',strtotime($request->form['start_transfer_date']));
                $stockTransfer['created_at'] = new \DateTime();
                $stockTransfer['updated_at'] = new \DateTime();
                $stockTransferId = DB::table('stock_transfer')->insertGetId($stockTransfer);
                $process = [];
                $array = StockTransfer::where('id',$stockTransferId)->first();
                foreach ($request->process as $item => $key){
                    $process[] = $request->process[$item];
                    if ($key['supplier'] != null){
                        $supplier = Supplier::select('supplier_name','supplier_surname','supplier_phone','supplier_email','supplier_phone_dial_code')
                            ->where('id','=',$key['supplier'])->first();
                        $process[$item]['supplierInformation'] = $supplier;
                    }
                    if ($key['truck'] != null){
                        $truck = Truck::select('truck_brand','truck_model','truck_plate_number','truck_code','truck_start_km','truck_end_km','truck_image')
                            ->where('id','=',$key['truck'])->first();
                        $process[$item]['truckInformation'] = $truck;
                    }
                }
                $array->stock_transfer_process = $process;
                $array->save();
                $this->stockTransferItem($stockTransferId);
                $this->stockTransferCartDelete();
                $this->mainWarehouseItemDecrement($stockTransferId);
                $archiveControl = Years::where('year',date('Y',strtotime($request->form['start_transfer_date'])))->first();
                if (!$archiveControl){
                    $archive = new Years();
                    $archive->year = date('Y',strtotime($request->form['start_transfer_date']));
                    $archive->save();
                }
                $this->sendMail($stockTransferId);

            } else {
                $stockTransfer = array();
                $stockTransfer['code'] = $code;
                $stockTransfer['from_warehouse_id'] = $request->form['from_warehouse_id'];
                $stockTransfer['to_warehouse_id'] = $request->form['to_warehouse_id'];
                $stockTransfer['total_process'] = $request->form['processCount'];
                $stockTransfer['start_transfer_date'] =$request->form['start_transfer_date'];
                $stockTransfer['finish_transfer_date'] = $request->form['finish_transfer_date'];
                $stockTransfer['pallet_id'] = $request->form['pallet_id'];
                $stockTransfer['pallet_count'] = $request->form['pallet_count'];
                $stockTransfer['total_pallet'] = $request->form['total_pallet'];
                $stockTransfer['total_weight'] = $request->form['total_weight'];
                $stockTransfer['total_product'] = $this->totalProduct();
                $stockTransfer['total_quantity'] = $this->totalQuantity();
                $stockTransfer['total'] = $request->form['total'];
                $stockTransfer['subtotal'] = $request->form['subtotal'];
                $stockTransfer['status'] = 0;
                $stockTransfer['isCanceled'] = 0;
                $stockTransfer['date'] = date('d', strtotime($request->form['start_transfer_date']));
                $stockTransfer['month'] = date('F',strtotime($request->form['start_transfer_date']));
                $stockTransfer['year'] = date('Y',strtotime($request->form['start_transfer_date']));
                $stockTransfer['created_at'] = new \DateTime();
                $stockTransfer['updated_at'] = new \DateTime();
                $stockTransferId = DB::table('stock_transfer')->insertGetId($stockTransfer);
                $process = [];
                $array = StockTransfer::where('id',$stockTransferId)->first();
//            return response()->json($request->process);
                foreach ($request->process as $item => $key){
                    $process[] = $request->process[$item];
                    if ($key['supplier'] != null){
                        $supplier = Supplier::select('supplier_name','supplier_surname','supplier_phone','supplier_email','supplier_phone_dial_code')
                            ->where('id','=',$key['supplier'])->first();
                        $process[$item]['supplierInformation'] = iconv("ISO-8859-1", "UTF-8", $supplier);
                    }
                    if ($key['truck'] != null){
                        $truck = Truck::select('truck_brand','truck_model','truck_plate_number','truck_code','truck_start_km','truck_end_km','truck_image')
                            ->where('id','=',$key['truck'])->first();
                        $process[$item]['truckInformation'] = $truck;
                    }
                }
                $array->stock_transfer_process = $process;
                $array->save();
                $this->stockTransferItem($stockTransferId);
                $this->stockTransferCartDelete();
                $this->warehouseItemDecrement($stockTransferId);
                $archiveControl = Years::where('year',date('Y',strtotime($request->form['start_transfer_date'])))->first();
                if (!$archiveControl){
                    $archive = new Years();
                    $archive->year = date('Y',strtotime($request->form['start_transfer_date']));
                    $archive->save();
                }
                $this->sendMail($stockTransferId);

            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    // All Stock Transfer Cart
    private function stockTransferCart() {
        $stockTransferCart = StockTransferCart::all();
        return $stockTransferCart;
    }

    // Total Product
    private function totalProduct() {
        $total_product = count($this->stockTransferCart());
        return $total_product;
    }

    // Total Quantity

    /**
     * @throws \Exception
     */
    private function totalQuantity() {
        DB::beginTransaction();
        try {
            $stockTransferCart = $this->stockTransferCart();
            $total_quantity = 0;
            foreach ($stockTransferCart as $cart) {
                $total_quantity += $cart->product_quantity;
            }
            DB::commit();
            return $total_quantity;
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    // Stock Transfer Item

    /**
     * @throws \Exception
     */
    private function stockTransferItem($stockTransferId) {
        DB::beginTransaction();
        try {
            $stockTransferCart = $this->stockTransferCart();
            foreach ($stockTransferCart as $cart) {
                $stockTransferItem = new StockTransferItem();
                $stockTransferItem->stock_transfer_id = $stockTransferId;
                $stockTransferItem->product_id = $cart->product_id;
                $stockTransferItem->variant_id = $cart->variant_id;
                $stockTransferItem->unit_id = $cart->unit_id;
                $stockTransferItem->product_quantity = $cart->product_quantity;
                $stockTransferItem->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    // Sub Warehouse Item
//    private function warehouseItem($id) {
//        $stockTransferItem = StockTransferItem::where('stock_transfer_id',$id)->get();
//        $findStockTransfer = StockTransfer::find($id);
//        $findWarehouse = Warehouse::find($findStockTransfer->to_warehouse_id);
//        if ($findStockTransfer->status == 5) {
//            if ($findWarehouse->warehouse_type == 1) {
//                foreach ($stockTransferItem as $cart) {
//                    if ($cart->variant_id == null) {
//                        DB::table('product')->where('id',$cart->product_id)
//                            ->update(['product_quantity' => DB::raw('product_quantity +' . $cart->product_quantity)]);
//                    } else {
//                        DB::table('variant')->where('id',$cart->variant_id)
//                            ->where('product_id',$cart->product_id)
//                            ->update(['variant_quantity' => DB::raw('variant_quantity +' . $cart->product_quantity)]);
//                    }
//                }
//            } else {
//                foreach ($stockTransferItem as $cart) {
//                    // Warehouse type 1 ise products veya variant kaydetsin değilse warehouse item tablosuna
//                    if ($cart->variant_id == null) {
//                        $control = WarehouseItem::where('warehouse_id', $findStockTransfer->to_warehouse_id)->where('product_id', $cart->product_id)
//                            ->where('variant_id', null)->first();
//                        if ($control) {
//                            DB::table('warehouse_item')
//                                ->where('warehouse_id', $findStockTransfer->to_warehouse_id)
//                                ->where('product_id', $cart->product_id)
//                                ->where('variant_id', null)
//                                ->update(['product_quantity' => DB::raw('product_quantity +' . $cart->product_quantity)]);
//                        } else {
//                            $warehouseItem = new WarehouseItem();
//                            $warehouseItem->warehouse_id = $findStockTransfer->to_warehouse_id;
//                            $warehouseItem->product_id = $cart->product_id;
//                            $warehouseItem->variant_id = null;
//                            $warehouseItem->unit_id = $cart->unit_id;
//                            $warehouseItem->product_quantity = $cart->product_quantity;
//                            $warehouseItem->status = 1;
//                            $warehouseItem->save();
//                        }
//                    } else {
//                        $control = WarehouseItem::where('warehouse_id', $findStockTransfer->to_warehouse_id)->where('product_id', $cart->product_id)
//                            ->where('variant_id', $cart->variant_id)->first();
//                        if ($control) {
//                            DB::table('warehouse_item')
//                                ->where('warehouse_id', $findStockTransfer->to_warehouse_id)
//                                ->where('product_id', $cart->product_id)
//                                ->where('variant_id', $cart->variant_id)
//                                ->update(['product_quantity' => DB::raw('product_quantity +' . $cart->product_quantity)]);
//                        } else {
//                            $warehouseItem = new WarehouseItem();
//                            $warehouseItem->warehouse_id = $findStockTransfer->to_warehouse_id;
//                            $warehouseItem->product_id = $cart->product_id;
//                            $warehouseItem->variant_id = $cart->variant_id;
//                            $warehouseItem->unit_id = $cart->unit_id;
//                            $warehouseItem->product_quantity = $cart->product_quantity;
//                            $warehouseItem->status = 1;
//                            $warehouseItem->save();
//                        }
//                    }
//                }
//            }
//
//        }
//    }

    // Sub Warehouse Item Quantity Decrement
    /**
     * @throws \Exception
     */
    private function warehouseItemDecrement($id) {
        DB::beginTransaction();
        try {
            $stockTransferItem = StockTransferItem::where('stock_transfer_id',$id)->get();
            $findStockTransfer = StockTransfer::find($id);
            foreach ($stockTransferItem as $cart) {
                $control = WarehouseItem::where('warehouse_id',$findStockTransfer->from_warehouse_id)
                    ->where('variant_id',$cart->variant_id)
                    ->where('product_id',$cart->product_id)
                    ->first();
                if ($control->product_quantity > 0) {
                    DB::table('warehouse_item')->where('warehouse_id',$findStockTransfer->from_warehouse_id)->where('variant_id',$cart->variant_id)
                        ->where('product_id',$cart->product_id)->update(['product_quantity' => DB::raw('product_quantity -' .$cart->product_quantity)]);
                } else {
                    return response()->json("error");
                }

            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    // Main Warehouse Item Product Quantity Decrement

    /**
     * @throws \Exception
     */
    private function mainWarehouseItemDecrement($id) {
        DB::beginTransaction();
        try {
            $stockTransferItem = StockTransferItem::where('stock_transfer_id',$id)->get();
            foreach ($stockTransferItem as $cart) {
                DB::table('product')->where('id',$cart->product_id)
                    ->update(['product_quantity' => DB::raw('product_quantity -' .$cart->product_quantity)]);
                DB::table('variant')->where('id',$cart->variant_id)
                    ->where('product_id',$cart->product_id)
                    ->update(['variant_quantity' => DB::raw('variant_quantity -' .$cart->product_quantity)]);
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

    // Main Warehouse Item
//    private function mainWarehouseItem($id) {
//        $stockTransferItem = StockTransferItem::where('stock_transfer_id',$id)->get();
//        $findStockTransfer = StockTransfer::find($id);
//        if ($findStockTransfer->status == 5) {
//            foreach ($stockTransferItem as $cart) {
//                // Warehouse type 1 ise products veya variant kaydetsin değilse warehouse item tablosuna
//                if ($cart->variant_id == null) {
//                    $control = WarehouseItem::where('warehouse_id',$findStockTransfer->to_warehouse_id)->where('product_id',$cart->product_id)
//                        ->where('variant_id',null)->first();
//                    if ($control) {
//                        DB::table('warehouse_item')
//                            ->where('warehouse_id',$findStockTransfer->to_warehouse_id)
//                            ->where('product_id',$cart->product_id)
//                            ->where('variant_id',null)
//                            ->update(['product_quantity' => DB::raw('product_quantity +' .$cart->product_quantity)]);
//                    } else {
//                        $warehouseItem = new WarehouseItem();
//                        $warehouseItem->warehouse_id = $findStockTransfer->to_warehouse_id;
//                        $warehouseItem->product_id = $cart->product_id;
//                        $warehouseItem->variant_id = null;
//                        $warehouseItem->unit_id = $cart->unit_id;
//                        $warehouseItem->product_quantity = $cart->product_quantity;
//                        $warehouseItem->status = 1;
//                        $warehouseItem->save();
//                    }
//                } else {
//                    $control = WarehouseItem::where('warehouse_id',$findStockTransfer->to_warehouse_id)->where('product_id',$cart->product_id)
//                        ->where('variant_id',$cart->variant_id)->first();
//                    if ($control) {
//                        DB::table('warehouse_item')
//                            ->where('warehouse_id',$findStockTransfer->to_warehouse_id)
//                            ->where('product_id',$cart->product_id)
//                            ->where('variant_id',$cart->variant_id)
//                            ->update(['product_quantity' => DB::raw('product_quantity +' .$cart->product_quantity)]);
//                    } else {
//                        $warehouseItem = new WarehouseItem();
//                        $warehouseItem->warehouse_id = $findStockTransfer->to_warehouse_id;
//                        $warehouseItem->product_id = $cart->product_id;
//                        $warehouseItem->variant_id = $cart->variant_id;
//                        $warehouseItem->unit_id = $cart->unit_id;
//                        $warehouseItem->product_quantity = $cart->product_quantity;
//                        $warehouseItem->status = 1;
//                        $warehouseItem->save();
//                    }
//                }
//            }
//        }
//
//    }

    // Stock Transfer Cart Delete
    /**
     * @throws \Exception
     */
    private function stockTransferCartDelete() {
        DB::beginTransaction();
        try {
            foreach ($this->stockTransferCart() as $cart) {
                StockTransferCart::destroy($cart->id);
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }


    public function getStockTransferProcess($id)
    {
        $stockTransfer = StockTransfer::find($id);
        $process = json_decode($stockTransfer->stock_transfer_process);
        return response()->json($process);
    }



}
