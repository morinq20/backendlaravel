<?php

namespace App\Http\Controllers\Backend;

use App\Component\Order\SearchOrderProcess\SearchOrderProcessHandler;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SearchOrderProcessController extends Controller
{
    //

    public function __construct(private SearchOrderProcessHandler $orderProcessHandler)
    {

    }


    public function __invoke(Request $request)
    {
        $this->orderProcessHandler->handle($request);
        // TODO: Implement __invoke() method.
    }
}
