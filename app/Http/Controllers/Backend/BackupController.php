<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\DB;

class BackupController extends Controller
{
    /**
     * @throws Exception
     */
    public function backup()
    {
        $dbhost = env('DB_HOST');
        $dbuser = env('DB_USERNAME');
        $dbpass = env('DB_PASSWORD');
        $dbname = env('DB_DATABASE');
        DB::beginTransaction();
        try {
            $connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
            $backupAlert = '';
            $tables = array();
            $result = mysqli_query($connection, "SHOW TABLES");
            if (!$result) {
                $backupAlert = 'Error found.<br/>ERROR : ' . mysqli_error($connection) . 'ERROR NO :' . mysqli_errno($connection);
            } else {
                while ($row = mysqli_fetch_row($result)) {
                    $tables[] = $row[0];
                }
                mysqli_free_result($result);

                $return = '';
                foreach ($tables as $table) {

                    $result = mysqli_query($connection, "SELECT * FROM " . $table);
                    if (!$result) {
                        $backupAlert = 'Error found.<br/>ERROR : ' . mysqli_error($connection) . 'ERROR NO :' . mysqli_errno($connection);
                    } else {
                        $num_fields = mysqli_num_fields($result);
                        if (!$num_fields) {
                            $backupAlert = 'Error found.<br/>ERROR : ' . mysqli_error($connection) . 'ERROR NO :' . mysqli_errno($connection);
                        } else {
                            $return .= 'DROP TABLE ' . $table . ';';
                            $row2 = mysqli_fetch_row(mysqli_query($connection, 'SHOW CREATE TABLE ' . $table));
                            if (!$row2) {
                                $backupAlert = 'Error found.<br/>ERROR : ' . mysqli_error($connection) . 'ERROR NO :' . mysqli_errno($connection);
                            } else {
                                $return .= "\n\n" . $row2[1] . ";\n\n";
                                for ($i = 0; $i < $num_fields; $i++) {
                                    while ($row = mysqli_fetch_row($result)) {
                                        $return .= 'INSERT INTO ' . $table . ' VALUES(';
                                        for ($j = 0; $j < $num_fields; $j++) {
                                            $row[$j] = addslashes($row[$j]);
                                            if (isset($row[$j])) {
                                                $return .= '"' . $row[$j] . '"';
                                            } else {
                                                $return .= '""';
                                            }
                                            if ($j < $num_fields - 1) {
                                                $return .= ',';
                                            }
                                        }
                                        $return .= ");\n";
                                    }
                                }
                                $return .= "\n\n\n";
                            }
                            date_default_timezone_set('Europe/Istanbul');
                            $path = public_path()."/backup/";
                            $backup_file = $path."backup_".date("d-m-Y-H-i-s").".sql";
//                        $backup_file = $dbname . date("Y-m-d-H-i-s") . '.sql';
                            $handle = fopen("{$backup_file}", 'w+');
                            fwrite($handle, $return);
                            fclose($handle);
                            $backupAlert = 'Succesfully got the backup!';
                        }
                    }
                }
            }
            DB::commit();
            return response()->json(true);
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
