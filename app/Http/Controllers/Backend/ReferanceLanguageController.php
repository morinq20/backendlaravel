<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Referance;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReferanceLanguageController extends Controller
{
    public function show(Request $request,$id): \Illuminate\Http\JsonResponse
    {
        $referance = Referance::where('id',$id)->first();
        return response()->json(json_decode($referance->lang));
    }

    /**
     * @throws Exception
     */
    public function store(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            $referance = Referance::where('id',$id)->first();
            $arr = [];
            foreach ($requestAll as $key => $item){
                $lang = Language::find($item['language_id']);
                $arr[$lang->code]['language_id'] = $item['language_id'];
                $arr[$lang->code]['language_code'] = $lang->code;
                $arr[$lang->code]['referance_title'] = $item['referance_title'];
                $arr[$lang->code]['referance_description'] = strip_tags($item['referance_description']);
            }
            if ($referance->lang !== null){
                $lang = json_decode($referance->lang);
                foreach ($lang as $key => $item){
                    $arr[$key] = $item;
                }
            }
            $referance->lang = json_encode($arr);
            $referance->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }

    }

    /**
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $requestAll = $request->all();
            $referance = Referance::where('id',$id)->first();
            $arr = [];
            foreach ($requestAll as $key => $item){
                $lang = Language::find($item['language_id']);
                $arr[$lang->code]['language_id'] = $item['language_id'];
                $arr[$lang->code]['language_code'] = $lang->code;
                $arr[$lang->code]['referance_title'] = $item['referance_title'];
                $arr[$lang->code]['referance_description'] = strip_tags($item['referance_description']);
            }
            $referance->lang = json_encode($arr);
            $referance->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function delete($id,$code)
    {
        DB::beginTransaction();
        try {
            $referance = Referance::where('id',$id)->first();
            $lang = json_decode($referance->lang);
            $arr = [];
            foreach ($lang as $key => $item){
                if ($key !== $code){
                    $arr[$key] = $item;
                }
            }
            $referance->lang = json_encode($arr);
            $referance->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
