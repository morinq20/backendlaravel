<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\File;
trait FolderControlTrait
{
    public function folderIfNotExists(string $folder)
    {
        if(!File::isDirectory(public_path().'/'.$folder)){
            File::makeDirectory(public_path().'/'.$folder, 0777, true, true);
        }
    }
}
