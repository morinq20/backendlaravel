<?php

namespace App\Component\Country;

use App\Models\Country;

class CountryIndexHandler{

    public function __construct(private Country $country){

    }


    public function handle(){
        $countryEntity = $this->country->all();
        return response()->json($countryEntity);

    }
}
