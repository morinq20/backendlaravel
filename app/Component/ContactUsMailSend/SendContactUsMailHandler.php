<?php

namespace App\Component\ContactUsMailSend;

use App\Enum\EmailTemplateEnum;
use App\Events\ContactUsEmail;
use App\Listeners\SendContactUsEmail;
use Illuminate\Http\Request;

class SendContactUsMailHandler{

    public function __construct()
    {
    }

    public function handle(Request $request)
    {
        event(new ContactUsEmail($request->toArray(),EmailTemplateEnum::STOCK_TRANSFER_TEMPLATE->value));
    }
}
