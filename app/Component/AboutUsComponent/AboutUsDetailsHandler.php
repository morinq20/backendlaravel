<?php

namespace App\Component\AboutUsComponent;

use App\Models\AboutUs;

class AboutUsDetailsHandler{

    public function __construct(private AboutUs  $aboutUs)
    {
    }

    public function handle()
    {
        $aboutUsEntity = $this->aboutUs->where('about_us_status',1)->first();
        if($aboutUsEntity){
            return response()->json($aboutUsEntity);
        }else{
            return response()->json('no found about us');
        }

    }
}
