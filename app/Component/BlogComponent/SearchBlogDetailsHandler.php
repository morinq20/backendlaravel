<?php

namespace App\Component\BlogComponent;

use App\Models\Blog;

class SearchBlogDetailsHandler
{

    public function __construct(private Blog $blog)
    {

    }

    public function handle($id)
    {
        $blogEntity = $this->blog->where('id',$id)->first();
        if($blogEntity){
            return response()->json($blogEntity);
        }else{
            return response()->json('no found blog');
        }

    }
}
