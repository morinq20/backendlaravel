<?php

namespace App\Component\BlogComponent;

use App\Models\Blog;

class BlogIndexHandler
{
    public function __construct(private Blog $blog)
    {

    }

    public function handle()
    {
        $blogEntity = $this->blog->where('blog_status',1)->orderBy('id','DESC')->get();
        if($blogEntity)
        {
            return response()->json($blogEntity);
        }else{
            return response()->json('no found blogs');
        }

    }
}
