<?php

namespace App\Component\SmtpSettings;

use App\Models\SmtpSetting;

class SmtpSettingsIndexHandler{

    public function __construct(private SmtpSetting $smtpSetting){

    }

    public function handle(){
        $smtpSettings = $this->smtpSetting->first();
        return response()->json($smtpSettings);
    }
}
