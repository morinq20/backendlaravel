<?php

namespace App\Component\SmtpSettings;

use App\Models\SmtpSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SmtpSettingsHandler{

    public function __construct()
    {

    }

    /**
     * @throws \Exception
     */
    public function handle(Request $request): void
    {
        DB::beginTransaction();
        try {
            if($request->id === null){
                $smtpEntity = new SmtpSetting();
                $smtpEntity->smtp_server = $request->smtp_server;
                $smtpEntity->smtp_email = $request->smtp_email;
                $smtpEntity->smtp_password = $request->smtp_password;
                $smtpEntity->smtp_port = $request->smtp_port;
                $smtpEntity->save();
            }else{
                $smtpEntity = SmtpSetting::where('id',$request->id)->first();
                $smtpEntity->smtp_server = $request->smtp_server;
                $smtpEntity->smtp_email = $request->smtp_email;
                $smtpEntity->smtp_password = $request->smtp_password;
                $smtpEntity->smtp_port = $request->smtp_port;
                $smtpEntity->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }
}
