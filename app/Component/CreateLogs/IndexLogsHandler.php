<?php

namespace App\Component\CreateLogs;

use App\Models\Logs;
use App\Models\Role;
use App\Models\User;
use http\Exception;

class IndexLogsHandler{
    public function __construct(private Logs $logs
        , private User $user, private Role $role)
    {

    }

    public function handle()
    {
        $logsEntity = $this->logs->orderBy('id','DESC')->limit(10)->get();
        foreach ($logsEntity as $item=>$key){
            $user = $this->user->where('id',$key->user_id)->first();
            $role = $this->role->where('id',$user->role_id)->get();
            $logsEntity[$item]['user'] = $user;
            $logsEntity[$item]['user']['user_role'] = $role;
        }
        return $logsEntity;
    }
}
