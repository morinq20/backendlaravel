<?php

namespace App\Component\CreateLogs;

use App\Models\Logs;
use Illuminate\Http\Request;

class CreateLogsHandler{



    public function handle(string $logType, string $logCode,$request = null){

        if($request === null){
            $logsEntity = new Logs();
            $logsEntity->user_id = auth()->user()->getAuthIdentifier();
            $logsEntity->log_type = $logType;
            $logsEntity->log_code = $logCode;
            $logsEntity->remote_addr = $_SERVER['REMOTE_ADDR'];
            $logsEntity->request_method = $_SERVER['REQUEST_METHOD'];
            $logsEntity->request_uri = $_SERVER['REQUEST_URI'];
            $logsEntity->request_time = $_SERVER['REQUEST_TIME'];
            $logsEntity->http_user_agent = $_SERVER['HTTP_USER_AGENT'];
        }else{
            $logsEntity = new Logs();
            $logsEntity->user_id = auth()->user()->getAuthIdentifier();
            $logsEntity->log_type = $logType;
            $logsEntity->log_code = $logCode;
            $logsEntity->request_body = $request->getContent();
            $logsEntity->remote_addr = $_SERVER['REMOTE_ADDR'];
            $logsEntity->request_method = $_SERVER['REQUEST_METHOD'];
            $logsEntity->request_uri = $_SERVER['REQUEST_URI'];
            $logsEntity->request_time = $_SERVER['REQUEST_TIME'];
            $logsEntity->http_user_agent = $_SERVER['HTTP_USER_AGENT'];
            $logsEntity->http_accept_language = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
        }


        $logsEntity->save();

    }
}
