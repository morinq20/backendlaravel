<?php

namespace App\Component\FrontService;

use App\Models\Service;

class IndexServiceHandler{

    public function __construct(private Service $service)
    {
    }
    public function handle()
    {
        $serviceEntity = $this->service->where('service_status',1)->limit(4)->orderBy('id','DESC')->get();
        return response()->json($serviceEntity);
    }
}
