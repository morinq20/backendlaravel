<?php

namespace App\Component\GoogleMapsApi;

use App\Models\GoogleMapsApi;

class GoogleMapsApiIndex{

    public function __construct(private GoogleMapsApi $googleMapsApi)
    {
    }

    public function handle()
    {
        $googleMapsApi = $this->googleMapsApi->first();
        return response()->json($googleMapsApi);
    }
}
