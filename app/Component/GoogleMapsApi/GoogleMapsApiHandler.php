<?php

namespace App\Component\GoogleMapsApi;

use App\Models\GoogleMapsApi;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GoogleMapsApiHandler{

    public function __construct(private GoogleMapsApi $googleMapsApi){

    }

    /**
     * @throws Exception
     */
    public function handle(Request $request){
        DB::beginTransaction();
        try {
            if($request->id === null){
                $googleApiEntity = $this->googleMapsApi->first();
                if($googleApiEntity === null){
                    $googleApiEntity = new GoogleMapsApi();
                    $googleApiEntity->google_api = $request->google_api;
                    $googleApiEntity->save();
                }
                $googleApiEntity->google_api = $request->google_api;
                $googleApiEntity->save();
            }else{
                $googleApiEntity = $this->googleMapsApi->where('id',$request->id)->first();
                $googleApiEntity->google_api = $request->google_api;
                $googleApiEntity->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
