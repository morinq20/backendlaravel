<?php

namespace App\Component\Order\OrderReportSearch;

use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\Pallet;
use App\Models\StockTransfer;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderReportSearchHandler{

    public function __construct(private OrderStatus $orderStatus)
    {

    }

    /**
     * @throws \Exception
     */
    public function handle(Request $request)
    {
        DB::begintransaction();
        try {
            $startDate = $request->startDate;
            $endDate = $request->endDate;
            $order = [];
            $allOrder = Order::whereBetween('start_transfer_date',[$startDate,$endDate])->orderBy('id','DESC')->get();
            foreach ($allOrder as $key => $item) {
                $fromWarehouse = Warehouse::select('warehouse_name','warehouse_type')->where('id',$item->from_warehouse_id)->first();
                $pallet = Pallet::select('pallet_name')->where('id',$item->pallet_id)->first();
                $customer = Customer::select('customer_name','customer_surname','customer_phone')->where('id',$item->customer_id)->first();
                $order[$key]['code'] = $item->code;
                $order[$key]['fromWarehouse_name'] = $fromWarehouse->warehouse_name;
                $order[$key]['fromWarehouse_type'] = $fromWarehouse->warehouse_type;
                $order[$key]['total'] = $item->total;
                $order[$key]['total_product'] = $item->total_product;
                $order[$key]['total_quantity'] = $item->total_quantity;
                $order[$key]['total_process'] = $item->total_process;
                $order[$key]['total_weight'] = $item->total_weight;
                $order[$key]['pallet_count'] = $item->pallet_count;
                $order[$key]['pallet_name'] = $pallet->pallet_name;
                $order[$key]['start_transfer_date'] = $item->start_transfer_date;
                $order[$key]['finish_transfer_date'] = $item->finish_transfer_date;
                $order[$key]['status'] = $item->status;
                $order[$key]['id'] = $item->id;
                $order[$key]['order_status'] = $this->orderStatus->where('status_id', $item->status)->get();
                $order[$key]['customer'] = $customer;
            }
            DB::commit();
            return response()->json($order);
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }


    }
}
