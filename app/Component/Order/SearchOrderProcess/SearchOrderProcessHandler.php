<?php

namespace App\Component\Order\SearchOrderProcess;

use App\Models\Order;
use Illuminate\Http\Request;

class SearchOrderProcessHandler{

    public function __construct(private Order $order)
    {
    }


    public function handle(Request $request){

        $orderEntity = $this->order->find($request->id);
        $data = json_decode($orderEntity->order_transfer_process);
        if($orderEntity->order_transfer_process !== null){
            foreach ($data as $key=>$value){
                $index = $key+1;
                if($value->id === (int)$request->processId){
                    $value->status = 3;
                }
                if (in_array($index,array_keys($data))){

                    $data[$index]->status = 2;
                }
            }
            $orderEntity->order_transfer_process = json_encode($data);
            $orderEntity->save();
        }
    }
}
