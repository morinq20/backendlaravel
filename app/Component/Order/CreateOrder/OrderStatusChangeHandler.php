<?php

namespace App\Component\Order\CreateOrder;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\Variant;
use App\Models\Warehouse;
use App\Models\WarehouseItem;
use Exception;
use Illuminate\Support\Facades\DB;

class OrderStatusChangeHandler{

    public function __construct(private Order $order){

    }

    /**
     * @throws Exception
     */
    public function handle($request)
    {
        DB::beginTransaction();
        try {
            $order = $this->order->where('id',$request->orderId)->first();
            $uptadeStatus = (int)$request->status + 1;
            $order->status = $uptadeStatus;
            $this->getProcessStatusChange($order);
//            if($order->status === 6 || $order->status > 6){
//                $this->updateReturnRequestProcessStatus($stockTransfer);
//            }else if ($stockTransfer->status > 0 && $stockTransfer->status <= 5){
//                $this->updateStockTransferProcessStatus($stockTransfer);
//            }
            $order->save();
            if ($order->status === "11" || $order->status === 11){
                $this->addStockBack($request->orderId);
            }
            if ($order->status === "4" || $order->status === 4){
                $findWarehouse = Warehouse::find($order->from_warehouse_id);
                if ($findWarehouse->warehouse_type == 1) {
                    $this->mainWarehouseItem($request->orderId);
                } else {
                    $this->warehouseItem($request->orderId);
                }
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    private function addStockBack($id): void
    {
        DB::beginTransaction();
        try {
            $order = $this->order->find($id);
            $ordersItem = OrderItem::where('orders_id',$id)->get();
            if ($ordersItem){
                foreach ($ordersItem as $item){
                    $warehouseItem = WarehouseItem::where('warehouse_id',$order->from_warehouse_id)->where('product_id',$item->product_id)->where('variant_id',$item->variant_id)->get();
                    foreach ($warehouseItem as $value){
                        if ($value->product_quantity > 0){
                            $variant = Variant::find($item->variant_id);
                            $variant->variant_quantity += $item->product_quantity;
                            $variant->save();
                            $product = Product::find($variant->product_id);
                            $product->product_quantity += $item->product_quantity;
                            $product->save();
                            $value->product_quantity -= $item->product_quantity;
                            $value->save();
                        }
                    }
                }
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    // Sub Warehouse Item

    /**
     * @throws Exception
     */
    private function warehouseItem($id): void
    {
        DB::beginTransaction();
        try {
            $orderItem = OrderItem::where('orders_id',$id)->get();
            $findOrder = Order::find($id);
            $findWarehouse = Warehouse::find($findOrder->from_warehouse_id);
            if ($findWarehouse->warehouse_type == 1) {
                foreach ($orderItem as $cart) {
                    DB::table('variant')->where('id',$cart->variant_id)
                        ->where('product_id',$cart->product_id)
                        ->update(['variant_quantity' => DB::raw('variant_quantity +' . $cart->product_quantity)]);
                    DB::table('product')->where('id',$cart->product_id)
                        ->update(['product_quantity' => DB::raw('product_quantity +' .$cart->product_quantity)]);
                }
            } else {
                foreach ($orderItem as $cart) {
                    // Warehouse type 1 ise products veya variant kaydetsin değilse warehouse item tablosuna
                    $control = WarehouseItem::where('warehouse_id', $findOrder->from_warehouse_id)->where('product_id', $cart->product_id)
                        ->where('variant_id', $cart->variant_id)->first();
                    if ($control) {
                        DB::table('warehouse_item')
                            ->where('warehouse_id', $findOrder->from_warehouse_id)
                            ->where('product_id', $cart->product_id)
                            ->where('variant_id', $cart->variant_id)
                            ->update(['product_quantity' => DB::raw('product_quantity +' . $cart->product_quantity)]);
                    } else {
                        $warehouseItem = new WarehouseItem();
                        $warehouseItem->warehouse_id = $findOrder->from_warehouse_id;
                        $warehouseItem->product_id = $cart->product_id;
                        $warehouseItem->variant_id = $cart->variant_id;
                        $warehouseItem->unit_id = $cart->unit_id;
                        $warehouseItem->product_quantity = $cart->product_quantity;
                        $warehouseItem->status = 1;
                        $warehouseItem->save();
                    }
                }
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    // Main Warehouse Item

    /**
     * @throws Exception
     */
    private function mainWarehouseItem($id): void
    {
        DB::beginTransaction();
        try {
            $orderItem = OrderItem::where('orders_id',$id)->get();
            $findOrder = Order::find($id);
            foreach ($orderItem as $cart) {
                // Warehouse type 1 ise products veya variant kaydetsin değilse warehouse item tablosuna
                $control = WarehouseItem::where('warehouse_id',$findOrder->from_warehouse_id)->where('product_id',$cart->product_id)
                    ->where('variant_id',$cart->variant_id)->first();
                if ($control) {
                    DB::table('warehouse_item')
                        ->where('warehouse_id',$findOrder->from_warehouse_id)
                        ->where('product_id',$cart->product_id)
                        ->where('variant_id',$cart->variant_id)
                        ->update(['product_quantity' => DB::raw('product_quantity +' .$cart->product_quantity)]);
                } else {
                    $warehouseItem = new WarehouseItem();
                    $warehouseItem->warehouse_id = $findOrder->from_warehouse_id;
                    $warehouseItem->product_id = $cart->product_id;
                    $warehouseItem->variant_id = $cart->variant_id;
                    $warehouseItem->unit_id = $cart->unit_id;
                    $warehouseItem->product_quantity = $cart->product_quantity;
                    $warehouseItem->status = 1;
                    $warehouseItem->save();
                }
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }


    /**
     * @throws Exception
     */
    private function getProcessStatusChange($order):void
    {
        DB::beginTransaction();
        try {
            $data = json_decode($order->order_transfer_process);
            if($order->status === 2){
                $data[0]->status = 1;
                $order->order_transfer_process = json_encode($data);
                $order->save();
            }else if($order->status === 3){
                $data[0]->status = 2;
                foreach ($data as $key => $item){
                    if($data[$key]->status === 2){
                        $index = $key+1;
                    }
                    if(in_array($index, array_keys($data))){
                        $data[$index]->status = 1;
                    }

                }
                $order->order_transfer_process = json_encode($data);
                $order->save();
            }else if($order->status === 4){
                $data[0]->status = 3;
                foreach ($data as $key => $item){
                    if($data[$key]->status === 3){
                        $index = $key+1;
                    }
                    if(in_array($index, array_keys($data))){
                        $data[$index]->status = 2;
                    }

                }
                $order->order_transfer_process = json_encode($data);
                $order->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
