<?php

namespace App\Component\MailTemplatePayloadContent\NewsLetterTemplate;

use App\Component\MailTemplatePayloadContent\ModelsCheck;
use App\Models\Order;
use App\Models\User;
use function Symfony\Component\String\s;

class NewsLetterMailTemplate{

    public function __construct(private User $user,
    private ModelsCheck $modelsCheck)
    {

    }

    /**
     * @throws \ReflectionException
     */
    public function handle($entity)
    {
        $userEntity = $this->user->where('id',1)->get();
        dd($userEntity);
        $model = $this->modelsCheck->getModel($userEntity);
        if($entity instanceof  User){

            $date = date("d-m-Y", strtotime($userEntity->created_at));

            $array = [
                'name'=>$userEntity->name,
                'surname'=>$userEntity->surname,
                'date'=>$date
            ];

            return $array;
        }



    }
}
