<?php
namespace App\Component\MailTemplatePayloadContent;

use App\Enum\EmailTemplateEnum;
use App\Models\Announcements;
use App\Models\Blog;
use App\Models\Campaign;
use App\Models\Customer;
use App\Models\Newsletter;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\StockTransfer;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class ModelsCheck{


    public function __construct(private MailTemplateHelper $mailTemplateHelper)
    {

    }

    /**
     * @throws \ReflectionException
     */
    public function getModel($entity)
    {
        $reflection = new \ReflectionClass($entity);

        $models = [
                    User::class => $this->mailTemplateHelper->getUserWelcomeTemplate(),
                    StockTransfer::class => $this->mailTemplateHelper->getStockTransferTemplate() ,
                    Order::class=>$this->mailTemplateHelper->getOrderTemplate(),
                    OrderStatus::class => $this->mailTemplateHelper->getOrderStatusTemplate() ,
                    Newsletter::class => $this->mailTemplateHelper->getNewsletterTemplate(),
                    Campaign::class => $this->mailTemplateHelper->getCampaignTemplate() ,
                    Announcements::class => $this->mailTemplateHelper->getAAnnouncementsTemplate(),
                     ];
           if($models[$reflection->name]) {
               return $models[$reflection->name];
           }else{
               return false;
           }

    }

}
