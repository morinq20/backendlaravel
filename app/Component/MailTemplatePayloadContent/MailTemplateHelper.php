<?php


namespace App\Component\MailTemplatePayloadContent;



use App\Enum\EmailTemplateEnum;

class MailTemplateHelper{

    public function __construct(){

    }


    public function getNewsletterTemplate():array
    {
        $array =[];
        $array['template'] = EmailTemplateEnum::NEWSLETTER_TEMPLATE->value;
        $array['content'] = [];
        return $array;
    }

    public function getOrderTemplate():array
    {
        $array = [];
        $array['template'] = EmailTemplateEnum::ORDER_TEMPLATE->value;
        $array['content']= [];
        return $array;
    }

    public function getStockTransferTemplate():array
    {
        $array = [];
        $array['template'] = EmailTemplateEnum::STOCK_TRANSFER_TEMPLATE->value;
        $array['content'] = [];
        return $array;
    }

    public function getCampaignTemplate():array
    {
        $array = [];
        $array['template'] = EmailTemplateEnum::CAMPAIGN_TEMPLATE->value;
        $array['content'] = [];
        return $array;
    }

    public  function getOrderStatusTemplate():array
    {
        $array = [];
        $array['template'] = EmailTemplateEnum::ORDER_STATUS_TEMPLATE->value;
        $array['content'] = [];
        return $array;
    }


    public function getAAnnouncementsTemplate():array
    {
        $array = [];
        $array['template'] = EmailTemplateEnum::ANNOUNCEMENTS_TEMPLATE->value;
        $array['content'] = [];
        return  $array;
    }

    public function  getUserWelcomeTemplate():array
    {
        $array = [];
        $array['template'] = EmailTemplateEnum::WELCOME_TEMPLATE->value;
        $array['content'] = [];
        return $array;

        }




}
