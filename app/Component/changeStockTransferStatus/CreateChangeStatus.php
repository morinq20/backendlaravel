<?php
namespace App\Component\changeStockTransferStatus;
use App\Http\Controllers\Backend\StockTransferController;
use App\Models\Product;
use App\Models\StockTransfer;
use App\Models\StockTransferItem;
use App\Models\StockTransferStatus;
use App\Models\Truck;
use App\Models\Variant;
use App\Models\Warehouse;
use App\Models\WarehouseItem;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\JsonResponse;
use function Symfony\Component\Translation\t;

class CreateChangeStatus{

    public function __construct(
        private StockTransfer $stockTransfer,
    )
    {
    }

    /**
     * @throws Exception
     */
    public function handle($request): void
    {
        DB::beginTransaction();
        try {
            $stockTransfer = $this->stockTransfer->where('id',$request->stockTransferId)->first();
            $uptadeStatus = (int)$request->status + 1;
            $stockTransfer->status = $uptadeStatus;
            if($stockTransfer->status === 6 || $stockTransfer->status > 6){
                $this->updateReturnRequestProcessStatus($stockTransfer);
            }else if ($stockTransfer->status > 0 && $stockTransfer->status <= 5){
                $this->updateStockTransferProcessStatus($stockTransfer);
            }
            $stockTransfer->save();
            if ($stockTransfer->status === "11" || $stockTransfer->status === 11){
                $this->addStockBack($request->stockTransferId);
            }
            if ($stockTransfer->status === "5" || $stockTransfer->status === 5){
                $findWarehouse = Warehouse::find($stockTransfer->from_warehouse_id);
                if ($findWarehouse->warehouse_type == 1) {
                    $this->mainWarehouseItem($request->stockTransferId);
                } else {
                    $this->warehouseItem($request->stockTransferId);
                }
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    private function addStockBack($id): void
    {
        DB::beginTransaction();
        try {
            $stockTransfer = $this->stockTransfer->find($id);
            $stockTransferItem = StockTransferItem::where('stock_transfer_id',$id)->get();
            if ($stockTransferItem){
                foreach ($stockTransferItem as $item){
                    $warehouseItem = WarehouseItem::where('warehouse_id',$stockTransfer->to_warehouse_id)->where('product_id',$item->product_id)->where('variant_id',$item->variant_id)->get();
                    foreach ($warehouseItem as $value){
                        if ($value->product_quantity > 0){
                            $variant = Variant::find($item->variant_id);
                            $variant->variant_quantity += $item->product_quantity;
                            $variant->save();
                            $product = Product::find($variant->product_id);
                            $product->product_quantity += $item->product_quantity;
                            $product->save();
                            $value->product_quantity -= $item->product_quantity;
                            $value->save();
                        }
                    }
                }
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    // Sub Warehouse Item

    /**
     * @throws Exception
     */
    private function warehouseItem($id): void
    {
        DB::beginTransaction();
        try {
            $stockTransferItem = StockTransferItem::where('stock_transfer_id',$id)->get();
            $findStockTransfer = StockTransfer::find($id);
            $findWarehouse = Warehouse::find($findStockTransfer->to_warehouse_id);
            if ($findWarehouse->warehouse_type == 1) {
                foreach ($stockTransferItem as $cart) {
                    DB::table('variant')->where('id',$cart->variant_id)
                        ->where('product_id',$cart->product_id)
                        ->update(['variant_quantity' => DB::raw('variant_quantity +' . $cart->product_quantity)]);
                    DB::table('product')->where('id',$cart->product_id)
                        ->update(['product_quantity' => DB::raw('product_quantity +' .$cart->product_quantity)]);
                }
            } else {
                foreach ($stockTransferItem as $cart) {
                    // Warehouse type 1 ise products veya variant kaydetsin değilse warehouse item tablosuna
                    $control = WarehouseItem::where('warehouse_id', $findStockTransfer->to_warehouse_id)->where('product_id', $cart->product_id)
                        ->where('variant_id', $cart->variant_id)->first();
                    if ($control) {
                        DB::table('warehouse_item')
                            ->where('warehouse_id', $findStockTransfer->to_warehouse_id)
                            ->where('product_id', $cart->product_id)
                            ->where('variant_id', $cart->variant_id)
                            ->update(['product_quantity' => DB::raw('product_quantity +' . $cart->product_quantity)]);
                    } else {
                        $warehouseItem = new WarehouseItem();
                        $warehouseItem->warehouse_id = $findStockTransfer->to_warehouse_id;
                        $warehouseItem->product_id = $cart->product_id;
                        $warehouseItem->variant_id = $cart->variant_id;
                        $warehouseItem->unit_id = $cart->unit_id;
                        $warehouseItem->product_quantity = $cart->product_quantity;
                        $warehouseItem->status = 1;
                        $warehouseItem->save();
                    }
                }
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    // Main Warehouse Item

    /**
     * @throws Exception
     */
    private function mainWarehouseItem($id): void
    {
        DB::beginTransaction();
        try {
            $stockTransferItem = StockTransferItem::where('stock_transfer_id',$id)->get();
            $findStockTransfer = StockTransfer::find($id);
            foreach ($stockTransferItem as $cart) {
                // Warehouse type 1 ise products veya variant kaydetsin değilse warehouse item tablosuna
                $control = WarehouseItem::where('warehouse_id',$findStockTransfer->to_warehouse_id)->where('product_id',$cart->product_id)
                    ->where('variant_id',$cart->variant_id)->first();
                if ($control) {
                    DB::table('warehouse_item')
                        ->where('warehouse_id',$findStockTransfer->to_warehouse_id)
                        ->where('product_id',$cart->product_id)
                        ->where('variant_id',$cart->variant_id)
                        ->update(['product_quantity' => DB::raw('product_quantity +' .$cart->product_quantity)]);
                } else {
                    $warehouseItem = new WarehouseItem();
                    $warehouseItem->warehouse_id = $findStockTransfer->to_warehouse_id;
                    $warehouseItem->product_id = $cart->product_id;
                    $warehouseItem->variant_id = $cart->variant_id;
                    $warehouseItem->unit_id = $cart->unit_id;
                    $warehouseItem->product_quantity = $cart->product_quantity;
                    $warehouseItem->status = 1;
                    $warehouseItem->save();
                }
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    // Truck End Km Calculate

    /**
     * @throws Exception
     */
    private function updateEndKm($id): void
    {
        DB::beginTransaction();
        try {
            $stockTransfer = $this->stockTransfer->where('id',$id)->first();
            $truck = Truck::where('id',$stockTransfer->truck_id)->first();
            $truck->truck_end_km += intval($stockTransfer->distance);
            $truck->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    private function updateStockTransferProcessStatus($stockTransfer):void
    {
        DB::beginTransaction();
        try {
            if($stockTransfer->stock_transfer_process !== null){
                if($stockTransfer->status === 1 || $stockTransfer->status === "1"){
                    $data = json_decode($stockTransfer->stock_transfer_process) ;
                    $data[0]->status = 1;
                    $stockTransfer->stock_transfer_process = json_encode($data);
                    $stockTransfer->save();
                }else if($stockTransfer->status === 3 || $stockTransfer->status === "3"){
                    $data = json_decode($stockTransfer->stock_transfer_process) ;
                    $data[0]->status = 2;
                    $stockTransfer->stock_transfer_process = json_encode($data);
                    $stockTransfer->save();
                }else if($stockTransfer->status >= 5){
                    $data = json_decode($stockTransfer->stock_transfer_process);
                    foreach (array_keys($data) as $key ) {
                        $data[$key]->status = 3;
                    }
                    $stockTransfer->stock_transfer_process = json_encode($data) ;
                    $stockTransfer->save();
                }
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    private function updateReturnRequestProcessStatus($stockTransfer):void
    {
        DB::beginTransaction();
        try {
            if($stockTransfer->stock_transfer_process !== null){
                $data = json_decode($stockTransfer->stock_transfer_process);
                if($stockTransfer->status === 6){
                    foreach ($data as $key=> $item){
                        $item->status = 0;
                    }
                    $stockTransfer->stock_transfer_process = json_encode($data);
                    $stockTransfer->save();
                }else if ( $stockTransfer->status === 7){
                    $data[0]->status = 1;
                }else if($stockTransfer->status === 9){
                    $data[0]->status = 2;
                }else if($stockTransfer->status >= 11){
                    foreach ($data as $key =>$item){
                        $item->status = 3;
                    }
                }
                $stockTransfer->stock_transfer_process = json_encode($data);
                $stockTransfer->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
