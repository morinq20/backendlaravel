<?php

namespace App\Component\ResetPassword;

use App\Jobs\SendEmailResetPassword;
use App\Models\User;
use Dotenv\Util\Str;
use Illuminate\Http\Request;
use App\Component\ResetPassword\CreateResetPasswordToken;

class SendResetPasswordHandler{

    public function  __construct(private CreateResetPasswordToken $createResetPasswordToken){

    }

    public function handle(Request $request){
        $token = \Illuminate\Support\Str::random(60);
        $url = $this->createResetPasswordToken->getTokenAndUrl($request->email);
        $users = User::where('email',$request->email)->first();
        if(!$users){
            abort(200);
        }
        SendEmailResetPassword::dispatch($request->email, $url);


    }
}
