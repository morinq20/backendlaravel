<?php

namespace App\Component\ResetPassword;

use App\Models\ResetPassword;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class SetNewPasswordHandler{

    public function __construct()
    {
    }

    public function handle(Request $request){
        $password = $request->password;
        $token = $request->token;
        $resetPasswordRecord = ResetPassword::where('token',$token)->first();
        $user = User::where('id',$resetPasswordRecord->user_id)->first();
//        if(Password::tokenExists($user,$request->token)){
//            return response()->json(['Already token exits'],404);
//        }
        $user->password = Hash::make($password);
        $user->update();

        Password::deleteToken($request->token);

        return response()->json(['success']);

    }
}
