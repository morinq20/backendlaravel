<?php


namespace App\Component\ResetPassword;


use App\Models\User;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Illuminate\Testing\Fluent\Concerns\Has;
use Tymon\JWTAuth\Facades\JWTAuth;

class CreateResetPasswordToken  {


    public function __construct()
    {


    }


    public function getTokenAndUrl($email){
        $token = $this->createToken($email);
        return env('BASE_RESET_PASSWORD_URL').$token;
    }

    private function createToken($email){

        $user = User::where('email',$email)->first();
        $token = Password::createToken($user);
        $resetPassword = new \App\Models\ResetPassword();
        $resetPassword->user_id = $user->id;
        $resetPassword->user_email = $user->email;
        $resetPassword->token = $token;
        $resetPassword->save();
//        if(Password::tokenExists($user)){
//            return
//        }

        return $token;

    }


}
