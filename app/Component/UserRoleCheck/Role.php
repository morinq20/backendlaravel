<?php

namespace App\Component\UserRoleCheck;

class Role{
    const ROLE = [
                "ROLE_ROOT",
            "ROLE_USER",
             "ROLE_WAREHOUSE",
             "ROLE_COUNSELOR"
    ];


    public static function handle($role){
        if(in_array($role,self::ROLE)){
            return true;
        }
       return false;
    }
}
