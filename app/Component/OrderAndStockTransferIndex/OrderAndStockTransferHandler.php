<?php


namespace App\Component\OrderAndStockTransferIndex;

use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\StockTransfer;
use App\Models\StockTransferStatus;
use Exception;
use Illuminate\Support\Facades\DB;

class OrderAndStockTransferHandler{

    public function __construct(private StockTransfer $stockTransfer,
    private Order $order)
    {
    }

    /**
     * @throws Exception
     */
    public function handle()
    {
        DB::beginTransaction();
        try {
            $orderEntity = $this->order->orderBy('id','ASC')->limit(5)->get();
            $stockTransferEntity = $this->stockTransfer->orderBy('id','ASC')->limit(5)->get();
            foreach ($stockTransferEntity as $key => $item){
                $status = StockTransferStatus::where('status_id',$item->status)->first();
                $stockTransferEntity[$key]['status'] = $status;
            }    foreach ($orderEntity as $key => $item){
                $status = OrderStatus::where('status_id',$item->status)->first();
                $orderEntity[$key]['status'] = $status;
            }
            $array['stock_transfer'] = $stockTransferEntity;
            $array['order'] = $orderEntity;
            DB::commit();
            return response()->json($array);
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
}
