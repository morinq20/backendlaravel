<?php

namespace App\Component\SearchStockTransferProcess;

use App\Models\StockTransfer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchStockTransferProcessHandler{

    public  function __construct(private StockTransfer $stockTransfer)
    {
    }


    /**
     * @throws \Exception
     */
    public function handle(Request $request)
    {
        DB::beginTransaction();
        try {
            $stockTransferEntity = $this->stockTransfer->find($request->id);
            $data = json_decode($stockTransferEntity->stock_transfer_process);
            if($stockTransferEntity->stock_transfer_process !== null){
                foreach ($data as $key=>$value){
                    $index = $key+1;
                    if($value->id === (int)$request->processId){
                        $value->status = 3;
                    }
                    if (in_array($index,array_keys($data))){

                        $data[$index]->status = 2;
                    }
                }
                $stockTransferEntity->stock_transfer_process = json_encode($data);
                $stockTransferEntity->save();
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }
}
