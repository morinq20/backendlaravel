<?php

namespace App\Component\DeleteProductImage;

use App\Models\ProductImage;
use Illuminate\Http\Request;

class DeleteProductImageHandler{

    public function __construct(private  ProductImage $productImage)
    {
    }


    public function handle(Request $request)
    {
        $productImage = $this->productImage->where(['product_id'=>$request->productId,
            'id'=>$request->imageId])->first();
        unlink($productImage->product_image);
        $productImage->delete();

        return response()->json('success');

    }
}
