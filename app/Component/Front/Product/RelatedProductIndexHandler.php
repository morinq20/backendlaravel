<?php

namespace App\Component\Front\Product;

use App\Models\Product;
use Illuminate\Http\Request;

class RelatedProductIndexHandler{

    public function __construct(private Product $product){

    }

    public function handle(Request $request)
    {
        $product = $this->product->where('category_id',$request->category)->get();
       $array = [];

       foreach ($product as $key => $item){
           if((int)$request->product !== (int)$item->id){
               $array[]= $item;
           }
       }
       return response()->json($array);
    }
}
