<?php

namespace App\Component\Front\Product;

use App\Models\Category;
use App\Models\Color;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Size;
use App\Models\SubCategory;
use App\Models\Unit;
use App\Models\Variant;
use Illuminate\Http\Request;

class ProductSearchForFrontHandler{

    public function __construct(private Product $product,
    private Variant $variant,

    )
    {
    }

    public function handle(Request $request){
        $product = $this->product->with(['category',
            'subcategory'
            ])->where(['id'=>$request->query('productId'),
            'product_status'=>1])->select('id','category_id','subcategory_id',
            'product_name','lang',
            'product_code','product_description',
            'product_image','product_quantity',
            'product_tax')->get();
        $data = [];
        foreach ($product as $key=>$item){
            $productImageLimit = ProductImage::where('product_id',$item->id)->limit(4)->orderBy('id','ASC')->get();
            $productImage = ProductImage::where('product_id',$item->id)->get();
        $variantEntity = $this->variant->with('color')->with('size')->where('product_id',$item->id)->get();
        $item['variant'] = $variantEntity;
        $item['product_multiple_image'] = $productImageLimit;
        $item['product_all_image'] = $productImage;
        $data[] = $item;


        }
       return response()->json($data);


    }
}
