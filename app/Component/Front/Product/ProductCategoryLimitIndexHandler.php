<?php

namespace App\Component\Front\Product;

use App\Models\Category;
use App\Models\Product;

class ProductCategoryLimitIndexHandler{

    public function __construct(private Category $category,
    private Product $product)
    {
    }

    public function handle()
    {
        $category = [];
        $categoryEntity = $this->category->orderBy('id','DESC')->get();
        foreach ($categoryEntity as $key =>$item){
            $productEntity = $this->product->where('category_id',$item->id)->first();
            if($productEntity){
                array_push($category,$categoryEntity[$key]);
            }

    }
        return response()->json($category);

    }
}
