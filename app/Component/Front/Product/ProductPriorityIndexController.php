<?php

namespace App\Component\Front\Product;

use App\Models\Product;
use App\Models\ProductPriority;
use Illuminate\Http\Request;


class ProductPriorityIndexController{

    public function __construct(private ProductPriority $priority,
    private Product $product)
    {
    }


    public function handle(Request $request)
    {
        if($request->query('type') === 'Trend'){
          return response()->json($this->getPriority('trend'));
        }else if($request->query('type') === 'hotNew'){
            return response()->json($this->getPriority('hot_new'));
        }else if($request->query('type') === 'bestSeller'){
            return response()->json($this->getPriority('best_sellers'));
        }else if ($request->query('type') === 'featured'){
            return response()->json($this->getPriority('featured'));

        }
    }

    private function getPriority($type){
        $data = [];
        $priority = $this->priority->where([$type=>1,'status'=>1])->get();
        foreach ($priority as $key => $item){
            $product = $this->product->where('id',$item->product_id)->select('id','category_id','subcategory_id', 'product_name','product_code','product_description',
                'product_image','product_quantity','lang',
                'product_tax')->limit(8)->first();

            if($product){
                $data[$key] = $product;
            }else{
                return \response()->json('no found product');
            }

        }
        return $data;
    }

}
