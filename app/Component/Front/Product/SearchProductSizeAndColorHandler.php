<?php

namespace App\Component\Front\Product;

use App\Models\Variant;
use Illuminate\Http\Request;

class SearchProductSizeAndColorHandler{

    public function __construct(private Variant $variant)
    {

    }

    public function handle(Request $request)
    {
            $colorId = (int)$request->query('color');
        $sizeId = (int)$request->query('size');
            $productId = (int)$request->query('product');
//            dd($productId, $sizeId, $colorId);


//            if(!$productId){
//                return response()->json('not blank');
//            }
            if($colorId && $sizeId){
                $variantEntity= $this->variant->where([
                    'size_id'=>$sizeId,
                    'color_id'=>$colorId,
                    'product_id'=>$productId
                ])->get();
                return response()->json($variantEntity);
            }



    }
}
