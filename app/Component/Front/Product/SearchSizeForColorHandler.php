<?php


namespace App\Component\Front\Product;

use App\Models\Size;
use App\Models\Variant;
use Illuminate\Http\Request;

class SearchSizeForColorHandler{

    public function __construct(private Variant $variant)
    {



    }

    public function handle(Request $request){
        $productId = $request->query('product');
        $colorId = $request->query('color');
       $variantEntity = $this->variant->where(['product_id'=>$productId,
           'color_id'=>$colorId])->get();
       $array = [];
       foreach ($variantEntity as $key => $item){
           $size = Size::where('id',$item->size_id)->first();
           $array[] = $size;
       }
       return response()->json($array);


    }
}
