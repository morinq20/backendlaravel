<?php

namespace App\Component\Front\Product;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductPriority;
use App\Models\SubCategory;
use App\Models\Unit;

class ProductIndexHandler{

    public function __construct(private Product $product,
    private Category $category,
    private SubCategory $subCategory,
    private Unit $unit,

    private ProductPriority $priority){

    }

    public function handle()
    {
        $productEntity = $this->product->where('product_status',1)
            ->select('id','category_id','subcategory_id','lang',
                'product_name','product_code','product_description',
            'product_image','product_quantity',
                )->limit(8)
            ->get();


        foreach ($productEntity as $key=> $item){
            $categoryEntity = $this->category->where('id',$item->category_id)->first();
            $subCategory = $this->subCategory->where('id',$item->subcategory_id)->first();
            $unitEntity = $this->unit->where('id',$item->unit_id)->first();
            $priorityEntity = $this->priority->where(['product_id'=>$item->id,
            'status'=>1])->first();
            $productEntity[$key]['category'] = $categoryEntity;
            $productEntity[$key]['priority'] = $priorityEntity;
            $productEntity[$key]['category']['subCategory'] = $subCategory;
            $productEntity[$key]['unit'] = $unitEntity;

        }
        return response()->json($productEntity);

    }
}
