<?php

namespace App\Component\Front\Slider;

use App\Models\Slider;

class SliderIndexHandler{

    public function __construct(private Slider $slider)
    {
    }

    public function handle()
    {
        $sliderEntity = $this->slider->where('slider_status',1)->orderBy('id','DESC')->get();
        return response()->json($sliderEntity);

    }
}
