<?php

namespace App\Component\Front\CategorySubCategory;

use App\Models\Product;
use Illuminate\Http\Request;

class CategorySubCategorySearchHandler{

    public function __construct(private Product $product){

    }

    public function handle(Request $request)
    {
        $subCategoryId = $request->query('subCategoryId');
        $categoryId = $request->query('categoryId');
        if($categoryId === null){
            $subCategoryProduct = $this->product->select('id','category_id','subcategory_id',
                'product_name','product_code','product_description','lang',
                'product_image','product_quantity',
               'product_tax')->where(['subcategory_id'=>$subCategoryId,
                'product_status'=>1])->get();
            return response()->json($subCategoryProduct);
        }else{
            $categoryProduct = $this->product->select('id','category_id','subcategory_id',
                'product_name','product_code','product_description','lang',
                'product_image','product_quantity',
                'product_tax')->where(['category_id'=>$categoryId,
                'product_status'=>1])->get();
            return response()->json($categoryProduct);
        }
    }
}
