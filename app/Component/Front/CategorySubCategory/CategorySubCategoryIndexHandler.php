<?php

namespace App\Component\Front\CategorySubCategory;

use App\Models\Category;
use App\Models\Product;
use App\Models\SubCategory;

class CategorySubCategoryIndexHandler{

    public function __construct(private Category $category,
    private Product $product){

    }

    public function handle(){
        $categoryEntity = $this->category->with('subcategory')->get();
        $data = [];
        foreach ($categoryEntity as $key=>$item) {
            $product = $this->product->where('category_id',$item->id)->first();
            if($product){
                $data[]=$item;
            }
        }
        return response()->json($data);

    }
}
