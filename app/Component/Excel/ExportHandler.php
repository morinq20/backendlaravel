<?php

namespace App\Component\Excel;

use App\Http\Requests\ExcelExportRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\File;

class ExportHandler
{
    public function __construct()
    {

    }

    public function handle(ExcelExportRequest $request)
    {
        $tableName = $request->table_name;
//        $tableFields = $request->get('tableFields');
        $tableFields = DB::getSchemaBuilder()->getColumnListing($tableName);
        $allData = DB::table($tableName)->get();
        // Excel Export
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // All Data
        $dataArray = [];
        foreach ($allData as $index => $datum){
            foreach ($datum as $key => $item){
                $dataArray[$index][] = $item;
            }

        }
        // Column
        $fields = $tableFields;
        $i = '1';
        $col = 'A';
        foreach ($fields as $key => $field){
            $sheet->setCellValue($col.$i, $field)->getColumnDimension($col)->setAutoSize(true);
            $col++;
        }

        // DATA
        $rowIndex = '2';
        $colIndex = 'A';

        foreach ($dataArray as $item){
            foreach ($item as $key => $dt){
                $sheet->setCellValue($colIndex.$rowIndex, $dt)->getColumnDimension($colIndex)->setAutoSize(true);
                $colIndex++;
                if (array_key_last($item) === $key){
                    $rowIndex++;
                    $colIndex = 'A';
                }
            }
//                $rowIndex++;
        }
        // Excel Save And Download

//        $publicDirectory = Storage::disk('public')->get('private/excel');
//        if (File::exists(public_path('private/excel'))){
//            return response()->json("var");
//        }else{
//            return response()->json("yok");
//        }
//        return response()->json($publicDirectory);
        $publicDirectory = "private/excel";
        // Folder Process
        $this->createFolderIfNotExist($publicDirectory);

        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // In this case, we want to write the file in the public directory

        $fileName = $tableName.'-'.uniqid().'.xlsx';
        // e.g /var/www/project/public/my_first_excel_symfony4.xlsx
        $excelFilepath =  public_path().'/'.$publicDirectory . '/' . $fileName;


        // Create the file
        $writer->save($excelFilepath);
//        $this->downloadExcel($excelFilepath,$fileName);
    }

//    private function downloadExcel($path,$fileName){
//        return response()->download($path.$fileName);
//        $content = file_get_contents($path);
//        header("Content-Disposition: attachment; filename=".$fileName);
//         unlink($filename);
//        exit($content);
//    }

    private function createFolderIfNotExist(string $folder): void
    {
        if(!File::isDirectory(public_path().'/'.$folder)){
            File::makeDirectory(public_path().'/'.$folder, 0777, true, true);
        }
    }

}
