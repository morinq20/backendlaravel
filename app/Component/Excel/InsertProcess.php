<?php

namespace App\Component\Excel;

use Illuminate\Support\Facades\DB;

class InsertProcess
{
    public function __construct()
    {
    }

    /**
     * @throws \Exception
     */
    public function handle($data, $fields, $table): void
    {
        DB::beginTransaction();
        try {
            $insertData = [];
            foreach ($data as $x => $dt){
                $insertData[$fields[$x]] = $dt;
            }
            DB::table($table)->insertGetId($insertData);
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }
}
