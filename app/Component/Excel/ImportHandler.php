<?php

namespace App\Component\Excel;

use App\Http\Requests\ExcelImportRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ImportHandler
{
    public function __construct(private InsertProcess $insertProcess)
    {

    }

    /**
     * @throws \Exception
     */
    public function handle(Request $request)
    {
        DB::beginTransaction();
        try {
            $importExcel = $request->file_name;
            $fileExt = pathinfo($importExcel, PATHINFO_EXTENSION);
            $allowedExt = ['xls','csv','xlsx'];
            $explode = explode('-',$importExcel);
            $tableName = $explode[0];
            if (in_array($fileExt,$allowedExt)) {
                $spreadsheet = IOFactory::load(public_path().'/private/excel/'.$importExcel);
                $data = $spreadsheet->getActiveSheet()->toArray();
                $fields = array_shift($data);
                $idKey = array_search('id', $fields);
                if (in_array('id', $fields)) {
                    foreach ($data as $key => $datum) {

                        $id = $datum[$idKey];
                        $control = DB::table($tableName)->where('id',$id)->first();
                        if ($control){
                            DB::table($tableName)->where('id',$id)->delete();
                            $this->insertProcess->handle($datum,$fields,$tableName);
                        }else{
                            $this->insertProcess->handle($datum,$fields,$tableName);
                        }
                    }
                }
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }

}
