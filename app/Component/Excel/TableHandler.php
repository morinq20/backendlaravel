<?php

namespace App\Component\Excel;

use Illuminate\Support\Facades\DB;

class TableHandler
{
    public function __construct()
    {

    }

    public function handle(): \Illuminate\Http\JsonResponse
    {
        $tableArray = [];
        $tables = DB::select('SHOW TABLES');
        foreach ($tables as $table) {
            foreach ($table as $key => $value)
                $tableArray[] = $value;
        }
        return response()->json($tableArray);
    }
}
