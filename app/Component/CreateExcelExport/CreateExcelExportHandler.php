<?php

namespace  App\Component\CreateExcelExport;

use App\Exports\CustomerExport;
use App\Exports\ProductExport;
use App\Exports\StockTransferExport;
use App\Exports\SupplierExport;
use App\Exports\UserExport;
use App\Models\StockTransfer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class CreateExcelExportHandler{

    public function __construct(){
    }

    /**
     * @throws Exception
     */
    public function handle(Request $request){
        DB::beginTransaction();
        try {
            $uuid = Str::uuid()->toString();
            $path = 'private/';
            $excelType = ['user', 'stockTransfer', 'customer', 'supplier', 'product'];
            foreach ($excelType as $item) {
                if ($request->query('excelType') === $item) {
                    $filename = $path.$item.'_'.$uuid.'.xlsx';
                    return Excel::store($this->getExport($item),$filename,'customPublic',\Maatwebsite\Excel\Excel::XLSX);
                }
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new Exception($exception->getMessage());
        }
    }
    private function getExport($export){
        $excelType = ['user'=>new UserExport(),
            'stockTransfer'=> new StockTransferExport(),
            'customer'=>new CustomerExport(),
            'supplier'=>new SupplierExport(),
            'product'=>new ProductExport()];
        return $excelType[$export];
    }
}
