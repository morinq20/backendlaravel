<?php

namespace App\Component\SalaryMonthEmployee\MonthlyAmountControl;


use App\Models\Month;
use App\Models\Salary;
use Illuminate\Http\Request;

use League\CommonMark\Delimiter\Processor\StaggeredDelimiterProcessor;

class MonthlyAmountControlHandler {

    public function __construct(private Salary $salary,
    private Month $month)
    {

    }

    public function handle(Request $request){
        if(null === $request->query('month_id') && null === $request->query('employee_id')){
            $salaryEntity = $this->salary
                ->where('status',1)
                ->get();
            foreach ($salaryEntity as $index =>$item){
                $month = $this->month
                    ->select('month')
                    ->where('id',$item->month_id)
                    ->first();
                $salaryEntity[$index]['month'] = $month->month;
            }
            return $salaryEntity;
        }else if (null !== $request->query('month_id') && null !== $request->query('employee_id')){
            $salaryEntity = $this->salary
                ->where('month_id',$request->query('month_id'))
                ->where('employee_id',$request->query('employee_id'))
                ->first();
            if($salaryEntity){
                return $salaryEntity;
            }else{
                return 'no amount';
            }

        }

    }
}
