<?php

namespace App\Component\Year;

use App\Models\Years;

class IndexYearHandler
{
    public function __construct()
    {

    }

    public function handle(): \Illuminate\Http\JsonResponse
    {
        $years = Years::orderBy('id','DESC')->get();
        return response()->json($years);
    }
}
