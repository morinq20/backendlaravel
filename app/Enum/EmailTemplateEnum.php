<?php

namespace App\Enum;

enum EmailTemplateEnum: string
{
    case STOCK_TRANSFER_TEMPLATE = "email.stock_transfer_template";
    case ORDER_TEMPLATE = "email.order_template";
    case ORDER_STATUS_TEMPLATE = "email.order_status_template";
    case NEWSLETTER_TEMPLATE = "email.newsletter_template";
    case CAMPAIGN_TEMPLATE = "email.campaign_template";
    case WELCOME_TEMPLATE = "email.welcome_template";
    case ANNOUNCEMENTS_TEMPLATE = "email.announcements_template";
}
