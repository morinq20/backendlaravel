<?php

namespace App\Jobs;

use App\Mail\Newsletter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailByNewsletter implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $settings;
    private $newsletter;

    public function __construct($settings,$newsletter)
    {
        $this->settings = $settings;
        $this->newsletter = $newsletter;
    }

    public function handle()
    {
        $template = new Newsletter($this->settings);
        Mail::to($this->newsletter->newsletter_email)->send($template);
    }
}
