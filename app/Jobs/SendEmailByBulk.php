<?php

namespace App\Jobs;

use App\Mail\SendBulk;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailByBulk implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $settings;
    private $data;

    public function __construct($settings,$data)
    {
        $this->settings = $settings;
        $this->data = $data;
    }

    public function handle()
    {
        $template = new SendBulk($this->settings,$this->data);
        Mail::to($this->data['to'])->send($template);
    }
}
