<?php

namespace App\Jobs;

use App\Mail\StockTransfer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailByStockTransfer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $stockTransfer;
    private $settings;
    private $stockTransferItems;
    private $fromWarehouse;
    private $toWarehouse;
    private $stockTransferStatus;

    public function __construct($toWarehouse,$stockTransfer,$settings,$stockTransferItems,$fromWarehouse,$stockTransferStatus)
    {
        $this->toWarehouse = $toWarehouse;
        $this->stockTransfer = $stockTransfer;
        $this->settings = $settings;
        $this->stockTransferItems = $stockTransferItems;
        $this->fromWarehouse = $fromWarehouse;
        $this->stockTransferStatus = $stockTransferStatus;
    }

    public function handle()
    {
        $template = new StockTransfer($this->toWarehouse,$this->stockTransfer,$this->settings,$this->stockTransferItems,$this->fromWarehouse,$this->stockTransferStatus);
        Mail::to($this->toWarehouse->warehouse_email)->send($template);
    }
}
