<?php

namespace App\Jobs;

use App\Mail\Order;
use App\Mail\SendEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $customer;
    private $order;
    private $settings;
    private $orderItems;
    private $fromWarehouse;
    private $orderStatus;
    private $total;
    private $subtotal;

    public function __construct($customer,$order,$settings,$orderItems,$fromWarehouse,$orderStatus,$total,$subtotal)
    {
        $this->customer = $customer;
        $this->order = $order;
        $this->settings = $settings;
        $this->orderItems = $orderItems;
        $this->fromWarehouse = $fromWarehouse;
        $this->orderStatus = $orderStatus;
        $this->total = $total;
        $this->subtotal = $subtotal;
    }

    public function handle()
    {
//        return response()->json($this->details);
//        dd($this->details);
//        $email = new SendEmail();
        $template = new Order($this->customer,$this->order,$this->settings,$this->orderItems,$this->fromWarehouse,$this->orderStatus,$this->total,$this->subtotal);
        Mail::to($this->customer->customer_email)->send($template);
    }
}
