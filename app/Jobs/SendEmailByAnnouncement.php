<?php

namespace App\Jobs;

use App\Mail\Announcement;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailByAnnouncement implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $announcement;
    private $settings;

    public function __construct($announcement,$settings)
    {
        $this->announcement = $announcement;
        $this->settings = $settings;
    }

    public function handle()
    {
        $template = new Announcement($this->announcement,$this->settings);
        $user = User::find($this->announcement->user_id);
        Mail::to($user->email)->send($template);
    }
}
