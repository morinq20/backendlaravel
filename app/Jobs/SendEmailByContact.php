<?php

namespace App\Jobs;

use App\Mail\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailByContact implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $settings;
    private $contact;

    public function __construct($settings,$contact)
    {
        $this->settings = $settings;
        $this->contact = $contact;
    }

    public function handle()
    {
        $settings = $this->settings;
        $contact = $this->contact;
        $template = new Contact($settings,$contact);
        Mail::to($settings->email)->send($template);
    }
}
