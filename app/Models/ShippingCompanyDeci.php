<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShippingCompanyDeci extends Model
{
    use HasFactory;
    protected $table = 'shipping_company_deci';

    public function shippingCompany()
    {
        return $this->belongsTo('App\Models\ShippingCompany');
    }

}
