<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    use HasFactory;
    protected $table = 'size';

    public function getSizeInVariant()
    {
        return $this->hasMany('App\Models\Variant');
    }
}
