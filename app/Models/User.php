<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $table = 'users';
//    protected $fillable = [
//        'name',
//        'surname',
//        'email',
//        'phone',
//        'gender',
//        'country',
//        'city',
//        'district',
//        'address',
//        'photo',
//        'password',
////        'role_id'
//    ];


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];






    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


//    public function getRoles()
//    {
//        return $this->belongsTo(Role::class,'user_id','id');
//    }
    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }
    public function logs()
    {
        return $this->belongsTo('App\Models\Logs');
    }

    public function warehouse()
    {
        return $this->belongsTo('App\Models\Warehouse');
    }

    public function task()
    {
        return $this->hasMany('App\Models\Task');
    }

    public function calendar()
    {
        return $this->hasMany('App\Models\Calendar');
    }

    public function announcements()
    {
        return $this->hasMany('App\Models\Announcements');
    }
}
