<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Announcements extends Model
{
    use HasFactory;
    protected $table = 'announcements';

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
