<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GoogleMapsApi extends Model
{
    use HasFactory;
    protected $table = 'google_maps_api';

}
