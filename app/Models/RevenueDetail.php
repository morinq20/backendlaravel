<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RevenueDetail extends Model
{
    use HasFactory;
    protected $table = 'revenue_detail';

    public function revenue()
    {
        return $this->belongsTo('App\Models\Revenue');
    }
}
