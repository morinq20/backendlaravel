<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    use HasFactory;
    protected $table = 'sub_categories';

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function subcategory_product()
    {
        return $this->hasMany('App\Models\SubCategory');
    }
}
