<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PanelSettings extends Model
{
    use HasFactory;
    protected $table = 'panel_settings';
}
