<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $table = 'roles';

//    public function user()
//    {
//        return $this->belongsTo(User::class);
//    }
//    public function getUsers()
//    {
//        return $this->hasMany(User::class,'role_id','id');
//    }
    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

    public function permission()
    {
        return $this->hasMany('App\Models\RoutePermission');
    }

    public function announcements()
    {
        return $this->hasMany('App\Models\Announcements');
    }
}
