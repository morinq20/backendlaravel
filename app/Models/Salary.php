<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    use HasFactory;
    protected $table = 'salaries';

    public function employee()
    {
        return $this->belongsTo('App\Models\Employee');
    }

    public function month()
    {
        return $this->belongsTo('App\Models\Month');
    }


}
