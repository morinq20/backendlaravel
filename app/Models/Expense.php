<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    use HasFactory;
    protected $table = 'expense';

    public function accountingStatus()
    {
        return $this->belongsTo('App\Models\AccountingStatus');
    }


    public function expense_detail()
    {
        return $this->hasMany('App\Models\ExpenseDetail');
    }
}
