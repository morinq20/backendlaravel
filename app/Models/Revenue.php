<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Revenue extends Model
{
    use HasFactory;
    protected $table = 'revenue';

    public function accountingStatus()
    {
        return $this->belongsTo('App\Models\AccountingStatus');
    }

    public function revenue_detail()
    {
        return $this->hasMany('App\Models\RevenueDetail');
    }
}
