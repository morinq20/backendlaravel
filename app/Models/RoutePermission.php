<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoutePermission extends Model
{
    use HasFactory;
    protected $table = 'route_permission';

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }
}
