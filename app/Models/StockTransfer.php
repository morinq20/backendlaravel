<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockTransfer extends Model
{
    use HasFactory;
    protected $table = 'stock_transfer';

    public function stocktransferitem()
    {
        return $this->hasMany('App\Models\StockTransferItem');
    }
}
