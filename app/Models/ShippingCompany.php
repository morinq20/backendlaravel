<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShippingCompany extends Model
{
    use HasFactory;
    protected $table = 'shipping_company';


    public function getDeci()
    {
        return $this->hasMany('App\Models\ShippingCompanyDeci');
    }

//    public function getDistance()
//    {
//        return $this->hasMany('App\Models\ShippingCompanyDistance');
//    }
//
//    public function getPostType()
//    {
//        return $this->hasMany('App\Models\ShippingCompanyPostType');
//    }
//
//    public function getService()
//    {
//        return $this->hasMany('App\Models\ShippingCompanyService');
//    }

}
