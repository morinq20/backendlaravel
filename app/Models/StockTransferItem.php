<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockTransferItem extends Model
{
    use HasFactory;
    protected $table = 'stock_transfer_item';

    public function stocktransfer()
    {
        return $this->belongsTo('App\Models\StockTransfer');
    }
}
