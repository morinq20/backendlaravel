<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExpenseDetail extends Model
{
    use HasFactory;
    protected $table = 'expense_detail';


    public function expense()
    {
        return $this->belongsTo('App\Models\Expense');
    }
}
