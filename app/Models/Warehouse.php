<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    use HasFactory;
    protected $table = 'warehouse';

    public function warehouseitem()
    {
        return $this->hasMany('App\Models\WarehouseItem');
    }

    public function product()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

}
