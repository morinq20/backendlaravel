<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShippingCompanyPostType extends Model
{
    use HasFactory;
    protected $table = 'shipping_company_post_type';

}
