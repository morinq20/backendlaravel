<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;
    protected $table = 'supplier';

    public function getWithProduct()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function requestProduct()
    {
        return $this->hasMany('App\Models\RequestProduct');
    }

    public function truck()
    {
        return $this->hasMany('App\Models\Truck');
    }
}
