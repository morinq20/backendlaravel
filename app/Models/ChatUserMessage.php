<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatUserMessage extends Model
{
    use HasFactory;
    protected $table = 'chat_user_message';

    public function room(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne('App\Models\ChatUser','id','chat_user_id');
    }
}
