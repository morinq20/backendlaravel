<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockTransferCart extends Model
{
    use HasFactory;
    protected $table = 'stock_transfer_cart';
}
