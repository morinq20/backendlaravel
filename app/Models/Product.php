<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'product';


    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Models\SubCategory');
    }

    public function collection()
    {
        return $this->belongsTo('App\Models\Collection');
    }

    public function productPriority()
    {
        return $this->belongsTo('App\Models\ProductPriority');
    }
    public function productImage()
    {
        return $this->belongsTo('App\Models\ProductImage');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier');
    }

    public function getProductInVariant()
    {
        return $this->hasMany('App\Models\Variant');
    }

    public function requestProduct()
    {
        return $this->hasMany('App\Models\RequestProduct');
    }

    public function warehouse()
    {
        return $this->belongsTo('App\Models\Warehouse');
    }

    public function warehouseItem()
    {
        return $this->hasMany('App\Models\WarehouseItem');
    }
}
