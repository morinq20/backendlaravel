<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountingStatus extends Model
{
    use HasFactory;
    protected $table = 'accounting_status';

    public function expenses()
    {
        return $this->hasMany('App\Models\Expense');
    }

    public function revenues()
    {
        return $this->hasMany('App\Models\Revenue');
    }
}
