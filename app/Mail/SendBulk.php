<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class SendBulk extends Mailable
{
    use Queueable, SerializesModels;

    private $settings;
    private $data;

    public function __construct($settings,$data)
    {
        $this->settings = $settings;
        $this->data = $data;
    }

    public function build()
    {
        $settings = $this->settings;
        $data = $this->data;
        return $this->view('emails.bulk.index', compact('settings','data'))
            ->subject('Reset Password');
    }
}
