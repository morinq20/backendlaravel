<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    private $settings;
    private $contact;

    public function __construct($settings,$contact)
    {
        $this->settings = $settings;
        $this->contact = $contact;
    }

    public function build()
    {
        $settings = $this->settings;
        $contact = $this->contact;
        return $this->view('emails.contact.index', compact('settings','contact'))
            ->subject($contact['subject'])
            ->replyTo($contact['email']);
    }
}
