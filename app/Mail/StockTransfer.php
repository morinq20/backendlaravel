<?php

namespace App\Mail;

use App\Models\Customer;
use App\Models\PanelSettings;
use App\Models\Warehouse;
use App\Models\WebSiteSettings;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class StockTransfer extends Mailable
{
    use Queueable, SerializesModels;

    private $stockTransfer;
    private $settings;
    private $stockTransferItems;
    private $fromWarehouse;
    private $toWarehouse;
    private $stockTransferStatus;

    public function __construct($toWarehouse,$stockTransfer,$settings,$stockTransferItems,$fromWarehouse,$stockTransferStatus)
    {
        $this->toWarehouse = $toWarehouse;
        $this->stockTransfer = $stockTransfer;
        $this->settings = $settings;
        $this->stockTransferItems = $stockTransferItems;
        $this->fromWarehouse = $fromWarehouse;
        $this->stockTransferStatus = $stockTransferStatus;
    }

    public function build()
    {
        $toWarehouse = $this->toWarehouse;
        $stockTransfer = $this->stockTransfer;
        $settings = $this->settings;
        $stockTransferItems = $this->stockTransferItems;
        $fromWarehouse = $this->fromWarehouse;
        $stockTransferStatus = $this->stockTransferStatus;
        return $this->view('emails.stockTransfer.index', compact('toWarehouse','stockTransfer','settings','stockTransferItems','fromWarehouse','stockTransferStatus'))
            ->subject('Stock Transfer');
    }


}
