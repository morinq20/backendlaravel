<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetPassword extends Mailable{

    use Queueable, SerializesModels;
    private $url;
    private $email;

    public function __construct($email, $url){
    $this->email = $email;
    $this->url = $url;
    }

    public function build(){
        $url = $this->url;
        return $this->view('emails.resetpassword.index', compact('url'))
            ->subject('Reset Password');
    }


}
