<?php

namespace App\Mail;

use App\Models\Customer;
use App\Models\PanelSettings;
use App\Models\Warehouse;
use App\Models\WebSiteSettings;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class Newsletter extends Mailable
{
    use Queueable, SerializesModels;

    private $settings;

    public function __construct($settings)
    {
        $this->settings = $settings;
    }

    public function build()
    {
        $settings = $this->settings;
        return $this->view('emails.newsletter.index', compact('settings'))
            ->subject('Newsletter');
    }


}
