<?php

namespace App\Mail;

use App\Models\Customer;
use App\Models\PanelSettings;
use App\Models\Warehouse;
use App\Models\WebSiteSettings;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class Order extends Mailable
{
    use Queueable, SerializesModels;

    private $customer;
    private $order;
    private $settings;
    private $orderItems;
    private $fromWarehouse;
    private $orderStatus;
    private $total;
    private $subtotal;

    public function __construct($customer,$order,$settings,$orderItems,$fromWarehouse,$orderStatus,$total,$subtotal)
    {
        $this->customer = $customer;
        $this->order = $order;
        $this->settings = $settings;
        $this->orderItems = $orderItems;
        $this->fromWarehouse = $fromWarehouse;
        $this->orderStatus = $orderStatus;
        $this->total = $total;
        $this->subtotal = $subtotal;
    }

    public function build()
    {
//        $array = $this->array;
        $customer = $this->customer;
        $order = $this->order;
        $settings = $this->settings;
        $orderItems = $this->orderItems;
        $fromWarehouse = $this->fromWarehouse;
        $orderStatus = $this->orderStatus;
        $total = $this->total;
        $subtotal = $this->subtotal;
        return $this->view('emails.order.index', compact('customer','order','settings','orderItems','fromWarehouse','orderStatus','total','subtotal'))
            ->subject('Order');
    }


}
