<?php

namespace App\Mail;

use App\Models\Customer;
use App\Models\PanelSettings;
use App\Models\Warehouse;
use App\Models\WebSiteSettings;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class Announcement extends Mailable
{
    use Queueable, SerializesModels;

    private $announcement;
    private $settings;

    public function __construct($announcement,$settings)
    {
        $this->announcement = $announcement;
        $this->settings = $settings;
    }

    public function build()
    {
        $announcement = $this->announcement;
        $settings = $this->settings;
        return $this->view('emails.announcement.index', compact('announcement','settings'))
            ->subject('Announcement');
    }


}
