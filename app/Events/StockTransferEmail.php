<?php

namespace App\Events;

use App\Models\StockTransfer;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class StockTransferEmail
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $stockTransfer;
    public $emailTemplate;
    /**
     *
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(StockTransfer $stockTransfer,$emailTemplate)
    {
        $this->stockTransfer = $stockTransfer;
        $this->emailTemplate = $emailTemplate;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
