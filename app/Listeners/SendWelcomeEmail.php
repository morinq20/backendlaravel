<?php

namespace App\Listeners;

use App\Events\WelcomeEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendWelcomeEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\WelcomeEmail  $event
     * @return void
     */
    public function handle(WelcomeEmail $event)
    {
        //
    }
}
