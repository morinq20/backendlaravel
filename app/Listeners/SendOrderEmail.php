<?php

namespace App\Listeners;

use App\Events\OrderEmail;
use App\Http\Services\LogManager\LogManager;
use App\Models\Customer;
use App\Models\OrderStatus;
use App\Models\PanelSettings;
use App\Models\Warehouse;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SendOrderEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\OrderEmail  $event
     * @return void
     */
    public function handle(OrderEmail $event)
    {
        $customer = Customer::find($event->order->customer_id);
        $order = $event->order;
        $fromWarehouse = Warehouse::find($order->from_warehouse_id);
        $orderStatus = OrderStatus::where('status_id',$order->status)->first();
        $orderItems = DB::table('orders_item')
            ->select('orders_item.*','product.product_name','product.product_code','product.product_image',
                'variant.sku_no','variant.variant_image','variant.variant_tax','variant.variant_selling_price','variant.variant_discount_price')
            ->join('product','orders_item.product_id','=','product.id')
            ->join('variant','orders_item.variant_id','=','variant.id')
            ->where('orders_item.orders_id',$order->id)
            ->get();
        $total = 0;
        $subtotal = 0;
        foreach ($orderItems as $item){
            if ($item->variant_tax == null){
                if ($item->variant_discount_price == null) {
                    $total += $item->product_quantity * $item->variant_selling_price;
                    $subtotal = $item->product_quantity * $item->variant_selling_price;
                }else{
                    $total += $item->product_quantity * $item->variant_discount_price;
                    $subtotal = $item->product_quantity * $item->variant_discount_price;
                }
            }else{
                if ($item->variant_discount_price == null) {
                    $total += (($item->product_quantity * $item->variant_selling_price) * $item->variant_tax) / 100;
                    $subtotal += $item->product_quantity * $item->variant_selling_price;
                }else{
                    $total += (($item->product_quantity * $item->variant_discount_price) * $item->variant_tax) / 100;
                    $subtotal += $item->product_quantity * $item->variant_discount_price;
                }
            }
        }
        $settings = PanelSettings::find(0);
        $array = [
            'order' => [
                'code' => $order->code,
                'start_transfer_date' => $order->start_transfer_date,
                'finish_transfer_date' => $order->finish_transfer_date,
                'total_product' => $order->total_product,
                'total_quantity' => $order->total_quantity,
                'total_process' => $order->total_process,
                'total_weight' => $order->total_weight,
                'pallet_count' => $order->pallet_count,
                'status' => $orderStatus->status_text,
                'process_total' => $order->total,
                'total' => $total,
                'subtotal' => $subtotal,
            ],
            'from' => [
                'warehouse_name' => $fromWarehouse->warehouse_name,
                'warehouse_address' => $fromWarehouse->warehouse_address,
                'warehouse_country' => $fromWarehouse->warehouse_country,
                'warehouse_city' => $fromWarehouse->warehouse_city,
                'warehouse_phone' => $fromWarehouse->warehouse_phone,
                'warehouse_email' => $fromWarehouse->warehouse_email,
                'warehouse_type' => $fromWarehouse->warehouse_type == 0 ? 'Sub Warehouse' : 'Main Warehouse'
            ],
            'to' => [
                'customer_name' => $customer->customer_name,
                'customer_surname' => $customer->customer_surname,
                'customer_shipping_address' => $customer->customer_shipping_address,
                'customer_billing_address' => $customer->customer_billing_address,
                'customer_phone' => $customer->customer_phone,
                'customer_email' => $customer->customer_email,
            ],
            'items' => $orderItems,
            'settings' => $settings
        ];
        $email = $customer->customer_email;
        mail::send($event->emailTemplate,$array,function($message) use ($email){
            $message->subject("New Order");
            $message->to($email);
        });
    }
}
