<?php

namespace App\Listeners;

use App\Events\CampaignEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendCampaignEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\CampaignEmail  $event
     * @return void
     */
    public function handle(CampaignEmail $event)
    {
        //
    }
}
