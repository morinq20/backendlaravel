<?php

namespace App\Listeners;

use App\Events\StockTransferEmail;
use App\Models\Customer;
use App\Models\PanelSettings;
use App\Models\StockTransfer;
use App\Models\StockTransferItem;
use App\Models\StockTransferStatus;
use App\Models\User;
use App\Models\Warehouse;
use App\Models\WarehouseItem;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SendStockTransferEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\StockTransferEmail  $event
     * @return void
     */
    public function handle(StockTransferEmail $event)
    {
        $stockTransfer = $event->stockTransfer;
        $fromWarehouse = Warehouse::find($stockTransfer->from_warehouse_id);
        $toWarehouse = Warehouse::find($stockTransfer->to_warehouse_id);
        $stockTransferStatus = StockTransferStatus::where('status_id',$stockTransfer->status)->first();
        $stockTransferItems = DB::table('stock_transfer_item')
            ->select('stock_transfer_item.*','product.product_name','product.product_code','product.product_image',
            'variant.sku_no','variant.variant_image','variant.variant_tax','variant.variant_selling_price','variant.variant_discount_price')
            ->join('product','stock_transfer_item.product_id','=','product.id')
            ->join('variant','stock_transfer_item.variant_id','=','variant.id')
            ->where('stock_transfer_item.stock_transfer_id',$stockTransfer->id)
            ->get();
        $settings = PanelSettings::find(0);
        $array = [
            'stockTransfer' => [
                'code' => $stockTransfer->code,
                'start_transfer_date' => $stockTransfer->start_transfer_date,
                'finish_transfer_date' => $stockTransfer->finish_transfer_date,
                'total_product' => $stockTransfer->total_product,
                'total_quantity' => $stockTransfer->total_quantity,
                'total_process' => $stockTransfer->total_process,
                'total_weight' => $stockTransfer->total_weight,
                'pallet_count' => $stockTransfer->pallet_count,
                'status' => $stockTransferStatus->status_text,
                'process_total' => $stockTransfer->total,
            ],
            'from' => [
                'warehouse_name' => $fromWarehouse->warehouse_name,
                'warehouse_address' => $fromWarehouse->warehouse_address,
                'warehouse_country' => $fromWarehouse->warehouse_country,
                'warehouse_city' => $fromWarehouse->warehouse_city,
                'warehouse_phone' => $fromWarehouse->warehouse_phone,
                'warehouse_email' => $fromWarehouse->warehouse_email,
                'warehouse_type' => $fromWarehouse->warehouse_type == 0 ? 'Sub Warehouse' : 'Main Warehouse'
            ],
            'to' => [
                'warehouse_name' => $toWarehouse->warehouse_name,
                'warehouse_address' => $toWarehouse->warehouse_address,
                'warehouse_country' => $toWarehouse->warehouse_country,
                'warehouse_city' => $toWarehouse->warehouse_city,
                'warehouse_phone' => $toWarehouse->warehouse_phone,
                'warehouse_email' => $toWarehouse->warehouse_email,
                'warehouse_type' => $toWarehouse->warehouse_type == 0 ? 'Sub Warehouse' : 'Main Warehouse'
            ],
            'items' => $stockTransferItems,
            'settings' => $settings
        ];
        $email = $toWarehouse->warehouse_email;
        mail::send($event->emailTemplate,$array,function($message) use ($email) {
            $message->subject("New Stock Transfer");
            $message->to($email);
        });
    }
}
