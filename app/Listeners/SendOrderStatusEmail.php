<?php

namespace App\Listeners;

use App\Component\MailTemplatePayloadContent\ModelsCheck;
use App\Events\OrderStatusEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendOrderStatusEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(private  ModelsCheck $modelsCheck)
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\OrderStatusEmail  $event
     * @return void
     */
    public function handle(OrderStatusEmail $event)
    {
        $template = $this->modelsCheck->getModel($event);

        //
    }
}
