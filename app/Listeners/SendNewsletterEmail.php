<?php

namespace App\Listeners;

use App\Events\NewsletterEmail;
use App\Http\Services\LogManager\LogManager;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendNewsletterEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\NewsletterEmail  $event
     * @return void
     */
    public function handle(NewsletterEmail $event)
    {
//        $event->newsletter->newModelQuery()->first();
        $email = "efe@balinsoft.com";
        $array = [
            'name' => "Efe",
            'surname' => "Akman",
            'date' => date("d-m-Y")
        ];
        mail::send($event->emailTemplate,$array,function($message) use ($email){
            $message->subject("Selam Hoşgeldin");
            $message->to($email);
        });
    }
}
