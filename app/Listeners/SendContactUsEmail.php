<?php

namespace App\Listeners;

use App\Events\ContactUsEmail;
use App\Events\OrderEmail;
use App\Http\Services\LogManager\LogManager;
use App\Models\Customer;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendContactUsEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\ContactUsEmail  $event
     * @return void
     */
    public function handle(ContactUsEmail $event)
    {

        $email = "efe@balinsoft.com";
        $array = [
            'name' => $event->content->name,
            'email' => $event->content->email,
            'subject' => $event->content->subject,
            'message' => $event->content->message,
            'phone' => $event->content->phone
        ];
        mail::send($event->emailTemplate,$array,function($message) use ($email){
            $message->subject("Contact Us");
            $message->to($email);
        });
    }
}
