<?php

namespace  App\Exports;

use App\Models\StockTransfer;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UserExport implements FromCollection,WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()

    {
       return  User::select('name','surname','email','phone','gender','country','city','district','address')->get();
    }

    public function headings(): array
    {
        return [
            'NAME',
            'SURNAME',
            'EMAIL',
            'PHONE',
            'GENDER',
            'COUNTRY',
            'CITY',
            'DISTRICT',
            'ADDRESS'
        ];
    }
}
