<?php

namespace App\Providers;

use App\Events\BlogAdded;
use App\Events\CampaignEmail;
use App\Events\CustomEmailEvent;
use App\Events\NewsletterEmail;
use App\Events\OrderEmail;
use App\Events\OrderStatusEmail;
use App\Events\StockTransferEmail;
use App\Events\StockTransferStatusEmail;
use App\Events\WelcomeEmail;
use App\Listeners\sendBlogAddedMail;
use App\Listeners\SendCampaignEmail;
use App\Listeners\sendCustomEmail;
use App\Listeners\SendNewsletterEmail;
use App\Listeners\SendOrderEmail;
use App\Listeners\SendOrderStatusEmail;
use App\Listeners\SendStockTransferEmail;
use App\Listeners\SendStockTransferStatusEmail;
use App\Listeners\SendWelcomeEmail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        NewsletterEmail::class => [
            SendNewsletterEmail::class,
        ],
        OrderEmail::class => [
            SendOrderEmail::class,
        ],
        StockTransferEmail::class => [
            SendStockTransferEmail::class
        ],
        WelcomeEmail::class => [
            SendWelcomeEmail::class,
        ],
        OrderStatusEmail::class => [
            SendOrderStatusEmail::class,
        ],
        StockTransferStatusEmail::class => [
            SendStockTransferStatusEmail::class,
        ],
        CampaignEmail::class => [
            SendCampaignEmail::class,
        ]

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
