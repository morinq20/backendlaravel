<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class CustomGit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'custom:git';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Custom Git Command';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->addOption('selam','c',InputArgument::REQUIRED);
//        $this->exec('git status');
//        return Command::SUCCESS;
    }
}
