{{--<!DOCTYPE html>--}}
{{--<html lang="en">--}}
{{--<head>--}}
{{--    <meta charset="utf-8">--}}
{{--    <meta http-equiv="X-UA-Compatible" content="IE=edge">--}}
{{--    <meta name="viewport" content="width=device-width, initial-scale=1">--}}
{{--    <link href="{{ asset('backend/email/images/favicon.png') }}" rel="icon">--}}
{{--    <title>General Invoice - Koice</title>--}}
{{--    <meta name="author" content="harnishdesign.net">--}}

{{--    <!-- Web Fonts--}}
{{--    ======================= -->--}}
{{--    <link rel='stylesheet' href='{{ asset('backend/email/font/css?family=Poppins:100,200,300,400,500,600,700,800,900') }}' type='text/css'>--}}

{{--    <!-- Stylesheet--}}
{{--    ======================= -->--}}
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('backend/email/vendor/bootstrap/css/bootstrap.min.css') }}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('backend/email/vendor/font-awesome/css/all.min.css') }}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('backend/email/css/stylesheet.css') }}">--}}
{{--</head>--}}
{{--<body>--}}
{{--<!-- Container -->--}}
{{--<div class="container-fluid invoice-container">--}}
{{--    <!-- Header -->--}}
{{--    <header>--}}
{{--        <div class="row align-items-center">--}}
{{--            <div class="col-sm-7 text-center text-sm-start mb-3 mb-sm-0">--}}
{{--                <img id="logo" src="{{ asset($settings->panel_image) }}" title="{{ $settings->title }}" alt="{{ $settings->title }}">--}}
{{--                <h3>{{ $settings->title }}</h3>--}}
{{--            </div>--}}
{{--            <div class="col-sm-5 text-center text-sm-end">--}}
{{--                <h4 class="text-7 mb-0">Invoice</h4>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <hr>--}}
{{--    </header>--}}

{{--    <!-- Main Content -->--}}
{{--    <main>--}}
{{--        <div class="row">--}}
{{--            <div class="col-sm-6"><strong>Date: </strong>{{ $order['start_transfer_date'] }} - {{ $order['finish_transfer_date'] }}</div>--}}
{{--            <div class="col-sm-6 text-sm-end"> <strong>Invoice No:</strong> {{ $order['code'] }}</div>--}}

{{--        </div>--}}
{{--        <hr>--}}
{{--        <div class="row">--}}
{{--            <div class="col-sm-6 text-sm-end order-sm-1"> <strong>From:</strong>--}}
{{--                <address>--}}
{{--                    {{ $from['warehouse_name'] }}<br>--}}
{{--                    {{ $from['warehouse_address'] }}<br>--}}
{{--                    {{ $from['warehouse_country'] }}<br>--}}
{{--                    {{ $from['warehouse_city'] }}<br>--}}
{{--                    {{ $from['warehouse_phone'] }}<br>--}}
{{--                    {{ $from['warehouse_email'] }}<br>--}}
{{--                    {{ $from['warehouse_type'] }}--}}
{{--                </address>--}}
{{--            </div>--}}
{{--            <div class="col-sm-6 order-sm-0"> <strong>To:</strong>--}}
{{--                <address>--}}
{{--                    {{ $to['customer_name'] }} {{ $to['customer_surname'] }}<br>--}}
{{--                    {{ $to['customer_phone'] }}<br>--}}
{{--                    Shipping Address: {{ $to['customer_shipping_address'] }}<br>--}}
{{--                    Billing Address: {{ $to['customer_billing_address'] }}<br>--}}
{{--                    {{ $to['customer_phone'] }}<br>--}}
{{--                    {{ $to['customer_email'] }}--}}
{{--                </address>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="card">--}}
{{--            <div class="card-body p-0">--}}
{{--                <div class="table-responsive">--}}
{{--                    <table class="table mb-0">--}}
{{--                        <thead class="card-header">--}}
{{--                        <tr>--}}
{{--                            <td class="col-3"><strong>Product Name</strong></td>--}}
{{--                            <td class="col-4"><strong>Product Tax</strong></td>--}}
{{--                            <td class="col-2 text-center"><strong>Product Price</strong></td>--}}
{{--                            <td class="col-1 text-center"><strong>Quantity</strong></td>--}}
{{--                            <td class="col-2 text-end"><strong>Subtotal</strong></td>--}}
{{--                            <td class="col-2 text-end"><strong>Total</strong></td>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                        @foreach($items as $item)--}}
{{--                            <tr>--}}
{{--                                <td class="col-3">{{ $item->product_name }}</td>--}}
{{--                                <td class="col-4 text-1">{{ $item->variant_tax }}</td>--}}
{{--                                @if($item->variant_discount_price == null)--}}
{{--                                    <td class="col-2 text-center">{{ $item->variant_selling_price }}</td>--}}
{{--                                @else--}}
{{--                                    <td class="col-2 text-center">{{ $item->variant_discount_price }}</td>--}}
{{--                                @endif--}}
{{--                                <td class="col-1 text-center">{{ $item->product_quantity }}</td>--}}
{{--                                @if($item->variant_discount_price == null)--}}
{{--                                    <td class="col-2 text-center">{{ ($item->variant_selling_price * $item->product_quantity) }}</td>--}}
{{--                                @else--}}
{{--                                    <td class="col-2 text-center">{{ ($item->variant_discount_price * $item->product_quantity) }}</td>--}}
{{--                                @endif--}}
{{--                                @if($item->variant_discount_price == null)--}}
{{--                                    @if($item->variant_tax == null)--}}
{{--                                        <td class="col-2 text-center">{{ ($item->variant_selling_price * $item->product_quantity) }}</td>--}}
{{--                                    @else--}}
{{--                                        <td class="col-2 text-center">{{ (($item->variant_selling_price * $item->product_quantity) * $item->variant_tax) / 100 }}</td>--}}
{{--                                    @endif--}}
{{--                                @else--}}
{{--                                    @if($item->variant_tax == null)--}}
{{--                                        <td class="col-2 text-center">{{ ($item->variant_discount_price * $item->product_quantity) }}</td>--}}
{{--                                    @else--}}
{{--                                        <td class="col-2 text-center">{{ (($item->variant_discount_price * $item->product_quantity) * $item->variant_tax) / 100 }}</td>--}}
{{--                                    @endif--}}
{{--                                @endif--}}
{{--                            </tr>--}}
{{--                        @endforeach--}}
{{--                        </tbody>--}}
{{--                        <tfoot class="card-footer">--}}
{{--                        <tr>--}}
{{--                            <td colspan="4" class="text-end"><strong>Total Product:</strong></td>--}}
{{--                            <td class="text-end">{{ $order['total_product'] }}</td>--}}
{{--                        </tr>--}}
{{--                        <tr>--}}
{{--                            <td colspan="4" class="text-end"><strong>Total Quantity:</strong></td>--}}
{{--                            <td class="text-end">{{ $order['total_quantity'] }}</td>--}}
{{--                        </tr>--}}
{{--                        <tr>--}}
{{--                            <td colspan="4" class="text-end"><strong>Total Process:</strong></td>--}}
{{--                            <td class="text-end">{{ $order['total_process'] }}</td>--}}
{{--                        </tr>--}}
{{--                        <tr>--}}
{{--                            <td colspan="4" class="text-end"><strong>Total Weight:</strong></td>--}}
{{--                            <td class="text-end">{{ $order['total_weight'] }}</td>--}}
{{--                        </tr>--}}
{{--                        <tr>--}}
{{--                            <td colspan="4" class="text-end"><strong>Pallet Count:</strong></td>--}}
{{--                            <td class="text-end">{{ $order['pallet_count'] }}</td>--}}
{{--                        </tr>--}}
{{--                        <tr>--}}
{{--                            <td colspan="4" class="text-end border-bottom-0"><strong>Total Process Amount:</strong></td>--}}
{{--                            <td class="text-end border-bottom-0">{{ $order['process_total'] }}</td>--}}
{{--                        </tr>--}}
{{--                        <tr>--}}
{{--                            <td colspan="4" class="text-end border-bottom-0"><strong>Subtotal:</strong></td>--}}
{{--                            <td class="text-end border-bottom-0">{{ $order['subtotal'] }}</td>--}}
{{--                        </tr>--}}
{{--                        <tr>--}}
{{--                            <td colspan="4" class="text-end border-bottom-0"><strong>Total:</strong></td>--}}
{{--                            <td class="text-end border-bottom-0">{{ $order['total'] }}</td>--}}
{{--                        </tr>--}}
{{--                        </tfoot>--}}
{{--                    </table>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </main>--}}
{{--    <!-- Footer -->--}}
{{--    <footer class="text-center mt-4">--}}
{{--        <p class="text-1"><strong>Status :</strong> {{ $order['status'] }}</p>--}}
{{--    </footer>--}}
{{--</div>--}}
{{--</body>--}}
{{--</html>--}}


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $settings->title }} - {{ $order['code'] }} Order Invoice</title>
    <link rel="stylesheet" href="style.css">
    <style>
        .invoice-box {
            max-width: 1200px;
            max-height: 100vh;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0,0.15);
            font-size: 16px;
            line-height: 24px;
            font-family: Arial, sans-serif;
        }
        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }
        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }
        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }
        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }
        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }
        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }
        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }
        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
        }

        @media only screen and (max-width=600px) {
            .invoice-box tabla tr.top table td {
                width: 100%;
                display: block;
                /*text-align: center;*/
            }
            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                /*text-align: center;*/
            }
        }
        .invoice-box table tr .invoice-right {
            margin-right: auto;
            position: absolute;
            text-align: right;
            width: 500px;
        }
        .information table tr .fatura-right {
            margin-right: auto;
            position: relative;
            text-align: right;
            width: 500px;
        }
        .information table .b-right {
            margin-right: auto;
            position: relative;
            text-align: right;
            width: 500px;
        }
    </style>
</head>
<body>
<div class="invoice-box">
    <table cellpadding="0" cellspadding="0">
        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            <img id="logo" src="{{ asset($settings->panel_image) }}" title="{{ $settings->title }}" alt="{{ $settings->title }}">
                            <h4>{{ $settings->title }}</h4>
                        </td>
                        <td class="invoice-right">
                            <b>Invoice No:</b> {{ $order['code'] }} <br>
                            <b>Date:</b> {{ $order['start_transfer_date'] }} - {{ $order['finish_transfer_date'] }} <br>
                            <b>Invoice No:</b> {{ $order->code }} <br>
                            <b>Date:</b> {{ $order->start_transfer_date }} - {{ $order->finish_transfer_date }} <br>
                            <p>Selam naber</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="information">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            {{ $from['warehouse_name'] }}<br>
                            {{ $from['warehouse_address'] }}<br>
                            {{ $from['warehouse_country'] }}<br>
                            {{ $from['warehouse_city'] }}<br>
                            {{ $from['warehouse_phone'] }}<br>
                            {{ $from['warehouse_email'] }}<br>
                            {{ $from['warehouse_type'] }}
                        </td>
                        <td class="fatura-right">
                            {{ $to['customer_name'] }} {{ $to['customer_surname'] }}<br>
                            {{ $to['customer_phone'] }}<br>
                            Shipping Address: {{ $to['customer_shipping_address'] }}<br>
                            Billing Address: {{ $to['customer_billing_address'] }}<br>
                            {{ $to['customer_phone'] }}<br>
                            {{ $to['customer_email'] }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="heading">
            <td>Product Name</td>
            <td>Product Tax</td>
            <td>Product Price</td>
            <td>Quantity</td>
            <td>Subtotal</td>
            <td>Total</td>
        </tr>

        @foreach($items as $item)
            <tr class="item">
                <td>{{ $item->product_name }}</td>
                <td>{{ $item->variant_tax }}</td>
                @if($item->variant_discount_price == null)
                    <td>{{ $item->variant_selling_price }}</td>
                @else
                    <td>{{ $item->variant_discount_price }}</td>
                @endif
                <td>{{ $item->product_quantity }}</td>
                @if($item->variant_discount_price == null)
                    <td>{{ ($item->variant_selling_price * $item->product_quantity) }}</td>
                @else
                    <td>{{ ($item->variant_discount_price * $item->product_quantity) }}</td>
                @endif
                @if($item->variant_discount_price == null)
                    @if($item->variant_tax == null)
                        <td>{{ ($item->variant_selling_price * $item->product_quantity) }}</td>
                    @else
                        <td>{{ (($item->variant_selling_price * $item->product_quantity) * $item->variant_tax) / 100 }}</td>
                    @endif
                @else
                    @if($item->variant_tax == null)
                        <td>{{ ($item->variant_discount_price * $item->product_quantity) }}</td>
                    @else
                        <td>{{ (($item->variant_discount_price * $item->product_quantity) * $item->variant_tax) / 100 }}</td>
                    @endif
                @endif
            </tr>
        @endforeach
        <tr class="information">
            <td colspan="2">
                <table>
                    <tr>
                        <td></td>
                        <td class="b-right">
                            <b>Total Product:</b> {{ $order['total_product'] }} <br>
                            <b>Total Quantity:</b> {{ $order['total_quantity'] }} <br>
                            <b>Total Process:</b> {{ $order['total_process'] }} <br>
                            <b>Total Weight:</b> {{ $order['total_weight'] }} <br>
                            <b>Pallet Count:</b> {{ $order['pallet_count'] }} <br>
                            <b>Total Process Amount:</b> {{ $order['process_total'] }} <br>
                            <b>Subtotal:</b> {{ $order['subtotal'] }} <br>
                            <b>Total:</b> {{ $order['total'] }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>

















