<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $settings->title }}</title>
    <style>
        body{
            display: flex;
            justify-content: center;
            align-items: center;
            text-align: center;
            margin-top: 200px;
        }
        .content{
            background: whitesmoke;
            margin: 10%;
            width: 100%;
            padding: 5%;
        }
    </style>
</head>
<body>
<div class="content">
    <h3>{{$contact['subject']}}</h3>
    <br>
    {{$contact['name']}}
    <br>
    {{$contact['phone']}}
    <br>
    {{$contact['email']}}
    <br>
    {{$contact['message']}}
</div>
</body>
</html>
