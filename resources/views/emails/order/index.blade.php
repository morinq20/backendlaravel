<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $settings->title }} - {{ $order->code }} Order Invoice</title>
    <style>
        .container{
            margin: 100px;
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
        }

        .company{
            float: left;
        }

        #date{
            float: left;
            text-align: left;
            font-weight: bold;
        }

        table {
            border-collapse: collapse;
            width: 100%;
            overflow-x: scroll;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        table thead tr {
            background: lightgrey;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        .address{
            display: inline-block;
            /*width: 100px;*/
            /*height: 100px;*/
            padding: 5px;
            font-weight: bold;
        }

        .fromLeft{
            float: left;
        }

        .fromRight{
            float: right;
        }

        .headerInfo{
            background: #f6f6f6;
            display: inline-block;
            /*width: 100px;*/
            margin-top: -1px;
            padding: 10px;
            border: 1px solid lightgrey;
            width: 100%;
            font-weight: bold;
        }

        .headerLeft{
            float: left;
        }

        .headerRight{
            float: right;
        }

        .addressInfo{
            display: inline-block;
            /*width: 100px;*/
            /*height: 100px;*/
            padding: 5px;
            font-weight: 500;
            list-style: none;
        }

        .addressInfoLeft{
            float: left;
            text-align: left;
        }

        .addressInfoRight{
            float: right;
            text-align: right;
        }

        .priceInfo {
            list-style-type: none;
            padding: 0;
            margin: 0;
            width: 40%;
            float: right;
        }

        .priceInfo li {
            border: 1px solid lightgrey;
            margin-top: -1px; /* Prevent double borders */
            background-color: #f6f6f6;
            padding: 12px;
            font-weight: bold;
        }
        .priceInfo li span{
            float: right;
            text-align: right;
        }
        .topSpace{
            margin-top: 50px;
        }
        .payment{
            display: inline-block;
            /*width: 100px;*/
            /*height: 100px;*/
            /*padding: 5px;*/
            font-weight: bold;
        }

        .paymentInfo{
            display: inline-block;
            /*width: 100px;*/
            /*height: 100px;*/
            padding: 5px;
            font-weight: 500;
            list-style: none;
        }

        .paymentInfoLeft{
            float: left;
            text-align: left;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="headerInfo">
            <span class="headerLeft">
                Invoice: {{ $order->code }}
           </span>
            <span class="headerRight">
                Status: {{$orderStatus->status_text}}
            </span>
        </div>
        <div class="topSpace"></div>
{{--        <div>--}}
{{--            <h2 class="company">{{ $settings->title }}</h2>--}}
{{--            <h4 id="date">Date: {{ $order->start_transfer_date }} - {{ $order->finish_transfer_date }}</h4>--}}
{{--        </div>--}}
{{--        <img class="company" src="{{ asset($settings->panel_image) }}" title="{{ $settings->title }}" alt="{{ $settings->title }}">--}}
{{--        <div class="topSpace"></div>--}}
        <h2 class="company">{{ $settings->title }}</h2>
        <br><br><br><br>
{{--        <img class="company" src="{{ asset($settings->panel_image) }}" title="{{ $settings->title }}" alt="{{ $settings->title }}">--}}
        <br><br><br><br>
        <p id="date">Date: {{ $order->start_transfer_date }} - {{ $order->finish_transfer_date }}</p>
        <br><br><br><br>
        <div>
           <span class="address fromLeft">
                From:
           </span>
            <span class="address fromRight">
                To:
            </span>
        </div>
        <div class="topSpace"></div>
        <div>
            <ul class="addressInfo addressInfoLeft">
                <li>{{$fromWarehouse->warehouse_name}}</li>
                <li>{{$fromWarehouse->warehouse_address}}</li>
                <li>{{$fromWarehouse->warehouse_country}}</li>
                <li>{{$fromWarehouse->warehouse_city}}</li>
                <li>{{$fromWarehouse->warehouse_phone}}</li>
                <li>{{$fromWarehouse->warehouse_email}}</li>
                @if($fromWarehouse->warehouse_type == 1)
                    <li>Main Warehouse</li>
                @else
                    <li>Sub Warehouse</li>
                @endif
            </ul>
            <ul class="addressInfo addressInfoRight">
                <li>{{$customer->customer_name}} {{$customer->customer_surname}}</li>
                <li>{{$customer->customer_phone}}</li>
                <li>Shipping Address: {{$customer->customer_shipping_address}}</li>
                <li>Billing Address: {{$customer->customer_billing_address}}</li>
                <li>{{$customer->customer_phone}}</li>
                <li>{{$customer->customer_email}}</li>
            </ul>
        </div>
        <br><br>
        <table>
            <thead>
            <tr>
                <th>Product Name</th>
                <th>Code</th>
                <th>Product Tax</th>
                <th>Product Price</th>
                <th>Quantity</th>
                <th>Subtotal</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orderItems as $item)
                <tr>
                    <td>{{ $item->product_name }}</td>
                    <td>{{ $item->sku_no }}</td>
                    <td>{{ $item->variant_tax }}</td>
                    @if($item->variant_discount_price == null)
                        <td>{{ $item->variant_selling_price }}</td>
                    @else
                        <td>{{ $item->variant_discount_price }}</td>
                    @endif
                    <td>{{ $item->product_quantity }}</td>
                    @if($item->variant_discount_price == null)
                        <td>{{ ($item->variant_selling_price * $item->product_quantity) }}</td>
                    @else
                        <td>{{ ($item->variant_discount_price * $item->product_quantity) }}</td>
                    @endif
                    @if($item->variant_discount_price == null)
                        @if($item->variant_tax == null)
                            <td>{{ ($item->variant_selling_price * $item->product_quantity) }}</td>
                        @else
                            <td>{{ ((($item->variant_selling_price * $item->product_quantity) * $item->variant_tax) / 100) + $item->variant_selling_price * $item->product_quantity }}</td>
                        @endif
                    @else
                        @if($item->variant_tax == null)
                            <td>{{ ($item->variant_discount_price * $item->product_quantity) }}</td>
                        @else
                            <td>{{ ((($item->variant_discount_price * $item->product_quantity) * $item->variant_tax) / 100) + $item->variant_discount_price * $item->product_quantity }}</td>
                        @endif
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="topSpace"></div>
        <div>
            <span class="payment paymentLeft">
                Payment Method:
           </span>
        </div>
{{--        <div>--}}
{{--            <ul class="paymentInfo paymentInfoLeft">--}}
{{--                <li>Payment Method: <span>{{$order->payment_method}}</span></li>--}}
{{--                @if($order->payment_status == 1)--}}
{{--                    <li>Payment Status: <span>Paid</span></li>--}}
{{--                @else--}}
{{--                    <li>Payment Status: <span>Not Paid</span></li>--}}
{{--                @endif--}}
{{--                <li>Advance Payment: <span>{{$order->advance_payment}}</span></li>--}}
{{--                @if($order->advance_payment_status == 1)--}}
{{--                    <li>Payment Status: <span>Paid</span></li>--}}
{{--                @else--}}
{{--                    <li>Payment Status: <span>Not Paid</span></li>--}}
{{--                @endif--}}
{{--            </ul>--}}
{{--        </div>--}}
        <div>
            <ul class="priceInfo">
                <li>Total Product: <span>{{$order->total_product}}</span></li>
                <li>Total Quantity: <span>{{$order->total_quantity}}</span></li>
                <li>Total Process: <span>{{$order->total_process}}</span></li>
                <li>Total Weight: <span>{{$order->total_weight}}</span></li>
                <li>Pallet Count: <span>{{$order->pallet_count}}</span></li>
                <li>Total Process Amount: <span>{{$order->total}}</span></li>
                <li>Subtotal: <span>{{$subtotal}}</span></li>
                <li>Total: <span>{{$total}}</span></li>
            </ul>
        </div>

    </div>
</div>
</body>
</html>
