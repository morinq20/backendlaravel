<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $settings->title }} - Newsletter</title>
    <style>
        body{
            display: flex;
            justify-content: center;
            align-items: center;
            text-align: center;
            margin-top: 200px;
        }
        .content{
            background: whitesmoke;
            margin: 10%;
            width: 100%;
            padding: 5%;
        }
    </style>
</head>
<body>
<div class="content">
    First of all, welcome to our family.
    You will be the first to hear about campaigns and other announcements.
    Thank you very much for subscribing.
    <br><br><br>
    @if($settings->facebook)
        <a style="display: inline-block; height: 30px; width:30px;border-radius: 50%; background-color: #ffffff" href="{{$settings->facebook}}"><img style="width: 30px; height: 30px;" src="{{asset('/icons/facebook.png')}}" alt="brand"></a>
    @else
    @endif

    @if($settings->instagram)
        <a style="display: inline-block; height: 30px; width:30px;border-radius: 50%; background-color: #ffffff" href="{{$settings->instagram}}"><img style="width: 30px; height: 30px;" src="{{asset('/icons/instagram.png')}}" alt="brand"></a>
    @else
    @endif

    @if($settings->twitter)
        <a style="display: inline-block; height: 30px; width:30px;border-radius: 50%; background-color: #ffffff" href="{{$settings->twitter}}"><img style="width: 30px; height: 30px;" src="{{asset('/icons/twitter.png')}}" alt="brand"></a>
    @else
    @endif

    @if($settings->linkedin)
        <a style="display: inline-block; height: 30px; width:30px;border-radius: 50%; background-color: #ffffff" href="{{$settings->linkedin}}"><img style="width: 30px; height: 30px;" src="{{asset('/icons/linkedin.png')}}" alt="brand"></a>
    @else
    @endif

    @if($settings->youtube)
        <a style="display: inline-block; height: 30px; width:30px;border-radius: 50%; background-color: #ffffff" href="{{$settings->youtube}}"><img style="width: 30px; height: 30px;" src="{{asset('/icons/youtube.png')}}" alt="brand"></a>
    @else
    @endif
</div>
</body>
</html>
