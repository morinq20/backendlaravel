<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $settings->title }} - {{ $stockTransfer->code }} Order Invoice</title>
    <style>
        .container{
            margin: 100px;
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
        }

        .company{
            float: left;
        }

        #date{
            float: left;
            text-align: left;
            font-weight: bold;
        }

        table {
            border-collapse: collapse;
            width: 100%;
            overflow-x: scroll;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        table thead tr {
            background: lightgrey;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        .address{
            display: inline-block;
            /*width: 100px;*/
            /*height: 100px;*/
            padding: 5px;
            font-weight: bold;
        }

        .fromLeft{
            float: left;
        }

        .fromRight{
            float: right;
        }

        .headerInfo{
            background: #f6f6f6;
            display: inline-block;
            /*width: 100px;*/
            margin-top: -1px;
            padding: 10px;
            border: 1px solid lightgrey;
            width: 100%;
            font-weight: bold;
        }

        .headerLeft{
            float: left;
        }

        .headerRight{
            float: right;
        }

        .addressInfo{
            display: inline-block;
            /*width: 100px;*/
            /*height: 100px;*/
            padding: 5px;
            font-weight: 500;
            list-style: none;
        }

        .addressInfoLeft{
            float: left;
            text-align: left;
        }

        .addressInfoRight{
            float: right;
            text-align: right;
        }

        .priceInfo {
            list-style-type: none;
            padding: 0;
            margin: 0;
            width: 40%;
            float: right;
        }

        .priceInfo li {
            border: 1px solid lightgrey;
            margin-top: -1px; /* Prevent double borders */
            background-color: #f6f6f6;
            padding: 12px;
            font-weight: bold;
        }
        .priceInfo li span{
            float: right;
            text-align: right;
        }
        .topSpace{
            margin-top: 50px;
        }
        .payment{
            display: inline-block;
            /*width: 100px;*/
            /*height: 100px;*/
            /*padding: 5px;*/
            font-weight: bold;
        }

        .paymentInfo{
            display: inline-block;
            /*width: 100px;*/
            /*height: 100px;*/
            padding: 5px;
            font-weight: 500;
            list-style: none;
        }

        .paymentInfoLeft{
            float: left;
            text-align: left;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="headerInfo">
            <span class="headerLeft">
                Invoice: {{ $stockTransfer->code }}
           </span>
            <span class="headerRight">
                Status: {{$stockTransferStatus->status_text}}
            </span>
        </div>
        <div class="topSpace"></div>
        {{--        <div>--}}
        {{--            <h2 class="company">{{ $settings->title }}</h2>--}}
        {{--            <h4 id="date">Date: {{ $order->start_transfer_date }} - {{ $order->finish_transfer_date }}</h4>--}}
        {{--        </div>--}}
        {{--        <img class="company" src="{{ asset($settings->panel_image) }}" title="{{ $settings->title }}" alt="{{ $settings->title }}">--}}
        {{--        <div class="topSpace"></div>--}}
        <h2 class="company">{{ $settings->title }}</h2>
        <br><br><br><br>
        {{--        <img class="company" src="{{ asset($settings->panel_image) }}" title="{{ $settings->title }}" alt="{{ $settings->title }}">--}}
        <br><br><br><br>
        <p id="date">Date: {{ $stockTransfer->start_transfer_date }} - {{ $stockTransfer->finish_transfer_date }}</p>
        <br><br><br><br>
        <div>
           <span class="address fromLeft">
                From:
           </span>
            <span class="address fromRight">
                To:
            </span>
        </div>
        <div class="topSpace"></div>
        <div>
            <ul class="addressInfo addressInfoLeft">
                <li>{{$fromWarehouse->warehouse_name}}</li>
                <li>{{$fromWarehouse->warehouse_address}}</li>
                <li>{{$fromWarehouse->warehouse_country}}</li>
                <li>{{$fromWarehouse->warehouse_city}}</li>
                <li>{{$fromWarehouse->warehouse_phone}}</li>
                <li>{{$fromWarehouse->warehouse_email}}</li>
                @if($fromWarehouse->warehouse_type == 1)
                    <li>Main Warehouse</li>
                @else
                    <li>Sub Warehouse</li>
                @endif
            </ul>
            <ul class="addressInfo addressInfoRight">
                <li>{{$toWarehouse->warehouse_name}}</li>
                <li>{{$toWarehouse->warehouse_address}}</li>
                <li>{{$toWarehouse->warehouse_country}}</li>
                <li>{{$toWarehouse->warehouse_city}}</li>
                <li>{{$toWarehouse->warehouse_phone}}</li>
                <li>{{$toWarehouse->warehouse_email}}</li>
                @if($toWarehouse->warehouse_type == 1)
                    <li>Main Warehouse</li>
                @else
                    <li>Sub Warehouse</li>
                @endif
            </ul>
        </div>
        <br><br>
        <table>
            <thead>
            <tr>
                <th>Product Name</th>
                <th>Code</th>
                <th>Quantity</th>
            </tr>
            </thead>
            <tbody>
            @foreach($stockTransferItems as $item)
                <tr>
                    <td>{{ $item->product_name }}</td>
                    <td>{{ $item->sku_no }}</td>
                    <td>{{ $item->product_quantity }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="topSpace"></div>
        {{--        <div>--}}
        {{--            <span class="payment paymentLeft">--}}
        {{--                Payment Method:--}}
        {{--           </span>--}}
        {{--        </div>--}}
        {{--        <div>--}}
        {{--            <ul class="paymentInfo paymentInfoLeft">--}}
        {{--                <li>Cash</li>--}}
        {{--                <li>Pending</li>--}}
        {{--            </ul>--}}
        {{--        </div>--}}
        <div>
            <ul class="priceInfo">
                <li>Total Product: <span>{{$stockTransfer->total_product}}</span></li>
                <li>Total Quantity: <span>{{$stockTransfer->total_quantity}}</span></li>
                <li>Total Process: <span>{{$stockTransfer->total_process}}</span></li>
                <li>Total Weight: <span>{{$stockTransfer->total_weight}}</span></li>
                <li>Pallet Count: <span>{{$stockTransfer->pallet_count}}</span></li>
                <li>Total Process Amount: <span>{{$stockTransfer->total}}</span></li>
            </ul>
        </div>

    </div>
</div>
</body>
</html>
