<!DOCTYPE html>
<html lang="en">

<head>
    <title>Mentor - Bootstrap 4 Admin Dashboard Template</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Admin template that can be used to build dashboards for CRM, CMS, etc.">
    <meta name="author" content="Potenza Global Solutions">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    <!-- app favicon -->
    <link rel="shortcut icon" href="{{ asset('/backend/assets/img/favicon.ico') }}">
    <!-- google fonts -->
    <link href="{{ asset('/backend/css?family=Roboto:300,400,500,700') }}" rel="stylesheet">

    <!-- plugin stylesheets -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/backend/assets/css/vendors.css') }}">
    <!-- app style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/backend/assets/css/style.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <!-- Alertify -->
    <!-- CSS -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css"/>
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css"/>
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>
    <!-- Fancy Box 3 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
{{--    <link rel = "stylesheet"--}}
{{--          href = "https://cdnjs.cloudflare.com/ajax/libs/framework7/1.4.2/css/framework7.ios.min.css" />--}}
{{--    <link rel = "stylesheet"--}}
{{--          href = "https://cdnjs.cloudflare.com/ajax/libs/framework7/1.4.2/css/framework7.ios.colors.min.css" />--}}

</head>

<body>

<div id="app">
<!-- begin app -->
<div class="app">
    <!-- begin app-wrap -->
    <div class="app-wrap">
        <!-- begin pre-loader -->
        <div class="loader">
            <div class="h-100 d-flex justify-content-center">
                <div class="align-self-center">
                    <img src="{{ asset('/backend/assets/img/loader/loader.svg') }}" alt="loader">
                </div>
            </div>
        </div>
        <!-- end pre-loader -->
        <!-- begin app-header -->
        <header class="app-header top-bar" id="topbar" v-show="$route.path === '/' || $route.path === '/admin/login' || $route.path === '/admin/two-factor' || $route.path === '/admin/register' || $route.path === '/admin/forget' ? false : true" style="display: none;">
            <!-- begin navbar -->
            <nav class="navbar navbar-expand-md">

                <!-- begin navbar-header -->
                <div class="navbar-header d-flex align-items-center">
                    <a href="javascript:void(0)" class="mobile-toggle"><i class="ti ti-align-right"></i></a>
                    <a class="navbar-brand" href="index.html">
                        <img src="{{ asset('/backend/assets/img/logo.png') }}" class="img-fluid logo-desktop" alt="logo">
                        <img src="{{ asset('/backend/assets/img/logo-icon.png') }}" class="img-fluid logo-mobile" alt="logo">
                    </a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="ti ti-align-left"></i>
                </button>
                <!-- end navbar-header -->
                <!-- begin navigation -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <div class="navigation d-flex">
                        <ul class="navbar-nav nav-left">
                            <li class="nav-item">
                                <a href="javascript:void(0)" class="nav-link sidebar-toggle">
                                    <i class="ti ti-align-right"></i>
                                </a>
                            </li>
                            <li class="nav-item full-screen d-none d-lg-block" id="btnFullscreen">
                                <a href="javascript:void(0)" class="nav-link expand">
                                    <i class="icon-size-fullscreen"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="navbar-nav nav-right ml-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="ti ti-email"></i>
                                </a>
                                <div class="dropdown-menu extended animated fadeIn" aria-labelledby="navbarDropdown">
                                    <ul>
                                        <li class="dropdown-header bg-gradient p-4 text-white text-left">Messages
                                            <label class="label label-info label-round">6</label>
                                            <a href="#" class="float-right btn btn-square btn-inverse-light btn-xs m-0">
                                                <span class="font-13"> Mark all as read</span></a>
                                        </li>
                                        <li class="dropdown-body">
                                            <ul class="scrollbar scroll_dark max-h-240">
                                                <li>
                                                    <a href="javascript:void(0)">
                                                        <div class="notification d-flex flex-row align-items-center">
                                                            <div class="notify-icon bg-img align-self-center">
                                                                <img class="img-fluid" src="{{ asset('/backend/assets/img/avtar/03.jpg') }}" alt="user3">
                                                            </div>
                                                            <div class="notify-message">
                                                                <p class="font-weight-bold">Brianing Leyon</p>
                                                                <small>You will sail along until you...</small>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">
                                                        <div class="notification d-flex flex-row align-items-center">
                                                            <div class="notify-icon bg-img align-self-center">
                                                                <img class="img-fluid" src="{{ asset('/backend/assets/img/avtar/01.jpg') }}" alt="user">
                                                            </div>
                                                            <div class="notify-message">
                                                                <p class="font-weight-bold">Jimmyimg Leyon</p>
                                                                <small>Okay</small>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">
                                                        <div class="notification d-flex flex-row align-items-center">
                                                            <div class="notify-icon bg-img align-self-center">
                                                                <img class="img-fluid" src="{{ asset('/backend/assets/img/avtar/02.jpg') }}" alt="user2">
                                                            </div>
                                                            <div class="notify-message">
                                                                <p class="font-weight-bold">Brainjon Leyon</p>
                                                                <small>So, make the decision...</small>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">
                                                        <div class="notification d-flex flex-row align-items-center">
                                                            <div class="notify-icon bg-img align-self-center">
                                                                <img class="img-fluid" src="{{ asset('^/backend/assets/img/avtar/04.jpg') }}" alt="user4">
                                                            </div>
                                                            <div class="notify-message">
                                                                <p class="font-weight-bold">Smithmin Leyon</p>
                                                                <small>Thanks</small>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">
                                                        <div class="notification d-flex flex-row align-items-center">
                                                            <div class="notify-icon bg-img align-self-center">
                                                                <img class="img-fluid" src="{{ asset('/backend/assets/img/avtar/05.jpg') }}" alt="user5">
                                                            </div>
                                                            <div class="notify-message">
                                                                <p class="font-weight-bold">Jennyns Leyon</p>
                                                                <small>How are you</small>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">
                                                        <div class="notification d-flex flex-row align-items-center">
                                                            <div class="notify-icon bg-img align-self-center">
                                                                <img class="img-fluid" src="{{ asset('/backend/assets/img/avtar/06.jpg') }}" alt="user6">
                                                            </div>
                                                            <div class="notify-message">
                                                                <p class="font-weight-bold">Demian Leyon</p>
                                                                <small>I like your themes</small>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="dropdown-footer">
                                            <a class="font-13" href="javascript:void(0)"> View All messages </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fe fe-bell"></i>
                                    <span class="notify">
                                                    <span class="blink"></span>
                                        <span class="dot"></span>
                                        </span>
                                </a>
                                <div class="dropdown-menu extended animated fadeIn" aria-labelledby="navbarDropdown">
                                    <ul>
                                        <li class="dropdown-header bg-gradient p-4 text-white text-left">Notifications
                                            <a href="#" class="float-right btn btn-square btn-inverse-light btn-xs m-0">
                                                <span class="font-13"> Clear all</span></a>
                                        </li>
                                        <li class="dropdown-body min-h-240 nicescroll">
                                            <ul class="scrollbar scroll_dark max-h-240" id="adminNotification">
                                                @forelse(\Illuminate\Support\Facades\DB::table('notifications')->where('notifiable_id','=',1)->get() as $notification)
                                                    <li>
                                                        <a href="javascript:void(0)">
                                                            <div class="notification d-flex flex-row align-items-center">
                                                                <div class="notify-icon bg-img align-self-center">
                                                                    <div class="bg-type bg-type-md">
                                                                        <span>NWSL</span>
                                                                    </div>
                                                                </div>
                                                                <div class="notify-message">
                                                                    <p class="font-weight-bold">{{ $notification->data }}</p>
                                                                    <small>03/07/2022</small>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li>
                                                @empty
                                                @endforelse
                                            </ul>
                                        </li>
                                        <li class="dropdown-footer">
                                            <a class="font-13" href="javascript:void(0)"> View All Notifications
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link search" href="javascript:void(0)">
                                    <i class="ti ti-search"></i>
                                </a>
                                <div class="search-wrapper">
                                    <div class="close-btn">
                                        <i class="ti ti-close"></i>
                                    </div>
                                    <div class="search-content">
                                        <form>
                                            <div class="form-group">
                                                <i class="ti ti-search magnifier"></i>
                                                <input type="text" class="form-control autocomplete" placeholder="Search Here" id="autocomplete-ajax" autofocus="autofocus">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item dropdown user-profile">
                                <a href="javascript:void(0)" class="nav-link dropdown-toggle " id="navbarDropdown4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="{{ asset('/backend/assets/img/avtar/02.jpg') }}" alt="avtar-img">
                                    <span class="bg-success user-status"></span>
                                </a>
                                <div class="dropdown-menu animated fadeIn" aria-labelledby="navbarDropdown">
                                    <div class="bg-gradient px-4 py-3">
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="mr-1">
                                                <h4 class="text-white mb-0">Alice Williams</h4>
                                                <small class="text-white">Henry@example.com</small>
                                            </div>
                                            <router-link to="/admin/logout" class="text-white font-20 tooltip-wrapper" data-toggle="tooltip" data-placement="top" title="" data-original-title="Logout"> <i class="zmdi zmdi-power"></i></router-link>
                                        </div>
                                    </div>
                                    <div class="p-4">
                                        <a class="dropdown-item d-flex nav-link" href="javascript:void(0)">
                                            <i class="fa fa-user pr-2 text-success"></i> Profile</a>
                                        <a class="dropdown-item d-flex nav-link" href="javascript:void(0)">
                                            <i class="fa fa-envelope pr-2 text-primary"></i> Inbox
                                            <span class="badge badge-primary ml-auto">6</span>
                                        </a>
                                        <a class="dropdown-item d-flex nav-link" href="javascript:void(0)">
                                            <i class=" ti ti-settings pr-2 text-info"></i> Settings
                                        </a>
                                        <a class="dropdown-item d-flex nav-link" href="javascript:void(0)">
                                            <i class="fa fa-compass pr-2 text-warning"></i> Need help?</a>
                                        <div class="row mt-2">
                                            <div class="col">
                                                <a class="bg-light p-3 text-center d-block" href="#">
                                                    <i class="fe fe-mail font-20 text-primary"></i>
                                                    <span class="d-block font-13 mt-2">My messages</span>
                                                </a>
                                            </div>
                                            <div class="col">
                                                <a class="bg-light p-3 text-center d-block" href="#">
                                                    <i class="fe fe-plus font-20 text-primary"></i>
                                                    <span class="d-block font-13 mt-2">Compose new</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- end navigation -->
            </nav>
            <!-- end navbar -->
        </header>
        <!-- end app-header -->
        <!-- begin app-container -->
        <div class="app-container">
            <!-- begin app-nabar -->
            <aside class="app-navbar" id="sidebar" v-show="$route.path === '/' || $route.path === '/admin/login' || $route.path === '/admin/two-factor' || $route.path === '/admin/register' || $route.path === '/admin/forget' ? false : true" style="display: none;">
                <!-- begin sidebar-nav -->
                <div class="sidebar-nav scrollbar scroll_light">
                    <ul class="metismenu " id="sidebarNav">
                        <li class="nav-static-title">CRM</li>
                        <li class="active">
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false">
                                <i class="nav-icon ti ti-rocket"></i>
                                <span class="nav-title">Dashboards</span>
                            </a>
                            <ul aria-expanded="false">
                                <li class="active"> <router-link to='/home'>Home</router-link> </li>
                                <li> <router-link to='/pos'>POS</router-link> </li>
                                <li> <router-link to='/stock-transfer'>Stock Transfer</router-link> </li>
                                <li> <router-link to='/stock-transfer-list'>List Stock Transfer</router-link> </li>
                            </ul>
                        </li>
                        <!-- CUSTOMER -->
                        <li><a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-calendar"></i><span class="nav-title">Customer</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to='/store-customer'>Create Customer</router-link> </li>
                                <li> <router-link to='/customer'>List Customer</router-link> </li>
                            </ul>
                        </li>
                        <!-- ORDER REQUEST -->
                        <li><a href="mail-inbox.html" aria-expanded="false"><i class="nav-icon ti ti-email"></i><span class="nav-title">Order Request</span></a> </li>
                        <!-- ORDER -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-bag"></i> <span class="nav-title">Order</span></a>
                            <ul aria-expanded="false">
                                <li> <a href="ui-alerts.html">List Accepted Order</a> </li>
                                <li> <a href="ui-accordions.html">List Prepared Order</a> </li>
                                <li> <a href="ui-accordions.html">List On The Way Order</a> </li>
                                <li> <a href="ui-accordions.html">List Delivered Order</a> </li>
                            </ul>
                        </li>
                        <!-- ORDER TRACKING -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-info"></i><span class="nav-title">Order Tracking</span> </a>
                            <ul aria-expanded="false">
                                <li> <a href="icons-cryptocurrency.html">Search Order Tracking</a> </li>
                            </ul>
                        </li>
                        <li class="nav-static-title">Web Site Management</li>
                        <!-- SLIDER -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-layout"></i> <span class="nav-title">Slider</span></a>
                            <ul aria-expanded="false">
                                <li><router-link to="/store-slider">Create Slider</router-link></li>
                                <li><router-link to="/slider">List Slider</router-link></li>

                            </ul>
                        </li>
                        <!-- CATEGORY -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"> <i class="nav-icon ti ti-layout-grid4-alt"></i> <span class="nav-title">Category & Sub Category</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/store-category">Create Category</router-link> </li>
                                <li> <router-link to="/category">List Category</router-link> </li>
                                <li> <router-link to="/store-subcategory">Create Sub Category</router-link> </li>
                                <li> <router-link to="/subcategory">List Sub Category</router-link> </li>
                            </ul>
                        </li>
                        <!-- COLLECTION -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-layout-column3-alt"></i><span class="nav-title">Collection</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/store-collection">Create Collection</router-link> </li>
                                <li> <router-link to="/collection">List Collection </router-link> </li>
                            </ul>
                        </li>
                        <!-- PRODUCT -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-layout-column3-alt"></i><span class="nav-title">Product</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/store-product">Create Product</router-link> </li>
                                <li> <router-link to="/product">List Product </router-link> </li>
                                <li> <router-link to="/stock">Out Of Stock</router-link> </li>
                                <li> <router-link to="/store-variant">Create Variant</router-link> </li>
                                <li> <router-link to="/variant">List Variant</router-link> </li>
{{--                                <li> <router-link to="/request-product">List Request Product</router-link> </li>--}}
                            </ul>
                        </li>
                        <!-- BLOG -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-layout"></i> <span class="nav-title">Blog</span></a>
                            <ul aria-expanded="false">
                                <li><router-link to="/store-blog">Create Blog</router-link></li>
                                <li><router-link to="/blog">List Blog</router-link></li>

                            </ul>
                        </li>
                        <!-- ABOUT US -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-pie-chart"></i><span class="nav-title">About Us</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/store-about-us">Create About Us</router-link> </li>
                                <li> <router-link to="/about-us">List About Us</router-link> </li>
                            </ul>
                        </li>
                        <!-- FAQ -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-layout"></i> <span class="nav-title">FAQ</span></a>
                            <ul aria-expanded="false">
                                <li><router-link to="/store-faq">Create FAQ</router-link></li>
                                <li><router-link to="/faq">List FAQ</router-link></li>

                            </ul>
                        </li>
                        <!-- SERVICE -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-layout"></i> <span class="nav-title">Service</span></a>
                            <ul aria-expanded="false">
                                <li><router-link to="/store-service">Create Service</router-link></li>
                                <li><router-link to="/service">List Service</router-link></li>

                            </ul>
                        </li>
                        <!-- TEAM -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-layout"></i> <span class="nav-title">Team</span></a>
                            <ul aria-expanded="false">
                                <li><router-link to="/store-team">Create Team</router-link></li>
                                <li><router-link to="/team">List Team</router-link></li>

                            </ul>
                        </li>
                        <!-- PROJECT -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-layout"></i> <span class="nav-title">Project</span></a>
                            <ul aria-expanded="false">
                                <li><router-link to="/store-project">Create Project</router-link></li>
                                <li><router-link to="/project">List Project</router-link></li>

                            </ul>
                        </li>
                        <!-- REFERANCE -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-layout"></i> <span class="nav-title">Referance</span></a>
                            <ul aria-expanded="false">
                                <li><router-link to="/store-referance">Create Referance</router-link></li>
                                <li><router-link to="/referance">List Referance</router-link></li>

                            </ul>
                        </li>
                        <!-- CONTACT MESSAGE -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"> <i class="nav-icon ti ti-pencil-alt"></i> <span class="nav-title">Contact & Newslatter</span> </a>
                            <ul aria-expanded="false">
                                <li> <a href="form-autonumeric.html">List Contact</a> </li>
                                <li> <router-link to="/newsletter">List Newslatter</router-link> </li>
                            </ul>
                        </li>
                        <li class="nav-static-title">Private Management</li>
                        <!-- SIZE COLOR AND UNIT -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-map-alt"></i><span class="nav-title">Size, Color and Unit</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/store-size">Create Size</router-link> </li>
                                <li> <router-link to="/size">List Size</router-link> </li>
                                <li> <router-link to="/store-color">Create Color</router-link> </li>
                                <li> <router-link to="/color">List Color</router-link> </li>
                                <li> <router-link to="/store-unit">Create Unit</router-link> </li>
                                <li> <router-link to="/unit">List Unit</router-link> </li>
                            </ul>
                        </li>
                        <!-- NOTE -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-map-alt"></i><span class="nav-title">Note</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/store-note">Create Note</router-link> </li>
                                <li> <router-link to="/note">List Note</router-link> </li>
                            </ul>
                        </li>
                        <!-- TASK -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-map-alt"></i><span class="nav-title">Task</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/store-task">Create Task</router-link> </li>
                                <li> <router-link to="/task">List Task</router-link> </li>
                            </ul>
                        </li>
                        <!-- WAREHOUSE -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-map-alt"></i><span class="nav-title">Warehouse</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/store-warehouse">Create Warehouse</router-link> </li>
                                <li> <router-link to="/warehouse">List Warehouse</router-link> </li>
                                <li> <router-link to="/warehouse-items">Warehouse Items</router-link> </li>
                            </ul>
                        </li>
                        <!-- ADMIN -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-map-alt"></i><span class="nav-title">Admin</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/store-admin">Create Admin</router-link> </li>
                                <li> <router-link to="/admin">List Admin</router-link> </li>
                            </ul>
                        </li>
                        <!-- ROLE -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-layers"></i><span class="nav-title">Role</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/store-role">Create Role</router-link> </li>
                                <li> <router-link to="/role">List Role</router-link> </li>
                            </ul>
                        </li>
                        <!-- SUPPLIER -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-map-alt"></i><span class="nav-title">Supplier</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/store-supplier">Create Supplier</router-link> </li>
                                <li> <router-link to="/supplier">List Supplier</router-link> </li>
                            </ul>
                        </li>
                        <!-- Campaign -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-map-alt"></i><span class="nav-title">Campaign</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/store-campaign">Create Campaign</router-link> </li>
                                <li> <router-link to="/campaign">List Campaign</router-link> </li>
                            </ul>
                        </li>
                        <!-- SHIPPING COMPANY -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-map-alt"></i><span class="nav-title">Shipping Company</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/store-shipping-company">Create Shipping Company</router-link> </li>
                                <li> <router-link to="/shipping-company">List Shipping Company</router-link> </li>
                            </ul>
                        </li>
                        <!-- SHIPPING COMPANY DECI-->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-map-alt"></i><span class="nav-title">Shipping Company Deci</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/store-shipping-company-deci">Create Shipping Company Deci</router-link> </li>
                                <li> <router-link to="/shipping-company-deci">List Shipping Company Deci</router-link> </li>
                            </ul>
                        </li>
                        <!-- SHIPPING COMPANY DISTANCE-->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-map-alt"></i><span class="nav-title">Shipping Company Distance</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/store-shipping-company-distance">Create Shipping Company Distance</router-link> </li>
                                <li> <router-link to="/shipping-company-distance">List Shipping Company Distance</router-link> </li>
                            </ul>
                        </li>
                        <!-- SHIPPING COMPANY POST TYPE-->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-map-alt"></i><span class="nav-title">Shipping Company Post Type</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/store-shipping-company-post-type">Create Shipping Company Post Type</router-link> </li>
                                <li> <router-link to="/shipping-company-post-type">List Shipping Company Post Type</router-link> </li>
                            </ul>
                        </li>
                        <!-- SHIPPING COMPANY SERVICE-->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-map-alt"></i><span class="nav-title">Shipping Company Service</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/store-shipping-company-service">Create Shipping Company Service</router-link> </li>
                                <li> <router-link to="/shipping-company-service">List Shipping Company Service</router-link> </li>
                            </ul>
                        </li>
                        <!-- PAYMENT METHOD -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-key"></i><span class="nav-title">Payment Method</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/store-payment-method">Create Payment Method</router-link> </li>
                                <li> <router-link to="/payment-method">List Payment Method</router-link> </li>
                            </ul>
                        </li>
                        <!-- IMAGE RESIZE -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-key"></i><span class="nav-title">Image Resize</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/edit-image-resize">Image Resize</router-link> </li>
                            </ul>
                        </li>
                        <!-- SETTINGS -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-key"></i><span class="nav-title">Settings</span></a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/edit-website-settings">Web Site Settings</router-link> </li>
                                <li> <router-link to="/edit-panel-settings">Panel Settings</router-link> </li>
                                <li> <router-link href="/store-order-tracking-status">Create Order Tracking Status</router-link> </li>
                                <li> <router-link href="/order-tracking-status">List Order Tracking Status</router-link> </li>
                                <li> <a href="auth-lockscreen.html">Google Maps Settings</a> </li>
                            </ul>
                        </li>
                        <!-- EMPLOYEE -->
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"> <i class="nav-icon ti ti-pencil-alt"></i> <span class="nav-title">Employee</span> </a>
                            <ul aria-expanded="false">
                                <li> <router-link to="/store-employee">Create Employee</router-link> </li>
                                <li> <router-link to="/employee">List Employee</router-link> </li>
                            </ul>
                        </li>
                        <!-- SALARY -->
                        <li><a href="app-chat.html" aria-expanded="false"><i class="nav-icon ti ti-comment"></i><span class="nav-title">Salary</span></a> </li>
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="nav-icon ti ti-list"></i><span class="nav-title">Report</span></a>
                            <ul aria-expanded="false">
                                <li> <a href="javascript: void(0);">Search Report</a> </li>
                                <li> <a href="javascript: void(0);">General Report</a> </li>
                                <li> <a href="javascript: void(0);">Daily Report</a> </li>
                                <li> <a href="javascript: void(0);">Monthly Report</a> </li>
                                <li> <a href="javascript: void(0);">Yearly Report</a> </li>
                            </ul>
                        </li>
                        <!-- BALINSOFT -->
                        <li class="sidebar-banner p-4 bg-gradient text-center m-3 d-block rounded">
                            <h5 class="text-white mb-1">Balinsoft</h5>
                            <p class="font-13 text-white line-20">Contact +90 551 265 94 44</p>
                            <a class="btn btn-square btn-inverse-light btn-xs d-inline-block mt-2 mb-0" href="tel:+905512659444"> Call me</a>
                        </li>
                    </ul>
                </div>
                <!-- end sidebar-nav -->
            </aside>
            <!-- end app-navbar -->
            <!-- begin app-main -->
            <div class="app-main" id="main">
                <!-- begin container-fluid -->
                <div class="container-fluid">
                    <router-view></router-view>
                    <!-- event Modal -->
                    <div class="modal fade" id="eventModal" tabindex="-1" role="dialog" aria-labelledby="verticalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="verticalCenterTitle">Add New Event</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="modelemail">Event Name</label>
                                            <input type="email" class="form-control" id="modelemail">
                                        </div>
                                        <div class="form-group">
                                            <label>Choose Event Color</label>
                                            <select class="form-control">
                                                <option>Primary</option>
                                                <option>Warning</option>
                                                <option>Success</option>
                                                <option>Danger</option>
                                            </select>
                                        </div>

                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-success">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end container-fluid -->
            </div>
            <!-- end app-main -->
        </div>

        <!-- end app-container -->
        <!-- begin footer -->
        <footer class="footer" id="footer" v-show="$route.path === '/' || $route.path === '/admin/login' || $route.path === '/admin/two-factor' || $route.path === '/admin/register' || $route.path === '/admin/forget' ? false : true" style="display: none;">
            <div class="row">
                <div class="col-12 col-sm-6 text-center text-sm-left">
                    <p>&copy; Copyright 2019. All rights reserved. {{ @date('Y') }}</p>
                </div>
                <div class="col  col-sm-6 ml-sm-auto text-center text-sm-right">
                    <p>Hand-crafted made with <i class="fa fa-heart text-danger mx-1"></i> by Potenza</p>
                </div>
            </div>
        </footer>
        <!-- end footer -->
    </div>
    <!-- end app-wrap -->
</div>
<!-- end app -->
</div>
<!-- plugins -->
<script src="{{ asset('/backend/assets/js/vendors.js') }}"></script>

<!-- custom app -->
<script src="{{ asset('/backend/assets/js/app.js') }}"></script>
<script src="{{ asset('/js/app.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    let token = localStorage.getItem('token');
    if (token){
        $("#sidebar").css("display","");
        $("#topbar").css("display","");
        $("#footer").css("display","");
    }
</script>
<script type="text/javascript">
    $('input.number').keyup(function(event) {

        // skip for arrow keys
        if(event.which >= 37 && event.which <= 40) return;

        // format number
        $(this).val(function(index, value) {
            return value
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                ;
        });
    });
</script>
<!-- Admin Notification -->
{{--<script type="text/javascript">--}}
{{--        $.ajaxSetup({--}}
{{--            headers: {--}}
{{--                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
{{--            }--}}
{{--        });--}}
{{--        var id = User.userId();--}}
{{--        $.ajax({--}}
{{--            type: "POST",--}}
{{--            url: "{{route('admin.Notification')}}",--}}
{{--            data: {--}}
{{--                user_id: id,--}}
{{--            },--}}
{{--            dataType:"json",--}}
{{--            success: function (data) {--}}
{{--                $.each(data, function (key, value) {--}}
{{--                    var parseData = JSON.parse(value.data);--}}
{{--                    $('#adminNotification').append(--}}
{{--                        '<li>'+--}}
{{--                        '<a href="javascript:void(0)">'+--}}
{{--                        '<div class="notification d-flex flex-row align-items-center">'+--}}
{{--                        '<div class="notify-icon bg-img align-self-center">'+--}}
{{--                        '<div class="bg-type bg-type-md">'+--}}
{{--                        '<span>SBS</span>'+--}}
{{--                        '</div>'+--}}
{{--                        '</div>'+--}}
{{--                        '<div class="notify-message">'+--}}
{{--                        '<p class="font-weight-bold">'+parseData.newsletter_email+'</p>'+--}}
{{--                        '<small>'+value.created_at+'</small>'+--}}
{{--                        '</div>'+--}}
{{--                        '</div>'+--}}
{{--                        '</a>'+--}}
{{--                        '</li>'--}}
{{--                    );--}}
{{--                });--}}
{{--            }--}}
{{--        });--}}
{{--</script>--}}
<!-- Blog Sortable -->
<script>
    $(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#sortableBlog').sortable({
            revert: true,
            handle: ".sortableBlog",
            stop: function (event, ui) {
                var data = $(this).sortable('serialize');
                console.log(data);
                $.ajax({
                    type: "POST",
                    data: data,
                    url: "{{route('blog.Sortable')}}",
                    success: function (msg) {
                        // console.log(msg);
                        if (msg) {
                            alertify.success("Successfull");
                        } else {
                            alertify.error("Error");
                        }
                    }
                });

            }
        });
        $('#sortableBlog').disableSelection();

    });
</script>
<!-- Faq Sortable -->
<script>
    $(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#sortableFaq').sortable({
            revert: true,
            handle: ".sortableFaq",
            stop: function (event, ui) {
                var data = $(this).sortable('serialize');
                console.log(data);
                $.ajax({
                    type: "POST",
                    data: data,
                    url: "{{route('faq.Sortable')}}",
                    success: function (msg) {
                        // console.log(msg);
                        if (msg) {
                            alertify.success("Successfull");
                        } else {
                            alertify.error("Error");
                        }
                    }
                });

            }
        });
        $('#sortableFaq').disableSelection();

    });
</script>
<!-- Service Sortable -->
<script>
    $(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#sortableService').sortable({
            revert: true,
            handle: ".sortableService",
            stop: function (event, ui) {
                var data = $(this).sortable('serialize');
                console.log(data);
                $.ajax({
                    type: "POST",
                    data: data,
                    url: "{{route('service.Sortable')}}",
                    success: function (msg) {
                        // console.log(msg);
                        if (msg) {
                            alertify.success("Successfull");
                        } else {
                            alertify.error("Error");
                        }
                    }
                });

            }
        });
        $('#sortableService').disableSelection();

    });
</script>
<!-- Project Sortable -->
<script>
    $(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#sortableProject').sortable({
            revert: true,
            handle: ".sortableProject",
            stop: function (event, ui) {
                var data = $(this).sortable('serialize');
                console.log(data);
                $.ajax({
                    type: "POST",
                    data: data,
                    url: "{{route('project.Sortable')}}",
                    success: function (msg) {
                        // console.log(msg);
                        if (msg) {
                            alertify.success("Successfull");
                        } else {
                            alertify.error("Error");
                        }
                    }
                });

            }
        });
        $('#sortableProject').disableSelection();

    });
</script>
<!-- Team Sortable -->
<script>
    $(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#sortableTeam').sortable({
            revert: true,
            handle: ".sortableTeam",
            stop: function (event, ui) {
                var data = $(this).sortable('serialize');
                console.log(data);
                $.ajax({
                    type: "POST",
                    data: data,
                    url: "{{route('team.Sortable')}}",
                    success: function (msg) {
                        // console.log(msg);
                        if (msg) {
                            alertify.success("Successfull");
                        } else {
                            alertify.error("Error");
                        }
                    }
                });

            }
        });
        $('#sortableTeam').disableSelection();

    });
</script>
<!-- Referance Sortable -->
<script>
    $(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#sortableReferance').sortable({
            revert: true,
            handle: ".sortableReferance",
            stop: function (event, ui) {
                var data = $(this).sortable('serialize');
                console.log(data);
                $.ajax({
                    type: "POST",
                    data: data,
                    url: "{{route('referance.Sortable')}}",
                    success: function (msg) {
                        // console.log(msg);
                        if (msg) {
                            alertify.success("Successfull");
                        } else {
                            alertify.error("Error");
                        }
                    }
                });

            }
        });
        $('#sortableReferance').disableSelection();

    });
</script>
<!-- Slider Sortable -->
<script>
    $(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#sortableSlider').sortable({
            revert: true,
            handle: ".sortableSlider",
            stop: function (event, ui) {
                var data = $(this).sortable('serialize');
                console.log(data);
                $.ajax({
                    type: "POST",
                    data: data,
                    url: "{{route('slider.Sortable')}}",
                    success: function (msg) {
                        // console.log(msg);
                        if (msg) {
                            alertify.success("Successfull");
                        } else {
                            alertify.error("Error");
                        }
                    }
                });

            }
        });
        $('#sortableSlider').disableSelection();

    });
</script>
<!-- Payment Method Sortable -->
<script>
    $(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#sortablePaymentMethod').sortable({
            revert: true,
            handle: ".sortablePaymentMethod",
            stop: function (event, ui) {
                var data = $(this).sortable('serialize');
                console.log(data);
                $.ajax({
                    type: "POST",
                    data: data,
                    url: "{{route('paymentMethod.Sortable')}}",
                    success: function (msg) {
                        // console.log(msg);
                        if (msg) {
                            alertify.success("Successfull");
                        } else {
                            alertify.error("Error");
                        }
                    }
                });

            }
        });
        $('#sortablePaymentMethod').disableSelection();

    });
</script>
<!-- Collection Sortable -->
<script>
    $(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#sortableCollection').sortable({
            revert: true,
            handle: ".sortableCollection",
            stop: function (event, ui) {
                var data = $(this).sortable('serialize');
                console.log(data);
                $.ajax({
                    type: "POST",
                    data: data,
                    url: "{{route('collection.Sortable')}}",
                    success: function (msg) {
                        // console.log(msg);
                        if (msg) {
                            alertify.success("Successfull");
                        } else {
                            alertify.error("Error");
                        }
                    }
                });

            }
        });
        $('#sortableCollection').disableSelection();

    });
</script>
<!-- Campaign Sortable -->
<script>
    $(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#sortableCampaign').sortable({
            revert: true,
            handle: ".sortableCampaign",
            stop: function (event, ui) {
                var data = $(this).sortable('serialize');
                console.log(data);
                $.ajax({
                    type: "POST",
                    data: data,
                    url: "{{route('campaign.Sortable')}}",
                    success: function (msg) {
                        // console.log(msg);
                        if (msg) {
                            alertify.success("Successfull");
                        } else {
                            alertify.error("Error");
                        }
                    }
                });

            }
        });
        $('#sortableCampaign').disableSelection();

    });
</script>


<!-- Alertify -->
<!-- JavaScript -->
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
<!-- Fancy Box 3 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $("[data-fancybox]").fancybox({
            loop: true,
            buttons: [
                "zoom",
                "share",
                "slideShow",
                "fullScreen",
                "download",
                "thumbs",
                "close"
            ],
        });
    });
</script>


<script type = "text/javascript"
        src = "https://cdnjs.cloudflare.com/ajax/libs/framework7/1.4.2/js/framework7.min.js"></script>

<script>
    var myApp = new Framework7();

    var $$ = Dom7;

    $$('.task1').on('click', function () {
        myApp.alert('Task 1 Clicked !!');
    });
</script>


<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
<script>
    let customNavigation = document.querySelector('.customNavigation');
    customNavigation.onclick = function() {
        customNavigation.classList.toggle('active');
    }
</script>
@if(session()->has('success'))
    <script>alertify.success('{{session('success')}}')</script>
@endif
@if(session()->has('error'))
    <script>alertify.error('{{session('error')}}')</script>
@endif

@foreach($errors->all() as $error)
    <script>
        alertify.error('{{$error}}');
    </script>
@endforeach
<script type="text/javascript">
    $("#one").keyup(function () {
        if (this.value.length == this.maxLength) {
            $('#one').css('border-color',"limegreen");
            $('#two').css('display',"");
            $(this).next('.inputs').focus();
        }
    });
    $("#two").keyup(function () {
        if (this.value.length == this.maxLength) {
            $('#two').css('border-color',"limegreen");
            $('#three').css('display',"");
            $(this).next('.inputs').focus();
        }
    });
    $("#three").keyup(function () {
        if (this.value.length == this.maxLength) {
            $('#three').css('border-color',"limegreen");
            $('#four').css('display',"");
            $(this).next('.inputs').focus();
        }
    });
    $("#four").keyup(function () {
        if (this.value.length == this.maxLength) {
            $('#four').css('border-color',"limegreen");
            $('#five').css('display',"");
            $(this).next('.inputs').focus();
        }
    });
    $("#five").keyup(function () {
        if (this.value.length == this.maxLength) {
            $('#five').css('border-color',"limegreen");
            $('#six').css('display',"");
            $(this).next('.inputs').focus();
        }
    });
    $("#six").keyup(function () {
        if (this.value.length == this.maxLength) {
            $('#six').css('border-color',"limegreen");
        }
    });
    $('#six').keyup(function(e){
        if(e.keyCode == 46 || e.keyCode == 8) {
            $('#six').css('display',"none");
            $(this).prev('#five').focus();
        }
    });
    $('#five').keyup(function(e){
        if(e.keyCode == 46 || e.keyCode == 8) {
            $('#five').css('display',"none");
            $(this).prev('#four').focus();
        }
    });
    $('#four').keyup(function(e){
        if(e.keyCode == 46 || e.keyCode == 8) {
            $('#four').css('display',"none");
            $(this).prev('#three').focus();
        }
    });
    $('#three').keyup(function(e){
        if(e.keyCode == 46 || e.keyCode == 8) {
            $('#three').css('display',"none");
            $(this).prev('#two').focus();
        }
    });
    $('#two').keyup(function(e){
        if(e.keyCode == 46 || e.keyCode == 8) {
            $('#two').css('display',"none");
            $(this).prev('#one').focus();
        }
    });
</script>
</body>

</html>

