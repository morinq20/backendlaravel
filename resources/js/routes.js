// // Start Frontend Home
// let frontend_home = require('./components/frontend/default/home.vue').default;
// // End Frontend Home
//
//
//
// // Start Authentication
// let admin_login = require('./components/backend/auth/login.vue').default;
// let admin_register = require('./components/backend/auth/register.vue').default;
// let admin_forget = require('./components/backend/auth/forget.vue').default;
// let admin_logout = require('./components/backend/auth/logout.vue').default;
// let admin_two_factor = require('./components/backend/auth/two_factor.vue').default;
// // End Authentication
//
// // Start Home
// let home = require('./components/backend/default/home.vue').default;
// // End Home
//
// // Start Roles
// let store_role = require('./components/backend/roles/create.vue').default;
// let edit_role = require('./components/backend/roles/edit.vue').default;
// let role = require('./components/backend/roles/index.vue').default;
// // End Roles
//
// // Start Admin
// let store_admin = require('./components/backend/admin/create.vue').default;
// let edit_admin = require('./components/backend/admin/edit.vue').default;
// let admin = require('./components/backend/admin/index.vue').default;
// // End Admin
//
// // Start Category
// let store_category = require('./components/backend/category/create.vue').default;
// let edit_category = require('./components/backend/category/edit.vue').default;
// let category = require('./components/backend/category/index.vue').default;
// // End Category
//
// // Start Sub Category
// let store_subcategory = require('./components/backend/subcategory/create.vue').default;
// let edit_subcategory = require('./components/backend/subcategory/edit.vue').default;
// let subcategory = require('./components/backend/subcategory/index.vue').default;
// // End Sub Category
//
// // Start Slider
// let store_slider = require('./components/backend/slider/create.vue').default;
// let edit_slider = require('./components/backend/slider/edit.vue').default;
// let slider = require('./components/backend/slider/index.vue').default;
// // End Slider
//
// // Start Product
// let store_product = require('./components/backend/product/create.vue').default;
// let edit_product = require('./components/backend/product/edit.vue').default;
// let product = require('./components/backend/product/index.vue').default;
// let priority_product = require('./components/backend/product/priority.vue').default;
// let variant_product = require('./components/backend/product/variant.vue').default;
// let stock = require('./components/backend/product/stock.vue').default;
// let request_product = require('./components/backend/product/request_product.vue').default;
// // End Product
//
// // Start Pos
// let pos = require('./components/backend/pos/index.vue').default;
// // End Pos
//
// // Start Stock Transfer
// let stock_transfer = require('./components/backend/stock_transfer/index.vue').default;
// let stock_transfer_list = require('./components/backend/stock_transfer/list.vue').default;
// let stock_transfer_show = require('./components/backend/stock_transfer/show.vue').default;
// // End Stock Transfer
//
//
// // Start Blog
// let store_blog = require('./components/backend/blog/create.vue').default;
// let edit_blog = require('./components/backend/blog/edit.vue').default;
// let blog = require('./components/backend/blog/index.vue').default;
// // End Blog
//
// // Start About Us
// let store_about_us = require('./components/backend/about_us/create.vue').default;
// let edit_about_us = require('./components/backend/about_us/edit.vue').default;
// let about_us = require('./components/backend/about_us/index.vue').default;
// // End About Us
//
// // Start Faq
// let store_faq = require('./components/backend/faq/create.vue').default;
// let edit_faq = require('./components/backend/faq/edit.vue').default;
// let faq = require('./components/backend/faq/index.vue').default;
// // End Faq
//
// // Start Service
// let store_service = require('./components/backend/service/create.vue').default;
// let edit_service = require('./components/backend/service/edit.vue').default;
// let service = require('./components/backend/service/index.vue').default;
// // End Service
//
// // Start Team
// let store_team = require('./components/backend/team/create.vue').default;
// let edit_team = require('./components/backend/team/edit.vue').default;
// let team = require('./components/backend/team/index.vue').default;
// // End Team
//
// // Start Project
// let store_project = require('./components/backend/project/create.vue').default;
// let edit_project = require('./components/backend/project/edit.vue').default;
// let project = require('./components/backend/project/index.vue').default;
// // End Project
//
// // Start Referance
// let store_referance = require('./components/backend/referance/create.vue').default;
// let edit_referance = require('./components/backend/referance/edit.vue').default;
// let referance = require('./components/backend/referance/index.vue').default;
// // End Referance
//
// // Start Customer
// let store_customer = require('./components/backend/customer/create.vue').default;
// let edit_customer = require('./components/backend/customer/edit.vue').default;
// let customer = require('./components/backend/customer/index.vue').default;
// // End Customer
//
// // Start Task
// let store_task = require('./components/backend/task/create.vue').default;
// let edit_task = require('./components/backend/task/edit.vue').default;
// let task = require('./components/backend/task/index.vue').default;
// // End Task
//
// // Start Payment Method
// let store_payment_method = require('./components/backend/payment_method/create.vue').default;
// let edit_payment_method = require('./components/backend/payment_method/edit.vue').default;
// let payment_method = require('./components/backend/payment_method/index.vue').default;
// // End Payment Method
//
// // Start Image Resize
// let edit_image_resize = require('./components/backend/image_resize/edit.vue').default;
// // End Image Resize
//
// // Start Panel Settings
// let edit_panel_settings = require('./components/backend/panel_settings/edit.vue').default;
// // End Panel Settings
//
// // Start Web Site Settings
// let edit_website_settings = require('./components/backend/website_settings/edit.vue').default;
// // End Web Site Settings
//
// // Start Collection
// let store_collection = require('./components/backend/collection/create.vue').default;
// let edit_collection = require('./components/backend/collection/edit.vue').default;
// let collection = require('./components/backend/collection/index.vue').default;
// // End Collection
//
// // Start Shipping Company
// let store_shipping_company = require('./components/backend/shipping_company/create.vue').default;
// let edit_shipping_company = require('./components/backend/shipping_company/edit.vue').default;
// let shipping_company = require('./components/backend/shipping_company/index.vue').default;
// // End Shipping Company
//
// // Start Shipping Company Deci
// let store_shipping_company_deci = require('./components/backend/shipping_company_deci/create.vue').default;
// let edit_shipping_company_deci = require('./components/backend/shipping_company_deci/edit.vue').default;
// let shipping_company_deci = require('./components/backend/shipping_company_deci/index.vue').default;
// // End Shipping Company Deci
//
// // Start Shipping Company Distance
// let store_shipping_company_distance = require('./components/backend/shipping_company_distance/create.vue').default;
// let edit_shipping_company_distance = require('./components/backend/shipping_company_distance/edit.vue').default;
// let shipping_company_distance = require('./components/backend/shipping_company_distance/index.vue').default;
// // End Shipping Company Distance
//
// // Start Shipping Company Post Type
// let store_shipping_company_post_type = require('./components/backend/shipping_company_post_type/create.vue').default;
// let edit_shipping_company_post_type = require('./components/backend/shipping_company_post_type/edit.vue').default;
// let shipping_company_post_type = require('./components/backend/shipping_company_post_type/index.vue').default;
// // End Shipping Company Post Type
//
// // Start Shipping Company Service
// let store_shipping_company_service = require('./components/backend/shipping_company_service/create.vue').default;
// let edit_shipping_company_service = require('./components/backend/shipping_company_service/edit.vue').default;
// let shipping_company_service = require('./components/backend/shipping_company_service/index.vue').default;
// // End Shipping Company Service
//
// // Start Newsletter
// let newsletter = require('./components/backend/newsletter/index.vue').default;
// // End Newsletter
//
// // Start Supplier
// let store_supplier = require('./components/backend/supplier/create.vue').default;
// let edit_supplier = require('./components/backend/supplier/edit.vue').default;
// let supplier = require('./components/backend/supplier/index.vue').default;
// // End Supplier
//
// // Start Campaign
// let store_campaign = require('./components/backend/campaign/create.vue').default;
// let edit_campaign = require('./components/backend/campaign/edit.vue').default;
// let campaign = require('./components/backend/campaign/index.vue').default;
// // End Campaign
//
// // Start Size
// let store_size = require('./components/backend/size/create.vue').default;
// let edit_size = require('./components/backend/size/edit.vue').default;
// let size = require('./components/backend/size/index.vue').default;
// // End Size
//
// // Start Color
// let store_color = require('./components/backend/color/create.vue').default;
// let edit_color = require('./components/backend/color/edit.vue').default;
// let color = require('./components/backend/color/index.vue').default;
// // End Color
//
// // Start Unit
// let store_unit = require('./components/backend/unit/create.vue').default;
// let edit_unit = require('./components/backend/unit/edit.vue').default;
// let unit = require('./components/backend/unit/index.vue').default;
// // End Unit
//
// // Start Variant
// let store_variant = require('./components/backend/variant/create.vue').default;
// let edit_variant = require('./components/backend/variant/edit.vue').default;
// let variant = require('./components/backend/variant/index.vue').default;
// // End Variant
//
// // Start Employee
// let store_employee = require('./components/backend/employee/create.vue').default;
// let edit_employee = require('./components/backend/employee/edit.vue').default;
// let employee = require('./components/backend/employee/index.vue').default;
// // End Employee
//
// // Start Warehouse
// let store_warehouse = require('./components/backend/warehouse/create.vue').default;
// let edit_warehouse = require('./components/backend/warehouse/edit.vue').default;
// let warehouse_items = require('./components/backend/warehouse/warehouse_items.vue').default;
// let warehouse = require('./components/backend/warehouse/index.vue').default;
// // End Warehouse
//
// export const routes = [
//     // Start Frontend Home
//     { path: '/', component: frontend_home, name: '/' },
//     // End Frontend Home
//
//
//     // Start Authentication
//     { path: '/admin/login', component: admin_login, name: '/admin/login' },
//     { path: '/admin/login', component: admin_register, name: '/admin/register' },
//     { path: '/admin/forget', component: admin_forget, name: '/admin/forget' },
//     { path: '/admin/logout', component: admin_logout, name: '/admin/logout' },
//     { path: '/admin/two-factor', component: admin_two_factor, name: '/admin/two-factor' },
//     // End Authentication
//
//     // Start Home
//     { path: '/home', component: home, name: 'home' },
//     // End Home
//
//     // Start Roles
//     { path: '/store-role', component: store_role, name: 'store-role' },
//     { path: '/role', component: role, name: 'role' },
//     { path: '/edit-role', component: edit_role, name: 'edit-role' },
//     // End Roles
//
//     // Start Admin
//     { path: '/store-admin', component: store_admin, name: 'store-admin' },
//     { path: '/admin', component: admin, name: 'admin' },
//     { path: '/edit-admin', component: edit_admin, name: 'edit-admin' },
//     // End Admin
//
//     // Start Category
//     { path: '/store-category', component: store_category, name: 'store-category' },
//     { path: '/category', component: category, name: 'category' },
//     { path: '/edit-category', component: edit_category, name: 'edit-category' },
//     // End Category
//
//     // Start Sub Category
//     { path: '/store-subcategory', component: store_subcategory, name: 'store-subcategory' },
//     { path: '/subcategory', component: subcategory, name: 'subcategory' },
//     { path: '/edit-subcategory', component: edit_subcategory, name: 'edit-subcategory' },
//     // End Sub Category
//
//     // Start Slider
//     { path: '/store-slider', component: store_slider, name: 'store-slider' },
//     { path: '/slider', component: slider, name: 'slider' },
//     { path: '/edit-slider', component: edit_slider, name: 'edit-slider' },
//     // End Slider
//
//     // Start Product
//     { path: '/store-product', component: store_product, name: 'store-product' },
//     { path: '/product', component: product, name: 'product' },
//     { path: '/edit-product', component: edit_product, name: 'edit-product' },
//     { path: '/priority-product', component: priority_product, name: 'priority-product' },
//     { path: '/variant-product', component: variant_product, name: 'variant-product' },
//     { path: '/stock', component: stock, name: 'stock' },
//     { path: '/request-product', component: request_product, name: 'request-product' },
//     // End Product
//
//     // Start Pos
//     { path: '/pos', component: pos, name: 'pos' },
//     // End Pos
//
//     // Start Stock Transfer
//     { path: '/stock-transfer', component: stock_transfer, name: 'stock-transfer' },
//     { path: '/stock-transfer-list', component: stock_transfer_list, name: 'stock-transfer-list' },
//     { path: '/stock-transfer-show', component: stock_transfer_show, name: 'stock-transfer-show' },
//     // End Stock Transfer
//
//
//     // Start Blog
//     { path: '/store-blog', component: store_blog, name: 'store-blog' },
//     { path: '/blog', component: blog, name: 'blog' },
//     { path: '/edit-blog', component: edit_blog, name: 'edit-blog' },
//     // End Blog
//
//     // Start About Us
//     { path: '/store-about-us', component: store_about_us, name: 'store-about-us' },
//     { path: '/about-us', component: about_us, name: 'about-us' },
//     { path: '/edit-about-us', component: edit_about_us, name: 'edit-about-us' },
//     // End About Us
//
//     // Start Faq
//     { path: '/store-faq', component: store_faq, name: 'store-faq' },
//     { path: '/faq', component: faq, name: 'faq' },
//     { path: '/edit-faq', component: edit_faq, name: 'edit-faq' },
//     // End Faq
//
//     // Start Service
//     { path: '/store-service', component: store_service, name: 'store-service' },
//     { path: '/service', component: service, name: 'service' },
//     { path: '/edit-service', component: edit_service, name: 'edit-service' },
//     // End Service
//
//     // Start Team
//     { path: '/store-team', component: store_team, name: 'store-team' },
//     { path: '/team', component: team, name: 'team' },
//     { path: '/edit-team', component: edit_team, name: 'edit-team' },
//     // End Team
//
//     // Start Project
//     { path: '/store-project', component: store_project, name: 'store-project' },
//     { path: '/project', component: project, name: 'project' },
//     { path: '/edit-project', component: edit_project, name: 'edit-project' },
//     // End Project
//
//     // Start Referance
//     { path: '/store-referance', component: store_referance, name: 'store-referance' },
//     { path: '/referance', component: referance, name: 'referance' },
//     { path: '/edit-referance', component: edit_referance, name: 'edit-referance' },
//     // End Referance
//
//     // Start Customer
//     { path: '/store-customer', component: store_customer, name: 'store-customer' },
//     { path: '/customer', component: customer, name: 'customer' },
//     { path: '/edit-customer', component: edit_customer, name: 'edit-customer' },
//     // End Customer
//
//     // Start Task
//     { path: '/store-task', component: store_task, name: 'store-task' },
//     { path: '/task', component: task, name: 'task' },
//     { path: '/edit-task', component: edit_task, name: 'edit-task' },
//     // End Task
//
//     // Start Payment Method
//     { path: '/store-payment-method', component: store_payment_method, name: 'store-payment-method' },
//     { path: '/payment-method', component: payment_method, name: 'payment-method' },
//     { path: '/edit-payment-method', component: edit_payment_method, name: 'edit-payment-method' },
//     // End Payment Method
//
//     // Start Image Resize
//     { path: '/edit-image-resize', component: edit_image_resize, name: 'edit-image-resize' },
//     // End Image Resize
//
//     // Start Panel Settings
//     { path: '/edit-panel-settings', component: edit_panel_settings, name: 'edit-panel-settings' },
//     // End Panel Settings
//
//     // Start Web Site Settings
//     { path: '/edit-website-settings', component: edit_website_settings, name: 'edit-website-settings' },
//     // End Web Site Settings
//
//     // Start Collection
//     { path: '/store-collection', component: store_collection, name: 'store-collection' },
//     { path: '/collection', component: collection, name: 'collection' },
//     { path: '/edit-collection', component: edit_collection, name: 'edit-collection' },
//     // End Collection
//
//     // Start Shipping Company
//     { path: '/store-shipping-company', component: store_shipping_company, name: 'store-shipping-company' },
//     { path: '/shipping-company', component: shipping_company, name: 'shipping-company' },
//     { path: '/edit-shipping-company', component: edit_shipping_company, name: 'edit-shipping-company' },
//     // End Shipping Company
//
//     // Start Shipping Company Deci
//     { path: '/store-shipping-company-deci', component: store_shipping_company_deci, name: 'store-shipping-company-deci' },
//     { path: '/shipping-company-deci', component: shipping_company_deci, name: 'shipping-company-deci' },
//     { path: '/edit-shipping-company-deci', component: edit_shipping_company_deci, name: 'edit-shipping-company-deci' },
//     // End Shipping Company Deci
//
//     // Start Shipping Company Distance
//     { path: '/store-shipping-company-distance', component: store_shipping_company_distance, name: 'store-shipping-company-distance' },
//     { path: '/shipping-company-distance', component: shipping_company_distance, name: 'shipping-company-distance' },
//     { path: '/edit-shipping-company-distance', component: edit_shipping_company_distance, name: 'edit-shipping-company-distance' },
//     // End Shipping Company Distance
//
//     // Start Shipping Company Post Type
//     { path: '/store-shipping-company-post-type', component: store_shipping_company_post_type, name: 'store-shipping-company-post-type' },
//     { path: '/shipping-company-post-type', component: shipping_company_post_type, name: 'shipping-company-post-type' },
//     { path: '/edit-shipping-company-post-type', component: edit_shipping_company_post_type, name: 'edit-shipping-company-post-type' },
//     // End Shipping Company Post Type
//
//     // Start Shipping Company Service
//     { path: '/store-shipping-company-service', component: store_shipping_company_service, name: 'store-shipping-company-service' },
//     { path: '/shipping-company-service', component: shipping_company_service, name: 'shipping-company-service' },
//     { path: '/edit-shipping-company-service', component: edit_shipping_company_service, name: 'edit-shipping-company-service' },
//     // End Shipping Company Service
//
//     // Start Newsletter
//     { path: '/newsletter', component: newsletter, name: 'newsletter' },
//     // End Newsletter
//
//     // Start Supplier
//     { path: '/store-supplier', component: store_supplier, name: 'store-supplier' },
//     { path: '/supplier', component: supplier, name: 'supplier' },
//     { path: '/edit-supplier', component: edit_supplier, name: 'edit-supplier' },
//     // End Supplier
//
//     // Start Campaign
//     { path: '/store-campaign', component: store_campaign, name: 'store-campaign' },
//     { path: '/campaign', component: campaign, name: 'campaign' },
//     { path: '/edit-campaign', component: edit_campaign, name: 'edit-campaign' },
//     // End Campaign
//
//     // Start Size
//     { path: '/store-size', component: store_size, name: 'store-size' },
//     { path: '/size', component: size, name: 'size' },
//     { path: '/edit-size', component: edit_size, name: 'edit-size' },
//     // End Size
//
//     // Start Color
//     { path: '/store-color', component: store_color, name: 'store-color' },
//     { path: '/color', component: color, name: 'color' },
//     { path: '/edit-color', component: edit_color, name: 'edit-color' },
//     // End Color
//
//     // Start Unit
//     { path: '/store-unit', component: store_unit, name: 'store-unit' },
//     { path: '/unit', component: unit, name: 'unit' },
//     { path: '/edit-unit', component: edit_unit, name: 'edit-unit' },
//     // End Unit
//
//     // Start Variant
//     { path: '/store-variant', component: store_variant, name: 'store-variant' },
//     { path: '/variant', component: variant, name: 'variant' },
//     { path: '/edit-variant', component: edit_variant, name: 'edit-variant' },
//     // End Variant
//
//     // Start Employee
//     { path: '/store-employee', component: store_employee, name: 'store-employee' },
//     { path: '/employee', component: employee, name: 'employee' },
//     { path: '/edit-employee', component: edit_employee, name: 'edit-employee' },
//     // End Employee
//
//     // Start Warehouse
//     { path: '/store-warehouse', component: store_warehouse, name: 'store-warehouse' },
//     { path: '/warehouse', component: warehouse, name: 'warehouse' },
//     { path: '/edit-warehouse', component: edit_warehouse, name: 'edit-warehouse' },
//     { path: '/warehouse-items', component: warehouse_items, name: 'warehouse-items' },
//     // End Warehouse
//
// ]
