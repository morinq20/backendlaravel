<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_resize', function (Blueprint $table) {
            $table->id();
            $table->string('slider_image_width');
            $table->string('slider_image_height');
            $table->string('blog_image_width');
            $table->string('blog_image_height');
            $table->string('product_image_width');
            $table->string('product_image_height');
            $table->string('about_us_image_width');
            $table->string('about_us_image_height');
            $table->string('service_image_width');
            $table->string('service_image_height');
            $table->string('team_image_width');
            $table->string('team_image_height');
            $table->string('project_image_width');
            $table->string('project_image_height');
            $table->string('referance_image_width');
            $table->string('referance_image_height');
            $table->string('admin_image_width');
            $table->string('admin_image_height');
            $table->string('note_image_width');
            $table->string('note_image_height');
            $table->string('website_settings_image_width');
            $table->string('website_settings_image_height');
            $table->string('panel_settings_image_width');
            $table->string('panel_settings_image_height');
            $table->string('order_tracking_status_image_width');
            $table->string('order_tracking_status_image_height');
            $table->string('employee_image_width');
            $table->string('employee_image_height');
            $table->string('campaign_image_width');
            $table->string('campaign_image_height');
            $table->string('shipping_company_image_width');
            $table->string('shipping_company_image_height');
            $table->string('variant_image_width');
            $table->string('variant_image_height');
            $table->string('comment_image_width');
            $table->string('comment_image_height');
            $table->string('collection_image_width');
            $table->string('collection_image_height');
            $table->string('truck_image_width');
            $table->string('truck_image_height');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_resize');
    }
};
