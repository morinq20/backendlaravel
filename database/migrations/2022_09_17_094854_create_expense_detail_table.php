<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense_detail', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('expense_id')->unsigned();
            $table->foreign('expense_id')->references('id')->on('expense')->onDelete('cascade');
            $table->string('quantity');
            $table->string('amount');
            $table->string('total_amount');
            $table->string('tax');
            $table->string('total_tax');
            $table->string('total');
            $table->text('description');
            $table->string('invoice_no');
            $table->dateTime('invoice_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense_detail');
    }
};
