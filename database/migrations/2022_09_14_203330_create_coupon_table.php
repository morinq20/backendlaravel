<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon', function (Blueprint $table) {
            $table->id();
            $table->string('coupon_name');
            $table->text('coupon_description')->nullable();
            $table->string('coupon_quantity');
            $table->string('coupon_discount');
            $table->dateTime('coupon_start_date')->nullable();
            $table->dateTime('coupon_end_date')->nullable();
            $table->boolean('coupon_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon');
    }
};
