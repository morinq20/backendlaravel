<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team', function (Blueprint $table) {
            $table->id();
            $table->string('team_name');
            $table->string('team_surname');
            $table->string('team_phone');
            $table->string('team_email');
            $table->string('team_fax')->nullable();
            $table->string('team_facebook')->nullable();
            $table->string('team_twitter')->nullable();
            $table->string('team_linkedin')->nullable();
            $table->string('team_instagram')->nullable();
            $table->string('team_image');
            $table->text('team_description');
            $table->text('team_location')->nullable();
            $table->string('team_position')->nullable();
            $table->string('team_practice_area')->nullable();
            $table->string('team_experience')->nullable();
            $table->string('team_job');
            $table->string('team_seq');
            $table->integer('country_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->string('team_phone_dial_code')->nullable();
            $table->json('lang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team');
    }
};
