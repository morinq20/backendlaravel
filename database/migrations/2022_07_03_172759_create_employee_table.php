<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->id();
            $table->string('employee_code');
            $table->string('employee_name');
            $table->string('employee_surname');
            $table->string('employee_phone');
            $table->string('employee_email');
            $table->string('employee_image');
            $table->text('employee_description');
            $table->text('employee_location')->nullable();
            $table->string('employee_position')->nullable();
            $table->string('employee_practice_area')->nullable();
            $table->string('employee_experience')->nullable();
            $table->string('employee_job');
            $table->integer('country_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->string('employee_phone_dial_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee');
    }
};
