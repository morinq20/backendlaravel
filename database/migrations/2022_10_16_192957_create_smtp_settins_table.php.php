<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('smtp_settings', function (Blueprint $table) {
                    $table->id();
                    $table->string('smtp_server');
                    $table->string('smtp_email');
                    $table->string('smtp_password');
                    $table->string('smtp_port');
                    $table->timestamps();
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
