<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revenue', function (Blueprint $table) {
            $table->id();
            $table->string('revenue_title');
            $table->string('revenue_tax')->nullable();
            $table->dateTime('revenue_date')->nullable();
            $table->text('revenue_description')->nullable();
            $table->bigInteger('accounting_status_id')->unsigned();
            $table->foreign('accounting_status_id')->references('id')->on('accounting_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revenue');
    }
};
