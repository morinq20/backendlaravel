<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pallets', function (Blueprint $table) {
            $table->id();
            $table->string('pallet_name');
            $table->string('pallet_default_weight');
            $table->string('pallet_width');
            $table->string('pallet_height');
            $table->string('pallet_length');
            $table->string('pallet_capacity');
            $table->string('pallet_max_weight');
            $table->string('pallet_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pallets');
    }
};
