<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->id();
            $table->string('customer_name');
            $table->string('customer_surname');
            $table->string('customer_phone');
            $table->string('customer_email');
            $table->string('customer_country');
            $table->string('customer_city');
            $table->string('customer_zipcode');
            $table->text('customer_address');
            $table->text('customer_shipping_address');
            $table->text('customer_billing_address');
            $table->integer('country_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->string('customer_phone_dial_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
};
