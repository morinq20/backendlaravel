<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_transfer', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->integer('from_warehouse_id');
            $table->integer('to_warehouse_id');
            $table->integer('supplier_id')->nullable();
            $table->integer('truck_id')->nullable();
            $table->text('notes')->nullable();
            $table->date('start_transfer_date');
            $table->date('finish_transfer_date');
            $table->integer('total_product');
            $table->float('total_quantity');
            $table->tinyInteger('status');
            $table->string('distance')->nullable();
            $table->string('duration')->nullable();
            $table->string('price')->nullable();
            $table->string('extra_price')->nullable();
            $table->string('total_pallet')->nullable();
            $table->string('subtotal')->nullable();
            $table->string('total')->nullable();
            $table->boolean('isCanceled');
            $table->string('total_process')->nullable();
            $table->json('stock_transfer_process')->nullable();
            $table->string('pallet_id');
            $table->string('pallet_count');
            $table->string('total_weight');
            $table->string('date');
            $table->string('month');
            $table->string('year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_transfer');
    }
};
