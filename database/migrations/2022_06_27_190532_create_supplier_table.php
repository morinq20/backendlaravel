<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier', function (Blueprint $table) {
            $table->id();
            $table->string('supplier_name');
            $table->string('supplier_surname');
            $table->string('supplier_phone');
            $table->string('supplier_email');
            $table->string('supplier_country');
            $table->string('supplier_city');
            $table->string('supplier_zipcode');
            $table->text('supplier_address');
            $table->integer('country_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->string('supplier_phone_dial_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier');
    }
};
