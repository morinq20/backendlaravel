<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_cart', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('product_id');
            $table->integer('variant_id')->nullable();
            $table->integer('unit_id')->nullable();
            $table->string('product_name');
            $table->string('variant_sku')->nullable(true);
            $table->string('color_name')->nullable(true);
            $table->string('color_code')->nullable(true);
            $table->string('size_name')->nullable(true);
            $table->float('product_quantity');
            $table->string('unit_name')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_cart');
    }
};
