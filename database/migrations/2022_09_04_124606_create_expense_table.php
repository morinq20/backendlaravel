<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense', function (Blueprint $table) {
            $table->id();
            $table->string('expense_title');
            $table->string('expense_tax')->nullable();
            $table->dateTime('expense_date')->nullable();
            $table->text('expense_description')->nullable();
            $table->bigInteger('accounting_status_id')->unsigned();
            $table->foreign('accounting_status_id')->references('id')->on('accounting_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense');
    }
};
