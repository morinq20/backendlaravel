<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign', function (Blueprint $table) {
            $table->id();
            $table->string('campaign_code');
            $table->string('campaign_title');
            $table->text('campaign_description');
            $table->string('campaign_url');
            $table->string('campaign_image');
            $table->string('campaign_seq');
            $table->boolean('campaign_status');
            $table->boolean('is_published');
            $table->json('lang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign');
    }
};
