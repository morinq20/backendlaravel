<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->string('product_name');
            $table->string('product_code');
            $table->text('product_description');
            $table->text('product_meta_description');
            $table->string('product_image');
            $table->integer('category_id');
            $table->integer('subcategory_id')->nullable();
            $table->integer('collection_id')->nullable();
            $table->integer('supplier_id')->nullable();
            $table->integer('warehouse_id');
            $table->text('barcode')->nullable();
            $table->boolean('product_status');
            $table->float('product_quantity',20,2);
            $table->float('product_tax',20,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
};
