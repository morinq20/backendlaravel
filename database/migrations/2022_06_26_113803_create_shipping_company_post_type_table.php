<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_company_post_type', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('shipping_company_id')->unsigned();
            $table->foreign('shipping_company_id')->references('id')->on('shipping_company');
            $table->tinyInteger('post_type');
            $table->double('post_type_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_company_post_type');
    }
};
