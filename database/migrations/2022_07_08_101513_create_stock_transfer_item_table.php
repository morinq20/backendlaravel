<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_transfer_item', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('stock_transfer_id')->unsigned();
            $table->foreign('stock_transfer_id')->references('id')->on('stock_transfer');
            $table->integer('product_id');
            $table->integer('variant_id')->nullable(true);
            $table->integer('unit_id');
            $table->float('product_quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_transfer_item');
    }
};
