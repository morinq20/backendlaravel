<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse', function (Blueprint $table) {
            $table->id();
            $table->string('warehouse_name');
            $table->text('warehouse_address');
            $table->string('warehouse_country');
            $table->string('warehouse_city');
            $table->string('warehouse_phone');
            $table->string('warehouse_email');
            $table->string('warehouse_postcode');
            $table->boolean('warehouse_type');
            $table->boolean('warehouse_status');
            $table->string('warehouse_latitude')->nullable();
            $table->string('warehouse_longitude')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->string('warehouse_phone_dial_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse');
    }
};
