<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referance', function (Blueprint $table) {
            $table->id();
            $table->string('referance_title');
            $table->string('referance_image');
            $table->text('referance_description');
            $table->boolean('referance_status');
            $table->string('referance_seq');
            $table->json('lang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referance');
    }
};
