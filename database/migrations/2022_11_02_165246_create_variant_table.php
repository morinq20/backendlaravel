<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variant', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('product');
            $table->bigInteger('size_id')->unsigned();
            $table->foreign('size_id')->references('id')->on('size');
            $table->bigInteger('color_id')->unsigned();
            $table->foreign('color_id')->references('id')->on('color');
            $table->bigInteger('unit_id')->unsigned();
            $table->foreign('unit_id')->references('id')->on('unit');
            $table->string('sku_no');
            $table->string('variant_quantity');
            $table->string('variant_image');
            $table->string('variant_weight');
            $table->string('variant_capacity');
            $table->string('variant_tax')->nullable();
            $table->string('variant_cost_price')->nullable();
            $table->string('variant_selling_price');
            $table->string('variant_discount_price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variant');
    }
};
