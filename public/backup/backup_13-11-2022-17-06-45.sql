DROP TABLE about_us;

CREATE TABLE `about_us` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `about_us_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about_us_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `about_us_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about_us_status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO about_us VALUES("6","Who are we?","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sem ante, eleifend vel porttitor sed, sollicitudin id dui. Pellentesque nulla urna, porta vitae risus et, dictum finibus dolor. Mauris condimentum neque ut ipsum pharetra aliquet. Ut cursus, libero vel commodo ultrices, risus turpis blandit lorem, ac viverra metus massa vel lectus. Sed et hendrerit arcu. Quisque blandit tortor consectetur volutpat tristique. Curabitur dolor odio, feugiat vitae leo vel, eleifend iaculis lectus. Vestibulum lorem justo, luctus sit amet turpis a, malesuada tristique ante. Pellentesque sagittis ante neque, a hendrerit quam rutrum eleifend. Pellentesque nec scelerisque elit, eget porta magna.Cras tincidunt ante vel convallis suscipit. Aliquam lorem nulla, mattis in lorem non, accumsan ornare ex. Pellentesque ut velit lobortis, malesuada sapien id, gravida dolor. Curabitur hendrerit diam nec augue fermentum rutrum. Quisque molestie augue nec justo luctus, eget ultricies est condimentum. Aliquam convallis turpis lectus. Maecenas tempus venenatis mollis. Donec at lacus non est posuere malesuada.","backend/uploads/about_us/1667733552.jpeg","1","2022-11-06 10:42:01","2022-11-06 11:19:12");



DROP TABLE accounting;

CREATE TABLE `accounting` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `accounting_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accounting_tax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accounting_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accounting_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accounting_status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO accounting VALUES("9","12313","","asdad","","0","2022-08-30 19:05:09","2022-08-30 19:05:09");
INSERT INTO accounting VALUES("10","sad","","asda","","1","2022-08-30 19:06:05","2022-08-30 19:06:05");
INSERT INTO accounting VALUES("11","deneme","10","deneme","","1","2022-09-04 11:44:08","2022-09-04 11:44:08");



DROP TABLE accounting_status;

CREATE TABLE `accounting_status` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `accounting_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO accounting_status VALUES("1","daily","","");
INSERT INTO accounting_status VALUES("2","weekly","","");
INSERT INTO accounting_status VALUES("3","monthly","","");
INSERT INTO accounting_status VALUES("4","yearly","","");
INSERT INTO accounting_status VALUES("5","Every 2 years","","");
INSERT INTO accounting_status VALUES("6","every 5 years","","");
INSERT INTO accounting_status VALUES("7","every 10 years","","");



DROP TABLE announcements;

CREATE TABLE `announcements` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `isRead` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `announcements_role_id_foreign` (`role_id`),
  CONSTRAINT `announcements_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO announcements VALUES("22","2","1","Deneme Duyuru","Bu bir deneme duyurusudur.","1","","2022-11-04 19:40:51","2022-11-04 19:40:58");
INSERT INTO announcements VALUES("23","11","11","Deneme Duyuru","Bu bir deneme duyurusudur.","1","","2022-11-04 19:40:51","2022-11-04 19:41:06");
INSERT INTO announcements VALUES("24","11","13","Deneme Duyuru","Bu bir deneme duyurusudur.","1","","2022-11-04 19:40:51","2022-11-04 19:41:09");
INSERT INTO announcements VALUES("25","2","14","Deneme Duyuru","Bu bir deneme duyurusudur.","1","","2022-11-04 19:40:51","2022-11-04 19:41:11");
INSERT INTO announcements VALUES("26","12","15","Deneme Duyuru","Bu bir deneme duyurusudur.","1","","2022-11-04 19:40:51","2022-11-04 19:41:14");
INSERT INTO announcements VALUES("27","12","16","Deneme Duyuru","Bu bir deneme duyurusudur.","1","","2022-11-04 19:40:51","2022-11-04 19:41:16");



DROP TABLE bank;

CREATE TABLE `bank` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_account_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_iban_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO bank VALUES("3","Garanti Bankası","Denizli Bayramyeri Şubesi","11111111111111","2222222222222","2022-09-14 21:21:39","2022-09-14 21:21:39");



DROP TABLE blog;

CREATE TABLE `blog` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `blog_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog_status` tinyint(1) NOT NULL,
  `blog_seq` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO blog VALUES("13","Kitchen Stone","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sem ante, eleifend vel porttitor sed, sollicitudin id dui. Pellentesque nulla urna, porta vitae risus et, dictum finibus dolor. Mauris condimentum neque ut ipsum pharetra aliquet. Ut cursus, libero vel commodo ultrices, risus turpis blandit lorem, ac viverra metus massa vel lectus. Sed et hendrerit arcu. Quisque blandit tortor consectetur volutpat tristique. Curabitur dolor odio, feugiat vitae leo vel, eleifend iaculis lectus. Vestibulum lorem justo, luctus sit amet turpis a, malesuada tristique ante. Pellentesque sagittis ante neque, a hendrerit quam rutrum eleifend. Pellentesque nec scelerisque elit, eget porta magna.Cras tincidunt ante vel convallis suscipit. Aliquam lorem nulla, mattis in lorem non, accumsan ornare ex. Pellentesque ut velit lobortis, malesuada sapien id, gravida dolor. Curabitur hendrerit diam nec augue fermentum rutrum. Quisque molestie augue nec justo luctus, eget ultricies est condimentum. Aliquam convallis turpis lectus. Maecenas tempus venenatis mollis. Donec at lacus non est posuere malesuada.","backend/uploads/blog/1667733693.jpeg","1","1","2022-11-06 10:45:31","2022-11-06 11:21:33");
INSERT INTO blog VALUES("14","Bathroom Stone","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sem ante, eleifend vel porttitor sed, sollicitudin id dui. Pellentesque nulla urna, porta vitae risus et, dictum finibus dolor. Mauris condimentum neque ut ipsum pharetra aliquet. Ut cursus, libero vel commodo ultrices, risus turpis blandit lorem, ac viverra metus massa vel lectus. Sed et hendrerit arcu. Quisque blandit tortor consectetur volutpat tristique. Curabitur dolor odio, feugiat vitae leo vel, eleifend iaculis lectus. Vestibulum lorem justo, luctus sit amet turpis a, malesuada tristique ante. Pellentesque sagittis ante neque, a hendrerit quam rutrum eleifend. Pellentesque nec scelerisque elit, eget porta magna.Cras tincidunt ante vel convallis suscipit. Aliquam lorem nulla, mattis in lorem non, accumsan ornare ex. Pellentesque ut velit lobortis, malesuada sapien id, gravida dolor. Curabitur hendrerit diam nec augue fermentum rutrum. Quisque molestie augue nec justo luctus, eget ultricies est condimentum. Aliquam convallis turpis lectus. Maecenas tempus venenatis mollis. Donec at lacus non est posuere malesuada.","backend/uploads/blog/1667733705.jpeg","1","2","2022-11-06 10:45:48","2022-11-06 11:21:45");
INSERT INTO blog VALUES("15","Living Room Stone","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sem ante, eleifend vel porttitor sed, sollicitudin id dui. Pellentesque nulla urna, porta vitae risus et, dictum finibus dolor. Mauris condimentum neque ut ipsum pharetra aliquet. Ut cursus, libero vel commodo ultrices, risus turpis blandit lorem, ac viverra metus massa vel lectus. Sed et hendrerit arcu. Quisque blandit tortor consectetur volutpat tristique. Curabitur dolor odio, feugiat vitae leo vel, eleifend iaculis lectus. Vestibulum lorem justo, luctus sit amet turpis a, malesuada tristique ante. Pellentesque sagittis ante neque, a hendrerit quam rutrum eleifend. Pellentesque nec scelerisque elit, eget porta magna.Cras tincidunt ante vel convallis suscipit. Aliquam lorem nulla, mattis in lorem non, accumsan ornare ex. Pellentesque ut velit lobortis, malesuada sapien id, gravida dolor. Curabitur hendrerit diam nec augue fermentum rutrum. Quisque molestie augue nec justo luctus, eget ultricies est condimentum. Aliquam convallis turpis lectus. Maecenas tempus venenatis mollis. Donec at lacus non est posuere malesuada.","backend/uploads/blog/1667733716.jpeg","1","3","2022-11-06 10:46:25","2022-11-06 11:21:56");
INSERT INTO blog VALUES("16","Ofice Stone","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sem ante, eleifend vel porttitor sed, sollicitudin id dui. Pellentesque nulla urna, porta vitae risus et, dictum finibus dolor. Mauris condimentum neque ut ipsum pharetra aliquet. Ut cursus, libero vel commodo ultrices, risus turpis blandit lorem, ac viverra metus massa vel lectus. Sed et hendrerit arcu. Quisque blandit tortor consectetur volutpat tristique. Curabitur dolor odio, feugiat vitae leo vel, eleifend iaculis lectus. Vestibulum lorem justo, luctus sit amet turpis a, malesuada tristique ante. Pellentesque sagittis ante neque, a hendrerit quam rutrum eleifend. Pellentesque nec scelerisque elit, eget porta magna.Cras tincidunt ante vel convallis suscipit. Aliquam lorem nulla, mattis in lorem non, accumsan ornare ex. Pellentesque ut velit lobortis, malesuada sapien id, gravida dolor. Curabitur hendrerit diam nec augue fermentum rutrum. Quisque molestie augue nec justo luctus, eget ultricies est condimentum. Aliquam convallis turpis lectus. Maecenas tempus venenatis mollis. Donec at lacus non est posuere malesuada.","backend/uploads/blog/1667733724.jpeg","1","4","2022-11-06 10:46:41","2022-11-06 11:22:04");
INSERT INTO blog VALUES("17","mail blog","asdsadasd","backend/uploads/blog/1668019474.jpeg","1","213","2022-11-09 18:44:34","2022-11-09 18:44:34");
INSERT INTO blog VALUES("18","asdasd","asdasd","backend/uploads/blog/1668019513.jpeg","1","123213","2022-11-09 18:45:13","2022-11-09 18:45:13");
INSERT INTO blog VALUES("19","deneme blog","asdasdasd","backend/uploads/blog/1668020113.jpeg","1","32323","2022-11-09 18:55:13","2022-11-09 18:55:13");
INSERT INTO blog VALUES("20","deneme blog","asdasdasd","backend/uploads/blog/1668020139.jpeg","1","32323","2022-11-09 18:55:39","2022-11-09 18:55:39");
INSERT INTO blog VALUES("21","deneme777213421","asdasd","backend/uploads/blog/1668020235.jpeg","1","213213213","2022-11-09 18:57:15","2022-11-09 18:57:15");
INSERT INTO blog VALUES("22","deneme44","asdsadas","backend/uploads/blog/1668021247.jpeg","1","44","2022-11-09 19:14:07","2022-11-09 19:14:07");
INSERT INTO blog VALUES("27","deneme777123","asdsadas","backend/uploads/blog/1668021953.jpeg","1","21321321","2022-11-09 19:25:53","2022-11-09 19:25:53");
INSERT INTO blog VALUES("34","asdsad12313","asdsad","backend/uploads/blog/1668022563.jpeg","1","321313","2022-11-09 19:36:03","2022-11-09 19:36:03");



DROP TABLE calendar;

CREATE TABLE `calendar` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `calendar_user_id_foreign` (`user_id`),
  CONSTRAINT `calendar_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO calendar VALUES("1","1","deneme","bg-success","2022-10-02 18:48:00","2022-10-02 18:52:00","1","2022-10-02 15:48:39","2022-10-02 15:48:39");
INSERT INTO calendar VALUES("2","1","asdsad","bg-danger","2022-10-02 18:49:00","2022-10-02 19:49:00","1","2022-10-02 15:49:07","2022-10-02 15:49:07");
INSERT INTO calendar VALUES("3","1","asdasd","bg-danger","2022-10-02 19:49:00","2022-10-02 19:49:00","1","2022-10-02 16:49:42","2022-10-02 16:49:42");
INSERT INTO calendar VALUES("4","1","asd123","bg-primary","2022-10-02 19:51:00","2022-10-02 19:51:00","1","2022-10-02 16:51:31","2022-10-02 16:51:31");
INSERT INTO calendar VALUES("5","1","deneme66","bg-danger","2022-10-02 20:11:00","2022-10-02 20:16:00","1","2022-10-02 17:11:05","2022-10-02 17:11:05");
INSERT INTO calendar VALUES("6","1","asdasd","bg-success","2022-10-02 20:15:00","2022-10-02 20:20:00","1","2022-10-02 17:15:07","2022-10-02 17:15:07");
INSERT INTO calendar VALUES("7","1","asdasd","bg-info","2022-10-02 20:45:00","2022-10-02 20:45:00","1","2022-10-02 17:45:26","2022-10-02 17:45:26");
INSERT INTO calendar VALUES("8","1","edede","bg-primary","2022-10-02 20:47:00","2022-10-02 20:47:00","1","2022-10-02 17:48:02","2022-10-02 17:48:02");
INSERT INTO calendar VALUES("9","1","q312","bg-info","2022-10-02 20:48:00","2022-10-02 20:53:00","1","2022-10-02 17:48:17","2022-10-02 17:48:17");
INSERT INTO calendar VALUES("10","1","qweqwe123","bg-danger","2022-10-06 21:56:00","2022-10-06 21:56:00","1","2022-10-06 18:56:51","2022-10-06 18:56:51");
INSERT INTO calendar VALUES("11","1","qwe1321","bg-primary","2022-10-06 21:57:00","2022-10-06 21:57:00","1","2022-10-06 18:57:24","2022-10-06 18:57:24");



DROP TABLE campaign;

CREATE TABLE `campaign` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `campaign_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `campaign_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `campaign_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `campaign_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `campaign_seq` int(11) NOT NULL,
  `campaign_status` tinyint(1) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO campaign VALUES("2","deneme002","deneme","deneme","www.google.com","backend/uploads/campaign/1656361621.jpeg","1","1","0","2022-06-27 20:27:01","2022-06-27 20:27:01");



DROP TABLE categories;

CREATE TABLE `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=70 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO categories VALUES("69","Ofice","2022-11-06 10:48:27","2022-11-06 10:48:27");
INSERT INTO categories VALUES("68","Living Room","2022-11-06 10:48:22","2022-11-06 10:48:22");
INSERT INTO categories VALUES("67","Bathroom","2022-11-06 10:48:17","2022-11-06 10:48:17");
INSERT INTO categories VALUES("66","Kitchen","2022-11-06 10:48:12","2022-11-06 10:48:12");



DROP TABLE chat_messages;

CREATE TABLE `chat_messages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `chat_room_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO chat_messages VALUES("1","1","1","sa","2022-10-22 21:25:39","2022-10-22 21:25:39");
INSERT INTO chat_messages VALUES("2","1","1","sa","2022-10-22 21:28:45","2022-10-22 21:28:45");
INSERT INTO chat_messages VALUES("3","1","1","sa","2022-10-22 21:29:53","2022-10-22 21:29:53");
INSERT INTO chat_messages VALUES("4","1","1","as","2022-10-22 21:57:26","2022-10-22 21:57:26");
INSERT INTO chat_messages VALUES("5","1","1","as","2022-10-22 21:57:27","2022-10-22 21:57:27");
INSERT INTO chat_messages VALUES("6","1","1","as","2022-10-22 21:57:28","2022-10-22 21:57:28");
INSERT INTO chat_messages VALUES("7","1","1","selam","2022-10-22 21:57:38","2022-10-22 21:57:38");
INSERT INTO chat_messages VALUES("8","1","1","as","2022-10-22 21:58:30","2022-10-22 21:58:30");
INSERT INTO chat_messages VALUES("9","1","1","as1","2022-10-22 21:58:32","2022-10-22 21:58:32");
INSERT INTO chat_messages VALUES("10","1","1","as12","2022-10-22 21:58:33","2022-10-22 21:58:33");
INSERT INTO chat_messages VALUES("11","1","1","as123","2022-10-22 21:58:33","2022-10-22 21:58:33");
INSERT INTO chat_messages VALUES("12","1","1","as1234","2022-10-22 21:58:34","2022-10-22 21:58:34");
INSERT INTO chat_messages VALUES("13","1","1","as12345","2022-10-22 21:58:35","2022-10-22 21:58:35");
INSERT INTO chat_messages VALUES("14","1","1","as123456","2022-10-22 21:58:36","2022-10-22 21:58:36");
INSERT INTO chat_messages VALUES("15","1","1","as1234567","2022-10-22 21:58:37","2022-10-22 21:58:37");
INSERT INTO chat_messages VALUES("16","1","1","1","2022-10-22 21:58:53","2022-10-22 21:58:53");
INSERT INTO chat_messages VALUES("17","1","1","11","2022-10-22 21:58:53","2022-10-22 21:58:53");
INSERT INTO chat_messages VALUES("18","1","1","111","2022-10-22 21:58:54","2022-10-22 21:58:54");
INSERT INTO chat_messages VALUES("19","1","1","1111","2022-10-22 21:58:54","2022-10-22 21:58:54");
INSERT INTO chat_messages VALUES("20","1","1","11111","2022-10-22 21:58:55","2022-10-22 21:58:55");
INSERT INTO chat_messages VALUES("21","1","1","111111","2022-10-22 21:58:55","2022-10-22 21:58:55");
INSERT INTO chat_messages VALUES("22","1","1","1111111","2022-10-22 21:58:56","2022-10-22 21:58:56");
INSERT INTO chat_messages VALUES("23","1","1","1111111","2022-10-22 21:58:56","2022-10-22 21:58:56");
INSERT INTO chat_messages VALUES("24","1","1","11111111","2022-10-22 21:58:56","2022-10-22 21:58:56");
INSERT INTO chat_messages VALUES("25","1","1","111111111","2022-10-22 21:58:57","2022-10-22 21:58:57");
INSERT INTO chat_messages VALUES("26","1","1","1111111111","2022-10-22 21:58:57","2022-10-22 21:58:57");
INSERT INTO chat_messages VALUES("27","1","1","1111111111","2022-10-22 21:58:57","2022-10-22 21:58:57");
INSERT INTO chat_messages VALUES("28","1","1","1111111111","2022-10-22 21:58:58","2022-10-22 21:58:58");
INSERT INTO chat_messages VALUES("29","1","1","111111111123","2022-10-22 21:58:58","2022-10-22 21:58:58");
INSERT INTO chat_messages VALUES("30","1","1","1111111111232","2022-10-22 21:58:59","2022-10-22 21:58:59");
INSERT INTO chat_messages VALUES("31","1","1","1111111111232","2022-10-22 21:58:59","2022-10-22 21:58:59");
INSERT INTO chat_messages VALUES("32","1","1","1111111111232","2022-10-22 21:58:59","2022-10-22 21:58:59");
INSERT INTO chat_messages VALUES("33","1","1","1111111111232","2022-10-22 21:59:00","2022-10-22 21:59:00");
INSERT INTO chat_messages VALUES("34","1","1","deneme","2022-10-22 22:28:57","2022-10-22 22:28:57");
INSERT INTO chat_messages VALUES("35","1","1","deneme1","2022-10-22 22:30:15","2022-10-22 22:30:15");
INSERT INTO chat_messages VALUES("36","1","1","deneme2","2022-10-22 22:36:01","2022-10-22 22:36:01");
INSERT INTO chat_messages VALUES("37","1","1","deneme3","2022-10-22 22:36:20","2022-10-22 22:36:20");
INSERT INTO chat_messages VALUES("38","1","1","deneme3","2022-10-22 22:36:24","2022-10-22 22:36:24");
INSERT INTO chat_messages VALUES("39","1","1","deneme3","2022-10-22 22:36:24","2022-10-22 22:36:24");
INSERT INTO chat_messages VALUES("40","1","1","deneme3","2022-10-22 22:36:24","2022-10-22 22:36:24");
INSERT INTO chat_messages VALUES("41","1","1","test1","2022-10-22 22:37:13","2022-10-22 22:37:13");
INSERT INTO chat_messages VALUES("42","1","1","test2","2022-10-22 22:38:02","2022-10-22 22:38:02");
INSERT INTO chat_messages VALUES("43","1","1","test3","2022-10-22 22:40:25","2022-10-22 22:40:25");
INSERT INTO chat_messages VALUES("44","1","1","test4","2022-10-22 22:41:04","2022-10-22 22:41:04");
INSERT INTO chat_messages VALUES("45","1","1","test5","2022-10-22 22:43:17","2022-10-22 22:43:17");
INSERT INTO chat_messages VALUES("46","1","1","test6","2022-10-22 22:43:56","2022-10-22 22:43:56");
INSERT INTO chat_messages VALUES("47","1","1","test7","2022-10-22 22:45:24","2022-10-22 22:45:24");
INSERT INTO chat_messages VALUES("48","1","1","test8","2022-10-22 22:45:32","2022-10-22 22:45:32");
INSERT INTO chat_messages VALUES("49","1","1","test9","2022-10-22 22:45:56","2022-10-22 22:45:56");
INSERT INTO chat_messages VALUES("50","1","1","test10","2022-10-22 22:46:37","2022-10-22 22:46:37");
INSERT INTO chat_messages VALUES("51","1","1","test11","2022-10-22 22:46:52","2022-10-22 22:46:52");
INSERT INTO chat_messages VALUES("52","1","1","test12","2022-10-22 23:00:13","2022-10-22 23:00:13");



DROP TABLE chat_rooms;

CREATE TABLE `chat_rooms` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO chat_rooms VALUES("1","General","","");
INSERT INTO chat_rooms VALUES("2","Tech Talk","","");
INSERT INTO chat_rooms VALUES("3","Private","","");



DROP TABLE chat_user;

CREATE TABLE `chat_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `crm_user_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `chatroom_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO chat_user VALUES("1","15","1","Ahmet Sohbet Odası","1","","2022-10-23 09:33:59");
INSERT INTO chat_user VALUES("2","16","","Fatma Sohbet Odası","0","","");



DROP TABLE chat_user_message;

CREATE TABLE `chat_user_message` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `chat_user_id` int(11) NOT NULL,
  `message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`message`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




